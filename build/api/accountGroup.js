"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const typedi_1 = require("typedi");
const express_rate_limit_1 = __importDefault(require("express-rate-limit"));
const celebrate_1 = require("celebrate");
const accountGroup_1 = __importDefault(require("../services/accountGroup"));
const biliAccountBindGroupRecord_1 = __importDefault(require("../services/biliAccountBindGroupRecord"));
const route = (0, express_1.Router)();
exports.default = (app) => {
    app.use('/accountGroup', route);
    route.get('/', async (req, res, next) => {
        const logger = typedi_1.Container.get('logger');
        try {
            const service = typedi_1.Container.get(accountGroup_1.default);
            const data = await service.list();
            return res.send({ code: 200, data });
        }
        catch (e) {
            logger.error('🔥 error: %o', e);
            return next(e);
        }
    });
    route.get('/list', (0, express_rate_limit_1.default)({
        windowMs: 15 * 60 * 1000,
        max: 200,
    }), (0, celebrate_1.celebrate)({
        query: celebrate_1.Joi.object({
            t: celebrate_1.Joi.any(),
            page_index: celebrate_1.Joi.number().required().min(1).max(99999),
            page_size: celebrate_1.Joi.number().required().min(10).max(100),
            search_text: celebrate_1.Joi.any(),
        }),
    }), async (req, res, next) => {
        try {
            const service = typedi_1.Container.get(accountGroup_1.default);
            const data = await service.getList({
                page_index: Number(req.query.page_index),
                page_size: Number(req.query.page_size),
                search_text: String(req.query.search_text),
            });
            return res.send(data);
        }
        catch (e) {
            return next(e);
        }
    });
    route.get('/accountList', (0, express_rate_limit_1.default)({
        windowMs: 15 * 60 * 1000,
        max: 200,
    }), (0, celebrate_1.celebrate)({
        query: celebrate_1.Joi.object({
            t: celebrate_1.Joi.any(),
            page_index: celebrate_1.Joi.number().required().min(1).max(99999),
            page_size: celebrate_1.Joi.number().required().min(10).max(100),
            template_code: celebrate_1.Joi.any(),
        }),
    }), async (req, res, next) => {
        try {
            const service = typedi_1.Container.get(accountGroup_1.default);
            const data = await service.getAccountList({
                page_index: Number(req.query.page_index),
                page_size: Number(req.query.page_size),
                template_code: String(req.query.template_code),
            });
            return res.send(data);
        }
        catch (e) {
            return next(e);
        }
    });
    route.delete('/boundAccounts', (0, celebrate_1.celebrate)({
        body: celebrate_1.Joi.array().items(celebrate_1.Joi.string().required()),
    }), async (req, res, next) => {
        try {
            const bindService = typedi_1.Container.get(biliAccountBindGroupRecord_1.default);
            const data = await bindService.delete(req.body, req.locals.user.id);
            return res.send(data);
        }
        catch (e) {
            return next(e);
        }
    });
    route.get('/search', (0, express_rate_limit_1.default)({
        windowMs: 15 * 60 * 1000,
        max: 200,
    }), (0, celebrate_1.celebrate)({
        query: celebrate_1.Joi.object({
            t: celebrate_1.Joi.any(),
            search_text: celebrate_1.Joi.string(),
        }),
    }), async (req, res, next) => {
        const logger = typedi_1.Container.get('logger');
        try {
            const service = typedi_1.Container.get(accountGroup_1.default);
            const data = await service.search({
                search_text: req.query.search_text,
            });
            return res.send(data);
        }
        catch (e) {
            return next(e);
        }
    });
    route.post('/modify', (0, express_rate_limit_1.default)({
        windowMs: 15 * 60 * 1000,
        max: 200,
    }), (0, celebrate_1.celebrate)({
        body: celebrate_1.Joi.object({
            template_code: celebrate_1.Joi.string().required(),
            template_name: celebrate_1.Joi.string().required(),
            distribute_type: celebrate_1.Joi.string().required(),
            cities: celebrate_1.Joi.array().required(),
            publish_remark: celebrate_1.Joi.string().optional().allow('').allow(null),
        }),
    }), async (req, res, next) => {
        try {
            const service = typedi_1.Container.get(accountGroup_1.default);
            const data = await service.modify(Object.assign({}, req.body), req.locals.user.id);
            return res.send(data);
        }
        catch (e) {
            return next(e);
        }
    });
    route.post('/delete', (0, express_rate_limit_1.default)({
        windowMs: 15 * 60 * 1000,
        max: 200,
    }), (0, celebrate_1.celebrate)({
        body: celebrate_1.Joi.object({
            template_code: celebrate_1.Joi.string().required(),
        }),
    }), async (req, res, next) => {
        const logger = typedi_1.Container.get('logger');
        try {
            const service = typedi_1.Container.get(accountGroup_1.default);
            const data = await service.delete(Object.assign({}, req.body), req.locals.user.id);
            return res.send(data);
        }
        catch (e) {
            logger.error('🔥 error: %o', e);
            return next(e);
        }
    });
    route.post('/changeEnable', (0, express_rate_limit_1.default)({
        windowMs: 15 * 60 * 1000,
        max: 200,
    }), (0, celebrate_1.celebrate)({
        body: celebrate_1.Joi.object({
            template_code: celebrate_1.Joi.string().required(),
            enable_flag: celebrate_1.Joi.boolean().required(),
        }),
    }), async (req, res, next) => {
        try {
            const service = typedi_1.Container.get(accountGroup_1.default);
            const data = await service.changeEnable(Object.assign({}, req.body), req.locals.user.id);
            return res.send(data);
        }
        catch (e) {
            return next(e);
        }
    });
    route.post('/publish', (0, express_rate_limit_1.default)({
        windowMs: 15 * 60 * 1000,
        max: 200,
    }), (0, celebrate_1.celebrate)({
        body: celebrate_1.Joi.object({
            template_code: celebrate_1.Joi.string().required(),
        }),
    }), async (req, res, next) => {
        try {
            const service = typedi_1.Container.get(accountGroup_1.default);
            const data = await service.publish(Object.assign({}, req.body), req.locals.user.id);
            return res.send(data);
        }
        catch (e) {
            return next(e);
        }
    });
    route.get('/cities', (0, express_rate_limit_1.default)({
        windowMs: 15 * 60 * 1000,
        max: 200,
    }), (0, celebrate_1.celebrate)({
        query: celebrate_1.Joi.object({
            t: celebrate_1.Joi.any(),
        }),
    }), async (req, res, next) => {
        try {
            const service = typedi_1.Container.get(accountGroup_1.default);
            const data = await service.getCities();
            return res.send(data);
        }
        catch (e) {
            return next(e);
        }
    });
};
//# sourceMappingURL=accountGroup.js.map