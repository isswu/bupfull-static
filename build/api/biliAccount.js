"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const typedi_1 = require("typedi");
const biliAccount_1 = __importDefault(require("../services/biliAccount"));
const biliAccountTag_1 = __importDefault(require("../services/biliAccountTag"));
const biliAccountBindProxyIpRecord_1 = __importDefault(require("../services/biliAccountBindProxyIpRecord"));
const biliAccountBindGroupRecord_1 = __importDefault(require("../services/biliAccountBindGroupRecord"));
const celebrate_1 = require("celebrate");
const multer_1 = __importDefault(require("multer"));
const config_1 = __importDefault(require("../config"));
const express_rate_limit_1 = __importDefault(require("express-rate-limit"));
const route = (0, express_1.Router)();
const storage = multer_1.default.diskStorage({
    destination: function (req, file, cb) {
        cb(null, config_1.default.scriptPath);
    },
    filename: function (req, file, cb) {
        cb(null, file.originalname);
    },
});
const upload = (0, multer_1.default)({ storage: storage });
exports.default = (app) => {
    app.use('/biliAccounts', route);
    route.get('/', (0, express_rate_limit_1.default)({
        windowMs: 20 * 60 * 1000,
        max: 200,
    }), (0, celebrate_1.celebrate)({
        query: celebrate_1.Joi.object({
            t: celebrate_1.Joi.any(),
            searchValue: celebrate_1.Joi.string().optional().allow('').allow(null),
            page_index: celebrate_1.Joi.number().min(1).max(99999).required(),
            page_size: celebrate_1.Joi.number().min(10).max(100).required(),
            sorter: celebrate_1.Joi.string().optional().allow('').allow(null),
        }),
    }), async (req, res, next) => {
        const logger = typedi_1.Container.get('logger');
        try {
            const biliAccountService = typedi_1.Container.get(biliAccount_1.default);
            const data = await biliAccountService.biliAccounts(req.query, req.locals.user.isAdmin, req.locals.dataIn);
            return res.send(data);
        }
        catch (e) {
            logger.error('🔥 error: %o', e);
            return next(e);
        }
    });
    route.get('/training', (0, express_rate_limit_1.default)({
        windowMs: 20 * 60 * 1000,
        max: 200,
    }), async (req, res, next) => {
        const logger = typedi_1.Container.get('logger');
        try {
            const biliAccountService = typedi_1.Container.get(biliAccount_1.default);
            const data = await biliAccountService.getTrainingList(req.query.searchValue);
            return res.send(data);
        }
        catch (e) {
            return next(e);
        }
    });
    route.post('/', (0, celebrate_1.celebrate)({
        body: celebrate_1.Joi.array().items(celebrate_1.Joi.object({
            proxy_ip: celebrate_1.Joi.string().required(),
            user_agent: celebrate_1.Joi.string().required(),
            remarks: celebrate_1.Joi.string().optional().allow(''),
        })),
    }), async (req, res, next) => {
        const logger = typedi_1.Container.get('logger');
        try {
            const biliAccountService = typedi_1.Container.get(biliAccount_1.default);
            const data = await biliAccountService.create(req.body);
            return res.send({ code: 200, data });
        }
        catch (e) {
            return next(e);
        }
    });
    route.put('/', (0, celebrate_1.celebrate)({
        body: celebrate_1.Joi.object({
            proxy_ip: celebrate_1.Joi.string().required(),
            user_agent: celebrate_1.Joi.string().required(),
            tags: celebrate_1.Joi.array().optional().allow(null),
            remarks: celebrate_1.Joi.string().optional().allow('').allow(null),
            mid: celebrate_1.Joi.string().required(),
            accountGroups: celebrate_1.Joi.array().optional().allow(null),
        }),
    }), async (req, res, next) => {
        const logger = typedi_1.Container.get('logger');
        try {
            const biliAccountService = typedi_1.Container.get(biliAccount_1.default);
            const biliAccountTagService = typedi_1.Container.get(biliAccountTag_1.default);
            const biliAccountBindProxyIpRecordService = typedi_1.Container.get(biliAccountBindProxyIpRecord_1.default);
            const biliAccountBindGroupRecordService = typedi_1.Container.get(biliAccountBindGroupRecord_1.default);
            await biliAccountTagService.createBiliAccountTagIfNotExist(req.body.tags, req.body.mid);
            await biliAccountBindProxyIpRecordService.createBindProxyIpRecordIfNotExist(req.body.mid, req.body.proxy_ip);
            for (const item of req.body.accountGroups) {
                await biliAccountBindGroupRecordService.createBindAccountGroupRecordIfNotExist(req.body.mid, item.template_code, req.locals.user.id);
            }
            const data = await biliAccountService.update(req.body);
            return res.send({ code: 200, data });
        }
        catch (e) {
            return next(e);
        }
    });
    route.put('/batch', (0, celebrate_1.celebrate)({
        body: celebrate_1.Joi.object({
            proxy_ip: celebrate_1.Joi.string().required(),
            user_agent: celebrate_1.Joi.string().required(),
            tags: celebrate_1.Joi.array().optional().allow(null),
            remarks: celebrate_1.Joi.string().optional().allow('').allow(null),
            mids: celebrate_1.Joi.array().required(),
            accountGroups: celebrate_1.Joi.array().optional().allow(null),
        }),
    }), async (req, res, next) => {
        const logger = typedi_1.Container.get('logger');
        try {
            const biliAccountService = typedi_1.Container.get(biliAccount_1.default);
            const biliAccountTagService = typedi_1.Container.get(biliAccountTag_1.default);
            const biliAccountBindProxyIpRecordService = typedi_1.Container.get(biliAccountBindProxyIpRecord_1.default);
            const biliAccountBindGroupRecordService = typedi_1.Container.get(biliAccountBindGroupRecord_1.default);
            for (const mid of req.body.mids) {
                await biliAccountTagService.createBiliAccountTagIfNotExist(req.body.tags, mid);
                await biliAccountBindProxyIpRecordService.createBindProxyIpRecordIfNotExist(mid, req.body.proxy_ip);
                for (const item of req.body.accountGroups) {
                    await biliAccountBindGroupRecordService.createBindAccountGroupRecordIfNotExist(mid, item.template_code, req.locals.user.id);
                }
            }
            const data = await biliAccountService.batchUpdate(Object.assign({}, req.body), req.locals.user.id);
            return res.send(data);
        }
        catch (e) {
            return next(e);
        }
    });
    route.delete('/', (0, celebrate_1.celebrate)({
        body: celebrate_1.Joi.array().items(celebrate_1.Joi.string().required()),
    }), async (req, res, next) => {
        const logger = typedi_1.Container.get('logger');
        try {
            const biliAccountService = typedi_1.Container.get(biliAccount_1.default);
            const data = await biliAccountService.remove(req.body, req.locals.user.id);
            return res.send({ code: 200, data });
        }
        catch (e) {
            return next(e);
        }
    });
    route.put('/disable', (0, celebrate_1.celebrate)({
        body: celebrate_1.Joi.array().items(celebrate_1.Joi.string().required()),
    }), async (req, res, next) => {
        const logger = typedi_1.Container.get('logger');
        try {
            const biliAccountService = typedi_1.Container.get(biliAccount_1.default);
            const data = await biliAccountService.disabled(req.body);
            return res.send({ code: 200, data });
        }
        catch (e) {
            return next(e);
        }
    });
    route.put('/enable', (0, celebrate_1.celebrate)({
        body: celebrate_1.Joi.array().items(celebrate_1.Joi.string().required()),
    }), async (req, res, next) => {
        const logger = typedi_1.Container.get('logger');
        try {
            const biliAccountService = typedi_1.Container.get(biliAccount_1.default);
            const data = await biliAccountService.enabled(req.body);
            return res.send({ code: 200, data });
        }
        catch (e) {
            return next(e);
        }
    });
};
//# sourceMappingURL=biliAccount.js.map