"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const typedi_1 = require("typedi");
const express_rate_limit_1 = __importDefault(require("express-rate-limit"));
const biliAggregateTask_1 = __importDefault(require("../services/biliAggregateTask"));
const route = (0, express_1.Router)();
exports.default = (app) => {
    app.use('/biliAggregateTask', route);
    route.get('/', (0, express_rate_limit_1.default)({
        windowMs: 20 * 60 * 1000,
        max: 200,
    }), async (req, res, next) => {
        try {
            const taskService = typedi_1.Container.get(biliAggregateTask_1.default);
            const data = await taskService.getAggregatedTaskList();
            return res.send(data);
        }
        catch (e) {
            return next(e);
        }
    });
};
//# sourceMappingURL=biliAggregateTask.js.map