"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const typedi_1 = require("typedi");
const express_rate_limit_1 = __importDefault(require("express-rate-limit"));
const celebrate_1 = require("celebrate");
const util_1 = require("../config/util");
const biliDanmakuTask_1 = __importDefault(require("../services/biliDanmakuTask"));
const route = (0, express_1.Router)();
exports.default = (app) => {
    app.use('/biliDanmakuTask', route);
    route.get('/danmaku/list', (0, express_rate_limit_1.default)({
        windowMs: 20 * 60 * 1000,
        max: 200,
    }), (0, celebrate_1.celebrate)({
        query: celebrate_1.Joi.object({
            t: celebrate_1.Joi.any(),
            uid: celebrate_1.Joi.string().optional().allow('').allow(null),
            taskType: celebrate_1.Joi.string().optional().allow('').allow(null),
            searchValue: celebrate_1.Joi.string().optional().allow('').allow(null),
            page_index: celebrate_1.Joi.number().min(1).max(99999).required(),
            page_size: celebrate_1.Joi.number().min(10).max(100).required(),
            sorter: celebrate_1.Joi.string().optional().allow('').allow(null),
        }),
    }), async (req, res, next) => {
        const logger = typedi_1.Container.get('logger');
        try {
            const taskService = typedi_1.Container.get(biliDanmakuTask_1.default);
            const data = await taskService.getDanmakuList(req.query, req.locals.user.isAdmin, req.locals.dataIn);
            return res.send(data);
        }
        catch (e) {
            return next(e);
        }
    });
    route.post('/danmaku/modify', (0, express_rate_limit_1.default)({
        windowMs: 20 * 60 * 1000,
        max: 200,
    }), (0, celebrate_1.celebrate)({
        body: celebrate_1.Joi.object({
            id: celebrate_1.Joi.any(),
            task_type: celebrate_1.Joi.number().required(),
            uid: celebrate_1.Joi.string().required(),
            bvid: celebrate_1.Joi.string().required(),
            danmaku_tag_id: celebrate_1.Joi.string().optional().allow('').allow(null),
            template_code: celebrate_1.Joi.string().optional().allow('').allow(null),
            control_time: celebrate_1.Joi.date().optional().allow(null),
            mid_white_list: celebrate_1.Joi.any().optional().allow('').allow(null),
            publish_remark: celebrate_1.Joi.string().optional().allow('').allow(null),
        }),
    }), async (req, res, next) => {
        const logger = typedi_1.Container.get('logger');
        try {
            const taskService = typedi_1.Container.get(biliDanmakuTask_1.default);
            const data = await taskService.createOrUpdateDanmakuTask(Object.assign({}, req.body), req.locals.user.id);
            return res.send(data);
        }
        catch (e) {
            return next(e);
        }
    });
    route.post('/danmaku/publish', (0, express_rate_limit_1.default)({
        windowMs: 20 * 60 * 1000,
        max: 200,
    }), (0, celebrate_1.celebrate)({
        body: celebrate_1.Joi.object({
            id: celebrate_1.Joi.string().required(),
        }),
    }), async (req, res, next) => {
        const logger = typedi_1.Container.get('logger');
        try {
            const taskService = typedi_1.Container.get(biliDanmakuTask_1.default);
            const data = await taskService.publishDanmaku(Object.assign({}, req.body), req.locals.user.id);
            return res.send(data);
        }
        catch (e) {
            return next(e);
        }
    });
    route.post('/danmaku/changeEnable', (0, express_rate_limit_1.default)({
        windowMs: 20 * 60 * 1000,
        max: 200,
    }), (0, celebrate_1.celebrate)({
        body: celebrate_1.Joi.object({
            id: celebrate_1.Joi.string().required(),
            enable_flag: celebrate_1.Joi.boolean().required(),
        }),
    }), async (req, res, next) => {
        const logger = typedi_1.Container.get('logger');
        try {
            const taskService = typedi_1.Container.get(biliDanmakuTask_1.default);
            const data = await taskService.changeDanmakuEnable(Object.assign({}, req.body), req.locals.user.id);
            return res.send(data);
        }
        catch (e) {
            return next(e);
        }
    });
    route.post('/danmaku/changeControl', (0, express_rate_limit_1.default)({
        windowMs: 20 * 60 * 1000,
        max: 200,
    }), (0, celebrate_1.celebrate)({
        body: celebrate_1.Joi.object({
            id: celebrate_1.Joi.string().required(),
            control_flag: celebrate_1.Joi.boolean().required(),
        }),
    }), async (req, res, next) => {
        const logger = typedi_1.Container.get('logger');
        try {
            const taskService = typedi_1.Container.get(biliDanmakuTask_1.default);
            const data = await taskService.changeDanmakuControl(Object.assign({}, req.body), req.locals.user.id);
            return res.send(data);
        }
        catch (e) {
            return next(e);
        }
    });
    route.post('/danmaku/del', (0, express_rate_limit_1.default)({
        windowMs: 20 * 60 * 1000,
        max: 200,
    }), (0, celebrate_1.celebrate)({
        body: celebrate_1.Joi.object({
            id: celebrate_1.Joi.string().required(),
        }),
    }), async (req, res, next) => {
        const logger = typedi_1.Container.get('logger');
        try {
            const taskService = typedi_1.Container.get(biliDanmakuTask_1.default);
            const data = await taskService.delDanmakuTask(Object.assign({}, req.body), req.locals.user.id);
            return res.send(data);
        }
        catch (e) {
            return next(e);
        }
    });
    route.post('/danmaku/execute', (0, express_rate_limit_1.default)({
        windowMs: 20 * 60 * 1000,
        max: 200,
    }), (0, celebrate_1.celebrate)({
        body: celebrate_1.Joi.object({
            id: celebrate_1.Joi.string().required(),
        }),
    }), async (req, res, next) => {
        const logger = typedi_1.Container.get('logger');
        try {
            const accessToken = (0, util_1.getToken)(req);
            const taskService = typedi_1.Container.get(biliDanmakuTask_1.default);
            const data = await taskService.executeDanmakuTask(Object.assign({}, req.body), req.locals.user.id, accessToken);
            return res.send(data);
        }
        catch (e) {
            return next(e);
        }
    });
    route.post('/danmaku/stop', (0, express_rate_limit_1.default)({
        windowMs: 20 * 60 * 1000,
        max: 200,
    }), (0, celebrate_1.celebrate)({
        body: celebrate_1.Joi.object({
            id: celebrate_1.Joi.string().required(),
        }),
    }), async (req, res, next) => {
        const logger = typedi_1.Container.get('logger');
        try {
            const taskService = typedi_1.Container.get(biliDanmakuTask_1.default);
            const data = await taskService.stopDanmakuControlTask(Object.assign({}, req.body), req.locals.user.id);
            return res.send(data);
        }
        catch (e) {
            return next(e);
        }
    });
    route.put('/reboot', async (req, res, next) => {
        try {
            const accessToken = (0, util_1.getToken)(req);
            const taskService = typedi_1.Container.get(biliDanmakuTask_1.default);
            // 启动弹幕任务
            const data = await taskService.autoRunDanmakuTasksWithoutLimit(req.locals.user.id, accessToken);
            return res.send(data);
        }
        catch (e) {
            return next(e);
        }
    });
};
//# sourceMappingURL=biliDanmakuTask.js.map