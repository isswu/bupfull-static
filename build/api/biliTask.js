"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const typedi_1 = require("typedi");
const express_rate_limit_1 = __importDefault(require("express-rate-limit"));
const celebrate_1 = require("celebrate");
const util_1 = require("../config/util");
const biliTask_1 = __importDefault(require("../services/biliTask"));
const route = (0, express_1.Router)();
exports.default = (app) => {
    app.use('/biliTask', route);
    route.get('/simple/list', (0, express_rate_limit_1.default)({
        windowMs: 20 * 60 * 1000,
        max: 200,
    }), (0, celebrate_1.celebrate)({
        query: celebrate_1.Joi.object({
            t: celebrate_1.Joi.any(),
            searchValue: celebrate_1.Joi.string().optional().allow('').allow(null),
            page_index: celebrate_1.Joi.number().min(1).max(99999).required(),
            page_size: celebrate_1.Joi.number().min(10).max(100).required(),
            sorter: celebrate_1.Joi.string().optional().allow('').allow(null),
        }),
    }), async (req, res, next) => {
        const logger = typedi_1.Container.get('logger');
        try {
            const taskService = typedi_1.Container.get(biliTask_1.default);
            const data = await taskService.getSimpleList({
                searchText: String(req.query.searchValue || ''),
                page_index: Number(req.query.page_index),
                page_size: Number(req.query.page_size),
                sorter: String(req.query.sorter || ''),
            }, req.locals.user.isAdmin, req.locals.dataIn);
            return res.send(data);
        }
        catch (e) {
            return next(e);
        }
    });
    route.post('/simple/modify', (0, express_rate_limit_1.default)({
        windowMs: 20 * 60 * 1000,
        max: 200,
    }), (0, celebrate_1.celebrate)({
        body: celebrate_1.Joi.object({
            id: celebrate_1.Joi.any(),
            type: celebrate_1.Joi.string().required(),
            uid: celebrate_1.Joi.string().optional().allow('').allow(null),
            bvid: celebrate_1.Joi.string().optional().allow('').allow(null),
            template_code: celebrate_1.Joi.string().required(),
            execute_duration: celebrate_1.Joi.number().required(),
            duration_unit: celebrate_1.Joi.string().required(),
            times: celebrate_1.Joi.number().optional(),
        }),
    }), async (req, res, next) => {
        const logger = typedi_1.Container.get('logger');
        try {
            const taskService = typedi_1.Container.get(biliTask_1.default);
            const data = await taskService.modifySimple(Object.assign({}, req.body), req.locals.user.id);
            return res.send(data);
        }
        catch (e) {
            return next(e);
        }
    });
    route.post('/simple/publish', (0, express_rate_limit_1.default)({
        windowMs: 20 * 60 * 1000,
        max: 200,
    }), (0, celebrate_1.celebrate)({
        body: celebrate_1.Joi.object({
            id: celebrate_1.Joi.string().required(),
        }),
    }), async (req, res, next) => {
        const logger = typedi_1.Container.get('logger');
        try {
            const taskService = typedi_1.Container.get(biliTask_1.default);
            const data = await taskService.publishSimple(Object.assign({}, req.body), req.locals.user.id);
            return res.send(data);
        }
        catch (e) {
            return next(e);
        }
    });
    route.post('/simple/changeEnable', (0, express_rate_limit_1.default)({
        windowMs: 20 * 60 * 1000,
        max: 200,
    }), (0, celebrate_1.celebrate)({
        body: celebrate_1.Joi.object({
            id: celebrate_1.Joi.string().required(),
            enable_flag: celebrate_1.Joi.boolean().required(),
        }),
    }), async (req, res, next) => {
        const logger = typedi_1.Container.get('logger');
        try {
            const taskService = typedi_1.Container.get(biliTask_1.default);
            const data = await taskService.changeSimpleEnable(Object.assign({}, req.body), req.locals.user.id);
            return res.send(data);
        }
        catch (e) {
            return next(e);
        }
    });
    route.post('/simple/del', (0, express_rate_limit_1.default)({
        windowMs: 20 * 60 * 1000,
        max: 200,
    }), (0, celebrate_1.celebrate)({
        body: celebrate_1.Joi.object({
            id: celebrate_1.Joi.string().required(),
        }),
    }), async (req, res, next) => {
        const logger = typedi_1.Container.get('logger');
        try {
            const taskService = typedi_1.Container.get(biliTask_1.default);
            const data = await taskService.delSimple(Object.assign({}, req.body), req.locals.user.id);
            return res.send(data);
        }
        catch (e) {
            return next(e);
        }
    });
    route.post('/simple/execute', (0, express_rate_limit_1.default)({
        windowMs: 20 * 60 * 1000,
        max: 200,
    }), (0, celebrate_1.celebrate)({
        body: celebrate_1.Joi.object({
            id: celebrate_1.Joi.string().required(),
        }),
    }), async (req, res, next) => {
        const logger = typedi_1.Container.get('logger');
        try {
            const accessToken = (0, util_1.getToken)(req);
            const taskService = typedi_1.Container.get(biliTask_1.default);
            const data = await taskService.executeSimple(Object.assign({}, req.body), req.locals.user.id, accessToken);
            return res.send(data);
        }
        catch (e) {
            return next(e);
        }
    });
    route.get('/comment/list', (0, express_rate_limit_1.default)({
        windowMs: 20 * 60 * 1000,
        max: 200,
    }), (0, celebrate_1.celebrate)({
        query: celebrate_1.Joi.object({
            t: celebrate_1.Joi.any(),
            uid: celebrate_1.Joi.string().optional().allow('').allow(null),
            taskType: celebrate_1.Joi.string().optional().allow('').allow(null),
            searchValue: celebrate_1.Joi.string().optional().allow('').allow(null),
            page_index: celebrate_1.Joi.number().min(1).max(99999).required(),
            page_size: celebrate_1.Joi.number().min(10).max(100).required(),
            sorter: celebrate_1.Joi.string().optional().allow('').allow(null),
        }),
    }), async (req, res, next) => {
        const logger = typedi_1.Container.get('logger');
        try {
            const taskService = typedi_1.Container.get(biliTask_1.default);
            const data = await taskService.getCommentList(req.query, req.locals.user.isAdmin, req.locals.dataIn);
            return res.send(data);
        }
        catch (e) {
            return next(e);
        }
    });
    route.post('/comment/modify', (0, express_rate_limit_1.default)({
        windowMs: 20 * 60 * 1000,
        max: 200,
    }), (0, celebrate_1.celebrate)({
        body: celebrate_1.Joi.object({
            id: celebrate_1.Joi.any(),
            task_type: celebrate_1.Joi.number().required(),
            uid: celebrate_1.Joi.string().required(),
            bvid: celebrate_1.Joi.string().required(),
            comment_tag_id: celebrate_1.Joi.string().optional().allow('').allow(null),
            template_code: celebrate_1.Joi.string().optional().allow('').allow(null),
            control_time: celebrate_1.Joi.date().optional().allow(null),
            mid_white_list: celebrate_1.Joi.any().optional().allow('').allow(null),
            publish_remark: celebrate_1.Joi.string().optional().allow('').allow(null),
            execute_duration: celebrate_1.Joi.number().optional(),
            duration_unit: celebrate_1.Joi.string().optional(),
        }),
    }), async (req, res, next) => {
        const logger = typedi_1.Container.get('logger');
        try {
            const taskService = typedi_1.Container.get(biliTask_1.default);
            const data = await taskService.modifyComment(Object.assign({}, req.body), req.locals.user.id);
            return res.send(data);
        }
        catch (e) {
            return next(e);
        }
    });
    route.post('/comment/publish', (0, express_rate_limit_1.default)({
        windowMs: 20 * 60 * 1000,
        max: 200,
    }), (0, celebrate_1.celebrate)({
        body: celebrate_1.Joi.object({
            id: celebrate_1.Joi.string().required(),
        }),
    }), async (req, res, next) => {
        const logger = typedi_1.Container.get('logger');
        try {
            const taskService = typedi_1.Container.get(biliTask_1.default);
            const data = await taskService.publishComment(Object.assign({}, req.body), req.locals.user.id);
            return res.send(data);
        }
        catch (e) {
            return next(e);
        }
    });
    route.post('/comment/changeEnable', (0, express_rate_limit_1.default)({
        windowMs: 20 * 60 * 1000,
        max: 200,
    }), (0, celebrate_1.celebrate)({
        body: celebrate_1.Joi.object({
            id: celebrate_1.Joi.string().required(),
            enable_flag: celebrate_1.Joi.boolean().required(),
        }),
    }), async (req, res, next) => {
        const logger = typedi_1.Container.get('logger');
        try {
            const taskService = typedi_1.Container.get(biliTask_1.default);
            const data = await taskService.changeCommentEnable(Object.assign({}, req.body), req.locals.user.id);
            return res.send(data);
        }
        catch (e) {
            return next(e);
        }
    });
    route.post('/comment/changeControl', (0, express_rate_limit_1.default)({
        windowMs: 20 * 60 * 1000,
        max: 200,
    }), (0, celebrate_1.celebrate)({
        body: celebrate_1.Joi.object({
            id: celebrate_1.Joi.string().required(),
            control_flag: celebrate_1.Joi.boolean().required(),
        }),
    }), async (req, res, next) => {
        const logger = typedi_1.Container.get('logger');
        try {
            const taskService = typedi_1.Container.get(biliTask_1.default);
            const data = await taskService.changeCommentControl(Object.assign({}, req.body), req.locals.user.id);
            return res.send(data);
        }
        catch (e) {
            return next(e);
        }
    });
    route.post('/comment/del', (0, express_rate_limit_1.default)({
        windowMs: 20 * 60 * 1000,
        max: 200,
    }), (0, celebrate_1.celebrate)({
        body: celebrate_1.Joi.object({
            id: celebrate_1.Joi.string().required(),
        }),
    }), async (req, res, next) => {
        const logger = typedi_1.Container.get('logger');
        try {
            const taskService = typedi_1.Container.get(biliTask_1.default);
            const data = await taskService.delComment(Object.assign({}, req.body), req.locals.user.id);
            return res.send(data);
        }
        catch (e) {
            return next(e);
        }
    });
    route.post('/comment/execute', (0, express_rate_limit_1.default)({
        windowMs: 20 * 60 * 1000,
        max: 200,
    }), (0, celebrate_1.celebrate)({
        body: celebrate_1.Joi.object({
            id: celebrate_1.Joi.string().required(),
        }),
    }), async (req, res, next) => {
        const logger = typedi_1.Container.get('logger');
        try {
            const accessToken = (0, util_1.getToken)(req);
            const taskService = typedi_1.Container.get(biliTask_1.default);
            const data = await taskService.executeCommentTask(Object.assign({}, req.body), req.locals.user.id, accessToken);
            return res.send(data);
        }
        catch (e) {
            return next(e);
        }
    });
    route.post('/comment/stop', (0, express_rate_limit_1.default)({
        windowMs: 20 * 60 * 1000,
        max: 200,
    }), (0, celebrate_1.celebrate)({
        body: celebrate_1.Joi.object({
            id: celebrate_1.Joi.string().required(),
        }),
    }), async (req, res, next) => {
        const logger = typedi_1.Container.get('logger');
        try {
            const taskService = typedi_1.Container.get(biliTask_1.default);
            const data = await taskService.stopCommentTask(Object.assign({}, req.body), req.locals.user.id);
            return res.send(data);
        }
        catch (e) {
            return next(e);
        }
    });
    route.put('/reboot', async (req, res, next) => {
        const logger = typedi_1.Container.get('logger');
        try {
            const accessToken = (0, util_1.getToken)(req);
            const taskService = typedi_1.Container.get(biliTask_1.default);
            // 启动评控任务
            const data = await taskService.autoRunCommentTasksWithoutLimit(req.locals.user.id, accessToken);
            return res.send(data);
        }
        catch (e) {
            return next(e);
        }
    });
    route.get('/standy-comments', (0, express_rate_limit_1.default)({
        windowMs: 20 * 60 * 1000,
        max: 200,
    }), (0, celebrate_1.celebrate)({
        query: celebrate_1.Joi.object({
            t: celebrate_1.Joi.any(),
            uid: celebrate_1.Joi.string().required(),
            bvid: celebrate_1.Joi.string().required(),
            page_index: celebrate_1.Joi.number().min(1).max(99999).required(),
            page_size: celebrate_1.Joi.number().min(10).max(100).required(),
            sorter: celebrate_1.Joi.string().optional().allow('').allow(null),
        }),
    }), async (req, res, next) => {
        try {
            const taskService = typedi_1.Container.get(biliTask_1.default);
            const data = await taskService.getStandyComments(req.query);
            return res.send(data);
        }
        catch (e) {
            return next(e);
        }
    });
    route.delete('/standy-comments', (0, express_rate_limit_1.default)({
        windowMs: 20 * 60 * 1000,
        max: 200,
    }), (0, celebrate_1.celebrate)({
        body: celebrate_1.Joi.object({
            uid: celebrate_1.Joi.string().required(),
            aid: celebrate_1.Joi.string().required(),
            bvid: celebrate_1.Joi.string().required(),
            ids: celebrate_1.Joi.array().required(),
            rpids: celebrate_1.Joi.array().required(),
        }),
    }), async (req, res, next) => {
        const logger = typedi_1.Container.get('logger');
        try {
            const accessToken = (0, util_1.getToken)(req);
            const taskService = typedi_1.Container.get(biliTask_1.default);
            const data = await taskService.delHitComments(Object.assign({}, req.body), req.locals.user.id, accessToken);
            return res.send(data);
        }
        catch (e) {
            return next(e);
        }
    });
};
//# sourceMappingURL=biliTask.js.map