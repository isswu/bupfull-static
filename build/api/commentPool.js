"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const typedi_1 = require("typedi");
const express_rate_limit_1 = __importDefault(require("express-rate-limit"));
const celebrate_1 = require("celebrate");
const commentPool_1 = __importDefault(require("../services/commentPool"));
const util_1 = require("../config/util");
const multer_1 = __importDefault(require("multer"));
const config_1 = __importDefault(require("../config"));
// import fs from 'fs';
const xlsx_1 = __importDefault(require("xlsx"));
const route = (0, express_1.Router)();
const storage = multer_1.default.diskStorage({
    destination: function (req, file, cb) {
        cb(null, config_1.default.scriptPath);
    },
    filename: function (req, file, cb) {
        cb(null, file.originalname);
    },
});
const upload = (0, multer_1.default)({ storage: storage });
/**
 * 评论池
 */
exports.default = (app) => {
    app.use('/commentPool', route);
    route.get('/groupedComments', (0, express_rate_limit_1.default)({
        windowMs: 15 * 60 * 1000,
        max: 100,
    }), (0, celebrate_1.celebrate)({
        query: celebrate_1.Joi.object({
            t: celebrate_1.Joi.any(),
            bvid: celebrate_1.Joi.string().required(),
        }),
    }), async (req, res, next) => {
        const logger = typedi_1.Container.get('logger');
        try {
            const service = typedi_1.Container.get(commentPool_1.default);
            const data = await service.findByTagIdGrouped(req.query.bvid);
            return res.send(data);
        }
        catch (e) {
            logger.error('🔥 error: %o', e);
            return next(e);
        }
    });
    /**
     * 分页列表
     */
    route.get('/list', (0, express_rate_limit_1.default)({
        windowMs: 15 * 60 * 1000,
        max: 100,
    }), (0, celebrate_1.celebrate)({
        query: celebrate_1.Joi.object({
            t: celebrate_1.Joi.any(),
            page_index: celebrate_1.Joi.number().required().min(1).max(99999),
            page_size: celebrate_1.Joi.number().required().min(10).max(100),
            bvid: celebrate_1.Joi.string().required(),
            tag_id: celebrate_1.Joi.string().required(),
            search_text: celebrate_1.Joi.any(),
        }),
    }), async (req, res, next) => {
        const logger = typedi_1.Container.get('logger');
        try {
            // const accessToken = getToken(req);
            const commentPoolService = typedi_1.Container.get(commentPool_1.default);
            const data = await commentPoolService.getList({
                page_index: Number(req.query.page_index),
                page_size: Number(req.query.page_size),
                bvid: req.query.bvid,
                tag_id: String(req.query.tag_id),
                search_text: String(req.query.search_text),
            });
            return res.send(data);
        }
        catch (e) {
            logger.error('🔥 error: %o', e);
            return next(e);
        }
    });
    /**
     * 新增/修改 记录
     */
    route.post('/modify', (0, express_rate_limit_1.default)({
        windowMs: 15 * 60 * 1000,
        max: 100,
    }), (0, celebrate_1.celebrate)({
        body: celebrate_1.Joi.object({
            id: celebrate_1.Joi.string(),
            bvid: celebrate_1.Joi.string().required(),
            comment: celebrate_1.Joi.string().required(),
            tag_id: celebrate_1.Joi.string(),
            tag_name: celebrate_1.Joi.string(),
        }),
    }), async (req, res, next) => {
        const logger = typedi_1.Container.get('logger');
        try {
            const commentPoolService = typedi_1.Container.get(commentPool_1.default);
            const data = await commentPoolService.modify(Object.assign({}, req.body), req.locals.user.id);
            return res.send(data);
        }
        catch (e) {
            logger.error('🔥 error: %o', e);
            return next(e);
        }
    });
    /**
     * 删除记录
     */
    route.post('/del', (0, express_rate_limit_1.default)({
        windowMs: 15 * 60 * 1000,
        max: 100,
    }), (0, celebrate_1.celebrate)({
        body: celebrate_1.Joi.object({
            id: celebrate_1.Joi.string().required(),
        }),
    }), async (req, res, next) => {
        const logger = typedi_1.Container.get('logger');
        try {
            const accessToken = (0, util_1.getToken)(req);
            const commentPoolService = typedi_1.Container.get(commentPool_1.default);
            const data = await commentPoolService.del(Object.assign({}, req.body), accessToken);
            return res.send(data);
        }
        catch (e) {
            return next(e);
        }
    });
    /**
     * 导入
     */
    route.post('/:bvid/upload', upload.single('file'), (0, celebrate_1.celebrate)({
        params: celebrate_1.Joi.object({
            bvid: celebrate_1.Joi.string().required(),
        }),
    }), async (req, res, next) => {
        var _a;
        const logger = typedi_1.Container.get('logger');
        try {
            const commentPoolService = typedi_1.Container.get(commentPool_1.default);
            // const fileContent = await fs.promises.readFile(req!.file!.path, 'utf8');
            // const parseContent = JSON.parse(fileContent);
            // const data = Array.isArray(parseContent)
            //   ? parseContent
            //   : [parseContent];
            // if (data.every((x) => x.comment && x.tag_name)) {
            //   const result = await commentPoolService.upload(
            //     data.map((x) => ({
            //       comment: x.comment,
            //       tag_name: x.tag_name,
            //     })),
            //     accessToken,
            //   );
            //   return res.send({ code: 0, data: result });
            // } else {
            //   return res.send({
            //     code: 400,
            //     message: '文件缺少comment或者tag_name字段，参考导入文件格式',
            //   });
            // }
            var workbook = xlsx_1.default.readFile(req.file.path); //整个　excel　文档
            var sheetNames = workbook.SheetNames; //获取所有工作薄名
            //解析
            var sheet1 = workbook.Sheets[sheetNames[0]]; //根据工作薄名获取工作薄
            /*
             sheet1['!ref']　获取工作薄的有效范围　'A1:C20'
             XLSX.utils.decode_range 将有效范围转为　range对象
             range: {s: {r:0, c:0}, e: {r:10, 3}}
             */
            var range = xlsx_1.default.utils.decode_range((_a = sheet1['!ref']) !== null && _a !== void 0 ? _a : '');
            var data = [];
            // 验证标题
            var title_tag = sheet1[xlsx_1.default.utils.encode_cell({ c: range.s.c, r: range.s.r })];
            logger.info('title_tag is', title_tag);
            var title_comment = sheet1[xlsx_1.default.utils.encode_cell({ c: range.s.c + 1, r: range.s.r })];
            logger.info('title_comment is', title_comment);
            var titleValid = true;
            if (!title_tag || title_tag.v.trim() != '标签') {
                titleValid = false;
            }
            if (!title_comment || title_comment.v.trim() != '评论') {
                titleValid = false;
            }
            if (!titleValid) {
                return res.send({
                    code: 400,
                    message: 'Excel文件格式不正确，参考导入文件模板',
                });
            }
            // 循环获取单元格值
            for (var r = range.s.r + 1; r <= range.e.r; ++r) {
                // 获取行
                var row = [];
                for (var c = range.s.c; c <= range.e.c; ++c) {
                    // 获取列
                    var row_value = null;
                    var cell_address = { c: c, r: r }; // 获取单元格地址
                    var cell = xlsx_1.default.utils.encode_cell(cell_address); //根据单元格地址获取单元格
                    if (sheet1[cell]) {
                        // 获取单元格值
                        row_value = sheet1[cell].v.trim();
                    }
                    else {
                        row_value = '';
                    }
                    row.push(row_value);
                }
                // 判断整行是否都不为空，否则去掉
                var flag = true;
                for (var i = 0; i < row.length; i++) {
                    if (row[i] == '') {
                        flag = false;
                        break;
                    }
                }
                // 整行都不为空
                if (flag) {
                    data.push({
                        comment: row[1],
                        tag_name: row[0],
                    });
                }
            }
            if (data.length == 0) {
                return res.send({
                    code: 400,
                    message: 'Excel文件数据为空，请填好数据后重新导入',
                });
            }
            const result = await commentPoolService.upload(data, req.params.bvid, req.locals.user.id);
            return res.send({ code: 0, data: result });
        }
        catch (e) {
            return next(e);
        }
    });
};
//# sourceMappingURL=commentPool.js.map