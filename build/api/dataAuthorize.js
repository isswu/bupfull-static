"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const typedi_1 = require("typedi");
const celebrate_1 = require("celebrate");
const dataAuthorize_1 = __importDefault(require("../services/dataAuthorize"));
const route = (0, express_1.Router)();
exports.default = (app) => {
    app.use('/dataAuthorize', route);
    route.post('/biliAccounts', (0, celebrate_1.celebrate)({
        body: celebrate_1.Joi.object({
            dataIds: celebrate_1.Joi.array().items(celebrate_1.Joi.string().required()),
            userIds: celebrate_1.Joi.array().items(celebrate_1.Joi.string().required()),
        }),
    }), async (req, res, next) => {
        try {
            const service = typedi_1.Container.get(dataAuthorize_1.default);
            const data = await service.authorizeBiliAccounts(req.body, req.locals.user.id);
            res.send(data);
        }
        catch (e) {
            return next(e);
        }
    });
    route.post('/simpleTasks', (0, celebrate_1.celebrate)({
        body: celebrate_1.Joi.object({
            dataIds: celebrate_1.Joi.array().items(celebrate_1.Joi.string().required()),
            userIds: celebrate_1.Joi.array().items(celebrate_1.Joi.string().required()),
        }),
    }), async (req, res, next) => {
        try {
            const service = typedi_1.Container.get(dataAuthorize_1.default);
            const data = await service.authorizeSimpleTasks(req.body, req.locals.user.id);
            res.send(data);
        }
        catch (e) {
            return next(e);
        }
    });
    route.post('/commentTasks', (0, celebrate_1.celebrate)({
        body: celebrate_1.Joi.object({
            dataIds: celebrate_1.Joi.array().items(celebrate_1.Joi.string().required()),
            userIds: celebrate_1.Joi.array().items(celebrate_1.Joi.string().required()),
        }),
    }), async (req, res, next) => {
        try {
            const service = typedi_1.Container.get(dataAuthorize_1.default);
            const data = await service.authorizeCommentTasks(req.body, req.locals.user.id);
            res.send(data);
        }
        catch (e) {
            return next(e);
        }
    });
    route.post('/danmakuTasks', (0, celebrate_1.celebrate)({
        body: celebrate_1.Joi.object({
            dataIds: celebrate_1.Joi.array().items(celebrate_1.Joi.string().required()),
            userIds: celebrate_1.Joi.array().items(celebrate_1.Joi.string().required()),
        }),
    }), async (req, res, next) => {
        try {
            const service = typedi_1.Container.get(dataAuthorize_1.default);
            const data = await service.authorizeDanmakuTasks(req.body, req.locals.user.id);
            res.send(data);
        }
        catch (e) {
            return next(e);
        }
    });
};
//# sourceMappingURL=dataAuthorize.js.map