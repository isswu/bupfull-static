"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const typedi_1 = require("typedi");
const permission_1 = __importDefault(require("../services/permission"));
const celebrate_1 = require("celebrate");
const util_1 = require("../config/util");
const route = (0, express_1.Router)();
exports.default = (app) => {
    app.use('/permission', route);
    route.post('/check', (0, celebrate_1.celebrate)({
        body: celebrate_1.Joi.object({
            path: celebrate_1.Joi.string().required(),
            action: celebrate_1.Joi.string().required(),
        }),
    }), async (req, res, next) => {
        try {
            const accessToken = (0, util_1.getToken)(req);
            const permissionService = typedi_1.Container.get(permission_1.default);
            const data = await permissionService.checkPermissions(req.body, accessToken);
            res.send(data);
        }
        catch (e) {
            return next(e);
        }
    });
};
//# sourceMappingURL=permission.js.map