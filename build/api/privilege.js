"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const typedi_1 = require("typedi");
const privilege_1 = __importDefault(require("../services/privilege"));
const route = (0, express_1.Router)();
exports.default = (app) => {
    app.use('/privilege', route);
    /**
     * 系统菜单列表
     */
    route.get('/menu/list', async (req, res, next) => {
        try {
            const privilegeManageService = typedi_1.Container.get(privilege_1.default);
            const data = await privilegeManageService.menuList('');
            return res.send({ code: 200, data });
        }
        catch (e) {
            return next(e);
        }
    });
};
//# sourceMappingURL=privilege.js.map