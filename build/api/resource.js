"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const typedi_1 = require("typedi");
const express_rate_limit_1 = __importDefault(require("express-rate-limit"));
const celebrate_1 = require("celebrate");
const util_1 = require("../config/util");
const resource_1 = __importDefault(require("../services/resource"));
const route = (0, express_1.Router)();
/**
 * 资源管理
 */
exports.default = (app) => {
    app.use('/resource', route);
    /**
     * 分页列表
     */
    route.get('/list', (0, express_rate_limit_1.default)({
        windowMs: 15 * 60 * 1000,
        max: 100,
    }), (0, celebrate_1.celebrate)({
        query: celebrate_1.Joi.object({
            t: celebrate_1.Joi.any(),
            page_index: celebrate_1.Joi.number().required().min(1).max(999999),
            page_size: celebrate_1.Joi.number().required().min(10).max(100),
            type: celebrate_1.Joi.number().required().min(1).max(3),
        }),
    }), async (req, res, next) => {
        const logger = typedi_1.Container.get('logger');
        try {
            const accessToken = (0, util_1.getToken)(req);
            const resourceService = typedi_1.Container.get(resource_1.default);
            const data = await resourceService.getList({
                page_index: Number(req.query.page_index),
                page_size: Number(req.query.page_size),
                type: Number(req.query.type),
            }, accessToken);
            return res.send(data);
        }
        catch (e) {
            return next(e);
        }
    });
    /**
     * 新增/修改 记录
     */
    route.post('/modify', (0, express_rate_limit_1.default)({
        windowMs: 15 * 60 * 1000,
        max: 100,
    }), (0, celebrate_1.celebrate)({
        body: celebrate_1.Joi.object({
            id: celebrate_1.Joi.number().empty(),
            obj: celebrate_1.Joi.string().required(),
            name: celebrate_1.Joi.string().required(),
            type: celebrate_1.Joi.number().required().min(1).max(3),
        }),
    }), async (req, res, next) => {
        const logger = typedi_1.Container.get('logger');
        try {
            const accessToken = (0, util_1.getToken)(req);
            const resourceService = typedi_1.Container.get(resource_1.default);
            const data = await resourceService.modify(Object.assign({}, req.body), accessToken);
            return res.send(data);
        }
        catch (e) {
            return next(e);
        }
    });
    /**
     * 删除 记录
     */
    route.post('/del', (0, express_rate_limit_1.default)({
        windowMs: 15 * 60 * 1000,
        max: 100,
    }), (0, celebrate_1.celebrate)({
        body: celebrate_1.Joi.object({
            id: celebrate_1.Joi.number().required().min(1).max(99999999),
        }),
    }), async (req, res, next) => {
        const logger = typedi_1.Container.get('logger');
        try {
            const accessToken = (0, util_1.getToken)(req);
            const resourceService = typedi_1.Container.get(resource_1.default);
            const data = await resourceService.delete(Object.assign({}, req.body), accessToken);
            return res.send(data);
        }
        catch (e) {
            return next(e);
        }
    });
};
//# sourceMappingURL=resource.js.map