"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const typedi_1 = require("typedi");
const express_rate_limit_1 = __importDefault(require("express-rate-limit"));
const celebrate_1 = require("celebrate");
const util_1 = require("../config/util");
const role_1 = __importDefault(require("../services/role"));
const route = (0, express_1.Router)();
/**
 * 角色管理
 */
exports.default = (app) => {
    app.use('/role', route);
    /**
     * 分页列表
     */
    route.get('/list', (0, express_rate_limit_1.default)({
        windowMs: 15 * 60 * 1000,
        max: 100,
    }), (0, celebrate_1.celebrate)({
        query: celebrate_1.Joi.object({
            t: celebrate_1.Joi.any(),
            page_index: celebrate_1.Joi.number().required().min(1).max(99999),
            page_size: celebrate_1.Joi.number().required().min(10).max(100),
        }),
    }), async (req, res, next) => {
        const logger = typedi_1.Container.get('logger');
        try {
            const accessToken = (0, util_1.getToken)(req);
            const roleService = typedi_1.Container.get(role_1.default);
            const data = await roleService.getList({
                page_index: Number(req.query.page_index),
                page_size: Number(req.query.page_size),
            }, accessToken);
            return res.send(data);
        }
        catch (e) {
            return next(e);
        }
    });
    /**
     * 新增/修改 记录
     */
    route.post('/modify', (0, express_rate_limit_1.default)({
        windowMs: 15 * 60 * 1000,
        max: 100,
    }), (0, celebrate_1.celebrate)({
        body: celebrate_1.Joi.object({
            id: celebrate_1.Joi.number().empty(),
            code: celebrate_1.Joi.string().required(),
            name: celebrate_1.Joi.string().required(),
            enName: celebrate_1.Joi.string().required(),
            tenantId: celebrate_1.Joi.any(),
            desc: celebrate_1.Joi.string().empty(),
        }),
    }), async (req, res, next) => {
        const logger = typedi_1.Container.get('logger');
        try {
            const accessToken = (0, util_1.getToken)(req);
            const roleService = typedi_1.Container.get(role_1.default);
            const data = await roleService.modify(Object.assign({}, req.body), accessToken);
            return res.send(data);
        }
        catch (e) {
            return next(e);
        }
    });
    /**
     * 删除 记录
     */
    route.post('/del', (0, express_rate_limit_1.default)({
        windowMs: 15 * 60 * 1000,
        max: 100,
    }), (0, celebrate_1.celebrate)({
        body: celebrate_1.Joi.object({
            id: celebrate_1.Joi.number().required().min(1).max(99999999),
        }),
    }), async (req, res, next) => {
        const logger = typedi_1.Container.get('logger');
        try {
            const accessToken = (0, util_1.getToken)(req);
            const roleService = typedi_1.Container.get(role_1.default);
            const data = await roleService.delete(Object.assign({}, req.body), accessToken);
            return res.send(data);
        }
        catch (e) {
            return next(e);
        }
    });
    /**
     * 获取角色详情
     */
    route.get('/:id', (0, celebrate_1.celebrate)({
        params: celebrate_1.Joi.object({
            id: celebrate_1.Joi.string().required(),
        }),
    }), async (req, res, next) => {
        const logger = typedi_1.Container.get('logger');
        try {
            const accessToken = (0, util_1.getToken)(req);
            const roleService = typedi_1.Container.get(role_1.default);
            const data = await roleService.getRoleInfoById(req.params.id, accessToken);
            return res.send(data);
        }
        catch (e) {
            return next(e);
        }
    });
    /**
     * 获取角色主体列表
     */
    route.get('/:id/subjects', (0, celebrate_1.celebrate)({
        params: celebrate_1.Joi.object({
            id: celebrate_1.Joi.string().required(),
        }),
    }), async (req, res, next) => {
        const logger = typedi_1.Container.get('logger');
        try {
            const accessToken = (0, util_1.getToken)(req);
            const roleService = typedi_1.Container.get(role_1.default);
            const data = await roleService.getSubjects(req.params.id, accessToken);
            return res.send(data);
        }
        catch (e) {
            return next(e);
        }
    });
    /**
     * 添加角色主体
     */
    route.post('/:id/subjects', (0, celebrate_1.celebrate)({
        params: celebrate_1.Joi.object({
            id: celebrate_1.Joi.string().required(),
        }),
        body: celebrate_1.Joi.object({
            type: celebrate_1.Joi.string().required(),
            subjectIds: celebrate_1.Joi.array().required(),
        }),
    }), async (req, res, next) => {
        const logger = typedi_1.Container.get('logger');
        try {
            const accessToken = (0, util_1.getToken)(req);
            const roleService = typedi_1.Container.get(role_1.default);
            const data = await roleService.addSubjects(Number(req.params.id), req.body.type, req.body.subjectIds, accessToken);
            return res.send(data);
        }
        catch (e) {
            return next(e);
        }
    });
    /**
     * 删除角色主体
     */
    route.post('/:id/subjects/del', (0, celebrate_1.celebrate)({
        params: celebrate_1.Joi.object({
            id: celebrate_1.Joi.string().required(),
        }),
        body: celebrate_1.Joi.object({
            type: celebrate_1.Joi.string().required(),
            subjectId: celebrate_1.Joi.number().required(),
        }),
    }), async (req, res, next) => {
        const logger = typedi_1.Container.get('logger');
        try {
            const accessToken = (0, util_1.getToken)(req);
            const roleService = typedi_1.Container.get(role_1.default);
            const data = await roleService.delSubject(Number(req.params.id), req.body.type, req.body.subjectId, accessToken);
            return res.send(data);
        }
        catch (e) {
            return next(e);
        }
    });
    /**
     * 获取角色菜单列表
     */
    route.get('/:id/menus', (0, celebrate_1.celebrate)({
        params: celebrate_1.Joi.object({
            id: celebrate_1.Joi.string().required(),
        }),
    }), async (req, res, next) => {
        const logger = typedi_1.Container.get('logger');
        try {
            const accessToken = (0, util_1.getToken)(req);
            const roleService = typedi_1.Container.get(role_1.default);
            const data = await roleService.getMenus(req.params.id, accessToken);
            return res.send(data);
        }
        catch (e) {
            return next(e);
        }
    });
    /**
     * 添加角色菜单
     */
    route.post('/:id/menus', (0, celebrate_1.celebrate)({
        params: celebrate_1.Joi.object({
            id: celebrate_1.Joi.string().required(),
        }),
        body: celebrate_1.Joi.object({
            menuIds: celebrate_1.Joi.array().required(),
        }),
    }), async (req, res, next) => {
        const logger = typedi_1.Container.get('logger');
        try {
            const accessToken = (0, util_1.getToken)(req);
            const roleService = typedi_1.Container.get(role_1.default);
            const data = await roleService.addMenus(Number(req.params.id), req.body.menuIds, accessToken);
            return res.send(data);
        }
        catch (e) {
            return next(e);
        }
    });
    /**
     * 删除角色菜单
     */
    route.post('/:id/menus/del', (0, celebrate_1.celebrate)({
        params: celebrate_1.Joi.object({
            id: celebrate_1.Joi.string().required(),
        }),
        body: celebrate_1.Joi.object({
            recordId: celebrate_1.Joi.number().required(),
        }),
    }), async (req, res, next) => {
        const logger = typedi_1.Container.get('logger');
        try {
            const accessToken = (0, util_1.getToken)(req);
            const roleService = typedi_1.Container.get(role_1.default);
            const data = await roleService.delMenu(Number(req.params.id), req.body.recordId, accessToken);
            return res.send(data);
        }
        catch (e) {
            return next(e);
        }
    });
    /**
     * 获取角色资源列表
     */
    route.get('/:id/resources', (0, celebrate_1.celebrate)({
        params: celebrate_1.Joi.object({
            id: celebrate_1.Joi.string().required(),
        }),
    }), async (req, res, next) => {
        const logger = typedi_1.Container.get('logger');
        try {
            const accessToken = (0, util_1.getToken)(req);
            const roleService = typedi_1.Container.get(role_1.default);
            const data = await roleService.getResources(req.params.id, accessToken);
            return res.send(data);
        }
        catch (e) {
            return next(e);
        }
    });
    /**
     * 添加角色资源
     */
    route.post('/:id/resources', (0, celebrate_1.celebrate)({
        params: celebrate_1.Joi.object({
            id: celebrate_1.Joi.string().required(),
        }),
        body: celebrate_1.Joi.object({
            type: celebrate_1.Joi.string().required(),
            resourceIds: celebrate_1.Joi.array().required(),
        }),
    }), async (req, res, next) => {
        const logger = typedi_1.Container.get('logger');
        try {
            const accessToken = (0, util_1.getToken)(req);
            const roleService = typedi_1.Container.get(role_1.default);
            const data = await roleService.addResources(Number(req.params.id), req.body.type, req.body.resourceIds, accessToken);
            return res.send(data);
        }
        catch (e) {
            return next(e);
        }
    });
    /**
     * 删除角色资源
     */
    route.post('/:id/resources/del', (0, celebrate_1.celebrate)({
        params: celebrate_1.Joi.object({
            id: celebrate_1.Joi.string().required(),
        }),
        body: celebrate_1.Joi.object({
            recordId: celebrate_1.Joi.number().required(),
        }),
    }), async (req, res, next) => {
        const logger = typedi_1.Container.get('logger');
        try {
            const accessToken = (0, util_1.getToken)(req);
            const roleService = typedi_1.Container.get(role_1.default);
            const data = await roleService.delResource(Number(req.params.id), req.body.recordId, accessToken);
            return res.send(data);
        }
        catch (e) {
            return next(e);
        }
    });
};
//# sourceMappingURL=role.js.map