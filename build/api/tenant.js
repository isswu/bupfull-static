"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const typedi_1 = require("typedi");
const tenant_1 = __importDefault(require("../services/tenant"));
const celebrate_1 = require("celebrate");
const express_rate_limit_1 = __importDefault(require("express-rate-limit"));
const util_1 = require("../config/util");
const route = (0, express_1.Router)();
exports.default = (app) => {
    app.use('/tenant', route);
    route.get('/list', (0, express_rate_limit_1.default)({
        windowMs: 15 * 60 * 1000,
        max: 100,
    }), (0, celebrate_1.celebrate)({
        query: celebrate_1.Joi.object({
            t: celebrate_1.Joi.any(),
            page_index: celebrate_1.Joi.number().required().min(1).max(99999),
            page_size: celebrate_1.Joi.number().required().min(10).max(100),
        }),
    }), async (req, res, next) => {
        const logger = typedi_1.Container.get('logger');
        try {
            const accessToken = (0, util_1.getToken)(req);
            const tenantService = typedi_1.Container.get(tenant_1.default);
            const data = await tenantService.getList({
                page_index: Number(req.query.page_index),
                page_size: Number(req.query.page_size),
            }, accessToken);
            return res.send(data);
        }
        catch (e) {
            return next(e);
        }
    });
    route.post('/', (0, celebrate_1.celebrate)({
        body: celebrate_1.Joi.object({
            name: celebrate_1.Joi.string().required(),
            code: celebrate_1.Joi.string().required(),
            remark: celebrate_1.Joi.string().optional().allow(''),
        }),
    }), async (req, res, next) => {
        const logger = typedi_1.Container.get('logger');
        try {
            const accessToken = (0, util_1.getToken)(req);
            const tenantService = typedi_1.Container.get(tenant_1.default);
            const data = await tenantService.createTenant(req.body, accessToken);
            res.send(data);
        }
        catch (e) {
            return next(e);
        }
    });
    route.get('/:id', (0, celebrate_1.celebrate)({
        params: celebrate_1.Joi.object({
            id: celebrate_1.Joi.string().required(),
        }),
    }), async (req, res, next) => {
        const logger = typedi_1.Container.get('logger');
        try {
            const accessToken = (0, util_1.getToken)(req);
            const tenantService = typedi_1.Container.get(tenant_1.default);
            const data = await tenantService.getTenantDetail(req.params.id, accessToken);
            return res.send(data);
        }
        catch (e) {
            return next(e);
        }
    });
    route.delete('/:id', (0, celebrate_1.celebrate)({
        params: celebrate_1.Joi.object({
            id: celebrate_1.Joi.string().required(),
        }),
    }), async (req, res, next) => {
        const logger = typedi_1.Container.get('logger');
        try {
            const accessToken = (0, util_1.getToken)(req);
            const tenantService = typedi_1.Container.get(tenant_1.default);
            const data = await tenantService.delete(req.params.id, accessToken);
            return res.send(data);
        }
        catch (e) {
            return next(e);
        }
    });
};
//# sourceMappingURL=tenant.js.map