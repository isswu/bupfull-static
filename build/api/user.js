"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const typedi_1 = require("typedi");
const user_1 = __importDefault(require("../services/user"));
const celebrate_1 = require("celebrate");
const multer_1 = __importDefault(require("multer"));
const path_1 = __importDefault(require("path"));
const uuid_1 = require("uuid");
const express_rate_limit_1 = __importDefault(require("express-rate-limit"));
const config_1 = __importDefault(require("../config"));
const util_1 = require("../config/util");
const route = (0, express_1.Router)();
const storage = multer_1.default.diskStorage({
    destination: function (req, file, cb) {
        cb(null, config_1.default.uploadPath);
    },
    filename: function (req, file, cb) {
        const ext = path_1.default.parse(file.originalname).ext;
        const key = (0, uuid_1.v4)();
        cb(null, key + ext);
    },
});
const upload = (0, multer_1.default)({ storage: storage });
exports.default = (app) => {
    app.use('/user', route);
    /**
     * 分页列表
     */
    route.get('/list', (0, express_rate_limit_1.default)({
        windowMs: 15 * 60 * 1000,
        max: 100,
    }), (0, celebrate_1.celebrate)({
        query: celebrate_1.Joi.object({
            t: celebrate_1.Joi.any(),
            page_index: celebrate_1.Joi.number().required().min(1).max(99999),
            page_size: celebrate_1.Joi.number().required().min(10).max(100),
        }),
    }), async (req, res, next) => {
        const logger = typedi_1.Container.get('logger');
        try {
            const accessToken = (0, util_1.getToken)(req);
            const userService = typedi_1.Container.get(user_1.default);
            const data = await userService.getList({
                page_index: Number(req.query.page_index),
                page_size: Number(req.query.page_size),
            }, accessToken);
            return res.send(data);
        }
        catch (e) {
            return next(e);
        }
    });
    route.post('/login', (0, express_rate_limit_1.default)({
        windowMs: 15 * 60 * 1000,
        max: 100,
    }), (0, celebrate_1.celebrate)({
        body: celebrate_1.Joi.object({
            username: celebrate_1.Joi.string().required(),
            password: celebrate_1.Joi.string().required(),
        }),
    }), async (req, res, next) => {
        const logger = typedi_1.Container.get('logger');
        try {
            const userService = typedi_1.Container.get(user_1.default);
            const data = await userService.login(Object.assign({}, req.body), req);
            return res.send(data);
        }
        catch (e) {
            return next(e);
        }
    });
    route.post('/logout', async (req, res, next) => {
        const logger = typedi_1.Container.get('logger');
        try {
            const accessToken = (0, util_1.getToken)(req);
            const userService = typedi_1.Container.get(user_1.default);
            const data = await userService.logout(req.platform, accessToken);
            res.send(data);
        }
        catch (e) {
            return next(e);
        }
    });
    route.post('/', (0, celebrate_1.celebrate)({
        body: celebrate_1.Joi.object({
            userName: celebrate_1.Joi.string().required(),
            password: celebrate_1.Joi.string().required(),
            mobile: celebrate_1.Joi.string().optional().allow(''),
            email: celebrate_1.Joi.string().optional().allow(''),
            tenantId: celebrate_1.Joi.string().optional().allow(''),
        }),
    }), async (req, res, next) => {
        const logger = typedi_1.Container.get('logger');
        try {
            const accessToken = (0, util_1.getToken)(req);
            const userService = typedi_1.Container.get(user_1.default);
            const data = await userService.createUser(req.body, accessToken);
            res.send(data);
        }
        catch (e) {
            return next(e);
        }
    });
    route.delete('/', (0, celebrate_1.celebrate)({
        body: celebrate_1.Joi.object({ id: celebrate_1.Joi.number().required() }),
    }), async (req, res, next) => {
        const logger = typedi_1.Container.get('logger');
        try {
            const accessToken = (0, util_1.getToken)(req);
            const userService = typedi_1.Container.get(user_1.default);
            const data = await userService.remove(req.body.id, accessToken);
            return res.send(data);
        }
        catch (e) {
            return next(e);
        }
    });
    route.put('/', (0, celebrate_1.celebrate)({
        body: celebrate_1.Joi.object({
            oldPassword: celebrate_1.Joi.string().required(),
            newPassword: celebrate_1.Joi.string().required(),
        }),
    }), async (req, res, next) => {
        const logger = typedi_1.Container.get('logger');
        try {
            const accessToken = (0, util_1.getToken)(req);
            const userService = typedi_1.Container.get(user_1.default);
            const data = await userService.updatePassword(req.body, accessToken);
            return res.send(data);
        }
        catch (e) {
            return next(e);
        }
    });
    route.get('/', async (req, res, next) => {
        const logger = typedi_1.Container.get('logger');
        try {
            const accessToken = (0, util_1.getToken)(req);
            const userService = typedi_1.Container.get(user_1.default);
            const authInfo = await userService.getUserInfo(accessToken);
            res.send({
                code: authInfo.code,
                data: Object.assign(Object.assign({}, authInfo.data), { username: authInfo.data.userName, avatar: authInfo.data.avatar, twoFactorActivated: authInfo.data.twoFactorActivated }),
            });
        }
        catch (e) {
            logger.error('🔥 error: %o', e);
            return next(e);
        }
    });
    route.get('/:id', (0, celebrate_1.celebrate)({
        params: celebrate_1.Joi.object({
            id: celebrate_1.Joi.string().required(),
        }),
    }), async (req, res, next) => {
        const logger = typedi_1.Container.get('logger');
        try {
            const accessToken = (0, util_1.getToken)(req);
            const userService = typedi_1.Container.get(user_1.default);
            const data = await userService.getUserInfoById(req.params.id, accessToken);
            return res.send(data);
        }
        catch (e) {
            return next(e);
        }
    });
    route.get('/:id/roles', (0, celebrate_1.celebrate)({
        params: celebrate_1.Joi.object({
            id: celebrate_1.Joi.string().required(),
        }),
    }), async (req, res, next) => {
        const logger = typedi_1.Container.get('logger');
        try {
            const accessToken = (0, util_1.getToken)(req);
            const userService = typedi_1.Container.get(user_1.default);
            const data = await userService.getRolesByUserId(req.params.id, accessToken);
            return res.send(data);
        }
        catch (e) {
            return next(e);
        }
    });
    route.get('/:id/groups', (0, celebrate_1.celebrate)({
        params: celebrate_1.Joi.object({
            id: celebrate_1.Joi.string().required(),
        }),
    }), async (req, res, next) => {
        const logger = typedi_1.Container.get('logger');
        try {
            const accessToken = (0, util_1.getToken)(req);
            const userService = typedi_1.Container.get(user_1.default);
            const data = await userService.getUserGroupsByUserId(req.params.id, accessToken);
            return res.send(data);
        }
        catch (e) {
            return next(e);
        }
    });
    route.post('/:id/roles', (0, celebrate_1.celebrate)({
        params: celebrate_1.Joi.object({
            id: celebrate_1.Joi.string().required(),
        }),
        body: celebrate_1.Joi.object({
            roleIds: celebrate_1.Joi.array().items(celebrate_1.Joi.number().required()),
            overwrite: celebrate_1.Joi.boolean().required(),
        }),
    }), async (req, res, next) => {
        const logger = typedi_1.Container.get('logger');
        try {
            const accessToken = (0, util_1.getToken)(req);
            const userService = typedi_1.Container.get(user_1.default);
            const data = await userService.addRoleToUser(req.params.id, req.body, accessToken);
            res.send(data);
        }
        catch (e) {
            return next(e);
        }
    });
    route.delete('/:userId/role/:roleId', (0, celebrate_1.celebrate)({
        params: celebrate_1.Joi.object({
            userId: celebrate_1.Joi.string().required(),
            roleId: celebrate_1.Joi.string().required(),
        }),
    }), async (req, res, next) => {
        const logger = typedi_1.Container.get('logger');
        try {
            const accessToken = (0, util_1.getToken)(req);
            const userService = typedi_1.Container.get(user_1.default);
            const data = await userService.undoRoleFromUser(req.params.userId, req.params.roleId, accessToken);
            res.send(data);
        }
        catch (e) {
            return next(e);
        }
    });
    route.get('/menu/privileged', async (req, res, next) => {
        const logger = typedi_1.Container.get('logger');
        try {
            const accessToken = (0, util_1.getToken)(req);
            const userService = typedi_1.Container.get(user_1.default);
            const data = await userService.getMenuDataByLogin(accessToken);
            res.send(data);
        }
        catch (e) {
            return next(e);
        }
    });
    route.get('/login/log', async (req, res, next) => {
        const logger = typedi_1.Container.get('logger');
        try {
            const userService = typedi_1.Container.get(user_1.default);
            const data = await userService.getLoginLog();
            res.send({ code: 200, data });
        }
        catch (e) {
            return next(e);
        }
    });
    route.get('/notification', async (req, res, next) => {
        const logger = typedi_1.Container.get('logger');
        try {
            const userService = typedi_1.Container.get(user_1.default);
            const data = await userService.getNotificationMode();
            res.send({ code: 200, data });
        }
        catch (e) {
            return next(e);
        }
    });
    route.put('/notification', async (req, res, next) => {
        const logger = typedi_1.Container.get('logger');
        try {
            const accessToken = (0, util_1.getToken)(req);
            const userService = typedi_1.Container.get(user_1.default);
            const result = await userService.updateNotificationMode(accessToken, req.body);
            res.send(result);
        }
        catch (e) {
            return next(e);
        }
    });
    route.put('/init', (0, celebrate_1.celebrate)({
        body: celebrate_1.Joi.object({
            username: celebrate_1.Joi.string().required(),
            password: celebrate_1.Joi.string().required(),
        }),
    }), async (req, res, next) => {
        const logger = typedi_1.Container.get('logger');
        try {
            const userService = typedi_1.Container.get(user_1.default);
            //await userService.updateUsernameAndPassword(req.body);
            res.send({ code: 200, message: '更新成功' });
        }
        catch (e) {
            return next(e);
        }
    });
    route.put('/notification/init', async (req, res, next) => {
        const logger = typedi_1.Container.get('logger');
        try {
            const accessToken = (0, util_1.getToken)(req);
            const userService = typedi_1.Container.get(user_1.default);
            const result = await userService.updateNotificationMode(accessToken, req.body);
            res.send(result);
        }
        catch (e) {
            return next(e);
        }
    });
    route.put('/avatar', upload.single('avatar'), async (req, res, next) => {
        const logger = typedi_1.Container.get('logger');
        try {
            const accessToken = (0, util_1.getToken)(req);
            const userService = typedi_1.Container.get(user_1.default);
            const result = await userService.updateAvatar(accessToken, req.file.filename);
            res.send(result);
        }
        catch (e) {
            return next(e);
        }
    });
};
//# sourceMappingURL=user.js.map