"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const typedi_1 = require("typedi");
const userGroup_1 = __importDefault(require("../services/userGroup"));
const celebrate_1 = require("celebrate");
const express_rate_limit_1 = __importDefault(require("express-rate-limit"));
const util_1 = require("../config/util");
const route = (0, express_1.Router)();
exports.default = (app) => {
    app.use('/userGroup', route);
    route.get('/list', (0, express_rate_limit_1.default)({
        windowMs: 15 * 60 * 1000,
        max: 100,
    }), (0, celebrate_1.celebrate)({
        query: celebrate_1.Joi.object({
            t: celebrate_1.Joi.any(),
            page_index: celebrate_1.Joi.number().required().min(1).max(99999),
            page_size: celebrate_1.Joi.number().required().min(10).max(100),
        }),
    }), async (req, res, next) => {
        const logger = typedi_1.Container.get('logger');
        try {
            const accessToken = (0, util_1.getToken)(req);
            const userService = typedi_1.Container.get(userGroup_1.default);
            const data = await userService.getList({
                page_index: Number(req.query.page_index),
                page_size: Number(req.query.page_size),
            }, accessToken);
            return res.send(data);
        }
        catch (e) {
            return next(e);
        }
    });
    route.delete('/', (0, celebrate_1.celebrate)({
        body: celebrate_1.Joi.object({ id: celebrate_1.Joi.number().required() }),
    }), async (req, res, next) => {
        const logger = typedi_1.Container.get('logger');
        try {
            const accessToken = (0, util_1.getToken)(req);
            const userGroupService = typedi_1.Container.get(userGroup_1.default);
            const data = await userGroupService.delete(req.body.id, accessToken);
            return res.send(data);
        }
        catch (e) {
            return next(e);
        }
    });
    route.delete('/:id/users/:userId', (0, celebrate_1.celebrate)({
        params: celebrate_1.Joi.object({
            id: celebrate_1.Joi.string().required(),
            userId: celebrate_1.Joi.string().required(),
        }),
    }), async (req, res, next) => {
        const logger = typedi_1.Container.get('logger');
        try {
            const accessToken = (0, util_1.getToken)(req);
            const userGroupService = typedi_1.Container.get(userGroup_1.default);
            const data = await userGroupService.deleteUserFromGroup(req.params.id, req.params.userId, accessToken);
            return res.send(data);
        }
        catch (e) {
            return next(e);
        }
    });
    route.get('/userListById', (0, express_rate_limit_1.default)({
        windowMs: 15 * 60 * 1000,
        max: 100,
    }), (0, celebrate_1.celebrate)({
        query: celebrate_1.Joi.object({
            t: celebrate_1.Joi.any(),
            group_id: celebrate_1.Joi.string().required(),
            page_index: celebrate_1.Joi.number().required().min(1).max(99999),
            page_size: celebrate_1.Joi.number().required().min(10).max(100),
        }),
    }), async (req, res, next) => {
        try {
            const accessToken = (0, util_1.getToken)(req);
            const userService = typedi_1.Container.get(userGroup_1.default);
            const data = await userService.getUserListByGroupId({
                group_id: req.query.group_id,
                page_index: Number(req.query.page_index),
                page_size: Number(req.query.page_size),
            }, accessToken);
            return res.send(data);
        }
        catch (e) {
            return next(e);
        }
    });
    route.post('/', (0, celebrate_1.celebrate)({
        body: celebrate_1.Joi.object({
            type: celebrate_1.Joi.number().required(),
            name: celebrate_1.Joi.string().required(),
            code: celebrate_1.Joi.string().required(),
            desc: celebrate_1.Joi.string().optional().allow(''),
            adminUserId: celebrate_1.Joi.number().optional().allow(null),
        }),
    }), async (req, res, next) => {
        const logger = typedi_1.Container.get('logger');
        try {
            const accessToken = (0, util_1.getToken)(req);
            const userService = typedi_1.Container.get(userGroup_1.default);
            const data = await userService.createUserGroup(req.body, accessToken);
            res.send(data);
        }
        catch (e) {
            return next(e);
        }
    });
    route.patch('/:id', (0, celebrate_1.celebrate)({
        params: celebrate_1.Joi.object({
            id: celebrate_1.Joi.string().required(),
        }),
        body: celebrate_1.Joi.object({
            name: celebrate_1.Joi.string().required(),
            code: celebrate_1.Joi.string().required(),
            desc: celebrate_1.Joi.string().optional().allow(''),
            adminUserId: celebrate_1.Joi.number().optional().allow(null),
        }),
    }), async (req, res, next) => {
        const logger = typedi_1.Container.get('logger');
        try {
            const accessToken = (0, util_1.getToken)(req);
            const userGroupService = typedi_1.Container.get(userGroup_1.default);
            const data = await userGroupService.update(req.params.id, req.body, accessToken);
            res.send(data);
        }
        catch (e) {
            return next(e);
        }
    });
    route.post('/grouping', (0, celebrate_1.celebrate)({
        body: celebrate_1.Joi.object({
            groupId: celebrate_1.Joi.number().required(),
            userIds: celebrate_1.Joi.array().items(celebrate_1.Joi.string().required()),
        }),
    }), async (req, res, next) => {
        const logger = typedi_1.Container.get('logger');
        try {
            const accessToken = (0, util_1.getToken)(req);
            const userService = typedi_1.Container.get(userGroup_1.default);
            const data = await userService.userGrouping(req.body, accessToken);
            res.send(data);
        }
        catch (e) {
            return next(e);
        }
    });
};
//# sourceMappingURL=userGroup.js.map