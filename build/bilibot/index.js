"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const grpc_js_1 = require("@grpc/grpc-js");
const cache_manager_1 = require("cache-manager");
const bili_bot_services_1 = require("../protos/bili_bot_services");
const sync_bili_account_1 = require("./sync_bili_account");
const sync_bili_task_1 = require("./sync_bili_task");
const syncQrCodeLogin_1 = require("./syncQrCodeLogin");
const sync_hitted_comment_1 = require("./sync_hitted_comment");
const sync_hitted_danmaku_1 = require("./sync_hitted_danmaku");
const syncArchiveAnalyzeStats_1 = require("./syncArchiveAnalyzeStats");
const syncPlayRateAnalysis_1 = require("./syncPlayRateAnalysis");
const syncViewerQuitTrend_1 = require("./syncViewerQuitTrend");
const config_1 = __importDefault(require("../config"));
const logger_1 = __importDefault(require("../loaders/logger"));
const biliAccount_1 = __importDefault(require("../services/biliAccount"));
const biliTask_1 = __importDefault(require("../services/biliTask"));
const user_1 = __importDefault(require("../services/user"));
const biliDanmakuTask_1 = __importDefault(require("../services/biliDanmakuTask"));
const typedi_1 = require("typedi");
const server = new grpc_js_1.Server();
server.addService(bili_bot_services_1.BiliBotService, {
    syncBiliAccount: sync_bili_account_1.syncBiliAccount,
    syncBiliTask: sync_bili_task_1.syncBiliTask,
    syncHittedComment: sync_hitted_comment_1.syncHittedComment,
    syncHittedDanmaku: sync_hitted_danmaku_1.syncHittedDanmaku,
    syncQrCodeLogin: syncQrCodeLogin_1.syncQrCodeLogin,
    syncArchiveAnalyzeStats: syncArchiveAnalyzeStats_1.syncArchiveAnalyzeStats,
    syncPlayRateAnalysis: syncPlayRateAnalysis_1.syncPlayRateAnalysis,
    syncViewerQuitTrend: syncViewerQuitTrend_1.syncViewerQuitTrend,
});
server.bindAsync(`localhost:${config_1.default.botPort}`, grpc_js_1.ServerCredentials.createInsecure(), (err, port) => {
    var _a;
    if (err) {
        throw err;
    }
    server.start();
    // Create memory cache synchronously
    const memoryCache = (0, cache_manager_1.createCache)((0, cache_manager_1.memoryStore)(), {
        max: 100,
        ttl: 10 * 1000 /*milliseconds*/,
    });
    typedi_1.Container.set('logger', logger_1.default);
    typedi_1.Container.set('CACHE_MANAGER', memoryCache);
    typedi_1.Container.set('userService', new user_1.default(logger_1.default));
    typedi_1.Container.set('biliAccountService', new biliAccount_1.default(logger_1.default));
    typedi_1.Container.set('biliTaskService', new biliTask_1.default(logger_1.default, memoryCache));
    typedi_1.Container.set('biliDanmakuTaskService', new biliDanmakuTask_1.default(logger_1.default, memoryCache));
    logger_1.default.debug(`✌️ BiliBot服务启动成功！` + port);
    (_a = process.send) === null || _a === void 0 ? void 0 : _a.call(process, 'ready go');
});
//# sourceMappingURL=index.js.map