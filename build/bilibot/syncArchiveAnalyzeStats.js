"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.syncArchiveAnalyzeStats = void 0;
const typedi_1 = require("typedi");
const uuid_1 = require("uuid");
const playCount_1 = require("../plugins/moxin/models/playCount");
const biliAccount_1 = require("../data/models/biliAccount");
const syncArchiveAnalyzeStats = async (call, callback) => {
    var _a;
    const logger = typedi_1.Container.get('logger');
    const archiveInc = call.request.archiveInc;
    if (archiveInc) {
        // UP主信息
        const biliUpAccount = await biliAccount_1.BiliAccountModel.findOne({
            where: {
                mid: archiveInc.uid,
            },
        });
        const now = new Date();
        let recordId = (0, uuid_1.v4)();
        await playCount_1.PlayCountModel.create({
            id: recordId,
            uid: archiveInc.uid,
            uname: (_a = biliUpAccount === null || biliUpAccount === void 0 ? void 0 : biliUpAccount.name) !== null && _a !== void 0 ? _a : '',
            aid: archiveInc.aid,
            bvid: archiveInc.bvid,
            incr: archiveInc.incr,
            total: archiveInc.total,
            title: archiveInc.title,
            daytime: new Date(Number(archiveInc.daytime || '0') * 1000),
            ptime: new Date(Number(archiveInc.ptime || '0') * 1000),
            interactive: archiveInc.interactive,
            create_by: 'bup',
            create_time: now,
            update_by: 'bup',
            update_time: now,
        });
        logger.debug('archiveInc: ' + archiveInc.title);
        callback(null, { reply: `Sync archiveInc [${archiveInc.title}] to db success` });
    }
};
exports.syncArchiveAnalyzeStats = syncArchiveAnalyzeStats;
//# sourceMappingURL=syncArchiveAnalyzeStats.js.map