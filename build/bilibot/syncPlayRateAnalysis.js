"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.syncPlayRateAnalysis = void 0;
const typedi_1 = require("typedi");
const uuid_1 = require("uuid");
const playRate_1 = require("../plugins/moxin/models/playRate");
const biliAccount_1 = require("../data/models/biliAccount");
const syncPlayRateAnalysis = async (call, callback) => {
    var _a;
    const logger = typedi_1.Container.get('logger');
    const archivePlayRate = call.request.archivePlayRate;
    if (archivePlayRate) {
        // UP主信息
        const biliUpAccount = await biliAccount_1.BiliAccountModel.findOne({
            where: {
                mid: archivePlayRate.uid,
            },
        });
        const now = new Date();
        let recordId = (0, uuid_1.v4)();
        await playRate_1.PlayRateModel.create({
            id: recordId,
            uid: archivePlayRate.uid,
            uname: (_a = biliUpAccount === null || biliUpAccount === void 0 ? void 0 : biliUpAccount.name) !== null && _a !== void 0 ? _a : '',
            aid: archivePlayRate.aid,
            bvid: archivePlayRate.bvid,
            view: archivePlayRate.view,
            rate: archivePlayRate.rate,
            title: archivePlayRate.title,
            ctime: new Date(Number(archivePlayRate.ctime || '0') * 1000),
            duration: archivePlayRate.duration,
            avg_duration: archivePlayRate.avgDuration,
            avg_play_time_value: archivePlayRate.avgPlayTimeValue,
            create_by: 'bup',
            create_time: now,
            update_by: 'bup',
            update_time: now,
        });
        logger.debug('archivePlayRate: ' + archivePlayRate.title);
        callback(null, { reply: `Sync archivePlayRate [${archivePlayRate.title}] to db success` });
    }
};
exports.syncPlayRateAnalysis = syncPlayRateAnalysis;
//# sourceMappingURL=syncPlayRateAnalysis.js.map