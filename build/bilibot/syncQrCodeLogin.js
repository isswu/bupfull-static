"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.syncQrCodeLogin = void 0;
const client_1 = __importDefault(require("../biliws/client"));
const syncQrCodeLogin = async (call, callback) => {
    call.on('data', async (request) => {
        // 处理客户端流式请求的每个数据块
        console.log(`Received code: ${request.code}, Message: ${request.message}`);
        client_1.default.sendMessage(JSON.stringify({ code: request.code, message: request.message }));
    });
    call.on('end', () => {
        // 客户端结束流
        // 在此处理整个流的结束
        const response = {
            reply: 'Stream processing complete 202399',
        };
        // 发送响应给客户端
        callback(null, response);
    });
    call.on('error', (error) => {
        // 处理错误
        console.error(error);
    });
    call.on('cancelled', () => {
        // 处理取消
        console.log('Client cancelled the request');
    });
};
exports.syncQrCodeLogin = syncQrCodeLogin;
//# sourceMappingURL=syncQrCodeLogin.js.map