"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.syncViewerQuitTrend = void 0;
const typedi_1 = require("typedi");
const uuid_1 = require("uuid");
const viewerQuitTrend_1 = require("../plugins/moxin/models/viewerQuitTrend");
const biliAccount_1 = require("../data/models/biliAccount");
const syncViewerQuitTrend = async (call, callback) => {
    var _a;
    const logger = typedi_1.Container.get('logger');
    const archiveViewerQuit = call.request.archiveViewerQuit;
    if (archiveViewerQuit) {
        // UP主信息
        const biliUpAccount = await biliAccount_1.BiliAccountModel.findOne({
            where: {
                mid: archiveViewerQuit.uid,
            },
        });
        const now = new Date();
        let recordId = (0, uuid_1.v4)();
        await viewerQuitTrend_1.ViewerQuitTrendModel.create({
            id: recordId,
            uid: archiveViewerQuit.uid,
            uname: (_a = biliUpAccount === null || biliUpAccount === void 0 ? void 0 : biliUpAccount.name) !== null && _a !== void 0 ? _a : '',
            aid: archiveViewerQuit.aid,
            cid: archiveViewerQuit.cid,
            bvid: archiveViewerQuit.bvid,
            viewer_quit: JSON.parse(archiveViewerQuit.quitTrends),
            create_by: 'bup',
            create_time: now,
            update_by: 'bup',
            update_time: now,
        });
        logger.debug('archiveViewerQuit: ' + archiveViewerQuit.cid);
        callback(null, { reply: `Sync archiveViewerQuit [${archiveViewerQuit.cid}] to db success` });
    }
};
exports.syncViewerQuitTrend = syncViewerQuitTrend;
//# sourceMappingURL=syncViewerQuitTrend.js.map