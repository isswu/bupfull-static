"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.syncBiliAccount = void 0;
const typedi_1 = require("typedi");
const uuid_1 = require("uuid");
const biliAccount_1 = require("../data/models/biliAccount");
const userBiliAccountBindRecord_1 = require("../data/models/binding/userBiliAccountBindRecord");
const syncBiliAccount = async (call, callback) => {
    var _a, _b, _c, _d, _e, _f;
    const logger = typedi_1.Container.get('logger');
    const userService = typedi_1.Container.get('userService');
    const biliAccountService = typedi_1.Container.get('biliAccountService');
    const callAccount = call.request.biliAccount;
    if (callAccount != null) {
        const vipType = callAccount.vipType;
        const vipStatus = callAccount.vipStatus;
        logger.debug('昵称: %s', callAccount.uname);
        const userData = await userService.getUserInfo(call.request.token);
        if (userData.code === 0) {
            const sysUserId = userData.data.id;
            //转换为BiliAccount
            let biliAccount;
            const now = new Date();
            const doc = await biliAccountService.getDb({ mid: callAccount.mid });
            if (doc != null) {
                if (callAccount.isLogin) {
                    biliAccount = new biliAccount_1.BiliAccount({
                        mid: callAccount.mid,
                        name: callAccount.uname,
                        money: callAccount.money,
                        avatar: callAccount.face,
                        login_status: callAccount.isLogin,
                        login_time: now,
                        cookie: callAccount.cookie,
                        update_by: sysUserId,
                        update_time: now,
                        vip_status: vipStatus,
                        vip_type: vipType,
                        current_level: (_a = callAccount.levelInfo) === null || _a === void 0 ? void 0 : _a.currentLevel,
                        current_exp: (_b = callAccount.levelInfo) === null || _b === void 0 ? void 0 : _b.currentExp,
                        next_exp: (_c = callAccount.levelInfo) === null || _c === void 0 ? void 0 : _c.nextExp,
                        del_flag: false,
                    });
                }
                else {
                    biliAccount = new biliAccount_1.BiliAccount({
                        mid: doc.mid,
                        name: doc.name,
                        money: doc.money,
                        avatar: doc.avatar,
                        login_status: callAccount.isLogin,
                        login_time: doc.login_time,
                        update_by: sysUserId,
                        update_time: now,
                        cookie: callAccount.cookie,
                        del_flag: false,
                    });
                }
                await biliAccountService.update(biliAccount);
                logger.info('B站账号更新成功, [mid: %s, token: %s]', callAccount.mid, call.request.token);
            }
            else {
                biliAccount = new biliAccount_1.BiliAccount({
                    mid: callAccount.mid,
                    name: callAccount.uname,
                    money: callAccount.money,
                    avatar: callAccount.face,
                    login_status: callAccount.isLogin,
                    login_time: now,
                    cookie: callAccount.cookie,
                    create_by: sysUserId,
                    create_time: now,
                    update_by: sysUserId,
                    update_time: now,
                    vip_status: vipStatus,
                    vip_type: vipType,
                    current_level: (_d = callAccount.levelInfo) === null || _d === void 0 ? void 0 : _d.currentLevel,
                    current_exp: (_e = callAccount.levelInfo) === null || _e === void 0 ? void 0 : _e.currentExp,
                    next_exp: (_f = callAccount.levelInfo) === null || _f === void 0 ? void 0 : _f.nextExp,
                    del_flag: false,
                });
                await biliAccountService.create(biliAccount);
            }
            const record = await userBiliAccountBindRecord_1.UserBiliAccountBindRecordModel.findOne({
                where: { user_id: sysUserId, mid: callAccount.mid },
            });
            if (!record) {
                await userBiliAccountBindRecord_1.UserBiliAccountBindRecordModel.create({
                    id: (0, uuid_1.v4)(),
                    user_id: sysUserId,
                    mid: callAccount.mid,
                    readable: true,
                    writable: true,
                    create_by: sysUserId,
                    create_time: now,
                    update_by: sysUserId,
                    update_time: now,
                });
                logger.info('B站账号访问授权成功, [userId: %s, mid: %s, token: %s]', sysUserId, callAccount.mid, call.request.token);
            }
            else {
                logger.warn('B站账号访问记录已存在, [userId: %s, mid: %s, token: %s]', sysUserId, callAccount.mid, call.request.token);
            }
        }
        else {
            logger.error('获取用户信息失败, [code: %s, message: %s]', userData.code, userData.msg);
        }
        callback(null, {
            reply: `Reply from BiliBotServer, the account status is ${callAccount.isLogin ? 'online' : 'offline'}`,
        });
    }
};
exports.syncBiliAccount = syncBiliAccount;
//# sourceMappingURL=sync_bili_account.js.map