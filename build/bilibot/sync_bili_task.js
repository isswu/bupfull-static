"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.syncBiliTask = void 0;
const bili_bot_services_1 = require("../protos/bili_bot_services");
const typedi_1 = require("typedi");
const syncBiliTask = async (call) => {
    const logger = typedi_1.Container.get('logger');
    const biliTaskService = typedi_1.Container.get('biliTaskService');
    const biliDanmakuTaskService = typedi_1.Container.get('biliDanmakuTaskService');
    call.on('data', async (request) => {
        // 处理客户端发送的请求消息
        try {
            switch (request.runTaskStatus) {
                // 获取响应并返回数据
                case bili_bot_services_1.RunTaskStatus.Starting:
                    logger.info('[Received request], [RunTaskStatus.Starting], [RecordId]: %s, [RunTasks]: %s', request.recordId, request.runTasks);
                    let biliTaskResult;
                    if (biliTaskService.COMMENT_RUN_TASKS == request.runTasks ||
                        biliTaskService.CONTROL_COMMENT_RUN_TASKS == request.runTasks) {
                        // 评论任务
                        biliTaskResult = await biliTaskService.getBiliCommentTaskResp({
                            record_id: request.recordId,
                            run_tasks: request.runTasks,
                        });
                    }
                    else if (biliDanmakuTaskService.DANMAKU_RUN_TASKS == request.runTasks ||
                        biliDanmakuTaskService.CONTROL_DANMAKU_RUN_TASKS == request.runTasks) {
                        // 弹幕任务
                        biliTaskResult =
                            await biliDanmakuTaskService.getBiliDanmakuTaskResp({
                                record_id: request.recordId,
                                run_tasks: request.runTasks,
                            });
                    }
                    else {
                        biliTaskResult = await biliTaskService.getBiliSimpleTaskResp({
                            record_id: request.recordId,
                        });
                    }
                    if (biliTaskResult.code === 200) {
                        const biliTaskResponse = {
                            command: '1',
                            reply: 'success',
                            biliTask: biliTaskResult.data,
                        };
                        logger.info('Write [%s] response, uid: [%s], bvid: [%s]', biliTaskResult.data.runTasks, biliTaskResult.data.uid, biliTaskResult.data.bvid);
                        call.write(biliTaskResponse);
                    }
                    else {
                        call.write({
                            command: '-1',
                            reply: '[bili-task]业务数据异常',
                            biliTask: undefined,
                        });
                    }
                    break;
                // 统计任务结果
                case bili_bot_services_1.RunTaskStatus.PayLoad:
                    logger.info('[Received request], [RunTaskStatus.PayLoad], [RecordId]: %s, [RunTasks]: %s', request.recordId, request.runTasks);
                    if (request.payLoad && request.payLoad.subtaskResult) {
                        if (biliTaskService.COMMENT_RUN_TASKS == request.runTasks ||
                            biliTaskService.CONTROL_COMMENT_RUN_TASKS == request.runTasks) {
                            // 评论任务
                            await biliTaskService.countCommentTaskResult(Object.assign({}, request.payLoad.subtaskResult));
                        }
                        else if (biliDanmakuTaskService.DANMAKU_RUN_TASKS == request.runTasks ||
                            biliDanmakuTaskService.CONTROL_DANMAKU_RUN_TASKS ==
                                request.runTasks) {
                            // 弹幕任务
                            await biliDanmakuTaskService.countDanmakuTaskResult(Object.assign({}, request.payLoad.subtaskResult));
                        }
                        else {
                            await biliTaskService.countSimpleTaskResult(Object.assign({}, request.payLoad.subtaskResult));
                        }
                    }
                    break;
                // 任务执行结束, 更新任务状态
                case bili_bot_services_1.RunTaskStatus.Completed:
                    logger.info('[Received request], [RunTaskStatus.Completed], [RecordId]: %s, [RunTasks]: %s', request.recordId, request.runTasks);
                    if (biliTaskService.COMMENT_RUN_TASKS == request.runTasks ||
                        biliTaskService.CONTROL_COMMENT_RUN_TASKS == request.runTasks) {
                        // 评论任务
                        await biliTaskService.endCommentTask({
                            recordId: request.recordId,
                        });
                    }
                    else if (biliDanmakuTaskService.DANMAKU_RUN_TASKS == request.runTasks ||
                        biliDanmakuTaskService.CONTROL_DANMAKU_RUN_TASKS == request.runTasks) {
                        // 弹幕任务
                        await biliDanmakuTaskService.endDanmakuTask({
                            recordId: request.recordId,
                        });
                    }
                    else {
                        await biliTaskService.endSimpleTask({
                            recordId: request.recordId,
                        });
                    }
                    break;
            }
        }
        catch (error) {
            logger.error('Error processing request: %s', error.message);
            call.write({
                command: '-1',
                reply: `Error processing request: ${error.message}`,
                biliTask: undefined,
            });
        }
    });
    // 数据流结束
    call.on('end', () => {
        call.end(); // 结束数据流
        logger.info('Server BiliTask 处理完成');
    });
    // 客户端取消（如超时或中止）
    call.on('cancelled', () => {
        logger.warn('客户端取消请求 (DeadlineExceeded or manually cancelled)');
        call.end(); // 确保流正确结束
    });
};
exports.syncBiliTask = syncBiliTask;
//# sourceMappingURL=sync_bili_task.js.map