"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.syncHittedComment = void 0;
const typedi_1 = require("typedi");
const uuid_1 = require("uuid");
const commentHitItemRecord_1 = require("../data/models/commentHitItemRecord");
const syncHittedComment = async (call, callback) => {
    const logger = typedi_1.Container.get('logger');
    const userService = typedi_1.Container.get('userService');
    const biliTaskService = typedi_1.Container.get('biliTaskService');
    const action = call.request.action;
    const hittedComment = call.request.hittedComment;
    if (hittedComment != null) {
        const userData = await userService.getUserInfo(call.request.token);
        if (userData.code === 0) {
            const sysUserId = userData.data.id;
            const now = new Date();
            let recordId = (0, uuid_1.v4)();
            if (action === '1') {
                const aiData = await biliTaskService.reviewComment(hittedComment.content, sysUserId);
                logger.info('Ai review response: %o', aiData);
                if (aiData.code === 0 && aiData.data.flagged) {
                    const hitComment = await commentHitItemRecord_1.CommentHitItemRecordModel.findOne({
                        where: {
                            rpid: hittedComment.rpid,
                        },
                    });
                    if (!hitComment) {
                        await commentHitItemRecord_1.CommentHitItemRecordModel.create({
                            id: recordId,
                            uid: hittedComment.uid,
                            bvid: hittedComment.bvid,
                            rpid: hittedComment.rpid,
                            mid: hittedComment.mid,
                            oid: hittedComment.oid,
                            record_id: hittedComment.recordId,
                            content: hittedComment.content,
                            uname: hittedComment.uname,
                            ctime: Number(hittedComment.ctime || '0'),
                            status: commentHitItemRecord_1.ReviewStatus.Pending,
                            summary: aiData.data.summary,
                            create_by: sysUserId,
                            create_time: now,
                            update_by: sysUserId,
                            update_time: now,
                            del_flag: false,
                        });
                    }
                }
            }
            else {
                const hitComment = await commentHitItemRecord_1.CommentHitItemRecordModel.findOne({
                    where: {
                        rpid: hittedComment.rpid,
                    },
                });
                if (!hitComment) {
                    await commentHitItemRecord_1.CommentHitItemRecordModel.create({
                        id: recordId,
                        uid: hittedComment.uid,
                        bvid: hittedComment.bvid,
                        rpid: hittedComment.rpid,
                        mid: hittedComment.mid,
                        oid: hittedComment.oid,
                        record_id: hittedComment.recordId,
                        content: hittedComment.content,
                        uname: hittedComment.uname,
                        ctime: Number(hittedComment.ctime || '0'),
                        create_by: sysUserId,
                        create_time: now,
                        update_by: sysUserId,
                        update_time: now,
                        del_flag: false,
                    });
                }
            }
            logger.info('hittedComment: ' + hittedComment.content);
            callback(null, { reply: 'Sync hitted comment to db success.' });
        }
        else {
            logger.error('获取用户信息失败, [code: %s, message: %s]', userData.code, userData.msg);
        }
    }
};
exports.syncHittedComment = syncHittedComment;
//# sourceMappingURL=sync_hitted_comment.js.map