"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.syncHittedDanmaku = void 0;
const typedi_1 = require("typedi");
const uuid_1 = require("uuid");
const danmakuHitItemRecord_1 = require("../data/models/danmakuHitItemRecord");
const syncHittedDanmaku = async (call, callback) => {
    const logger = typedi_1.Container.get('logger');
    const hittedDanmaku = call.request.hittedDanmaku;
    if (hittedDanmaku != null) {
        const now = new Date();
        let recordId = (0, uuid_1.v4)();
        await danmakuHitItemRecord_1.DanmakuHitItemRecordModel.create({
            id: recordId,
            uid: hittedDanmaku.uid,
            bvid: hittedDanmaku.bvid,
            dmid: hittedDanmaku.dmid,
            mid: hittedDanmaku.mid,
            oid: hittedDanmaku.oid,
            record_id: hittedDanmaku.recordId,
            content: hittedDanmaku.content,
            uname: hittedDanmaku.uname,
            ctime: Number(hittedDanmaku.ctime || '0'),
            create_by: 'system',
            create_time: now,
            update_by: 'system',
            update_time: now,
        });
        logger.debug('hittedDanmaku: ' + hittedDanmaku.content);
        callback(null, { reply: "Sync hitted danmaku to db success." });
    }
};
exports.syncHittedDanmaku = syncHittedDanmaku;
//# sourceMappingURL=sync_hitted_danmaku.js.map