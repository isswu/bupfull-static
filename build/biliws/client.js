"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const ws_1 = __importDefault(require("ws"));
const config_1 = __importDefault(require("../config"));
class Client {
    constructor(port) {
        this.socket = new ws_1.default(`ws://localhost:${port}`);
        this.setupWebSocket();
    }
    setupWebSocket() {
        // 监听连接打开事件
        this.socket.on('open', () => {
            console.log('Connected to server');
            // 发送消息给服务器
            this.socket.send('Hello from the client!');
        });
        // 监听服务器发送的消息
        this.socket.on('message', (message) => {
            console.log(`Received message: ${message}`);
        });
        // 监听连接关闭事件
        this.socket.on('close', () => {
            console.log('Connection closed');
        });
    }
    sendMessage(message) {
        if (this.socket.readyState === ws_1.default.OPEN) {
            this.socket.send(message);
        }
        else {
            console.error('Socket not open. Cannot send message.');
        }
    }
}
exports.default = new Client(config_1.default.wsPort);
//# sourceMappingURL=client.js.map