"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const ws_1 = __importDefault(require("ws"));
const config_1 = __importDefault(require("../config"));
const server = new ws_1.default.Server({ port: config_1.default.wsPort });
server.on('listening', () => {
    console.log(`WebSocket server is running on port ${config_1.default.wsPort}`);
});
server.on('connection', (socket) => {
    console.log('Client connected');
    // 监听客户端发送的消息
    socket.on('message', (message) => {
        console.log(`Received message: ${message}`);
        // 向客户端发送消息
        socket.send('Hello from the server!');
    });
    // 监听连接关闭
    socket.on('close', () => {
        console.log('Client disconnected');
    });
});
//# sourceMappingURL=index.js.map