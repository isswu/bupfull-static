'use strict';
/** @type {import('sequelize-cli').Migration} */
module.exports = {
    async up(queryInterface, Sequelize) {
        const transaction = await queryInterface.sequelize.transaction();
        try {
            await queryInterface.removeColumn('comment_task', 'comment_times', { transaction });
            await queryInterface.removeColumn('comment_task', 'comment_content', { transaction });
            await transaction.commit();
        }
        catch (err) {
            await transaction.rollback();
            throw err;
        }
    },
    async down(queryInterface, Sequelize) {
        const transaction = await queryInterface.sequelize.transaction();
        try {
            await queryInterface.addColumn('comment_task', 'comment_times', {
                type: Sequelize.DataTypes.INTEGER,
            }, { transaction });
            await queryInterface.addColumn('comment_task', 'comment_content', {
                type: Sequelize.DataTypes.BLOB,
            }, { transaction });
            await transaction.commit();
        }
        catch (err) {
            await transaction.rollback();
            throw err;
        }
    }
};
//# sourceMappingURL=20231006124123-remove_comment_times_and_comment_content_columns_from_comment_task_table.js.map