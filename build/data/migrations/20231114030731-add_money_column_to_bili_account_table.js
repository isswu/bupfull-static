'use strict';
/** @type {import('sequelize-cli').Migration} */
module.exports = {
    async up(queryInterface, Sequelize) {
        await queryInterface.addColumn('bili_account', 'money', {
            type: Sequelize.DataTypes.STRING(64),
            allowNull: false, // 可选，是否允许为空
            defaultValue: '', // 可选，字段默认值
        });
    },
    async down(queryInterface, Sequelize) {
        await queryInterface.removeColumn('bili_account', 'money');
    }
};
//# sourceMappingURL=20231114030731-add_money_column_to_bili_account_table.js.map