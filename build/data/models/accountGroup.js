"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.AccountGroupModel = exports.DistributeTypeEnum = exports.AccountGroup = void 0;
const _1 = require(".");
const sequelize_1 = require("sequelize");
/**
 * 账号组
 */
class AccountGroup {
    constructor(options) {
        this.template_code = options.template_code;
        this.template_name = options.template_name;
        this.distribute_type = options.distribute_type;
        this.publish_flag = options.publish_flag;
        this.publish_time = options.publish_time;
        this.publish_by = options.publish_by;
        this.publish_remark = options.publish_remark;
        this.enable_flag = options.enable_flag;
        this.del_flag = options.del_flag;
        this.create_by = options.create_by;
        this.create_time = options.create_time;
        this.update_by = options.update_by;
        this.update_time = options.update_time;
    }
}
exports.AccountGroup = AccountGroup;
var DistributeTypeEnum;
(function (DistributeTypeEnum) {
    DistributeTypeEnum["ROUND_ROBIN"] = "round_robin";
})(DistributeTypeEnum || (exports.DistributeTypeEnum = DistributeTypeEnum = {}));
exports.AccountGroupModel = _1.sequelize.define('account_group', {
    template_code: {
        primaryKey: true,
        type: sequelize_1.DataTypes.STRING(32),
    },
    template_name: sequelize_1.DataTypes.STRING(64),
    distribute_type: sequelize_1.DataTypes.STRING(32),
    publish_flag: sequelize_1.DataTypes.BOOLEAN,
    publish_time: {
        type: sequelize_1.DataTypes.DATE,
        allowNull: true,
    },
    publish_by: sequelize_1.DataTypes.STRING(64),
    publish_remark: sequelize_1.DataTypes.STRING(255),
    enable_flag: sequelize_1.DataTypes.BOOLEAN,
    del_flag: sequelize_1.DataTypes.BOOLEAN,
    create_by: sequelize_1.DataTypes.STRING(64),
    create_time: sequelize_1.DataTypes.DATE,
    update_by: sequelize_1.DataTypes.STRING(64),
    update_time: sequelize_1.DataTypes.DATE,
});
//# sourceMappingURL=accountGroup.js.map