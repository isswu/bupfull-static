"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.BiliAccountModel = exports.VipType = exports.VipStatus = exports.AccountStatus = exports.BiliAccount = void 0;
const _1 = require(".");
const sequelize_1 = require("sequelize");
/**
 * B站小号
 */
class BiliAccount {
    constructor(options) {
        this.mid = options.mid;
        this.name = options.name;
        this.money = options.money;
        this.avatar = options.avatar;
        this.proxy_ip = options.proxy_ip;
        this.login_status = options.login_status;
        this.login_time = options.login_time;
        this.cookie = options.cookie;
        this.user_agent = options.user_agent;
        this.vip_status =
            typeof options.vip_status === 'number' && VipStatus[options.vip_status]
                ? options.vip_status
                : VipStatus.Disable;
        this.vip_type =
            typeof options.vip_type === 'number' && VipType[options.vip_type]
                ? options.vip_type
                : VipType.None;
        this.status =
            typeof options.status === 'number' && AccountStatus[options.status]
                ? options.status
                : AccountStatus.normal;
        this.create_by = options.create_by;
        this.create_time = options.create_time;
        this.update_by = options.update_by;
        this.update_time = options.update_time;
        this.current_level = options.current_level;
        this.current_exp = options.current_exp;
        this.next_exp = options.next_exp;
        this.remarks = options.remarks;
        this.del_flag = options.del_flag;
    }
}
exports.BiliAccount = BiliAccount;
var AccountStatus;
(function (AccountStatus) {
    AccountStatus[AccountStatus["normal"] = 0] = "normal";
    AccountStatus[AccountStatus["disabled"] = 1] = "disabled";
})(AccountStatus || (exports.AccountStatus = AccountStatus = {}));
var VipStatus;
(function (VipStatus) {
    /** Disable - 无/过期 */
    VipStatus[VipStatus["Disable"] = 0] = "Disable";
    /** Enable - 正常 */
    VipStatus[VipStatus["Enable"] = 1] = "Enable";
    VipStatus[VipStatus["UNRECOGNIZED"] = -1] = "UNRECOGNIZED";
})(VipStatus || (exports.VipStatus = VipStatus = {}));
var VipType;
(function (VipType) {
    /** None - 无 */
    VipType[VipType["None"] = 0] = "None";
    /** Mensual - 月度大会员 */
    VipType[VipType["Mensual"] = 1] = "Mensual";
    /** Annual - 年度大会员 */
    VipType[VipType["Annual"] = 2] = "Annual";
    VipType[VipType["UNRECOGNIZED"] = -1] = "UNRECOGNIZED";
})(VipType || (exports.VipType = VipType = {}));
exports.BiliAccountModel = _1.sequelize.define('bili_account', {
    mid: {
        primaryKey: true,
        type: sequelize_1.DataTypes.STRING(64),
    },
    name: sequelize_1.DataTypes.STRING(64),
    money: sequelize_1.DataTypes.STRING(64),
    avatar: sequelize_1.DataTypes.STRING(255),
    proxy_ip: sequelize_1.DataTypes.STRING(255),
    login_status: sequelize_1.DataTypes.BOOLEAN,
    login_time: sequelize_1.DataTypes.DATE,
    cookie: sequelize_1.DataTypes.TEXT,
    user_agent: sequelize_1.DataTypes.TEXT,
    vip_status: sequelize_1.DataTypes.INTEGER,
    vip_type: sequelize_1.DataTypes.INTEGER,
    status: sequelize_1.DataTypes.INTEGER,
    create_by: sequelize_1.DataTypes.STRING(64),
    create_time: sequelize_1.DataTypes.DATE,
    update_by: sequelize_1.DataTypes.STRING(64),
    update_time: sequelize_1.DataTypes.DATE,
    current_level: sequelize_1.DataTypes.INTEGER,
    current_exp: sequelize_1.DataTypes.INTEGER,
    next_exp: sequelize_1.DataTypes.INTEGER,
    remarks: sequelize_1.DataTypes.STRING(255),
    del_flag: sequelize_1.DataTypes.BOOLEAN,
});
//# sourceMappingURL=biliAccount.js.map