"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.BiliAccountBindProxyIpRecordModel = exports.BiliAccountBindProxyIpRecord = void 0;
const _1 = require(".");
const sequelize_1 = require("sequelize");
/**
 * B站小号绑定的代理IP记录
 */
class BiliAccountBindProxyIpRecord {
    constructor(options) {
        this.id = options.id;
        this.mid = options.mid;
        this.proxy_ip = options.proxy_ip;
        this.create_by = options.create_by;
        this.create_time = options.create_time;
    }
}
exports.BiliAccountBindProxyIpRecord = BiliAccountBindProxyIpRecord;
exports.BiliAccountBindProxyIpRecordModel = _1.sequelize.define('bili_account_bind_proxy_ip_record', {
    id: {
        primaryKey: true,
        type: sequelize_1.DataTypes.STRING(64),
    },
    mid: sequelize_1.DataTypes.STRING(64),
    proxy_ip: sequelize_1.DataTypes.STRING(64),
    create_by: sequelize_1.DataTypes.STRING(64),
    create_time: sequelize_1.DataTypes.DATE,
});
//# sourceMappingURL=biliAccountBindProxyIpRecord.js.map