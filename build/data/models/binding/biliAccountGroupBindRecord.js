"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.BiliAccountGroupBindRecordModel = exports.BiliAccountGroupBindRecord = void 0;
const __1 = require("../");
const sequelize_1 = require("sequelize");
const biliAccount_1 = require("../biliAccount");
/**
 * B站账号和所属账号组的绑定记录
 */
class BiliAccountGroupBindRecord {
    constructor(options) {
        this.id = options.id;
        this.mid = options.mid;
        this.group_code = options.group_code;
        this.del_flag = options.del_flag;
        this.create_by = options.create_by;
        this.create_time = options.create_time;
        this.update_by = options.update_by;
        this.update_time = options.update_time;
    }
}
exports.BiliAccountGroupBindRecord = BiliAccountGroupBindRecord;
exports.BiliAccountGroupBindRecordModel = __1.sequelize.define('bili_account_group_bind_record', {
    id: {
        primaryKey: true,
        type: sequelize_1.DataTypes.STRING(64),
    },
    mid: {
        type: sequelize_1.DataTypes.STRING(64),
        unique: 'compositeIndex', // 将 mid 设置为唯一性约束的一部分
    },
    group_code: {
        type: sequelize_1.DataTypes.STRING(64),
        unique: 'compositeIndex', // 将 group_code 设置为唯一性约束的一部分
    },
    del_flag: sequelize_1.DataTypes.BOOLEAN,
    create_by: sequelize_1.DataTypes.STRING(64),
    create_time: sequelize_1.DataTypes.DATE,
    update_by: sequelize_1.DataTypes.STRING(64),
    update_time: sequelize_1.DataTypes.DATE,
}, {
    indexes: [
        {
            unique: true,
            fields: ['mid', 'group_code'], // 定义一个组合唯一索引，确保 mid 和 group_code 的组合是唯一的
            name: 'compositeIndex',
        },
    ],
});
// BiliAccountGroupBindRecordModel 中添加关联关系
exports.BiliAccountGroupBindRecordModel.belongsTo(biliAccount_1.BiliAccountModel, {
    foreignKey: 'mid',
    targetKey: 'mid',
    as: 'biliAccount', // 可以用作 include 中的别名
});
//# sourceMappingURL=biliAccountGroupBindRecord.js.map