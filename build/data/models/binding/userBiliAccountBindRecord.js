"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserBiliAccountBindRecordModel = exports.UserBiliAccountBindRecord = void 0;
const __1 = require("../");
const sequelize_1 = require("sequelize");
/**
 * 系统用户和B站帐号数据的绑定记录
 */
class UserBiliAccountBindRecord {
    constructor(options) {
        this.id = options.id;
        this.user_id = options.user_id;
        this.mid = options.mid;
        this.readable = options.readable;
        this.writable = options.writable;
        this.create_by = options.create_by;
        this.create_time = options.create_time;
        this.update_by = options.update_by;
        this.update_time = options.update_time;
    }
}
exports.UserBiliAccountBindRecord = UserBiliAccountBindRecord;
exports.UserBiliAccountBindRecordModel = __1.sequelize.define('user_bili_account_bind_record', {
    id: {
        primaryKey: true,
        type: sequelize_1.DataTypes.STRING(64),
    },
    user_id: {
        type: sequelize_1.DataTypes.STRING(64),
        unique: 'compositeIndex', // 将 user_id 设置为唯一性约束的一部分
    },
    mid: {
        type: sequelize_1.DataTypes.STRING(64),
        unique: 'compositeIndex', // 将 mid 设置为唯一性约束的一部分
    },
    readable: sequelize_1.DataTypes.BOOLEAN,
    writable: sequelize_1.DataTypes.BOOLEAN,
    create_by: sequelize_1.DataTypes.STRING(64),
    create_time: sequelize_1.DataTypes.DATE,
    update_by: sequelize_1.DataTypes.STRING(64),
    update_time: sequelize_1.DataTypes.DATE,
}, {
    indexes: [
        {
            unique: true,
            fields: ['user_id', 'mid'], // 定义一个组合唯一索引，确保 user_id 和 mid 的组合是唯一的
            name: 'compositeIndex',
        },
    ],
});
//# sourceMappingURL=userBiliAccountBindRecord.js.map