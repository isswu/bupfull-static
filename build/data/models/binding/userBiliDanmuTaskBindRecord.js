"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserBiliDanmuTaskBindRecordModel = exports.UserBiliDanmuTaskBindRecord = void 0;
const __1 = require("../");
const sequelize_1 = require("sequelize");
/**
 * 系统用户弹幕任务数据的绑定记录
 */
class UserBiliDanmuTaskBindRecord {
    constructor(options) {
        this.id = options.id;
        this.user_id = options.user_id;
        this.danmu_task_id = options.danmu_task_id;
        this.readable = options.readable;
        this.writable = options.writable;
        this.create_by = options.create_by;
        this.create_time = options.create_time;
        this.update_by = options.update_by;
        this.update_time = options.update_time;
    }
}
exports.UserBiliDanmuTaskBindRecord = UserBiliDanmuTaskBindRecord;
exports.UserBiliDanmuTaskBindRecordModel = __1.sequelize.define('user_bili_danmu_task_bind_record', {
    id: {
        primaryKey: true,
        type: sequelize_1.DataTypes.STRING(64),
    },
    user_id: {
        type: sequelize_1.DataTypes.STRING(64),
        unique: 'compositeIndex',
    },
    danmu_task_id: {
        type: sequelize_1.DataTypes.STRING(64),
        unique: 'compositeIndex',
    },
    readable: sequelize_1.DataTypes.BOOLEAN,
    writable: sequelize_1.DataTypes.BOOLEAN,
    create_by: sequelize_1.DataTypes.STRING(64),
    create_time: sequelize_1.DataTypes.DATE,
    update_by: sequelize_1.DataTypes.STRING(64),
    update_time: sequelize_1.DataTypes.DATE,
}, {
    indexes: [
        {
            unique: true,
            fields: ['user_id', 'danmu_task_id'],
            name: 'compositeIndex',
        },
    ],
});
//# sourceMappingURL=userBiliDanmuTaskBindRecord.js.map