"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CityDistributeTemplateItemModel = exports.CityDistributeTemplateItem = void 0;
const _1 = require(".");
const sequelize_1 = require("sequelize");
/**
 * 城市分配模板明细
 */
class CityDistributeTemplateItem {
    constructor(options) {
        this.template_code = options.template_code;
        this.city_code = options.city_code;
        this.city_name = options.city_name;
        this.enable_flag = options.enable_flag;
        this.create_by = options.create_by;
        this.create_time = options.create_time;
        this.update_by = options.update_by;
        this.update_time = options.update_time;
    }
}
exports.CityDistributeTemplateItem = CityDistributeTemplateItem;
exports.CityDistributeTemplateItemModel = _1.sequelize.define('city_distribute_template_item', {
    template_code: {
        primaryKey: true,
        type: sequelize_1.DataTypes.STRING(32),
    },
    city_code: {
        primaryKey: true,
        type: sequelize_1.DataTypes.STRING(64)
    },
    city_name: sequelize_1.DataTypes.STRING(64),
    enable_flag: sequelize_1.DataTypes.BOOLEAN,
    create_by: sequelize_1.DataTypes.STRING(64),
    create_time: sequelize_1.DataTypes.DATE,
    update_by: sequelize_1.DataTypes.STRING(64),
    update_time: sequelize_1.DataTypes.DATE,
});
//# sourceMappingURL=cityDistributeTemplateItem.js.map