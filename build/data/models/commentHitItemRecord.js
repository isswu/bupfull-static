"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CommentHitItemRecordModel = exports.ReviewStatus = exports.CommentHitItemRecord = void 0;
const _1 = require(".");
const sequelize_1 = require("sequelize");
/**
 * 命中的评论记录
 */
class CommentHitItemRecord {
    constructor(options) {
        this.id = options.id;
        this.uid = options.uid;
        this.bvid = options.bvid;
        this.rpid = options.rpid;
        this.mid = options.mid;
        this.oid = options.oid;
        this.record_id = options.record_id;
        this.content = options.content;
        this.uname = options.uname;
        this.ctime = options.ctime;
        this.status =
            typeof options.status === 'number' && ReviewStatus[options.status]
                ? options.status
                : ReviewStatus.None;
        this.summary = options.summary;
        this.create_by = options.create_by;
        this.create_time = options.create_time;
        this.update_by = options.update_by;
        this.update_time = options.update_time;
        this.del_flag = options.del_flag;
    }
}
exports.CommentHitItemRecord = CommentHitItemRecord;
var ReviewStatus;
(function (ReviewStatus) {
    /** None - 无 */
    ReviewStatus[ReviewStatus["None"] = 0] = "None";
    /** Pending - 待审核 */
    ReviewStatus[ReviewStatus["Pending"] = 1] = "Pending";
    /** Done - 已完成 */
    ReviewStatus[ReviewStatus["Done"] = 2] = "Done";
})(ReviewStatus || (exports.ReviewStatus = ReviewStatus = {}));
exports.CommentHitItemRecordModel = _1.sequelize.define('comment_hit_item_record', {
    id: {
        primaryKey: true,
        type: sequelize_1.DataTypes.STRING(64),
    },
    uid: sequelize_1.DataTypes.STRING(64),
    bvid: sequelize_1.DataTypes.STRING(64),
    rpid: sequelize_1.DataTypes.STRING(64),
    mid: sequelize_1.DataTypes.STRING(64),
    oid: sequelize_1.DataTypes.STRING(64),
    record_id: sequelize_1.DataTypes.STRING(64),
    content: sequelize_1.DataTypes.TEXT,
    uname: sequelize_1.DataTypes.STRING(128),
    ctime: sequelize_1.DataTypes.BIGINT,
    status: sequelize_1.DataTypes.INTEGER,
    summary: sequelize_1.DataTypes.TEXT,
    create_by: sequelize_1.DataTypes.STRING(64),
    create_time: sequelize_1.DataTypes.DATE,
    update_by: sequelize_1.DataTypes.STRING(64),
    update_time: sequelize_1.DataTypes.DATE,
    del_flag: sequelize_1.DataTypes.BOOLEAN,
}, {
    indexes: [
        {
            unique: false,
            fields: ['bvid'],
        },
    ],
});
//# sourceMappingURL=commentHitItemRecord.js.map