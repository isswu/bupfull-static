"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CommentPoolModel = exports.CommentPool = void 0;
const _1 = require(".");
const sequelize_1 = require("sequelize");
const tag_1 = require("./tag");
/**
 * 评论池
 */
class CommentPool {
    constructor(options) {
        this.id = options.id;
        this.bvid = options.bvid;
        this.comment = options.comment;
        this.tag_id = options.tag_id;
        this.create_by = options.create_by;
        this.create_time = options.create_time;
        this.update_by = options.update_by;
        this.update_time = options.update_time;
    }
}
exports.CommentPool = CommentPool;
exports.CommentPoolModel = _1.sequelize.define('comment_pool', {
    id: {
        primaryKey: true,
        type: sequelize_1.DataTypes.STRING(64),
    },
    bvid: sequelize_1.DataTypes.STRING(64),
    comment: sequelize_1.DataTypes.STRING(1024),
    tag_id: sequelize_1.DataTypes.STRING(64),
    create_by: sequelize_1.DataTypes.STRING(64),
    create_time: sequelize_1.DataTypes.DATE,
    update_by: sequelize_1.DataTypes.STRING(64),
    update_time: sequelize_1.DataTypes.DATE,
});
exports.CommentPoolModel.belongsTo(tag_1.TagModel, {
    foreignKey: 'tag_id',
    as: 'tag', // 确保使用与 include 中一致的别名
});
//# sourceMappingURL=commentPool.js.map