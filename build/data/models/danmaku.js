"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.DanmakuModel = exports.Danmaku = void 0;
const _1 = require(".");
const sequelize_1 = require("sequelize");
/**
 * 弹幕库
 */
class Danmaku {
    constructor(options) {
        this.id = options.id;
        this.content = options.content;
        this.tag_id = options.tag_id;
        this.create_by = options.create_by;
        this.create_time = options.create_time;
        this.update_by = options.update_by;
        this.update_time = options.update_time;
    }
}
exports.Danmaku = Danmaku;
exports.DanmakuModel = _1.sequelize.define('danmaku', {
    id: {
        primaryKey: true,
        type: sequelize_1.DataTypes.STRING(64),
    },
    content: sequelize_1.DataTypes.STRING(1024),
    tag_id: sequelize_1.DataTypes.STRING(64),
    create_by: sequelize_1.DataTypes.STRING(64),
    create_time: sequelize_1.DataTypes.DATE,
    update_by: sequelize_1.DataTypes.STRING(64),
    update_time: sequelize_1.DataTypes.DATE,
});
//# sourceMappingURL=danmaku.js.map