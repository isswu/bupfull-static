"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.DanmakuHitItemRecordModel = exports.DanmakuHitItemRecord = void 0;
const _1 = require(".");
const sequelize_1 = require("sequelize");
/**
 * 命中的弹幕记录
 */
class DanmakuHitItemRecord {
    constructor(options) {
        this.id = options.id;
        this.uid = options.uid;
        this.bvid = options.bvid;
        this.dmid = options.dmid;
        this.mid = options.mid;
        this.oid = options.oid;
        this.record_id = options.record_id;
        this.content = options.content;
        this.uname = options.uname;
        this.ctime = options.ctime;
        this.create_by = options.create_by;
        this.create_time = options.create_time;
        this.update_by = options.update_by;
        this.update_time = options.update_time;
    }
}
exports.DanmakuHitItemRecord = DanmakuHitItemRecord;
exports.DanmakuHitItemRecordModel = _1.sequelize.define('danmaku_hit_item_record', {
    id: {
        primaryKey: true,
        type: sequelize_1.DataTypes.STRING(64),
    },
    uid: sequelize_1.DataTypes.STRING(64),
    bvid: sequelize_1.DataTypes.STRING(64),
    dmid: sequelize_1.DataTypes.STRING(64),
    mid: sequelize_1.DataTypes.STRING(64),
    oid: sequelize_1.DataTypes.STRING(64),
    record_id: sequelize_1.DataTypes.STRING(64),
    content: sequelize_1.DataTypes.TEXT,
    uname: sequelize_1.DataTypes.STRING(128),
    ctime: sequelize_1.DataTypes.BIGINT,
    create_by: sequelize_1.DataTypes.STRING(64),
    create_time: sequelize_1.DataTypes.DATE,
    update_by: sequelize_1.DataTypes.STRING(64),
    update_time: sequelize_1.DataTypes.DATE,
}, {
    indexes: [
        {
            unique: false,
            fields: ['bvid'],
        },
    ],
});
//# sourceMappingURL=danmakuHitItemRecord.js.map