"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.DanmakuTaskExecuteRecordModel = exports.ScheduleTypeEnum = exports.DanmakuTaskExecuteRecord = void 0;
const _1 = require(".");
const sequelize_1 = require("sequelize");
/**
 * 弹幕任务执行记录
 */
class DanmakuTaskExecuteRecord {
    constructor(options) {
        this.id = options.id;
        this.schedule_type = options.schedule_type;
        this.schedule_id = options.schedule_id;
        this.task_id = options.task_id;
        this.start_time = options.start_time;
        this.end_time = options.end_time;
        this.total_count = options.total_count;
        this.success_count = options.success_count;
        this.create_by = options.create_by;
        this.create_time = options.create_time;
        this.update_by = options.update_by;
        this.update_time = options.update_time;
    }
}
exports.DanmakuTaskExecuteRecord = DanmakuTaskExecuteRecord;
var ScheduleTypeEnum;
(function (ScheduleTypeEnum) {
    ScheduleTypeEnum[ScheduleTypeEnum["RIGHT_NOW"] = 1] = "RIGHT_NOW";
    ScheduleTypeEnum[ScheduleTypeEnum["SCHEDULE"] = 2] = "SCHEDULE";
})(ScheduleTypeEnum || (exports.ScheduleTypeEnum = ScheduleTypeEnum = {}));
exports.DanmakuTaskExecuteRecordModel = _1.sequelize.define('danmaku_task_execute_record', {
    id: {
        primaryKey: true,
        type: sequelize_1.DataTypes.STRING(64),
    },
    schedule_type: sequelize_1.DataTypes.INTEGER,
    schedule_id: sequelize_1.DataTypes.STRING(64),
    task_id: sequelize_1.DataTypes.STRING(64),
    start_time: sequelize_1.DataTypes.DATE,
    end_time: sequelize_1.DataTypes.DATE,
    total_count: sequelize_1.DataTypes.INTEGER,
    success_count: sequelize_1.DataTypes.INTEGER,
    create_by: sequelize_1.DataTypes.STRING(64),
    create_time: sequelize_1.DataTypes.DATE,
    update_by: sequelize_1.DataTypes.STRING(64),
    update_time: sequelize_1.DataTypes.DATE,
});
//# sourceMappingURL=danmakuTaskExecuteRecord.js.map