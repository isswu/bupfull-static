"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.SimpleTaskExecuteStatisticModel = exports.StatisticTypeEnum = exports.SimpleTaskExecuteStatistic = void 0;
const _1 = require(".");
const sequelize_1 = require("sequelize");
/**
 * 简单任务（点赞、投币、收藏、一键三连、分享）执行记录统计
 */
class SimpleTaskExecuteStatistic {
    constructor(options) {
        this.id = options.id;
        this.mid = options.mid;
        this.type = options.type;
        this.date = options.date;
        this.total_count = options.total_count;
        this.success_count = options.success_count;
        this.create_by = options.create_by;
        this.create_time = options.create_time;
        this.update_by = options.update_by;
        this.update_time = options.update_time;
    }
}
exports.SimpleTaskExecuteStatistic = SimpleTaskExecuteStatistic;
var StatisticTypeEnum;
(function (StatisticTypeEnum) {
    StatisticTypeEnum[StatisticTypeEnum["DAY"] = 0] = "DAY";
    StatisticTypeEnum[StatisticTypeEnum["WEEK"] = 1] = "WEEK";
    StatisticTypeEnum[StatisticTypeEnum["MONTH"] = 2] = "MONTH";
    StatisticTypeEnum[StatisticTypeEnum["YEAR"] = 3] = "YEAR";
})(StatisticTypeEnum || (exports.StatisticTypeEnum = StatisticTypeEnum = {}));
exports.SimpleTaskExecuteStatisticModel = _1.sequelize.define('simple_task_execute_statistic', {
    id: {
        primaryKey: true,
        type: sequelize_1.DataTypes.STRING(64)
    },
    mid: sequelize_1.DataTypes.STRING(64),
    type: sequelize_1.DataTypes.STRING(16),
    date: sequelize_1.DataTypes.STRING(16),
    total_count: sequelize_1.DataTypes.INTEGER,
    success_count: sequelize_1.DataTypes.INTEGER,
    create_by: sequelize_1.DataTypes.STRING(64),
    create_time: sequelize_1.DataTypes.DATE,
    update_by: sequelize_1.DataTypes.STRING(64),
    update_time: sequelize_1.DataTypes.DATE,
});
//# sourceMappingURL=simpleTaskExecuteStatistic.js.map