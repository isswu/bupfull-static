"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.SysRolePrivilegeModel = exports.PrivilegeType = exports.SysRolePrivilege = void 0;
const _1 = require(".");
const sequelize_1 = require("sequelize");
/**
 * 系统角色权限
 */
class SysRolePrivilege {
    constructor(options) {
        this.role_id = options.role_id;
        this.privilege_type = options.privilege_type;
        this.privilege_id = options.privilege_id;
        this.create_by = options.create_by;
        this.create_time = options.create_time;
    }
}
exports.SysRolePrivilege = SysRolePrivilege;
var PrivilegeType;
(function (PrivilegeType) {
    PrivilegeType[PrivilegeType["MENU"] = 1] = "MENU";
    PrivilegeType[PrivilegeType["FUNC"] = 2] = "FUNC";
    PrivilegeType[PrivilegeType["API"] = 3] = "API";
})(PrivilegeType || (exports.PrivilegeType = PrivilegeType = {}));
exports.SysRolePrivilegeModel = _1.sequelize.define('sys_role_privilege', {
    role_id: {
        primaryKey: true,
        type: sequelize_1.DataTypes.INTEGER,
    },
    privilege_type: {
        primaryKey: true,
        type: sequelize_1.DataTypes.INTEGER,
    },
    privilege_id: {
        primaryKey: true,
        type: sequelize_1.DataTypes.INTEGER,
    },
    create_by: sequelize_1.DataTypes.STRING(64),
    create_time: sequelize_1.DataTypes.DATE,
});
//# sourceMappingURL=sysRolePrivilege.js.map