"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.SysUserModel = exports.SysUser = void 0;
const _1 = require(".");
const sequelize_1 = require("sequelize");
/**
 * 系统用户
 */
class SysUser {
    constructor(options) {
        this.id = options.id;
        this.username = options.username;
        this.avatar = options.avatar;
        this.openid = options.openid;
        this.phone_num = options.phone_num;
        this.email = options.email;
        this.super_admin = options.super_admin;
        this.access_token = options.access_token;
        this.login_pwd = options.login_pwd;
        this.latest_login_ip = options.latest_login_ip;
        this.latest_login_time = options.latest_login_time;
        this.create_by = options.create_by;
        this.create_time = options.create_time;
        this.update_by = options.update_by;
        this.update_time = options.update_time;
    }
}
exports.SysUser = SysUser;
exports.SysUserModel = _1.sequelize.define('sys_user', {
    id: {
        primaryKey: true,
        autoIncrement: true,
        type: sequelize_1.DataTypes.INTEGER,
    },
    username: sequelize_1.DataTypes.STRING(32),
    avatar: {
        type: sequelize_1.DataTypes.STRING(255),
        allowNull: true,
    },
    openid: {
        type: sequelize_1.DataTypes.STRING(64),
        allowNull: true,
    },
    phone_num: {
        type: sequelize_1.DataTypes.STRING(255),
        allowNull: true,
    },
    email: {
        type: sequelize_1.DataTypes.STRING(255),
        allowNull: true,
    },
    super_admin: sequelize_1.DataTypes.BOOLEAN,
    access_token: {
        type: sequelize_1.DataTypes.STRING(255),
        allowNull: true,
    },
    login_pwd: sequelize_1.DataTypes.STRING(255),
    latest_login_ip: {
        type: sequelize_1.DataTypes.STRING(255),
        allowNull: true,
    },
    latest_login_time: {
        type: sequelize_1.DataTypes.DATE,
        allowNull: true,
    },
    create_by: sequelize_1.DataTypes.STRING(64),
    create_time: sequelize_1.DataTypes.DATE,
    update_by: sequelize_1.DataTypes.STRING(64),
    update_time: sequelize_1.DataTypes.DATE,
});
//# sourceMappingURL=sysUser.js.map