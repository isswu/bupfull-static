"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.SysUserRoleModel = exports.SysUserRole = void 0;
const _1 = require(".");
const sequelize_1 = require("sequelize");
/**
 * 系统用户角色
 */
class SysUserRole {
    constructor(options) {
        this.user_id = options.user_id;
        this.role_id = options.role_id;
        this.create_by = options.create_by;
        this.create_time = options.create_time;
    }
}
exports.SysUserRole = SysUserRole;
exports.SysUserRoleModel = _1.sequelize.define('sys_user_role', {
    user_id: {
        primaryKey: true,
        type: sequelize_1.DataTypes.INTEGER,
    },
    role_id: {
        primaryKey: true,
        type: sequelize_1.DataTypes.INTEGER,
    },
    create_by: sequelize_1.DataTypes.STRING(64),
    create_time: sequelize_1.DataTypes.DATE,
});
//# sourceMappingURL=sysUserRole.js.map