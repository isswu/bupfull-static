'use strict';
/** @type {import('sequelize-cli').Migration} */
module.exports = {
    async up(queryInterface, Sequelize) {
        await queryInterface.bulkInsert('sys_user', [{
                id: 20,
                username: 'kcq',
                login_pwd: 'qwer123456',
                super_admin: false,
                create_by: 'system',
                create_time: new Date(),
                update_time: new Date(),
            }
        ]);
    },
    async down(queryInterface, Sequelize) {
        await queryInterface.bulkDelete('sys_user', null, {});
    }
};
//# sourceMappingURL=20240108103028-insert_users_to_sys_user_table.js.map