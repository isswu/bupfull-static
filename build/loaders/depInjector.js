"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const typedi_1 = require("typedi");
const logger_1 = __importDefault(require("./logger"));
const cache_manager_1 = require("cache-manager");
exports.default = () => {
    try {
        // Create memory cache synchronously
        const memoryCache = (0, cache_manager_1.createCache)((0, cache_manager_1.memoryStore)(), {
            max: 100,
            ttl: 10 * 1000 /*milliseconds*/,
        });
        typedi_1.Container.set('logger', logger_1.default);
        typedi_1.Container.set('CACHE_MANAGER', memoryCache);
    }
    catch (e) {
        logger_1.default.error('🔥 Error on dependency injector loader: %o', e);
        throw e;
    }
};
//# sourceMappingURL=depInjector.js.map