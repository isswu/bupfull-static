"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const typedi_1 = require("typedi");
const biliTask_1 = __importDefault(require("../services/biliTask"));
const biliDanmakuTask_1 = __importDefault(require("../services/biliDanmakuTask"));
exports.default = async () => {
    const biliTaskService = typedi_1.Container.get(biliTask_1.default);
    const biliDanmakuTaskService = typedi_1.Container.get(biliDanmakuTask_1.default);
    // 应用程序启动时重置CommentTask
    //await biliTaskService.resetCommentTaskStatus();
    // 应用程序启动时重置DanmakuTask
    //await biliDanmakuTaskService.resetDanmakuTaskStatus();
    // 应用程序启动时自动启动CommentTasks
    //await biliTaskService.autoRunCommentTasksWithoutLimit();
    // 应用程序启动时自动启动DanmakuTasks
    //await biliDanmakuTaskService.autoRunDanmakuTasksWithoutLimit();
    // 自动重启超时的CommentTasks
    //await biliTaskService.createIntervalTask();
    // 自动重启超时的DanmakuTasks
    //await biliDanmakuTaskService.createIntervalTask();
};
//# sourceMappingURL=initDefinedTasks.js.map