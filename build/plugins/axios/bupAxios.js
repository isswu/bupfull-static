"use strict";
// bupAxios.ts
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
var _a;
Object.defineProperty(exports, "__esModule", { value: true });
const axios_1 = __importDefault(require("axios"));
const config_1 = __importDefault(require("../../config"));
const logger_1 = __importDefault(require("../../loaders/logger"));
const bupAxios = axios_1.default.create({
    baseURL: (_a = config_1.default.authingHost) !== null && _a !== void 0 ? _a : 'https://biddev-authing.frp.tltr.top',
    timeout: 10000,
    headers: { 'X-Llama-Header': 'bup' },
});
// 添加请求拦截器
bupAxios.interceptors.request.use((config) => {
    // 在发送请求之前做些什么
    //console.log('Request Interceptor:', config);
    logger_1.default.info('[AxiosRequestConfig]: %o', JSON.stringify(config));
    return config;
}, (error) => {
    // 对请求错误做些什么
    return Promise.reject(error);
});
// 添加响应拦截器
bupAxios.interceptors.response.use((response) => {
    // 对响应数据做些什么
    //console.log('Response Interceptor:', response.data);
    logger_1.default.info('[AxiosResponse]: %s', JSON.stringify(response.data));
    return response;
}, (error) => {
    // 对响应错误做些什么
    return Promise.reject(error);
});
exports.default = bupAxios;
//# sourceMappingURL=bupAxios.js.map