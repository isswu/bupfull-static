"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
var _a, _b;
Object.defineProperty(exports, "__esModule", { value: true });
const axios_1 = __importDefault(require("axios"));
const config_1 = __importDefault(require("../../config"));
const difyAxios = axios_1.default.create({
    baseURL: (_a = config_1.default.difyAiAppHost) !== null && _a !== void 0 ? _a : 'http://localhost/v1',
    timeout: 100000,
    headers: {
        Authorization: `Bearer ${(_b = config_1.default.difyApiKey) !== null && _b !== void 0 ? _b : 'app-6ZHzvG2vtUGSk5GRnTFWfGby'}`,
    },
});
difyAxios.interceptors.request.use((config) => {
    console.log('[DifyAxiosRequestConfig]: %o', config);
    return config;
}, (error) => {
    return Promise.reject(error);
});
difyAxios.interceptors.response.use((response) => {
    console.log('[DifyAxiosResponse]: %o', response.data);
    return response;
}, (error) => {
    return Promise.reject(error);
});
exports.default = difyAxios;
//# sourceMappingURL=difyAxios.js.map