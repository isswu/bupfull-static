"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const bupAxios_1 = __importDefault(require("../axios/bupAxios"));
const util_1 = require("../../config/util");
const logger_1 = __importDefault(require("../../loaders/logger"));
const config_1 = __importDefault(require("../../config"));
const checkAuthingPermissions = async (req, res, next) => {
    try {
        const token = (0, util_1.getToken)(req);
        const { path, method } = req;
        const originPath = `${req.baseUrl}${req.path === '/' ? '' : req.path}`;
        if (!token && originPath && config_1.default.apiWhiteList.includes(originPath)) {
            logger_1.default.info('Skip check permissions path: [%s]', originPath);
            return next();
        }
        const checkRes = await bupAxios_1.default.post('/auth/permissions/check', {
            path,
            action: method,
        }, {
            headers: {
                token: token,
            },
        });
        console.log('checking permissions response:', checkRes.data);
        if (checkRes.data.err !== 0) {
            logger_1.default.error(`permissions check failed, ${JSON.stringify(checkRes.data)}`);
            const err = new Error(checkRes.data.errMsg);
            err['status'] = checkRes.data.err;
            return res
                .status(err.status)
                .send({ code: err.status, message: err.message })
                .end();
        }
        if (checkRes.data.data && !checkRes.data.data.permission) {
            logger_1.default.error(`permissions check error, ${JSON.stringify(checkRes.data)}`);
            //暂时不做处理，后期会加上
            // return res
            //   .status(403)
            //   .send({ code: 403, message: 'Permission denied' })
            //   .end();
        }
        if (checkRes.data.data && checkRes.data.data.user) {
            req.locals = {
                user: checkRes.data.data.user,
                token: token,
            };
            logger_1.default.verbose(`set user info in req.locals user: ${JSON.stringify(req['locals']['user'])}`);
        }
        next();
    }
    catch (error) {
        // 处理请求错误
        logger_1.default.error(`permissions check error, ${error}`);
        console.error('Error checking permissions:', error);
        res.status(500).send({ code: 500, message: 'Internal server error' }).end();
    }
};
exports.default = checkAuthingPermissions;
//# sourceMappingURL=checkAuthingPermissions.js.map