"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const typedi_1 = require("typedi");
const logger_1 = __importDefault(require("../../loaders/logger"));
const userBiliAccountBindRecord_1 = require("../../data/models/binding/userBiliAccountBindRecord");
const userBiliSimpleTaskBindRecord_1 = require("../../data/models/binding/userBiliSimpleTaskBindRecord");
const userBiliCommentTaskBindRecord_1 = require("../../data/models/binding/userBiliCommentTaskBindRecord");
const userBiliDanmuTaskBindRecord_1 = require("../../data/models/binding/userBiliDanmuTaskBindRecord");
const userGroup_1 = __importDefault(require("../../services/userGroup"));
const apiDataPermissionList = [
    '/api/biliAccounts',
    '/api/biliTask/simple/list',
    '/api/biliTask/comment/list',
    '/api/biliDanmakuTask/danmaku/list',
];
const checkDataPermissions = async (req, res, next) => {
    const { path } = req;
    const user = req.locals && req.locals.user;
    const token = req.locals && req.locals.token;
    if (user && apiDataPermissionList.includes(path)) {
        let userIds = new Set(); // 使用 Set 来存储唯一的用户 id
        const userGroupService = typedi_1.Container.get(userGroup_1.default);
        const groupResult = await userGroupService.getList({
            page_index: 1,
            page_size: 100,
        }, token);
        if (groupResult.code === 0) {
            const shareGroups = groupResult.data.rows.filter((item) => user.id === item.adminUserId);
            for (const group of shareGroups) {
                const userListResult = await userGroupService.getUserListByGroupId({
                    group_id: group.id,
                    page_index: 1,
                    page_size: 100,
                }, token);
                if (userListResult.code === 0) {
                    userListResult.data.rows.forEach((item) => {
                        userIds.add(item.id); // 将用户 id 添加到 Set 中
                    });
                }
            }
        }
        // 将 Set 转换为数组
        const uniqueUserIds = Array.from(userIds);
        uniqueUserIds.push(user.id);
        if (path === '/api/biliAccounts') {
            const accountResult = await userBiliAccountBindRecord_1.UserBiliAccountBindRecordModel.findAll({
                where: { user_id: uniqueUserIds, readable: true },
            });
            req.locals.dataIn = accountResult.map((item) => item.mid);
        }
        else if (path === '/api/biliTask/simple/list') {
            const simpleResult = await userBiliSimpleTaskBindRecord_1.UserBiliSimpleTaskBindRecordModel.findAll({
                where: { user_id: uniqueUserIds, readable: true },
            });
            req.locals.dataIn = simpleResult.map((item) => item.simple_task_id);
        }
        else if (path === '/api/biliTask/comment/list') {
            const commentResult = await userBiliCommentTaskBindRecord_1.UserBiliCommentTaskBindRecordModel.findAll({
                where: { user_id: uniqueUserIds, readable: true },
            });
            req.locals.dataIn = commentResult.map((item) => item.comment_task_id);
        }
        else if (path === '/api/biliDanmakuTask/danmaku/list') {
            const danmuResult = await userBiliDanmuTaskBindRecord_1.UserBiliDanmuTaskBindRecordModel.findAll({
                where: { user_id: uniqueUserIds, readable: true },
            });
            req.locals.dataIn = danmuResult.map((item) => item.danmu_task_id);
        }
        let dataSize = req.locals.dataIn ? req.locals.dataIn.length : 0;
        logger_1.default.info('Check data permissions in path: [%s], data size: [%d]', path, dataSize);
    }
    next();
};
exports.default = checkDataPermissions;
//# sourceMappingURL=checkDataPermissions.js.map