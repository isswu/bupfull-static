"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const developmentConfig = {
    username: 'root',
    password: '20Bup2023',
    database: 'bup',
    host: '127.0.0.1',
    port: 3306,
    dialect: 'mysql',
    dialectOptions: {
        bigNumberStrings: true
    },
    logging: false,
    timezone: '+08:00',
    define: {
        freezeTableName: true,
        timestamps: false
    }
};
const testConfig = {
    username: process.env.CI_DB_USERNAME || '',
    password: process.env.CI_DB_PASSWORD || '',
    database: process.env.CI_DB_NAME || '',
    host: '127.0.0.1',
    port: 3306,
    dialect: 'mysql',
    dialectOptions: {
        bigNumberStrings: true
    },
    logging: false,
    timezone: '+08:00',
    define: {
        freezeTableName: true,
        timestamps: false
    }
};
const productionConfig = {
    username: process.env.PROD_MDB_USERNAME || '',
    password: process.env.PROD_MDB_PASSWORD || '',
    database: process.env.PROD_MDB_NAME || '',
    host: process.env.PROD_MDB_HOSTNAME || '',
    port: parseInt(process.env.PROD_MDB_PORT || '0', 10),
    dialect: 'mysql',
    dialectOptions: {
        bigNumberStrings: true
    },
    logging: false,
    timezone: '+08:00',
    define: {
        freezeTableName: true,
        timestamps: false
    }
};
const databaseConfig = {
    'development': developmentConfig,
    'test': testConfig,
    'production': productionConfig
};
exports.default = databaseConfig;
//# sourceMappingURL=config.js.map