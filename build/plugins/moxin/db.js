"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const logger_1 = __importDefault(require("../../loaders/logger"));
const playCount_1 = require("./models/playCount");
const playRate_1 = require("./models/playRate");
const viewerQuitTrend_1 = require("./models/viewerQuitTrend");
exports.default = async () => {
    try {
        await playCount_1.PlayCountModel.sync();
        await playRate_1.PlayRateModel.sync();
        await viewerQuitTrend_1.ViewerQuitTrendModel.sync();
        logger_1.default.info('✌️ Moxin DB loaded');
    }
    catch (error) {
        logger_1.default.info('✌️ Moxin DB load failed');
        logger_1.default.info(error);
    }
};
//# sourceMappingURL=db.js.map