"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.PlayCountModel = exports.PlayCount = void 0;
const _1 = require(".");
const sequelize_1 = require("sequelize");
/**
 * 记录播放数
 */
class PlayCount {
    constructor(options) {
        this.id = options.id;
        this.uid = options.uid;
        this.uname = options.uname;
        this.aid = options.aid;
        this.bvid = options.bvid;
        this.incr = options.incr;
        this.total = options.total;
        this.title = options.title;
        this.daytime = options.daytime;
        this.ptime = options.ptime;
        this.interactive = options.interactive;
        this.create_by = options.create_by;
        this.create_time = options.create_time;
        this.update_by = options.update_by;
        this.update_time = options.update_time;
    }
}
exports.PlayCount = PlayCount;
exports.PlayCountModel = _1.sequelize.define('play_count', {
    id: {
        primaryKey: true,
        type: sequelize_1.DataTypes.STRING(64),
    },
    uid: sequelize_1.DataTypes.STRING(64),
    uname: sequelize_1.DataTypes.STRING(128),
    aid: sequelize_1.DataTypes.STRING(64),
    bvid: sequelize_1.DataTypes.STRING(64),
    incr: sequelize_1.DataTypes.BIGINT,
    total: sequelize_1.DataTypes.BIGINT,
    title: sequelize_1.DataTypes.TEXT,
    daytime: sequelize_1.DataTypes.DATE,
    ptime: sequelize_1.DataTypes.DATE,
    interactive: sequelize_1.DataTypes.BIGINT,
    create_by: sequelize_1.DataTypes.STRING(64),
    create_time: sequelize_1.DataTypes.DATE,
    update_by: sequelize_1.DataTypes.STRING(64),
    update_time: sequelize_1.DataTypes.DATE,
});
//# sourceMappingURL=playCount.js.map