"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.PlayRateModel = exports.PlayRate = void 0;
const _1 = require(".");
const sequelize_1 = require("sequelize");
/**
 * 记录完播率
 */
class PlayRate {
    constructor(options) {
        this.id = options.id;
        this.uid = options.uid;
        this.uname = options.uname;
        this.aid = options.aid;
        this.bvid = options.bvid;
        this.view = options.view;
        this.rate = options.rate;
        this.title = options.title;
        this.ctime = options.ctime;
        this.duration = options.duration;
        this.avg_duration = options.avg_duration;
        this.avg_play_time_value = options.avg_play_time_value;
        this.create_by = options.create_by;
        this.create_time = options.create_time;
        this.update_by = options.update_by;
        this.update_time = options.update_time;
    }
}
exports.PlayRate = PlayRate;
exports.PlayRateModel = _1.sequelize.define('play_rate', {
    id: {
        primaryKey: true,
        type: sequelize_1.DataTypes.STRING(64),
    },
    uid: sequelize_1.DataTypes.STRING(64),
    uname: sequelize_1.DataTypes.STRING(128),
    aid: sequelize_1.DataTypes.STRING(64),
    bvid: sequelize_1.DataTypes.STRING(64),
    view: sequelize_1.DataTypes.BIGINT,
    rate: sequelize_1.DataTypes.BIGINT,
    title: sequelize_1.DataTypes.TEXT,
    ctime: sequelize_1.DataTypes.DATE,
    duration: sequelize_1.DataTypes.BIGINT,
    avg_duration: sequelize_1.DataTypes.BIGINT,
    avg_play_time_value: sequelize_1.DataTypes.BIGINT,
    create_by: sequelize_1.DataTypes.STRING(64),
    create_time: sequelize_1.DataTypes.DATE,
    update_by: sequelize_1.DataTypes.STRING(64),
    update_time: sequelize_1.DataTypes.DATE,
});
//# sourceMappingURL=playRate.js.map