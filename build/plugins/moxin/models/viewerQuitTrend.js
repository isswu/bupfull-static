"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ViewerQuitTrendModel = exports.ViewerQuitTrend = void 0;
const _1 = require(".");
const sequelize_1 = require("sequelize");
/**
 * 记录观众离开趋势
 */
class ViewerQuitTrend {
    constructor(options) {
        this.id = options.id;
        this.uid = options.uid;
        this.uname = options.uname;
        this.aid = options.aid;
        this.cid = options.cid;
        this.bvid = options.bvid;
        this.viewer_quit = options.viewer_quit;
        this.create_by = options.create_by;
        this.create_time = options.create_time;
        this.update_by = options.update_by;
        this.update_time = options.update_time;
    }
}
exports.ViewerQuitTrend = ViewerQuitTrend;
exports.ViewerQuitTrendModel = _1.sequelize.define('viewer_quit_trend', {
    id: {
        primaryKey: true,
        type: sequelize_1.DataTypes.STRING(64),
    },
    uid: sequelize_1.DataTypes.STRING(64),
    uname: sequelize_1.DataTypes.STRING(128),
    aid: sequelize_1.DataTypes.STRING(64),
    cid: sequelize_1.DataTypes.STRING(64),
    bvid: sequelize_1.DataTypes.STRING(64),
    viewer_quit: sequelize_1.DataTypes.JSON,
    create_by: sequelize_1.DataTypes.STRING(64),
    create_time: sequelize_1.DataTypes.DATE,
    update_by: sequelize_1.DataTypes.STRING(64),
    update_time: sequelize_1.DataTypes.DATE,
});
//# sourceMappingURL=viewerQuitTrend.js.map