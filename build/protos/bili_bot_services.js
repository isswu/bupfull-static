"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.SyncArchiveAnalyzeStatsResponse = exports.SyncArchiveAnalyzeStatsRequest = exports.SyncPlayRateAnalysisResponse = exports.SyncPlayRateAnalysisRequest = exports.ArchiveViewerQuit = exports.ArchiveInc = exports.ArchivePlayRate = exports.BiliTaskResponse = exports.BiliTaskRequest_PayLoad = exports.BiliTaskRequest = exports.SyncBiliAccountResponse = exports.SyncBiliAccountRequest = exports.SyncHittedDanmakuResponse = exports.SyncHittedDanmakuRequest = exports.SyncHittedCommentResponse = exports.SyncHittedCommentRequest = exports.SyncQrCodeLoginResponse = exports.SyncQrCodeLoginRequest = exports.SubtaskResult = exports.HittedDanmaku = exports.HittedComment = exports.BiliTask_DanmakuController = exports.BiliTask_Danmaku = exports.BiliTask_CommentController = exports.BiliTask_WhiteList = exports.BiliTask_Comment = exports.BiliTask_DanmakuWrap = exports.BiliTask_CommentWrap = exports.BiliTask_Carrier = exports.BiliTask_BlCookie = exports.BiliTask_PayLoad = exports.BiliTask = exports.BiliAccount_LevelInfo = exports.BiliAccount = exports.subtaskResult_StatusToJSON = exports.subtaskResult_StatusFromJSON = exports.SubtaskResult_Status = exports.biliTask_CarryTypeToJSON = exports.biliTask_CarryTypeFromJSON = exports.BiliTask_CarryType = exports.biliAccount_VipStatusToJSON = exports.biliAccount_VipStatusFromJSON = exports.BiliAccount_VipStatus = exports.biliAccount_VipTypeToJSON = exports.biliAccount_VipTypeFromJSON = exports.BiliAccount_VipType = exports.runTaskStatusToJSON = exports.runTaskStatusFromJSON = exports.RunTaskStatus = exports.protobufPackage = void 0;
exports.BiliBotClient = exports.BiliBotService = exports.SyncViewerQuitTrendResponse = exports.SyncViewerQuitTrendRequest = void 0;
/* eslint-disable */
const grpc_js_1 = require("@grpc/grpc-js");
const minimal_1 = __importDefault(require("protobufjs/minimal"));
exports.protobufPackage = "";
var RunTaskStatus;
(function (RunTaskStatus) {
    RunTaskStatus[RunTaskStatus["Starting"] = 0] = "Starting";
    RunTaskStatus[RunTaskStatus["PayLoad"] = 1] = "PayLoad";
    RunTaskStatus[RunTaskStatus["Completed"] = 2] = "Completed";
    RunTaskStatus[RunTaskStatus["UNRECOGNIZED"] = -1] = "UNRECOGNIZED";
})(RunTaskStatus || (exports.RunTaskStatus = RunTaskStatus = {}));
function runTaskStatusFromJSON(object) {
    switch (object) {
        case 0:
        case "Starting":
            return RunTaskStatus.Starting;
        case 1:
        case "PayLoad":
            return RunTaskStatus.PayLoad;
        case 2:
        case "Completed":
            return RunTaskStatus.Completed;
        case -1:
        case "UNRECOGNIZED":
        default:
            return RunTaskStatus.UNRECOGNIZED;
    }
}
exports.runTaskStatusFromJSON = runTaskStatusFromJSON;
function runTaskStatusToJSON(object) {
    switch (object) {
        case RunTaskStatus.Starting:
            return "Starting";
        case RunTaskStatus.PayLoad:
            return "PayLoad";
        case RunTaskStatus.Completed:
            return "Completed";
        case RunTaskStatus.UNRECOGNIZED:
        default:
            return "UNRECOGNIZED";
    }
}
exports.runTaskStatusToJSON = runTaskStatusToJSON;
var BiliAccount_VipType;
(function (BiliAccount_VipType) {
    /** None - 无 */
    BiliAccount_VipType[BiliAccount_VipType["None"] = 0] = "None";
    /** Mensual - 月度大会员 */
    BiliAccount_VipType[BiliAccount_VipType["Mensual"] = 1] = "Mensual";
    /** Annual - 年度大会员 */
    BiliAccount_VipType[BiliAccount_VipType["Annual"] = 2] = "Annual";
    BiliAccount_VipType[BiliAccount_VipType["UNRECOGNIZED"] = -1] = "UNRECOGNIZED";
})(BiliAccount_VipType || (exports.BiliAccount_VipType = BiliAccount_VipType = {}));
function biliAccount_VipTypeFromJSON(object) {
    switch (object) {
        case 0:
        case "None":
            return BiliAccount_VipType.None;
        case 1:
        case "Mensual":
            return BiliAccount_VipType.Mensual;
        case 2:
        case "Annual":
            return BiliAccount_VipType.Annual;
        case -1:
        case "UNRECOGNIZED":
        default:
            return BiliAccount_VipType.UNRECOGNIZED;
    }
}
exports.biliAccount_VipTypeFromJSON = biliAccount_VipTypeFromJSON;
function biliAccount_VipTypeToJSON(object) {
    switch (object) {
        case BiliAccount_VipType.None:
            return "None";
        case BiliAccount_VipType.Mensual:
            return "Mensual";
        case BiliAccount_VipType.Annual:
            return "Annual";
        case BiliAccount_VipType.UNRECOGNIZED:
        default:
            return "UNRECOGNIZED";
    }
}
exports.biliAccount_VipTypeToJSON = biliAccount_VipTypeToJSON;
var BiliAccount_VipStatus;
(function (BiliAccount_VipStatus) {
    /** Disable - 无/过期 */
    BiliAccount_VipStatus[BiliAccount_VipStatus["Disable"] = 0] = "Disable";
    /** Enable - 正常 */
    BiliAccount_VipStatus[BiliAccount_VipStatus["Enable"] = 1] = "Enable";
    BiliAccount_VipStatus[BiliAccount_VipStatus["UNRECOGNIZED"] = -1] = "UNRECOGNIZED";
})(BiliAccount_VipStatus || (exports.BiliAccount_VipStatus = BiliAccount_VipStatus = {}));
function biliAccount_VipStatusFromJSON(object) {
    switch (object) {
        case 0:
        case "Disable":
            return BiliAccount_VipStatus.Disable;
        case 1:
        case "Enable":
            return BiliAccount_VipStatus.Enable;
        case -1:
        case "UNRECOGNIZED":
        default:
            return BiliAccount_VipStatus.UNRECOGNIZED;
    }
}
exports.biliAccount_VipStatusFromJSON = biliAccount_VipStatusFromJSON;
function biliAccount_VipStatusToJSON(object) {
    switch (object) {
        case BiliAccount_VipStatus.Disable:
            return "Disable";
        case BiliAccount_VipStatus.Enable:
            return "Enable";
        case BiliAccount_VipStatus.UNRECOGNIZED:
        default:
            return "UNRECOGNIZED";
    }
}
exports.biliAccount_VipStatusToJSON = biliAccount_VipStatusToJSON;
var BiliTask_CarryType;
(function (BiliTask_CarryType) {
    /** CComment - 评论 */
    BiliTask_CarryType[BiliTask_CarryType["CComment"] = 0] = "CComment";
    /** CDanmaku - 弹幕 */
    BiliTask_CarryType[BiliTask_CarryType["CDanmaku"] = 1] = "CDanmaku";
    BiliTask_CarryType[BiliTask_CarryType["UNRECOGNIZED"] = -1] = "UNRECOGNIZED";
})(BiliTask_CarryType || (exports.BiliTask_CarryType = BiliTask_CarryType = {}));
function biliTask_CarryTypeFromJSON(object) {
    switch (object) {
        case 0:
        case "CComment":
            return BiliTask_CarryType.CComment;
        case 1:
        case "CDanmaku":
            return BiliTask_CarryType.CDanmaku;
        case -1:
        case "UNRECOGNIZED":
        default:
            return BiliTask_CarryType.UNRECOGNIZED;
    }
}
exports.biliTask_CarryTypeFromJSON = biliTask_CarryTypeFromJSON;
function biliTask_CarryTypeToJSON(object) {
    switch (object) {
        case BiliTask_CarryType.CComment:
            return "CComment";
        case BiliTask_CarryType.CDanmaku:
            return "CDanmaku";
        case BiliTask_CarryType.UNRECOGNIZED:
        default:
            return "UNRECOGNIZED";
    }
}
exports.biliTask_CarryTypeToJSON = biliTask_CarryTypeToJSON;
var SubtaskResult_Status;
(function (SubtaskResult_Status) {
    SubtaskResult_Status[SubtaskResult_Status["Pending"] = 0] = "Pending";
    SubtaskResult_Status[SubtaskResult_Status["Running"] = 1] = "Running";
    SubtaskResult_Status[SubtaskResult_Status["Success"] = 2] = "Success";
    SubtaskResult_Status[SubtaskResult_Status["Failed"] = 3] = "Failed";
    SubtaskResult_Status[SubtaskResult_Status["UNRECOGNIZED"] = -1] = "UNRECOGNIZED";
})(SubtaskResult_Status || (exports.SubtaskResult_Status = SubtaskResult_Status = {}));
function subtaskResult_StatusFromJSON(object) {
    switch (object) {
        case 0:
        case "Pending":
            return SubtaskResult_Status.Pending;
        case 1:
        case "Running":
            return SubtaskResult_Status.Running;
        case 2:
        case "Success":
            return SubtaskResult_Status.Success;
        case 3:
        case "Failed":
            return SubtaskResult_Status.Failed;
        case -1:
        case "UNRECOGNIZED":
        default:
            return SubtaskResult_Status.UNRECOGNIZED;
    }
}
exports.subtaskResult_StatusFromJSON = subtaskResult_StatusFromJSON;
function subtaskResult_StatusToJSON(object) {
    switch (object) {
        case SubtaskResult_Status.Pending:
            return "Pending";
        case SubtaskResult_Status.Running:
            return "Running";
        case SubtaskResult_Status.Success:
            return "Success";
        case SubtaskResult_Status.Failed:
            return "Failed";
        case SubtaskResult_Status.UNRECOGNIZED:
        default:
            return "UNRECOGNIZED";
    }
}
exports.subtaskResult_StatusToJSON = subtaskResult_StatusToJSON;
function createBaseBiliAccount() {
    return {
        isLogin: false,
        face: "",
        mid: "",
        money: "",
        uname: "",
        cookie: "",
        levelInfo: undefined,
        vipType: 0,
        vipStatus: 0,
    };
}
exports.BiliAccount = {
    encode(message, writer = minimal_1.default.Writer.create()) {
        if (message.isLogin === true) {
            writer.uint32(8).bool(message.isLogin);
        }
        if (message.face !== "") {
            writer.uint32(18).string(message.face);
        }
        if (message.mid !== "") {
            writer.uint32(26).string(message.mid);
        }
        if (message.money !== "") {
            writer.uint32(34).string(message.money);
        }
        if (message.uname !== "") {
            writer.uint32(42).string(message.uname);
        }
        if (message.cookie !== "") {
            writer.uint32(50).string(message.cookie);
        }
        if (message.levelInfo !== undefined) {
            exports.BiliAccount_LevelInfo.encode(message.levelInfo, writer.uint32(58).fork()).ldelim();
        }
        if (message.vipType !== 0) {
            writer.uint32(64).int32(message.vipType);
        }
        if (message.vipStatus !== 0) {
            writer.uint32(72).int32(message.vipStatus);
        }
        return writer;
    },
    decode(input, length) {
        const reader = input instanceof minimal_1.default.Reader ? input : minimal_1.default.Reader.create(input);
        let end = length === undefined ? reader.len : reader.pos + length;
        const message = createBaseBiliAccount();
        while (reader.pos < end) {
            const tag = reader.uint32();
            switch (tag >>> 3) {
                case 1:
                    if (tag !== 8) {
                        break;
                    }
                    message.isLogin = reader.bool();
                    continue;
                case 2:
                    if (tag !== 18) {
                        break;
                    }
                    message.face = reader.string();
                    continue;
                case 3:
                    if (tag !== 26) {
                        break;
                    }
                    message.mid = reader.string();
                    continue;
                case 4:
                    if (tag !== 34) {
                        break;
                    }
                    message.money = reader.string();
                    continue;
                case 5:
                    if (tag !== 42) {
                        break;
                    }
                    message.uname = reader.string();
                    continue;
                case 6:
                    if (tag !== 50) {
                        break;
                    }
                    message.cookie = reader.string();
                    continue;
                case 7:
                    if (tag !== 58) {
                        break;
                    }
                    message.levelInfo = exports.BiliAccount_LevelInfo.decode(reader, reader.uint32());
                    continue;
                case 8:
                    if (tag !== 64) {
                        break;
                    }
                    message.vipType = reader.int32();
                    continue;
                case 9:
                    if (tag !== 72) {
                        break;
                    }
                    message.vipStatus = reader.int32();
                    continue;
            }
            if ((tag & 7) === 4 || tag === 0) {
                break;
            }
            reader.skipType(tag & 7);
        }
        return message;
    },
    fromJSON(object) {
        return {
            isLogin: isSet(object.isLogin) ? Boolean(object.isLogin) : false,
            face: isSet(object.face) ? String(object.face) : "",
            mid: isSet(object.mid) ? String(object.mid) : "",
            money: isSet(object.money) ? String(object.money) : "",
            uname: isSet(object.uname) ? String(object.uname) : "",
            cookie: isSet(object.cookie) ? String(object.cookie) : "",
            levelInfo: isSet(object.levelInfo) ? exports.BiliAccount_LevelInfo.fromJSON(object.levelInfo) : undefined,
            vipType: isSet(object.vipType) ? biliAccount_VipTypeFromJSON(object.vipType) : 0,
            vipStatus: isSet(object.vipStatus) ? biliAccount_VipStatusFromJSON(object.vipStatus) : 0,
        };
    },
    toJSON(message) {
        const obj = {};
        message.isLogin !== undefined && (obj.isLogin = message.isLogin);
        message.face !== undefined && (obj.face = message.face);
        message.mid !== undefined && (obj.mid = message.mid);
        message.money !== undefined && (obj.money = message.money);
        message.uname !== undefined && (obj.uname = message.uname);
        message.cookie !== undefined && (obj.cookie = message.cookie);
        message.levelInfo !== undefined &&
            (obj.levelInfo = message.levelInfo ? exports.BiliAccount_LevelInfo.toJSON(message.levelInfo) : undefined);
        message.vipType !== undefined && (obj.vipType = biliAccount_VipTypeToJSON(message.vipType));
        message.vipStatus !== undefined && (obj.vipStatus = biliAccount_VipStatusToJSON(message.vipStatus));
        return obj;
    },
    create(base) {
        return exports.BiliAccount.fromPartial(base !== null && base !== void 0 ? base : {});
    },
    fromPartial(object) {
        var _a, _b, _c, _d, _e, _f, _g, _h;
        const message = createBaseBiliAccount();
        message.isLogin = (_a = object.isLogin) !== null && _a !== void 0 ? _a : false;
        message.face = (_b = object.face) !== null && _b !== void 0 ? _b : "";
        message.mid = (_c = object.mid) !== null && _c !== void 0 ? _c : "";
        message.money = (_d = object.money) !== null && _d !== void 0 ? _d : "";
        message.uname = (_e = object.uname) !== null && _e !== void 0 ? _e : "";
        message.cookie = (_f = object.cookie) !== null && _f !== void 0 ? _f : "";
        message.levelInfo = (object.levelInfo !== undefined && object.levelInfo !== null)
            ? exports.BiliAccount_LevelInfo.fromPartial(object.levelInfo)
            : undefined;
        message.vipType = (_g = object.vipType) !== null && _g !== void 0 ? _g : 0;
        message.vipStatus = (_h = object.vipStatus) !== null && _h !== void 0 ? _h : 0;
        return message;
    },
};
function createBaseBiliAccount_LevelInfo() {
    return { currentLevel: 0, currentMin: 0, currentExp: 0, nextExp: 0 };
}
exports.BiliAccount_LevelInfo = {
    encode(message, writer = minimal_1.default.Writer.create()) {
        if (message.currentLevel !== 0) {
            writer.uint32(8).int32(message.currentLevel);
        }
        if (message.currentMin !== 0) {
            writer.uint32(16).int32(message.currentMin);
        }
        if (message.currentExp !== 0) {
            writer.uint32(24).int32(message.currentExp);
        }
        if (message.nextExp !== 0) {
            writer.uint32(32).int32(message.nextExp);
        }
        return writer;
    },
    decode(input, length) {
        const reader = input instanceof minimal_1.default.Reader ? input : minimal_1.default.Reader.create(input);
        let end = length === undefined ? reader.len : reader.pos + length;
        const message = createBaseBiliAccount_LevelInfo();
        while (reader.pos < end) {
            const tag = reader.uint32();
            switch (tag >>> 3) {
                case 1:
                    if (tag !== 8) {
                        break;
                    }
                    message.currentLevel = reader.int32();
                    continue;
                case 2:
                    if (tag !== 16) {
                        break;
                    }
                    message.currentMin = reader.int32();
                    continue;
                case 3:
                    if (tag !== 24) {
                        break;
                    }
                    message.currentExp = reader.int32();
                    continue;
                case 4:
                    if (tag !== 32) {
                        break;
                    }
                    message.nextExp = reader.int32();
                    continue;
            }
            if ((tag & 7) === 4 || tag === 0) {
                break;
            }
            reader.skipType(tag & 7);
        }
        return message;
    },
    fromJSON(object) {
        return {
            currentLevel: isSet(object.currentLevel) ? Number(object.currentLevel) : 0,
            currentMin: isSet(object.currentMin) ? Number(object.currentMin) : 0,
            currentExp: isSet(object.currentExp) ? Number(object.currentExp) : 0,
            nextExp: isSet(object.nextExp) ? Number(object.nextExp) : 0,
        };
    },
    toJSON(message) {
        const obj = {};
        message.currentLevel !== undefined && (obj.currentLevel = Math.round(message.currentLevel));
        message.currentMin !== undefined && (obj.currentMin = Math.round(message.currentMin));
        message.currentExp !== undefined && (obj.currentExp = Math.round(message.currentExp));
        message.nextExp !== undefined && (obj.nextExp = Math.round(message.nextExp));
        return obj;
    },
    create(base) {
        return exports.BiliAccount_LevelInfo.fromPartial(base !== null && base !== void 0 ? base : {});
    },
    fromPartial(object) {
        var _a, _b, _c, _d;
        const message = createBaseBiliAccount_LevelInfo();
        message.currentLevel = (_a = object.currentLevel) !== null && _a !== void 0 ? _a : 0;
        message.currentMin = (_b = object.currentMin) !== null && _b !== void 0 ? _b : 0;
        message.currentExp = (_c = object.currentExp) !== null && _c !== void 0 ? _c : 0;
        message.nextExp = (_d = object.nextExp) !== null && _d !== void 0 ? _d : 0;
        return message;
    },
};
function createBaseBiliTask() {
    return { uid: "", bvid: "", recordId: "", runTasks: "", payLoad: undefined };
}
exports.BiliTask = {
    encode(message, writer = minimal_1.default.Writer.create()) {
        if (message.uid !== "") {
            writer.uint32(10).string(message.uid);
        }
        if (message.bvid !== "") {
            writer.uint32(18).string(message.bvid);
        }
        if (message.recordId !== "") {
            writer.uint32(26).string(message.recordId);
        }
        if (message.runTasks !== "") {
            writer.uint32(34).string(message.runTasks);
        }
        if (message.payLoad !== undefined) {
            exports.BiliTask_PayLoad.encode(message.payLoad, writer.uint32(42).fork()).ldelim();
        }
        return writer;
    },
    decode(input, length) {
        const reader = input instanceof minimal_1.default.Reader ? input : minimal_1.default.Reader.create(input);
        let end = length === undefined ? reader.len : reader.pos + length;
        const message = createBaseBiliTask();
        while (reader.pos < end) {
            const tag = reader.uint32();
            switch (tag >>> 3) {
                case 1:
                    if (tag !== 10) {
                        break;
                    }
                    message.uid = reader.string();
                    continue;
                case 2:
                    if (tag !== 18) {
                        break;
                    }
                    message.bvid = reader.string();
                    continue;
                case 3:
                    if (tag !== 26) {
                        break;
                    }
                    message.recordId = reader.string();
                    continue;
                case 4:
                    if (tag !== 34) {
                        break;
                    }
                    message.runTasks = reader.string();
                    continue;
                case 5:
                    if (tag !== 42) {
                        break;
                    }
                    message.payLoad = exports.BiliTask_PayLoad.decode(reader, reader.uint32());
                    continue;
            }
            if ((tag & 7) === 4 || tag === 0) {
                break;
            }
            reader.skipType(tag & 7);
        }
        return message;
    },
    fromJSON(object) {
        return {
            uid: isSet(object.uid) ? String(object.uid) : "",
            bvid: isSet(object.bvid) ? String(object.bvid) : "",
            recordId: isSet(object.recordId) ? String(object.recordId) : "",
            runTasks: isSet(object.runTasks) ? String(object.runTasks) : "",
            payLoad: isSet(object.payLoad) ? exports.BiliTask_PayLoad.fromJSON(object.payLoad) : undefined,
        };
    },
    toJSON(message) {
        const obj = {};
        message.uid !== undefined && (obj.uid = message.uid);
        message.bvid !== undefined && (obj.bvid = message.bvid);
        message.recordId !== undefined && (obj.recordId = message.recordId);
        message.runTasks !== undefined && (obj.runTasks = message.runTasks);
        message.payLoad !== undefined &&
            (obj.payLoad = message.payLoad ? exports.BiliTask_PayLoad.toJSON(message.payLoad) : undefined);
        return obj;
    },
    create(base) {
        return exports.BiliTask.fromPartial(base !== null && base !== void 0 ? base : {});
    },
    fromPartial(object) {
        var _a, _b, _c, _d;
        const message = createBaseBiliTask();
        message.uid = (_a = object.uid) !== null && _a !== void 0 ? _a : "";
        message.bvid = (_b = object.bvid) !== null && _b !== void 0 ? _b : "";
        message.recordId = (_c = object.recordId) !== null && _c !== void 0 ? _c : "";
        message.runTasks = (_d = object.runTasks) !== null && _d !== void 0 ? _d : "";
        message.payLoad = (object.payLoad !== undefined && object.payLoad !== null)
            ? exports.BiliTask_PayLoad.fromPartial(object.payLoad)
            : undefined;
        return message;
    },
};
function createBaseBiliTask_PayLoad() {
    return { type: "", extra: "", blCookies: [] };
}
exports.BiliTask_PayLoad = {
    encode(message, writer = minimal_1.default.Writer.create()) {
        if (message.type !== "") {
            writer.uint32(10).string(message.type);
        }
        if (message.extra !== "") {
            writer.uint32(18).string(message.extra);
        }
        for (const v of message.blCookies) {
            exports.BiliTask_BlCookie.encode(v, writer.uint32(26).fork()).ldelim();
        }
        return writer;
    },
    decode(input, length) {
        const reader = input instanceof minimal_1.default.Reader ? input : minimal_1.default.Reader.create(input);
        let end = length === undefined ? reader.len : reader.pos + length;
        const message = createBaseBiliTask_PayLoad();
        while (reader.pos < end) {
            const tag = reader.uint32();
            switch (tag >>> 3) {
                case 1:
                    if (tag !== 10) {
                        break;
                    }
                    message.type = reader.string();
                    continue;
                case 2:
                    if (tag !== 18) {
                        break;
                    }
                    message.extra = reader.string();
                    continue;
                case 3:
                    if (tag !== 26) {
                        break;
                    }
                    message.blCookies.push(exports.BiliTask_BlCookie.decode(reader, reader.uint32()));
                    continue;
            }
            if ((tag & 7) === 4 || tag === 0) {
                break;
            }
            reader.skipType(tag & 7);
        }
        return message;
    },
    fromJSON(object) {
        return {
            type: isSet(object.type) ? String(object.type) : "",
            extra: isSet(object.extra) ? String(object.extra) : "",
            blCookies: Array.isArray(object === null || object === void 0 ? void 0 : object.blCookies)
                ? object.blCookies.map((e) => exports.BiliTask_BlCookie.fromJSON(e))
                : [],
        };
    },
    toJSON(message) {
        const obj = {};
        message.type !== undefined && (obj.type = message.type);
        message.extra !== undefined && (obj.extra = message.extra);
        if (message.blCookies) {
            obj.blCookies = message.blCookies.map((e) => e ? exports.BiliTask_BlCookie.toJSON(e) : undefined);
        }
        else {
            obj.blCookies = [];
        }
        return obj;
    },
    create(base) {
        return exports.BiliTask_PayLoad.fromPartial(base !== null && base !== void 0 ? base : {});
    },
    fromPartial(object) {
        var _a, _b, _c;
        const message = createBaseBiliTask_PayLoad();
        message.type = (_a = object.type) !== null && _a !== void 0 ? _a : "";
        message.extra = (_b = object.extra) !== null && _b !== void 0 ? _b : "";
        message.blCookies = ((_c = object.blCookies) === null || _c === void 0 ? void 0 : _c.map((e) => exports.BiliTask_BlCookie.fromPartial(e))) || [];
        return message;
    },
};
function createBaseBiliTask_BlCookie() {
    return { snapId: "", mid: "", proxyIp: "", cookie: "", ua: "", carrier: undefined, scheduleTime: "" };
}
exports.BiliTask_BlCookie = {
    encode(message, writer = minimal_1.default.Writer.create()) {
        if (message.snapId !== "") {
            writer.uint32(10).string(message.snapId);
        }
        if (message.mid !== "") {
            writer.uint32(18).string(message.mid);
        }
        if (message.proxyIp !== "") {
            writer.uint32(26).string(message.proxyIp);
        }
        if (message.cookie !== "") {
            writer.uint32(34).string(message.cookie);
        }
        if (message.ua !== "") {
            writer.uint32(42).string(message.ua);
        }
        if (message.carrier !== undefined) {
            exports.BiliTask_Carrier.encode(message.carrier, writer.uint32(50).fork()).ldelim();
        }
        if (message.scheduleTime !== "") {
            writer.uint32(58).string(message.scheduleTime);
        }
        return writer;
    },
    decode(input, length) {
        const reader = input instanceof minimal_1.default.Reader ? input : minimal_1.default.Reader.create(input);
        let end = length === undefined ? reader.len : reader.pos + length;
        const message = createBaseBiliTask_BlCookie();
        while (reader.pos < end) {
            const tag = reader.uint32();
            switch (tag >>> 3) {
                case 1:
                    if (tag !== 10) {
                        break;
                    }
                    message.snapId = reader.string();
                    continue;
                case 2:
                    if (tag !== 18) {
                        break;
                    }
                    message.mid = reader.string();
                    continue;
                case 3:
                    if (tag !== 26) {
                        break;
                    }
                    message.proxyIp = reader.string();
                    continue;
                case 4:
                    if (tag !== 34) {
                        break;
                    }
                    message.cookie = reader.string();
                    continue;
                case 5:
                    if (tag !== 42) {
                        break;
                    }
                    message.ua = reader.string();
                    continue;
                case 6:
                    if (tag !== 50) {
                        break;
                    }
                    message.carrier = exports.BiliTask_Carrier.decode(reader, reader.uint32());
                    continue;
                case 7:
                    if (tag !== 58) {
                        break;
                    }
                    message.scheduleTime = reader.string();
                    continue;
            }
            if ((tag & 7) === 4 || tag === 0) {
                break;
            }
            reader.skipType(tag & 7);
        }
        return message;
    },
    fromJSON(object) {
        return {
            snapId: isSet(object.snapId) ? String(object.snapId) : "",
            mid: isSet(object.mid) ? String(object.mid) : "",
            proxyIp: isSet(object.proxyIp) ? String(object.proxyIp) : "",
            cookie: isSet(object.cookie) ? String(object.cookie) : "",
            ua: isSet(object.ua) ? String(object.ua) : "",
            carrier: isSet(object.carrier) ? exports.BiliTask_Carrier.fromJSON(object.carrier) : undefined,
            scheduleTime: isSet(object.scheduleTime) ? String(object.scheduleTime) : "",
        };
    },
    toJSON(message) {
        const obj = {};
        message.snapId !== undefined && (obj.snapId = message.snapId);
        message.mid !== undefined && (obj.mid = message.mid);
        message.proxyIp !== undefined && (obj.proxyIp = message.proxyIp);
        message.cookie !== undefined && (obj.cookie = message.cookie);
        message.ua !== undefined && (obj.ua = message.ua);
        message.carrier !== undefined &&
            (obj.carrier = message.carrier ? exports.BiliTask_Carrier.toJSON(message.carrier) : undefined);
        message.scheduleTime !== undefined && (obj.scheduleTime = message.scheduleTime);
        return obj;
    },
    create(base) {
        return exports.BiliTask_BlCookie.fromPartial(base !== null && base !== void 0 ? base : {});
    },
    fromPartial(object) {
        var _a, _b, _c, _d, _e, _f;
        const message = createBaseBiliTask_BlCookie();
        message.snapId = (_a = object.snapId) !== null && _a !== void 0 ? _a : "";
        message.mid = (_b = object.mid) !== null && _b !== void 0 ? _b : "";
        message.proxyIp = (_c = object.proxyIp) !== null && _c !== void 0 ? _c : "";
        message.cookie = (_d = object.cookie) !== null && _d !== void 0 ? _d : "";
        message.ua = (_e = object.ua) !== null && _e !== void 0 ? _e : "";
        message.carrier = (object.carrier !== undefined && object.carrier !== null)
            ? exports.BiliTask_Carrier.fromPartial(object.carrier)
            : undefined;
        message.scheduleTime = (_f = object.scheduleTime) !== null && _f !== void 0 ? _f : "";
        return message;
    },
};
function createBaseBiliTask_Carrier() {
    return { carryType: 0, commentWrap: undefined, danmakuWrap: undefined };
}
exports.BiliTask_Carrier = {
    encode(message, writer = minimal_1.default.Writer.create()) {
        if (message.carryType !== 0) {
            writer.uint32(8).int32(message.carryType);
        }
        if (message.commentWrap !== undefined) {
            exports.BiliTask_CommentWrap.encode(message.commentWrap, writer.uint32(18).fork()).ldelim();
        }
        if (message.danmakuWrap !== undefined) {
            exports.BiliTask_DanmakuWrap.encode(message.danmakuWrap, writer.uint32(26).fork()).ldelim();
        }
        return writer;
    },
    decode(input, length) {
        const reader = input instanceof minimal_1.default.Reader ? input : minimal_1.default.Reader.create(input);
        let end = length === undefined ? reader.len : reader.pos + length;
        const message = createBaseBiliTask_Carrier();
        while (reader.pos < end) {
            const tag = reader.uint32();
            switch (tag >>> 3) {
                case 1:
                    if (tag !== 8) {
                        break;
                    }
                    message.carryType = reader.int32();
                    continue;
                case 2:
                    if (tag !== 18) {
                        break;
                    }
                    message.commentWrap = exports.BiliTask_CommentWrap.decode(reader, reader.uint32());
                    continue;
                case 3:
                    if (tag !== 26) {
                        break;
                    }
                    message.danmakuWrap = exports.BiliTask_DanmakuWrap.decode(reader, reader.uint32());
                    continue;
            }
            if ((tag & 7) === 4 || tag === 0) {
                break;
            }
            reader.skipType(tag & 7);
        }
        return message;
    },
    fromJSON(object) {
        return {
            carryType: isSet(object.carryType) ? biliTask_CarryTypeFromJSON(object.carryType) : 0,
            commentWrap: isSet(object.commentWrap) ? exports.BiliTask_CommentWrap.fromJSON(object.commentWrap) : undefined,
            danmakuWrap: isSet(object.danmakuWrap) ? exports.BiliTask_DanmakuWrap.fromJSON(object.danmakuWrap) : undefined,
        };
    },
    toJSON(message) {
        const obj = {};
        message.carryType !== undefined && (obj.carryType = biliTask_CarryTypeToJSON(message.carryType));
        message.commentWrap !== undefined &&
            (obj.commentWrap = message.commentWrap ? exports.BiliTask_CommentWrap.toJSON(message.commentWrap) : undefined);
        message.danmakuWrap !== undefined &&
            (obj.danmakuWrap = message.danmakuWrap ? exports.BiliTask_DanmakuWrap.toJSON(message.danmakuWrap) : undefined);
        return obj;
    },
    create(base) {
        return exports.BiliTask_Carrier.fromPartial(base !== null && base !== void 0 ? base : {});
    },
    fromPartial(object) {
        var _a;
        const message = createBaseBiliTask_Carrier();
        message.carryType = (_a = object.carryType) !== null && _a !== void 0 ? _a : 0;
        message.commentWrap = (object.commentWrap !== undefined && object.commentWrap !== null)
            ? exports.BiliTask_CommentWrap.fromPartial(object.commentWrap)
            : undefined;
        message.danmakuWrap = (object.danmakuWrap !== undefined && object.danmakuWrap !== null)
            ? exports.BiliTask_DanmakuWrap.fromPartial(object.danmakuWrap)
            : undefined;
        return message;
    },
};
function createBaseBiliTask_CommentWrap() {
    return { comment: undefined, commentController: undefined };
}
exports.BiliTask_CommentWrap = {
    encode(message, writer = minimal_1.default.Writer.create()) {
        if (message.comment !== undefined) {
            exports.BiliTask_Comment.encode(message.comment, writer.uint32(10).fork()).ldelim();
        }
        if (message.commentController !== undefined) {
            exports.BiliTask_CommentController.encode(message.commentController, writer.uint32(18).fork()).ldelim();
        }
        return writer;
    },
    decode(input, length) {
        const reader = input instanceof minimal_1.default.Reader ? input : minimal_1.default.Reader.create(input);
        let end = length === undefined ? reader.len : reader.pos + length;
        const message = createBaseBiliTask_CommentWrap();
        while (reader.pos < end) {
            const tag = reader.uint32();
            switch (tag >>> 3) {
                case 1:
                    if (tag !== 10) {
                        break;
                    }
                    message.comment = exports.BiliTask_Comment.decode(reader, reader.uint32());
                    continue;
                case 2:
                    if (tag !== 18) {
                        break;
                    }
                    message.commentController = exports.BiliTask_CommentController.decode(reader, reader.uint32());
                    continue;
            }
            if ((tag & 7) === 4 || tag === 0) {
                break;
            }
            reader.skipType(tag & 7);
        }
        return message;
    },
    fromJSON(object) {
        return {
            comment: isSet(object.comment) ? exports.BiliTask_Comment.fromJSON(object.comment) : undefined,
            commentController: isSet(object.commentController)
                ? exports.BiliTask_CommentController.fromJSON(object.commentController)
                : undefined,
        };
    },
    toJSON(message) {
        const obj = {};
        message.comment !== undefined &&
            (obj.comment = message.comment ? exports.BiliTask_Comment.toJSON(message.comment) : undefined);
        message.commentController !== undefined && (obj.commentController = message.commentController
            ? exports.BiliTask_CommentController.toJSON(message.commentController)
            : undefined);
        return obj;
    },
    create(base) {
        return exports.BiliTask_CommentWrap.fromPartial(base !== null && base !== void 0 ? base : {});
    },
    fromPartial(object) {
        const message = createBaseBiliTask_CommentWrap();
        message.comment = (object.comment !== undefined && object.comment !== null)
            ? exports.BiliTask_Comment.fromPartial(object.comment)
            : undefined;
        message.commentController = (object.commentController !== undefined && object.commentController !== null)
            ? exports.BiliTask_CommentController.fromPartial(object.commentController)
            : undefined;
        return message;
    },
};
function createBaseBiliTask_DanmakuWrap() {
    return { danmaku: undefined, danmakuController: undefined };
}
exports.BiliTask_DanmakuWrap = {
    encode(message, writer = minimal_1.default.Writer.create()) {
        if (message.danmaku !== undefined) {
            exports.BiliTask_Danmaku.encode(message.danmaku, writer.uint32(10).fork()).ldelim();
        }
        if (message.danmakuController !== undefined) {
            exports.BiliTask_DanmakuController.encode(message.danmakuController, writer.uint32(18).fork()).ldelim();
        }
        return writer;
    },
    decode(input, length) {
        const reader = input instanceof minimal_1.default.Reader ? input : minimal_1.default.Reader.create(input);
        let end = length === undefined ? reader.len : reader.pos + length;
        const message = createBaseBiliTask_DanmakuWrap();
        while (reader.pos < end) {
            const tag = reader.uint32();
            switch (tag >>> 3) {
                case 1:
                    if (tag !== 10) {
                        break;
                    }
                    message.danmaku = exports.BiliTask_Danmaku.decode(reader, reader.uint32());
                    continue;
                case 2:
                    if (tag !== 18) {
                        break;
                    }
                    message.danmakuController = exports.BiliTask_DanmakuController.decode(reader, reader.uint32());
                    continue;
            }
            if ((tag & 7) === 4 || tag === 0) {
                break;
            }
            reader.skipType(tag & 7);
        }
        return message;
    },
    fromJSON(object) {
        return {
            danmaku: isSet(object.danmaku) ? exports.BiliTask_Danmaku.fromJSON(object.danmaku) : undefined,
            danmakuController: isSet(object.danmakuController)
                ? exports.BiliTask_DanmakuController.fromJSON(object.danmakuController)
                : undefined,
        };
    },
    toJSON(message) {
        const obj = {};
        message.danmaku !== undefined &&
            (obj.danmaku = message.danmaku ? exports.BiliTask_Danmaku.toJSON(message.danmaku) : undefined);
        message.danmakuController !== undefined && (obj.danmakuController = message.danmakuController
            ? exports.BiliTask_DanmakuController.toJSON(message.danmakuController)
            : undefined);
        return obj;
    },
    create(base) {
        return exports.BiliTask_DanmakuWrap.fromPartial(base !== null && base !== void 0 ? base : {});
    },
    fromPartial(object) {
        const message = createBaseBiliTask_DanmakuWrap();
        message.danmaku = (object.danmaku !== undefined && object.danmaku !== null)
            ? exports.BiliTask_Danmaku.fromPartial(object.danmaku)
            : undefined;
        message.danmakuController = (object.danmakuController !== undefined && object.danmakuController !== null)
            ? exports.BiliTask_DanmakuController.fromPartial(object.danmakuController)
            : undefined;
        return message;
    },
};
function createBaseBiliTask_Comment() {
    return { id: "", content: "" };
}
exports.BiliTask_Comment = {
    encode(message, writer = minimal_1.default.Writer.create()) {
        if (message.id !== "") {
            writer.uint32(10).string(message.id);
        }
        if (message.content !== "") {
            writer.uint32(18).string(message.content);
        }
        return writer;
    },
    decode(input, length) {
        const reader = input instanceof minimal_1.default.Reader ? input : minimal_1.default.Reader.create(input);
        let end = length === undefined ? reader.len : reader.pos + length;
        const message = createBaseBiliTask_Comment();
        while (reader.pos < end) {
            const tag = reader.uint32();
            switch (tag >>> 3) {
                case 1:
                    if (tag !== 10) {
                        break;
                    }
                    message.id = reader.string();
                    continue;
                case 2:
                    if (tag !== 18) {
                        break;
                    }
                    message.content = reader.string();
                    continue;
            }
            if ((tag & 7) === 4 || tag === 0) {
                break;
            }
            reader.skipType(tag & 7);
        }
        return message;
    },
    fromJSON(object) {
        return {
            id: isSet(object.id) ? String(object.id) : "",
            content: isSet(object.content) ? String(object.content) : "",
        };
    },
    toJSON(message) {
        const obj = {};
        message.id !== undefined && (obj.id = message.id);
        message.content !== undefined && (obj.content = message.content);
        return obj;
    },
    create(base) {
        return exports.BiliTask_Comment.fromPartial(base !== null && base !== void 0 ? base : {});
    },
    fromPartial(object) {
        var _a, _b;
        const message = createBaseBiliTask_Comment();
        message.id = (_a = object.id) !== null && _a !== void 0 ? _a : "";
        message.content = (_b = object.content) !== null && _b !== void 0 ? _b : "";
        return message;
    },
};
function createBaseBiliTask_WhiteList() {
    return { mid: "" };
}
exports.BiliTask_WhiteList = {
    encode(message, writer = minimal_1.default.Writer.create()) {
        if (message.mid !== "") {
            writer.uint32(10).string(message.mid);
        }
        return writer;
    },
    decode(input, length) {
        const reader = input instanceof minimal_1.default.Reader ? input : minimal_1.default.Reader.create(input);
        let end = length === undefined ? reader.len : reader.pos + length;
        const message = createBaseBiliTask_WhiteList();
        while (reader.pos < end) {
            const tag = reader.uint32();
            switch (tag >>> 3) {
                case 1:
                    if (tag !== 10) {
                        break;
                    }
                    message.mid = reader.string();
                    continue;
            }
            if ((tag & 7) === 4 || tag === 0) {
                break;
            }
            reader.skipType(tag & 7);
        }
        return message;
    },
    fromJSON(object) {
        return { mid: isSet(object.mid) ? String(object.mid) : "" };
    },
    toJSON(message) {
        const obj = {};
        message.mid !== undefined && (obj.mid = message.mid);
        return obj;
    },
    create(base) {
        return exports.BiliTask_WhiteList.fromPartial(base !== null && base !== void 0 ? base : {});
    },
    fromPartial(object) {
        var _a;
        const message = createBaseBiliTask_WhiteList();
        message.mid = (_a = object.mid) !== null && _a !== void 0 ? _a : "";
        return message;
    },
};
function createBaseBiliTask_CommentController() {
    return { action: "", controlTime: "", whiteList: [] };
}
exports.BiliTask_CommentController = {
    encode(message, writer = minimal_1.default.Writer.create()) {
        if (message.action !== "") {
            writer.uint32(10).string(message.action);
        }
        if (message.controlTime !== "") {
            writer.uint32(18).string(message.controlTime);
        }
        for (const v of message.whiteList) {
            exports.BiliTask_WhiteList.encode(v, writer.uint32(26).fork()).ldelim();
        }
        return writer;
    },
    decode(input, length) {
        const reader = input instanceof minimal_1.default.Reader ? input : minimal_1.default.Reader.create(input);
        let end = length === undefined ? reader.len : reader.pos + length;
        const message = createBaseBiliTask_CommentController();
        while (reader.pos < end) {
            const tag = reader.uint32();
            switch (tag >>> 3) {
                case 1:
                    if (tag !== 10) {
                        break;
                    }
                    message.action = reader.string();
                    continue;
                case 2:
                    if (tag !== 18) {
                        break;
                    }
                    message.controlTime = reader.string();
                    continue;
                case 3:
                    if (tag !== 26) {
                        break;
                    }
                    message.whiteList.push(exports.BiliTask_WhiteList.decode(reader, reader.uint32()));
                    continue;
            }
            if ((tag & 7) === 4 || tag === 0) {
                break;
            }
            reader.skipType(tag & 7);
        }
        return message;
    },
    fromJSON(object) {
        return {
            action: isSet(object.action) ? String(object.action) : "",
            controlTime: isSet(object.controlTime) ? String(object.controlTime) : "",
            whiteList: Array.isArray(object === null || object === void 0 ? void 0 : object.whiteList)
                ? object.whiteList.map((e) => exports.BiliTask_WhiteList.fromJSON(e))
                : [],
        };
    },
    toJSON(message) {
        const obj = {};
        message.action !== undefined && (obj.action = message.action);
        message.controlTime !== undefined && (obj.controlTime = message.controlTime);
        if (message.whiteList) {
            obj.whiteList = message.whiteList.map((e) => e ? exports.BiliTask_WhiteList.toJSON(e) : undefined);
        }
        else {
            obj.whiteList = [];
        }
        return obj;
    },
    create(base) {
        return exports.BiliTask_CommentController.fromPartial(base !== null && base !== void 0 ? base : {});
    },
    fromPartial(object) {
        var _a, _b, _c;
        const message = createBaseBiliTask_CommentController();
        message.action = (_a = object.action) !== null && _a !== void 0 ? _a : "";
        message.controlTime = (_b = object.controlTime) !== null && _b !== void 0 ? _b : "";
        message.whiteList = ((_c = object.whiteList) === null || _c === void 0 ? void 0 : _c.map((e) => exports.BiliTask_WhiteList.fromPartial(e))) || [];
        return message;
    },
};
function createBaseBiliTask_Danmaku() {
    return { id: "", content: "" };
}
exports.BiliTask_Danmaku = {
    encode(message, writer = minimal_1.default.Writer.create()) {
        if (message.id !== "") {
            writer.uint32(10).string(message.id);
        }
        if (message.content !== "") {
            writer.uint32(18).string(message.content);
        }
        return writer;
    },
    decode(input, length) {
        const reader = input instanceof minimal_1.default.Reader ? input : minimal_1.default.Reader.create(input);
        let end = length === undefined ? reader.len : reader.pos + length;
        const message = createBaseBiliTask_Danmaku();
        while (reader.pos < end) {
            const tag = reader.uint32();
            switch (tag >>> 3) {
                case 1:
                    if (tag !== 10) {
                        break;
                    }
                    message.id = reader.string();
                    continue;
                case 2:
                    if (tag !== 18) {
                        break;
                    }
                    message.content = reader.string();
                    continue;
            }
            if ((tag & 7) === 4 || tag === 0) {
                break;
            }
            reader.skipType(tag & 7);
        }
        return message;
    },
    fromJSON(object) {
        return {
            id: isSet(object.id) ? String(object.id) : "",
            content: isSet(object.content) ? String(object.content) : "",
        };
    },
    toJSON(message) {
        const obj = {};
        message.id !== undefined && (obj.id = message.id);
        message.content !== undefined && (obj.content = message.content);
        return obj;
    },
    create(base) {
        return exports.BiliTask_Danmaku.fromPartial(base !== null && base !== void 0 ? base : {});
    },
    fromPartial(object) {
        var _a, _b;
        const message = createBaseBiliTask_Danmaku();
        message.id = (_a = object.id) !== null && _a !== void 0 ? _a : "";
        message.content = (_b = object.content) !== null && _b !== void 0 ? _b : "";
        return message;
    },
};
function createBaseBiliTask_DanmakuController() {
    return { controlTime: "", whiteList: [] };
}
exports.BiliTask_DanmakuController = {
    encode(message, writer = minimal_1.default.Writer.create()) {
        if (message.controlTime !== "") {
            writer.uint32(10).string(message.controlTime);
        }
        for (const v of message.whiteList) {
            exports.BiliTask_WhiteList.encode(v, writer.uint32(18).fork()).ldelim();
        }
        return writer;
    },
    decode(input, length) {
        const reader = input instanceof minimal_1.default.Reader ? input : minimal_1.default.Reader.create(input);
        let end = length === undefined ? reader.len : reader.pos + length;
        const message = createBaseBiliTask_DanmakuController();
        while (reader.pos < end) {
            const tag = reader.uint32();
            switch (tag >>> 3) {
                case 1:
                    if (tag !== 10) {
                        break;
                    }
                    message.controlTime = reader.string();
                    continue;
                case 2:
                    if (tag !== 18) {
                        break;
                    }
                    message.whiteList.push(exports.BiliTask_WhiteList.decode(reader, reader.uint32()));
                    continue;
            }
            if ((tag & 7) === 4 || tag === 0) {
                break;
            }
            reader.skipType(tag & 7);
        }
        return message;
    },
    fromJSON(object) {
        return {
            controlTime: isSet(object.controlTime) ? String(object.controlTime) : "",
            whiteList: Array.isArray(object === null || object === void 0 ? void 0 : object.whiteList)
                ? object.whiteList.map((e) => exports.BiliTask_WhiteList.fromJSON(e))
                : [],
        };
    },
    toJSON(message) {
        const obj = {};
        message.controlTime !== undefined && (obj.controlTime = message.controlTime);
        if (message.whiteList) {
            obj.whiteList = message.whiteList.map((e) => e ? exports.BiliTask_WhiteList.toJSON(e) : undefined);
        }
        else {
            obj.whiteList = [];
        }
        return obj;
    },
    create(base) {
        return exports.BiliTask_DanmakuController.fromPartial(base !== null && base !== void 0 ? base : {});
    },
    fromPartial(object) {
        var _a, _b;
        const message = createBaseBiliTask_DanmakuController();
        message.controlTime = (_a = object.controlTime) !== null && _a !== void 0 ? _a : "";
        message.whiteList = ((_b = object.whiteList) === null || _b === void 0 ? void 0 : _b.map((e) => exports.BiliTask_WhiteList.fromPartial(e))) || [];
        return message;
    },
};
function createBaseHittedComment() {
    return { uid: "", bvid: "", rpid: "", mid: "", oid: "", content: "", uname: "", ctime: "", recordId: "" };
}
exports.HittedComment = {
    encode(message, writer = minimal_1.default.Writer.create()) {
        if (message.uid !== "") {
            writer.uint32(10).string(message.uid);
        }
        if (message.bvid !== "") {
            writer.uint32(18).string(message.bvid);
        }
        if (message.rpid !== "") {
            writer.uint32(26).string(message.rpid);
        }
        if (message.mid !== "") {
            writer.uint32(34).string(message.mid);
        }
        if (message.oid !== "") {
            writer.uint32(42).string(message.oid);
        }
        if (message.content !== "") {
            writer.uint32(50).string(message.content);
        }
        if (message.uname !== "") {
            writer.uint32(58).string(message.uname);
        }
        if (message.ctime !== "") {
            writer.uint32(66).string(message.ctime);
        }
        if (message.recordId !== "") {
            writer.uint32(74).string(message.recordId);
        }
        return writer;
    },
    decode(input, length) {
        const reader = input instanceof minimal_1.default.Reader ? input : minimal_1.default.Reader.create(input);
        let end = length === undefined ? reader.len : reader.pos + length;
        const message = createBaseHittedComment();
        while (reader.pos < end) {
            const tag = reader.uint32();
            switch (tag >>> 3) {
                case 1:
                    if (tag !== 10) {
                        break;
                    }
                    message.uid = reader.string();
                    continue;
                case 2:
                    if (tag !== 18) {
                        break;
                    }
                    message.bvid = reader.string();
                    continue;
                case 3:
                    if (tag !== 26) {
                        break;
                    }
                    message.rpid = reader.string();
                    continue;
                case 4:
                    if (tag !== 34) {
                        break;
                    }
                    message.mid = reader.string();
                    continue;
                case 5:
                    if (tag !== 42) {
                        break;
                    }
                    message.oid = reader.string();
                    continue;
                case 6:
                    if (tag !== 50) {
                        break;
                    }
                    message.content = reader.string();
                    continue;
                case 7:
                    if (tag !== 58) {
                        break;
                    }
                    message.uname = reader.string();
                    continue;
                case 8:
                    if (tag !== 66) {
                        break;
                    }
                    message.ctime = reader.string();
                    continue;
                case 9:
                    if (tag !== 74) {
                        break;
                    }
                    message.recordId = reader.string();
                    continue;
            }
            if ((tag & 7) === 4 || tag === 0) {
                break;
            }
            reader.skipType(tag & 7);
        }
        return message;
    },
    fromJSON(object) {
        return {
            uid: isSet(object.uid) ? String(object.uid) : "",
            bvid: isSet(object.bvid) ? String(object.bvid) : "",
            rpid: isSet(object.rpid) ? String(object.rpid) : "",
            mid: isSet(object.mid) ? String(object.mid) : "",
            oid: isSet(object.oid) ? String(object.oid) : "",
            content: isSet(object.content) ? String(object.content) : "",
            uname: isSet(object.uname) ? String(object.uname) : "",
            ctime: isSet(object.ctime) ? String(object.ctime) : "",
            recordId: isSet(object.recordId) ? String(object.recordId) : "",
        };
    },
    toJSON(message) {
        const obj = {};
        message.uid !== undefined && (obj.uid = message.uid);
        message.bvid !== undefined && (obj.bvid = message.bvid);
        message.rpid !== undefined && (obj.rpid = message.rpid);
        message.mid !== undefined && (obj.mid = message.mid);
        message.oid !== undefined && (obj.oid = message.oid);
        message.content !== undefined && (obj.content = message.content);
        message.uname !== undefined && (obj.uname = message.uname);
        message.ctime !== undefined && (obj.ctime = message.ctime);
        message.recordId !== undefined && (obj.recordId = message.recordId);
        return obj;
    },
    create(base) {
        return exports.HittedComment.fromPartial(base !== null && base !== void 0 ? base : {});
    },
    fromPartial(object) {
        var _a, _b, _c, _d, _e, _f, _g, _h, _j;
        const message = createBaseHittedComment();
        message.uid = (_a = object.uid) !== null && _a !== void 0 ? _a : "";
        message.bvid = (_b = object.bvid) !== null && _b !== void 0 ? _b : "";
        message.rpid = (_c = object.rpid) !== null && _c !== void 0 ? _c : "";
        message.mid = (_d = object.mid) !== null && _d !== void 0 ? _d : "";
        message.oid = (_e = object.oid) !== null && _e !== void 0 ? _e : "";
        message.content = (_f = object.content) !== null && _f !== void 0 ? _f : "";
        message.uname = (_g = object.uname) !== null && _g !== void 0 ? _g : "";
        message.ctime = (_h = object.ctime) !== null && _h !== void 0 ? _h : "";
        message.recordId = (_j = object.recordId) !== null && _j !== void 0 ? _j : "";
        return message;
    },
};
function createBaseHittedDanmaku() {
    return { uid: "", bvid: "", dmid: "", mid: "", oid: "", content: "", uname: "", ctime: "", recordId: "" };
}
exports.HittedDanmaku = {
    encode(message, writer = minimal_1.default.Writer.create()) {
        if (message.uid !== "") {
            writer.uint32(10).string(message.uid);
        }
        if (message.bvid !== "") {
            writer.uint32(18).string(message.bvid);
        }
        if (message.dmid !== "") {
            writer.uint32(26).string(message.dmid);
        }
        if (message.mid !== "") {
            writer.uint32(34).string(message.mid);
        }
        if (message.oid !== "") {
            writer.uint32(42).string(message.oid);
        }
        if (message.content !== "") {
            writer.uint32(50).string(message.content);
        }
        if (message.uname !== "") {
            writer.uint32(58).string(message.uname);
        }
        if (message.ctime !== "") {
            writer.uint32(66).string(message.ctime);
        }
        if (message.recordId !== "") {
            writer.uint32(74).string(message.recordId);
        }
        return writer;
    },
    decode(input, length) {
        const reader = input instanceof minimal_1.default.Reader ? input : minimal_1.default.Reader.create(input);
        let end = length === undefined ? reader.len : reader.pos + length;
        const message = createBaseHittedDanmaku();
        while (reader.pos < end) {
            const tag = reader.uint32();
            switch (tag >>> 3) {
                case 1:
                    if (tag !== 10) {
                        break;
                    }
                    message.uid = reader.string();
                    continue;
                case 2:
                    if (tag !== 18) {
                        break;
                    }
                    message.bvid = reader.string();
                    continue;
                case 3:
                    if (tag !== 26) {
                        break;
                    }
                    message.dmid = reader.string();
                    continue;
                case 4:
                    if (tag !== 34) {
                        break;
                    }
                    message.mid = reader.string();
                    continue;
                case 5:
                    if (tag !== 42) {
                        break;
                    }
                    message.oid = reader.string();
                    continue;
                case 6:
                    if (tag !== 50) {
                        break;
                    }
                    message.content = reader.string();
                    continue;
                case 7:
                    if (tag !== 58) {
                        break;
                    }
                    message.uname = reader.string();
                    continue;
                case 8:
                    if (tag !== 66) {
                        break;
                    }
                    message.ctime = reader.string();
                    continue;
                case 9:
                    if (tag !== 74) {
                        break;
                    }
                    message.recordId = reader.string();
                    continue;
            }
            if ((tag & 7) === 4 || tag === 0) {
                break;
            }
            reader.skipType(tag & 7);
        }
        return message;
    },
    fromJSON(object) {
        return {
            uid: isSet(object.uid) ? String(object.uid) : "",
            bvid: isSet(object.bvid) ? String(object.bvid) : "",
            dmid: isSet(object.dmid) ? String(object.dmid) : "",
            mid: isSet(object.mid) ? String(object.mid) : "",
            oid: isSet(object.oid) ? String(object.oid) : "",
            content: isSet(object.content) ? String(object.content) : "",
            uname: isSet(object.uname) ? String(object.uname) : "",
            ctime: isSet(object.ctime) ? String(object.ctime) : "",
            recordId: isSet(object.recordId) ? String(object.recordId) : "",
        };
    },
    toJSON(message) {
        const obj = {};
        message.uid !== undefined && (obj.uid = message.uid);
        message.bvid !== undefined && (obj.bvid = message.bvid);
        message.dmid !== undefined && (obj.dmid = message.dmid);
        message.mid !== undefined && (obj.mid = message.mid);
        message.oid !== undefined && (obj.oid = message.oid);
        message.content !== undefined && (obj.content = message.content);
        message.uname !== undefined && (obj.uname = message.uname);
        message.ctime !== undefined && (obj.ctime = message.ctime);
        message.recordId !== undefined && (obj.recordId = message.recordId);
        return obj;
    },
    create(base) {
        return exports.HittedDanmaku.fromPartial(base !== null && base !== void 0 ? base : {});
    },
    fromPartial(object) {
        var _a, _b, _c, _d, _e, _f, _g, _h, _j;
        const message = createBaseHittedDanmaku();
        message.uid = (_a = object.uid) !== null && _a !== void 0 ? _a : "";
        message.bvid = (_b = object.bvid) !== null && _b !== void 0 ? _b : "";
        message.dmid = (_c = object.dmid) !== null && _c !== void 0 ? _c : "";
        message.mid = (_d = object.mid) !== null && _d !== void 0 ? _d : "";
        message.oid = (_e = object.oid) !== null && _e !== void 0 ? _e : "";
        message.content = (_f = object.content) !== null && _f !== void 0 ? _f : "";
        message.uname = (_g = object.uname) !== null && _g !== void 0 ? _g : "";
        message.ctime = (_h = object.ctime) !== null && _h !== void 0 ? _h : "";
        message.recordId = (_j = object.recordId) !== null && _j !== void 0 ? _j : "";
        return message;
    },
};
function createBaseSubtaskResult() {
    return { mid: "", snapId: "", recordId: "", status: 0 };
}
exports.SubtaskResult = {
    encode(message, writer = minimal_1.default.Writer.create()) {
        if (message.mid !== "") {
            writer.uint32(10).string(message.mid);
        }
        if (message.snapId !== "") {
            writer.uint32(18).string(message.snapId);
        }
        if (message.recordId !== "") {
            writer.uint32(26).string(message.recordId);
        }
        if (message.status !== 0) {
            writer.uint32(32).int32(message.status);
        }
        return writer;
    },
    decode(input, length) {
        const reader = input instanceof minimal_1.default.Reader ? input : minimal_1.default.Reader.create(input);
        let end = length === undefined ? reader.len : reader.pos + length;
        const message = createBaseSubtaskResult();
        while (reader.pos < end) {
            const tag = reader.uint32();
            switch (tag >>> 3) {
                case 1:
                    if (tag !== 10) {
                        break;
                    }
                    message.mid = reader.string();
                    continue;
                case 2:
                    if (tag !== 18) {
                        break;
                    }
                    message.snapId = reader.string();
                    continue;
                case 3:
                    if (tag !== 26) {
                        break;
                    }
                    message.recordId = reader.string();
                    continue;
                case 4:
                    if (tag !== 32) {
                        break;
                    }
                    message.status = reader.int32();
                    continue;
            }
            if ((tag & 7) === 4 || tag === 0) {
                break;
            }
            reader.skipType(tag & 7);
        }
        return message;
    },
    fromJSON(object) {
        return {
            mid: isSet(object.mid) ? String(object.mid) : "",
            snapId: isSet(object.snapId) ? String(object.snapId) : "",
            recordId: isSet(object.recordId) ? String(object.recordId) : "",
            status: isSet(object.status) ? subtaskResult_StatusFromJSON(object.status) : 0,
        };
    },
    toJSON(message) {
        const obj = {};
        message.mid !== undefined && (obj.mid = message.mid);
        message.snapId !== undefined && (obj.snapId = message.snapId);
        message.recordId !== undefined && (obj.recordId = message.recordId);
        message.status !== undefined && (obj.status = subtaskResult_StatusToJSON(message.status));
        return obj;
    },
    create(base) {
        return exports.SubtaskResult.fromPartial(base !== null && base !== void 0 ? base : {});
    },
    fromPartial(object) {
        var _a, _b, _c, _d;
        const message = createBaseSubtaskResult();
        message.mid = (_a = object.mid) !== null && _a !== void 0 ? _a : "";
        message.snapId = (_b = object.snapId) !== null && _b !== void 0 ? _b : "";
        message.recordId = (_c = object.recordId) !== null && _c !== void 0 ? _c : "";
        message.status = (_d = object.status) !== null && _d !== void 0 ? _d : 0;
        return message;
    },
};
function createBaseSyncQrCodeLoginRequest() {
    return { code: 0, message: "" };
}
exports.SyncQrCodeLoginRequest = {
    encode(message, writer = minimal_1.default.Writer.create()) {
        if (message.code !== 0) {
            writer.uint32(8).int32(message.code);
        }
        if (message.message !== "") {
            writer.uint32(18).string(message.message);
        }
        return writer;
    },
    decode(input, length) {
        const reader = input instanceof minimal_1.default.Reader ? input : minimal_1.default.Reader.create(input);
        let end = length === undefined ? reader.len : reader.pos + length;
        const message = createBaseSyncQrCodeLoginRequest();
        while (reader.pos < end) {
            const tag = reader.uint32();
            switch (tag >>> 3) {
                case 1:
                    if (tag !== 8) {
                        break;
                    }
                    message.code = reader.int32();
                    continue;
                case 2:
                    if (tag !== 18) {
                        break;
                    }
                    message.message = reader.string();
                    continue;
            }
            if ((tag & 7) === 4 || tag === 0) {
                break;
            }
            reader.skipType(tag & 7);
        }
        return message;
    },
    fromJSON(object) {
        return {
            code: isSet(object.code) ? Number(object.code) : 0,
            message: isSet(object.message) ? String(object.message) : "",
        };
    },
    toJSON(message) {
        const obj = {};
        message.code !== undefined && (obj.code = Math.round(message.code));
        message.message !== undefined && (obj.message = message.message);
        return obj;
    },
    create(base) {
        return exports.SyncQrCodeLoginRequest.fromPartial(base !== null && base !== void 0 ? base : {});
    },
    fromPartial(object) {
        var _a, _b;
        const message = createBaseSyncQrCodeLoginRequest();
        message.code = (_a = object.code) !== null && _a !== void 0 ? _a : 0;
        message.message = (_b = object.message) !== null && _b !== void 0 ? _b : "";
        return message;
    },
};
function createBaseSyncQrCodeLoginResponse() {
    return { reply: "" };
}
exports.SyncQrCodeLoginResponse = {
    encode(message, writer = minimal_1.default.Writer.create()) {
        if (message.reply !== "") {
            writer.uint32(10).string(message.reply);
        }
        return writer;
    },
    decode(input, length) {
        const reader = input instanceof minimal_1.default.Reader ? input : minimal_1.default.Reader.create(input);
        let end = length === undefined ? reader.len : reader.pos + length;
        const message = createBaseSyncQrCodeLoginResponse();
        while (reader.pos < end) {
            const tag = reader.uint32();
            switch (tag >>> 3) {
                case 1:
                    if (tag !== 10) {
                        break;
                    }
                    message.reply = reader.string();
                    continue;
            }
            if ((tag & 7) === 4 || tag === 0) {
                break;
            }
            reader.skipType(tag & 7);
        }
        return message;
    },
    fromJSON(object) {
        return { reply: isSet(object.reply) ? String(object.reply) : "" };
    },
    toJSON(message) {
        const obj = {};
        message.reply !== undefined && (obj.reply = message.reply);
        return obj;
    },
    create(base) {
        return exports.SyncQrCodeLoginResponse.fromPartial(base !== null && base !== void 0 ? base : {});
    },
    fromPartial(object) {
        var _a;
        const message = createBaseSyncQrCodeLoginResponse();
        message.reply = (_a = object.reply) !== null && _a !== void 0 ? _a : "";
        return message;
    },
};
function createBaseSyncHittedCommentRequest() {
    return { token: "", action: "", hittedComment: undefined };
}
exports.SyncHittedCommentRequest = {
    encode(message, writer = minimal_1.default.Writer.create()) {
        if (message.token !== "") {
            writer.uint32(10).string(message.token);
        }
        if (message.action !== "") {
            writer.uint32(18).string(message.action);
        }
        if (message.hittedComment !== undefined) {
            exports.HittedComment.encode(message.hittedComment, writer.uint32(26).fork()).ldelim();
        }
        return writer;
    },
    decode(input, length) {
        const reader = input instanceof minimal_1.default.Reader ? input : minimal_1.default.Reader.create(input);
        let end = length === undefined ? reader.len : reader.pos + length;
        const message = createBaseSyncHittedCommentRequest();
        while (reader.pos < end) {
            const tag = reader.uint32();
            switch (tag >>> 3) {
                case 1:
                    if (tag !== 10) {
                        break;
                    }
                    message.token = reader.string();
                    continue;
                case 2:
                    if (tag !== 18) {
                        break;
                    }
                    message.action = reader.string();
                    continue;
                case 3:
                    if (tag !== 26) {
                        break;
                    }
                    message.hittedComment = exports.HittedComment.decode(reader, reader.uint32());
                    continue;
            }
            if ((tag & 7) === 4 || tag === 0) {
                break;
            }
            reader.skipType(tag & 7);
        }
        return message;
    },
    fromJSON(object) {
        return {
            token: isSet(object.token) ? String(object.token) : "",
            action: isSet(object.action) ? String(object.action) : "",
            hittedComment: isSet(object.hittedComment) ? exports.HittedComment.fromJSON(object.hittedComment) : undefined,
        };
    },
    toJSON(message) {
        const obj = {};
        message.token !== undefined && (obj.token = message.token);
        message.action !== undefined && (obj.action = message.action);
        message.hittedComment !== undefined &&
            (obj.hittedComment = message.hittedComment ? exports.HittedComment.toJSON(message.hittedComment) : undefined);
        return obj;
    },
    create(base) {
        return exports.SyncHittedCommentRequest.fromPartial(base !== null && base !== void 0 ? base : {});
    },
    fromPartial(object) {
        var _a, _b;
        const message = createBaseSyncHittedCommentRequest();
        message.token = (_a = object.token) !== null && _a !== void 0 ? _a : "";
        message.action = (_b = object.action) !== null && _b !== void 0 ? _b : "";
        message.hittedComment = (object.hittedComment !== undefined && object.hittedComment !== null)
            ? exports.HittedComment.fromPartial(object.hittedComment)
            : undefined;
        return message;
    },
};
function createBaseSyncHittedCommentResponse() {
    return { reply: "" };
}
exports.SyncHittedCommentResponse = {
    encode(message, writer = minimal_1.default.Writer.create()) {
        if (message.reply !== "") {
            writer.uint32(10).string(message.reply);
        }
        return writer;
    },
    decode(input, length) {
        const reader = input instanceof minimal_1.default.Reader ? input : minimal_1.default.Reader.create(input);
        let end = length === undefined ? reader.len : reader.pos + length;
        const message = createBaseSyncHittedCommentResponse();
        while (reader.pos < end) {
            const tag = reader.uint32();
            switch (tag >>> 3) {
                case 1:
                    if (tag !== 10) {
                        break;
                    }
                    message.reply = reader.string();
                    continue;
            }
            if ((tag & 7) === 4 || tag === 0) {
                break;
            }
            reader.skipType(tag & 7);
        }
        return message;
    },
    fromJSON(object) {
        return { reply: isSet(object.reply) ? String(object.reply) : "" };
    },
    toJSON(message) {
        const obj = {};
        message.reply !== undefined && (obj.reply = message.reply);
        return obj;
    },
    create(base) {
        return exports.SyncHittedCommentResponse.fromPartial(base !== null && base !== void 0 ? base : {});
    },
    fromPartial(object) {
        var _a;
        const message = createBaseSyncHittedCommentResponse();
        message.reply = (_a = object.reply) !== null && _a !== void 0 ? _a : "";
        return message;
    },
};
function createBaseSyncHittedDanmakuRequest() {
    return { hittedDanmaku: undefined };
}
exports.SyncHittedDanmakuRequest = {
    encode(message, writer = minimal_1.default.Writer.create()) {
        if (message.hittedDanmaku !== undefined) {
            exports.HittedDanmaku.encode(message.hittedDanmaku, writer.uint32(10).fork()).ldelim();
        }
        return writer;
    },
    decode(input, length) {
        const reader = input instanceof minimal_1.default.Reader ? input : minimal_1.default.Reader.create(input);
        let end = length === undefined ? reader.len : reader.pos + length;
        const message = createBaseSyncHittedDanmakuRequest();
        while (reader.pos < end) {
            const tag = reader.uint32();
            switch (tag >>> 3) {
                case 1:
                    if (tag !== 10) {
                        break;
                    }
                    message.hittedDanmaku = exports.HittedDanmaku.decode(reader, reader.uint32());
                    continue;
            }
            if ((tag & 7) === 4 || tag === 0) {
                break;
            }
            reader.skipType(tag & 7);
        }
        return message;
    },
    fromJSON(object) {
        return { hittedDanmaku: isSet(object.hittedDanmaku) ? exports.HittedDanmaku.fromJSON(object.hittedDanmaku) : undefined };
    },
    toJSON(message) {
        const obj = {};
        message.hittedDanmaku !== undefined &&
            (obj.hittedDanmaku = message.hittedDanmaku ? exports.HittedDanmaku.toJSON(message.hittedDanmaku) : undefined);
        return obj;
    },
    create(base) {
        return exports.SyncHittedDanmakuRequest.fromPartial(base !== null && base !== void 0 ? base : {});
    },
    fromPartial(object) {
        const message = createBaseSyncHittedDanmakuRequest();
        message.hittedDanmaku = (object.hittedDanmaku !== undefined && object.hittedDanmaku !== null)
            ? exports.HittedDanmaku.fromPartial(object.hittedDanmaku)
            : undefined;
        return message;
    },
};
function createBaseSyncHittedDanmakuResponse() {
    return { reply: "" };
}
exports.SyncHittedDanmakuResponse = {
    encode(message, writer = minimal_1.default.Writer.create()) {
        if (message.reply !== "") {
            writer.uint32(10).string(message.reply);
        }
        return writer;
    },
    decode(input, length) {
        const reader = input instanceof minimal_1.default.Reader ? input : minimal_1.default.Reader.create(input);
        let end = length === undefined ? reader.len : reader.pos + length;
        const message = createBaseSyncHittedDanmakuResponse();
        while (reader.pos < end) {
            const tag = reader.uint32();
            switch (tag >>> 3) {
                case 1:
                    if (tag !== 10) {
                        break;
                    }
                    message.reply = reader.string();
                    continue;
            }
            if ((tag & 7) === 4 || tag === 0) {
                break;
            }
            reader.skipType(tag & 7);
        }
        return message;
    },
    fromJSON(object) {
        return { reply: isSet(object.reply) ? String(object.reply) : "" };
    },
    toJSON(message) {
        const obj = {};
        message.reply !== undefined && (obj.reply = message.reply);
        return obj;
    },
    create(base) {
        return exports.SyncHittedDanmakuResponse.fromPartial(base !== null && base !== void 0 ? base : {});
    },
    fromPartial(object) {
        var _a;
        const message = createBaseSyncHittedDanmakuResponse();
        message.reply = (_a = object.reply) !== null && _a !== void 0 ? _a : "";
        return message;
    },
};
function createBaseSyncBiliAccountRequest() {
    return { token: "", biliAccount: undefined };
}
exports.SyncBiliAccountRequest = {
    encode(message, writer = minimal_1.default.Writer.create()) {
        if (message.token !== "") {
            writer.uint32(10).string(message.token);
        }
        if (message.biliAccount !== undefined) {
            exports.BiliAccount.encode(message.biliAccount, writer.uint32(18).fork()).ldelim();
        }
        return writer;
    },
    decode(input, length) {
        const reader = input instanceof minimal_1.default.Reader ? input : minimal_1.default.Reader.create(input);
        let end = length === undefined ? reader.len : reader.pos + length;
        const message = createBaseSyncBiliAccountRequest();
        while (reader.pos < end) {
            const tag = reader.uint32();
            switch (tag >>> 3) {
                case 1:
                    if (tag !== 10) {
                        break;
                    }
                    message.token = reader.string();
                    continue;
                case 2:
                    if (tag !== 18) {
                        break;
                    }
                    message.biliAccount = exports.BiliAccount.decode(reader, reader.uint32());
                    continue;
            }
            if ((tag & 7) === 4 || tag === 0) {
                break;
            }
            reader.skipType(tag & 7);
        }
        return message;
    },
    fromJSON(object) {
        return {
            token: isSet(object.token) ? String(object.token) : "",
            biliAccount: isSet(object.biliAccount) ? exports.BiliAccount.fromJSON(object.biliAccount) : undefined,
        };
    },
    toJSON(message) {
        const obj = {};
        message.token !== undefined && (obj.token = message.token);
        message.biliAccount !== undefined &&
            (obj.biliAccount = message.biliAccount ? exports.BiliAccount.toJSON(message.biliAccount) : undefined);
        return obj;
    },
    create(base) {
        return exports.SyncBiliAccountRequest.fromPartial(base !== null && base !== void 0 ? base : {});
    },
    fromPartial(object) {
        var _a;
        const message = createBaseSyncBiliAccountRequest();
        message.token = (_a = object.token) !== null && _a !== void 0 ? _a : "";
        message.biliAccount = (object.biliAccount !== undefined && object.biliAccount !== null)
            ? exports.BiliAccount.fromPartial(object.biliAccount)
            : undefined;
        return message;
    },
};
function createBaseSyncBiliAccountResponse() {
    return { reply: "" };
}
exports.SyncBiliAccountResponse = {
    encode(message, writer = minimal_1.default.Writer.create()) {
        if (message.reply !== "") {
            writer.uint32(10).string(message.reply);
        }
        return writer;
    },
    decode(input, length) {
        const reader = input instanceof minimal_1.default.Reader ? input : minimal_1.default.Reader.create(input);
        let end = length === undefined ? reader.len : reader.pos + length;
        const message = createBaseSyncBiliAccountResponse();
        while (reader.pos < end) {
            const tag = reader.uint32();
            switch (tag >>> 3) {
                case 1:
                    if (tag !== 10) {
                        break;
                    }
                    message.reply = reader.string();
                    continue;
            }
            if ((tag & 7) === 4 || tag === 0) {
                break;
            }
            reader.skipType(tag & 7);
        }
        return message;
    },
    fromJSON(object) {
        return { reply: isSet(object.reply) ? String(object.reply) : "" };
    },
    toJSON(message) {
        const obj = {};
        message.reply !== undefined && (obj.reply = message.reply);
        return obj;
    },
    create(base) {
        return exports.SyncBiliAccountResponse.fromPartial(base !== null && base !== void 0 ? base : {});
    },
    fromPartial(object) {
        var _a;
        const message = createBaseSyncBiliAccountResponse();
        message.reply = (_a = object.reply) !== null && _a !== void 0 ? _a : "";
        return message;
    },
};
function createBaseBiliTaskRequest() {
    return { token: "", recordId: "", runTasks: "", payLoad: undefined, runTaskStatus: 0 };
}
exports.BiliTaskRequest = {
    encode(message, writer = minimal_1.default.Writer.create()) {
        if (message.token !== "") {
            writer.uint32(10).string(message.token);
        }
        if (message.recordId !== "") {
            writer.uint32(18).string(message.recordId);
        }
        if (message.runTasks !== "") {
            writer.uint32(26).string(message.runTasks);
        }
        if (message.payLoad !== undefined) {
            exports.BiliTaskRequest_PayLoad.encode(message.payLoad, writer.uint32(34).fork()).ldelim();
        }
        if (message.runTaskStatus !== 0) {
            writer.uint32(40).int32(message.runTaskStatus);
        }
        return writer;
    },
    decode(input, length) {
        const reader = input instanceof minimal_1.default.Reader ? input : minimal_1.default.Reader.create(input);
        let end = length === undefined ? reader.len : reader.pos + length;
        const message = createBaseBiliTaskRequest();
        while (reader.pos < end) {
            const tag = reader.uint32();
            switch (tag >>> 3) {
                case 1:
                    if (tag !== 10) {
                        break;
                    }
                    message.token = reader.string();
                    continue;
                case 2:
                    if (tag !== 18) {
                        break;
                    }
                    message.recordId = reader.string();
                    continue;
                case 3:
                    if (tag !== 26) {
                        break;
                    }
                    message.runTasks = reader.string();
                    continue;
                case 4:
                    if (tag !== 34) {
                        break;
                    }
                    message.payLoad = exports.BiliTaskRequest_PayLoad.decode(reader, reader.uint32());
                    continue;
                case 5:
                    if (tag !== 40) {
                        break;
                    }
                    message.runTaskStatus = reader.int32();
                    continue;
            }
            if ((tag & 7) === 4 || tag === 0) {
                break;
            }
            reader.skipType(tag & 7);
        }
        return message;
    },
    fromJSON(object) {
        return {
            token: isSet(object.token) ? String(object.token) : "",
            recordId: isSet(object.recordId) ? String(object.recordId) : "",
            runTasks: isSet(object.runTasks) ? String(object.runTasks) : "",
            payLoad: isSet(object.payLoad) ? exports.BiliTaskRequest_PayLoad.fromJSON(object.payLoad) : undefined,
            runTaskStatus: isSet(object.runTaskStatus) ? runTaskStatusFromJSON(object.runTaskStatus) : 0,
        };
    },
    toJSON(message) {
        const obj = {};
        message.token !== undefined && (obj.token = message.token);
        message.recordId !== undefined && (obj.recordId = message.recordId);
        message.runTasks !== undefined && (obj.runTasks = message.runTasks);
        message.payLoad !== undefined &&
            (obj.payLoad = message.payLoad ? exports.BiliTaskRequest_PayLoad.toJSON(message.payLoad) : undefined);
        message.runTaskStatus !== undefined && (obj.runTaskStatus = runTaskStatusToJSON(message.runTaskStatus));
        return obj;
    },
    create(base) {
        return exports.BiliTaskRequest.fromPartial(base !== null && base !== void 0 ? base : {});
    },
    fromPartial(object) {
        var _a, _b, _c, _d;
        const message = createBaseBiliTaskRequest();
        message.token = (_a = object.token) !== null && _a !== void 0 ? _a : "";
        message.recordId = (_b = object.recordId) !== null && _b !== void 0 ? _b : "";
        message.runTasks = (_c = object.runTasks) !== null && _c !== void 0 ? _c : "";
        message.payLoad = (object.payLoad !== undefined && object.payLoad !== null)
            ? exports.BiliTaskRequest_PayLoad.fromPartial(object.payLoad)
            : undefined;
        message.runTaskStatus = (_d = object.runTaskStatus) !== null && _d !== void 0 ? _d : 0;
        return message;
    },
};
function createBaseBiliTaskRequest_PayLoad() {
    return { type: "", subtaskResult: undefined };
}
exports.BiliTaskRequest_PayLoad = {
    encode(message, writer = minimal_1.default.Writer.create()) {
        if (message.type !== "") {
            writer.uint32(10).string(message.type);
        }
        if (message.subtaskResult !== undefined) {
            exports.SubtaskResult.encode(message.subtaskResult, writer.uint32(18).fork()).ldelim();
        }
        return writer;
    },
    decode(input, length) {
        const reader = input instanceof minimal_1.default.Reader ? input : minimal_1.default.Reader.create(input);
        let end = length === undefined ? reader.len : reader.pos + length;
        const message = createBaseBiliTaskRequest_PayLoad();
        while (reader.pos < end) {
            const tag = reader.uint32();
            switch (tag >>> 3) {
                case 1:
                    if (tag !== 10) {
                        break;
                    }
                    message.type = reader.string();
                    continue;
                case 2:
                    if (tag !== 18) {
                        break;
                    }
                    message.subtaskResult = exports.SubtaskResult.decode(reader, reader.uint32());
                    continue;
            }
            if ((tag & 7) === 4 || tag === 0) {
                break;
            }
            reader.skipType(tag & 7);
        }
        return message;
    },
    fromJSON(object) {
        return {
            type: isSet(object.type) ? String(object.type) : "",
            subtaskResult: isSet(object.subtaskResult) ? exports.SubtaskResult.fromJSON(object.subtaskResult) : undefined,
        };
    },
    toJSON(message) {
        const obj = {};
        message.type !== undefined && (obj.type = message.type);
        message.subtaskResult !== undefined &&
            (obj.subtaskResult = message.subtaskResult ? exports.SubtaskResult.toJSON(message.subtaskResult) : undefined);
        return obj;
    },
    create(base) {
        return exports.BiliTaskRequest_PayLoad.fromPartial(base !== null && base !== void 0 ? base : {});
    },
    fromPartial(object) {
        var _a;
        const message = createBaseBiliTaskRequest_PayLoad();
        message.type = (_a = object.type) !== null && _a !== void 0 ? _a : "";
        message.subtaskResult = (object.subtaskResult !== undefined && object.subtaskResult !== null)
            ? exports.SubtaskResult.fromPartial(object.subtaskResult)
            : undefined;
        return message;
    },
};
function createBaseBiliTaskResponse() {
    return { command: "", reply: "", biliTask: undefined };
}
exports.BiliTaskResponse = {
    encode(message, writer = minimal_1.default.Writer.create()) {
        if (message.command !== "") {
            writer.uint32(10).string(message.command);
        }
        if (message.reply !== "") {
            writer.uint32(18).string(message.reply);
        }
        if (message.biliTask !== undefined) {
            exports.BiliTask.encode(message.biliTask, writer.uint32(26).fork()).ldelim();
        }
        return writer;
    },
    decode(input, length) {
        const reader = input instanceof minimal_1.default.Reader ? input : minimal_1.default.Reader.create(input);
        let end = length === undefined ? reader.len : reader.pos + length;
        const message = createBaseBiliTaskResponse();
        while (reader.pos < end) {
            const tag = reader.uint32();
            switch (tag >>> 3) {
                case 1:
                    if (tag !== 10) {
                        break;
                    }
                    message.command = reader.string();
                    continue;
                case 2:
                    if (tag !== 18) {
                        break;
                    }
                    message.reply = reader.string();
                    continue;
                case 3:
                    if (tag !== 26) {
                        break;
                    }
                    message.biliTask = exports.BiliTask.decode(reader, reader.uint32());
                    continue;
            }
            if ((tag & 7) === 4 || tag === 0) {
                break;
            }
            reader.skipType(tag & 7);
        }
        return message;
    },
    fromJSON(object) {
        return {
            command: isSet(object.command) ? String(object.command) : "",
            reply: isSet(object.reply) ? String(object.reply) : "",
            biliTask: isSet(object.biliTask) ? exports.BiliTask.fromJSON(object.biliTask) : undefined,
        };
    },
    toJSON(message) {
        const obj = {};
        message.command !== undefined && (obj.command = message.command);
        message.reply !== undefined && (obj.reply = message.reply);
        message.biliTask !== undefined && (obj.biliTask = message.biliTask ? exports.BiliTask.toJSON(message.biliTask) : undefined);
        return obj;
    },
    create(base) {
        return exports.BiliTaskResponse.fromPartial(base !== null && base !== void 0 ? base : {});
    },
    fromPartial(object) {
        var _a, _b;
        const message = createBaseBiliTaskResponse();
        message.command = (_a = object.command) !== null && _a !== void 0 ? _a : "";
        message.reply = (_b = object.reply) !== null && _b !== void 0 ? _b : "";
        message.biliTask = (object.biliTask !== undefined && object.biliTask !== null)
            ? exports.BiliTask.fromPartial(object.biliTask)
            : undefined;
        return message;
    },
};
function createBaseArchivePlayRate() {
    return {
        uid: "",
        bvid: "",
        aid: "",
        title: "",
        view: 0,
        rate: 0,
        ctime: "",
        duration: 0,
        avgDuration: 0,
        avgPlayTimeValue: 0,
    };
}
exports.ArchivePlayRate = {
    encode(message, writer = minimal_1.default.Writer.create()) {
        if (message.uid !== "") {
            writer.uint32(10).string(message.uid);
        }
        if (message.bvid !== "") {
            writer.uint32(26).string(message.bvid);
        }
        if (message.aid !== "") {
            writer.uint32(34).string(message.aid);
        }
        if (message.title !== "") {
            writer.uint32(42).string(message.title);
        }
        if (message.view !== 0) {
            writer.uint32(48).int32(message.view);
        }
        if (message.rate !== 0) {
            writer.uint32(56).int32(message.rate);
        }
        if (message.ctime !== "") {
            writer.uint32(66).string(message.ctime);
        }
        if (message.duration !== 0) {
            writer.uint32(72).int32(message.duration);
        }
        if (message.avgDuration !== 0) {
            writer.uint32(80).int32(message.avgDuration);
        }
        if (message.avgPlayTimeValue !== 0) {
            writer.uint32(88).int32(message.avgPlayTimeValue);
        }
        return writer;
    },
    decode(input, length) {
        const reader = input instanceof minimal_1.default.Reader ? input : minimal_1.default.Reader.create(input);
        let end = length === undefined ? reader.len : reader.pos + length;
        const message = createBaseArchivePlayRate();
        while (reader.pos < end) {
            const tag = reader.uint32();
            switch (tag >>> 3) {
                case 1:
                    if (tag !== 10) {
                        break;
                    }
                    message.uid = reader.string();
                    continue;
                case 3:
                    if (tag !== 26) {
                        break;
                    }
                    message.bvid = reader.string();
                    continue;
                case 4:
                    if (tag !== 34) {
                        break;
                    }
                    message.aid = reader.string();
                    continue;
                case 5:
                    if (tag !== 42) {
                        break;
                    }
                    message.title = reader.string();
                    continue;
                case 6:
                    if (tag !== 48) {
                        break;
                    }
                    message.view = reader.int32();
                    continue;
                case 7:
                    if (tag !== 56) {
                        break;
                    }
                    message.rate = reader.int32();
                    continue;
                case 8:
                    if (tag !== 66) {
                        break;
                    }
                    message.ctime = reader.string();
                    continue;
                case 9:
                    if (tag !== 72) {
                        break;
                    }
                    message.duration = reader.int32();
                    continue;
                case 10:
                    if (tag !== 80) {
                        break;
                    }
                    message.avgDuration = reader.int32();
                    continue;
                case 11:
                    if (tag !== 88) {
                        break;
                    }
                    message.avgPlayTimeValue = reader.int32();
                    continue;
            }
            if ((tag & 7) === 4 || tag === 0) {
                break;
            }
            reader.skipType(tag & 7);
        }
        return message;
    },
    fromJSON(object) {
        return {
            uid: isSet(object.uid) ? String(object.uid) : "",
            bvid: isSet(object.bvid) ? String(object.bvid) : "",
            aid: isSet(object.aid) ? String(object.aid) : "",
            title: isSet(object.title) ? String(object.title) : "",
            view: isSet(object.view) ? Number(object.view) : 0,
            rate: isSet(object.rate) ? Number(object.rate) : 0,
            ctime: isSet(object.ctime) ? String(object.ctime) : "",
            duration: isSet(object.duration) ? Number(object.duration) : 0,
            avgDuration: isSet(object.avgDuration) ? Number(object.avgDuration) : 0,
            avgPlayTimeValue: isSet(object.avgPlayTimeValue) ? Number(object.avgPlayTimeValue) : 0,
        };
    },
    toJSON(message) {
        const obj = {};
        message.uid !== undefined && (obj.uid = message.uid);
        message.bvid !== undefined && (obj.bvid = message.bvid);
        message.aid !== undefined && (obj.aid = message.aid);
        message.title !== undefined && (obj.title = message.title);
        message.view !== undefined && (obj.view = Math.round(message.view));
        message.rate !== undefined && (obj.rate = Math.round(message.rate));
        message.ctime !== undefined && (obj.ctime = message.ctime);
        message.duration !== undefined && (obj.duration = Math.round(message.duration));
        message.avgDuration !== undefined && (obj.avgDuration = Math.round(message.avgDuration));
        message.avgPlayTimeValue !== undefined && (obj.avgPlayTimeValue = Math.round(message.avgPlayTimeValue));
        return obj;
    },
    create(base) {
        return exports.ArchivePlayRate.fromPartial(base !== null && base !== void 0 ? base : {});
    },
    fromPartial(object) {
        var _a, _b, _c, _d, _e, _f, _g, _h, _j, _k;
        const message = createBaseArchivePlayRate();
        message.uid = (_a = object.uid) !== null && _a !== void 0 ? _a : "";
        message.bvid = (_b = object.bvid) !== null && _b !== void 0 ? _b : "";
        message.aid = (_c = object.aid) !== null && _c !== void 0 ? _c : "";
        message.title = (_d = object.title) !== null && _d !== void 0 ? _d : "";
        message.view = (_e = object.view) !== null && _e !== void 0 ? _e : 0;
        message.rate = (_f = object.rate) !== null && _f !== void 0 ? _f : 0;
        message.ctime = (_g = object.ctime) !== null && _g !== void 0 ? _g : "";
        message.duration = (_h = object.duration) !== null && _h !== void 0 ? _h : 0;
        message.avgDuration = (_j = object.avgDuration) !== null && _j !== void 0 ? _j : 0;
        message.avgPlayTimeValue = (_k = object.avgPlayTimeValue) !== null && _k !== void 0 ? _k : 0;
        return message;
    },
};
function createBaseArchiveInc() {
    return { uid: "", bvid: "", aid: "", title: "", incr: 0, total: 0, daytime: "", ptime: "", interactive: 0 };
}
exports.ArchiveInc = {
    encode(message, writer = minimal_1.default.Writer.create()) {
        if (message.uid !== "") {
            writer.uint32(10).string(message.uid);
        }
        if (message.bvid !== "") {
            writer.uint32(26).string(message.bvid);
        }
        if (message.aid !== "") {
            writer.uint32(34).string(message.aid);
        }
        if (message.title !== "") {
            writer.uint32(42).string(message.title);
        }
        if (message.incr !== 0) {
            writer.uint32(48).int32(message.incr);
        }
        if (message.total !== 0) {
            writer.uint32(56).int32(message.total);
        }
        if (message.daytime !== "") {
            writer.uint32(66).string(message.daytime);
        }
        if (message.ptime !== "") {
            writer.uint32(74).string(message.ptime);
        }
        if (message.interactive !== 0) {
            writer.uint32(80).int32(message.interactive);
        }
        return writer;
    },
    decode(input, length) {
        const reader = input instanceof minimal_1.default.Reader ? input : minimal_1.default.Reader.create(input);
        let end = length === undefined ? reader.len : reader.pos + length;
        const message = createBaseArchiveInc();
        while (reader.pos < end) {
            const tag = reader.uint32();
            switch (tag >>> 3) {
                case 1:
                    if (tag !== 10) {
                        break;
                    }
                    message.uid = reader.string();
                    continue;
                case 3:
                    if (tag !== 26) {
                        break;
                    }
                    message.bvid = reader.string();
                    continue;
                case 4:
                    if (tag !== 34) {
                        break;
                    }
                    message.aid = reader.string();
                    continue;
                case 5:
                    if (tag !== 42) {
                        break;
                    }
                    message.title = reader.string();
                    continue;
                case 6:
                    if (tag !== 48) {
                        break;
                    }
                    message.incr = reader.int32();
                    continue;
                case 7:
                    if (tag !== 56) {
                        break;
                    }
                    message.total = reader.int32();
                    continue;
                case 8:
                    if (tag !== 66) {
                        break;
                    }
                    message.daytime = reader.string();
                    continue;
                case 9:
                    if (tag !== 74) {
                        break;
                    }
                    message.ptime = reader.string();
                    continue;
                case 10:
                    if (tag !== 80) {
                        break;
                    }
                    message.interactive = reader.int32();
                    continue;
            }
            if ((tag & 7) === 4 || tag === 0) {
                break;
            }
            reader.skipType(tag & 7);
        }
        return message;
    },
    fromJSON(object) {
        return {
            uid: isSet(object.uid) ? String(object.uid) : "",
            bvid: isSet(object.bvid) ? String(object.bvid) : "",
            aid: isSet(object.aid) ? String(object.aid) : "",
            title: isSet(object.title) ? String(object.title) : "",
            incr: isSet(object.incr) ? Number(object.incr) : 0,
            total: isSet(object.total) ? Number(object.total) : 0,
            daytime: isSet(object.daytime) ? String(object.daytime) : "",
            ptime: isSet(object.ptime) ? String(object.ptime) : "",
            interactive: isSet(object.interactive) ? Number(object.interactive) : 0,
        };
    },
    toJSON(message) {
        const obj = {};
        message.uid !== undefined && (obj.uid = message.uid);
        message.bvid !== undefined && (obj.bvid = message.bvid);
        message.aid !== undefined && (obj.aid = message.aid);
        message.title !== undefined && (obj.title = message.title);
        message.incr !== undefined && (obj.incr = Math.round(message.incr));
        message.total !== undefined && (obj.total = Math.round(message.total));
        message.daytime !== undefined && (obj.daytime = message.daytime);
        message.ptime !== undefined && (obj.ptime = message.ptime);
        message.interactive !== undefined && (obj.interactive = Math.round(message.interactive));
        return obj;
    },
    create(base) {
        return exports.ArchiveInc.fromPartial(base !== null && base !== void 0 ? base : {});
    },
    fromPartial(object) {
        var _a, _b, _c, _d, _e, _f, _g, _h, _j;
        const message = createBaseArchiveInc();
        message.uid = (_a = object.uid) !== null && _a !== void 0 ? _a : "";
        message.bvid = (_b = object.bvid) !== null && _b !== void 0 ? _b : "";
        message.aid = (_c = object.aid) !== null && _c !== void 0 ? _c : "";
        message.title = (_d = object.title) !== null && _d !== void 0 ? _d : "";
        message.incr = (_e = object.incr) !== null && _e !== void 0 ? _e : 0;
        message.total = (_f = object.total) !== null && _f !== void 0 ? _f : 0;
        message.daytime = (_g = object.daytime) !== null && _g !== void 0 ? _g : "";
        message.ptime = (_h = object.ptime) !== null && _h !== void 0 ? _h : "";
        message.interactive = (_j = object.interactive) !== null && _j !== void 0 ? _j : 0;
        return message;
    },
};
function createBaseArchiveViewerQuit() {
    return { uid: "", bvid: "", aid: "", cid: "", quitTrends: "" };
}
exports.ArchiveViewerQuit = {
    encode(message, writer = minimal_1.default.Writer.create()) {
        if (message.uid !== "") {
            writer.uint32(10).string(message.uid);
        }
        if (message.bvid !== "") {
            writer.uint32(26).string(message.bvid);
        }
        if (message.aid !== "") {
            writer.uint32(34).string(message.aid);
        }
        if (message.cid !== "") {
            writer.uint32(42).string(message.cid);
        }
        if (message.quitTrends !== "") {
            writer.uint32(50).string(message.quitTrends);
        }
        return writer;
    },
    decode(input, length) {
        const reader = input instanceof minimal_1.default.Reader ? input : minimal_1.default.Reader.create(input);
        let end = length === undefined ? reader.len : reader.pos + length;
        const message = createBaseArchiveViewerQuit();
        while (reader.pos < end) {
            const tag = reader.uint32();
            switch (tag >>> 3) {
                case 1:
                    if (tag !== 10) {
                        break;
                    }
                    message.uid = reader.string();
                    continue;
                case 3:
                    if (tag !== 26) {
                        break;
                    }
                    message.bvid = reader.string();
                    continue;
                case 4:
                    if (tag !== 34) {
                        break;
                    }
                    message.aid = reader.string();
                    continue;
                case 5:
                    if (tag !== 42) {
                        break;
                    }
                    message.cid = reader.string();
                    continue;
                case 6:
                    if (tag !== 50) {
                        break;
                    }
                    message.quitTrends = reader.string();
                    continue;
            }
            if ((tag & 7) === 4 || tag === 0) {
                break;
            }
            reader.skipType(tag & 7);
        }
        return message;
    },
    fromJSON(object) {
        return {
            uid: isSet(object.uid) ? String(object.uid) : "",
            bvid: isSet(object.bvid) ? String(object.bvid) : "",
            aid: isSet(object.aid) ? String(object.aid) : "",
            cid: isSet(object.cid) ? String(object.cid) : "",
            quitTrends: isSet(object.quitTrends) ? String(object.quitTrends) : "",
        };
    },
    toJSON(message) {
        const obj = {};
        message.uid !== undefined && (obj.uid = message.uid);
        message.bvid !== undefined && (obj.bvid = message.bvid);
        message.aid !== undefined && (obj.aid = message.aid);
        message.cid !== undefined && (obj.cid = message.cid);
        message.quitTrends !== undefined && (obj.quitTrends = message.quitTrends);
        return obj;
    },
    create(base) {
        return exports.ArchiveViewerQuit.fromPartial(base !== null && base !== void 0 ? base : {});
    },
    fromPartial(object) {
        var _a, _b, _c, _d, _e;
        const message = createBaseArchiveViewerQuit();
        message.uid = (_a = object.uid) !== null && _a !== void 0 ? _a : "";
        message.bvid = (_b = object.bvid) !== null && _b !== void 0 ? _b : "";
        message.aid = (_c = object.aid) !== null && _c !== void 0 ? _c : "";
        message.cid = (_d = object.cid) !== null && _d !== void 0 ? _d : "";
        message.quitTrends = (_e = object.quitTrends) !== null && _e !== void 0 ? _e : "";
        return message;
    },
};
function createBaseSyncPlayRateAnalysisRequest() {
    return { archivePlayRate: undefined };
}
exports.SyncPlayRateAnalysisRequest = {
    encode(message, writer = minimal_1.default.Writer.create()) {
        if (message.archivePlayRate !== undefined) {
            exports.ArchivePlayRate.encode(message.archivePlayRate, writer.uint32(10).fork()).ldelim();
        }
        return writer;
    },
    decode(input, length) {
        const reader = input instanceof minimal_1.default.Reader ? input : minimal_1.default.Reader.create(input);
        let end = length === undefined ? reader.len : reader.pos + length;
        const message = createBaseSyncPlayRateAnalysisRequest();
        while (reader.pos < end) {
            const tag = reader.uint32();
            switch (tag >>> 3) {
                case 1:
                    if (tag !== 10) {
                        break;
                    }
                    message.archivePlayRate = exports.ArchivePlayRate.decode(reader, reader.uint32());
                    continue;
            }
            if ((tag & 7) === 4 || tag === 0) {
                break;
            }
            reader.skipType(tag & 7);
        }
        return message;
    },
    fromJSON(object) {
        return {
            archivePlayRate: isSet(object.archivePlayRate) ? exports.ArchivePlayRate.fromJSON(object.archivePlayRate) : undefined,
        };
    },
    toJSON(message) {
        const obj = {};
        message.archivePlayRate !== undefined &&
            (obj.archivePlayRate = message.archivePlayRate ? exports.ArchivePlayRate.toJSON(message.archivePlayRate) : undefined);
        return obj;
    },
    create(base) {
        return exports.SyncPlayRateAnalysisRequest.fromPartial(base !== null && base !== void 0 ? base : {});
    },
    fromPartial(object) {
        const message = createBaseSyncPlayRateAnalysisRequest();
        message.archivePlayRate = (object.archivePlayRate !== undefined && object.archivePlayRate !== null)
            ? exports.ArchivePlayRate.fromPartial(object.archivePlayRate)
            : undefined;
        return message;
    },
};
function createBaseSyncPlayRateAnalysisResponse() {
    return { reply: "" };
}
exports.SyncPlayRateAnalysisResponse = {
    encode(message, writer = minimal_1.default.Writer.create()) {
        if (message.reply !== "") {
            writer.uint32(10).string(message.reply);
        }
        return writer;
    },
    decode(input, length) {
        const reader = input instanceof minimal_1.default.Reader ? input : minimal_1.default.Reader.create(input);
        let end = length === undefined ? reader.len : reader.pos + length;
        const message = createBaseSyncPlayRateAnalysisResponse();
        while (reader.pos < end) {
            const tag = reader.uint32();
            switch (tag >>> 3) {
                case 1:
                    if (tag !== 10) {
                        break;
                    }
                    message.reply = reader.string();
                    continue;
            }
            if ((tag & 7) === 4 || tag === 0) {
                break;
            }
            reader.skipType(tag & 7);
        }
        return message;
    },
    fromJSON(object) {
        return { reply: isSet(object.reply) ? String(object.reply) : "" };
    },
    toJSON(message) {
        const obj = {};
        message.reply !== undefined && (obj.reply = message.reply);
        return obj;
    },
    create(base) {
        return exports.SyncPlayRateAnalysisResponse.fromPartial(base !== null && base !== void 0 ? base : {});
    },
    fromPartial(object) {
        var _a;
        const message = createBaseSyncPlayRateAnalysisResponse();
        message.reply = (_a = object.reply) !== null && _a !== void 0 ? _a : "";
        return message;
    },
};
function createBaseSyncArchiveAnalyzeStatsRequest() {
    return { archiveInc: undefined };
}
exports.SyncArchiveAnalyzeStatsRequest = {
    encode(message, writer = minimal_1.default.Writer.create()) {
        if (message.archiveInc !== undefined) {
            exports.ArchiveInc.encode(message.archiveInc, writer.uint32(10).fork()).ldelim();
        }
        return writer;
    },
    decode(input, length) {
        const reader = input instanceof minimal_1.default.Reader ? input : minimal_1.default.Reader.create(input);
        let end = length === undefined ? reader.len : reader.pos + length;
        const message = createBaseSyncArchiveAnalyzeStatsRequest();
        while (reader.pos < end) {
            const tag = reader.uint32();
            switch (tag >>> 3) {
                case 1:
                    if (tag !== 10) {
                        break;
                    }
                    message.archiveInc = exports.ArchiveInc.decode(reader, reader.uint32());
                    continue;
            }
            if ((tag & 7) === 4 || tag === 0) {
                break;
            }
            reader.skipType(tag & 7);
        }
        return message;
    },
    fromJSON(object) {
        return { archiveInc: isSet(object.archiveInc) ? exports.ArchiveInc.fromJSON(object.archiveInc) : undefined };
    },
    toJSON(message) {
        const obj = {};
        message.archiveInc !== undefined &&
            (obj.archiveInc = message.archiveInc ? exports.ArchiveInc.toJSON(message.archiveInc) : undefined);
        return obj;
    },
    create(base) {
        return exports.SyncArchiveAnalyzeStatsRequest.fromPartial(base !== null && base !== void 0 ? base : {});
    },
    fromPartial(object) {
        const message = createBaseSyncArchiveAnalyzeStatsRequest();
        message.archiveInc = (object.archiveInc !== undefined && object.archiveInc !== null)
            ? exports.ArchiveInc.fromPartial(object.archiveInc)
            : undefined;
        return message;
    },
};
function createBaseSyncArchiveAnalyzeStatsResponse() {
    return { reply: "" };
}
exports.SyncArchiveAnalyzeStatsResponse = {
    encode(message, writer = minimal_1.default.Writer.create()) {
        if (message.reply !== "") {
            writer.uint32(10).string(message.reply);
        }
        return writer;
    },
    decode(input, length) {
        const reader = input instanceof minimal_1.default.Reader ? input : minimal_1.default.Reader.create(input);
        let end = length === undefined ? reader.len : reader.pos + length;
        const message = createBaseSyncArchiveAnalyzeStatsResponse();
        while (reader.pos < end) {
            const tag = reader.uint32();
            switch (tag >>> 3) {
                case 1:
                    if (tag !== 10) {
                        break;
                    }
                    message.reply = reader.string();
                    continue;
            }
            if ((tag & 7) === 4 || tag === 0) {
                break;
            }
            reader.skipType(tag & 7);
        }
        return message;
    },
    fromJSON(object) {
        return { reply: isSet(object.reply) ? String(object.reply) : "" };
    },
    toJSON(message) {
        const obj = {};
        message.reply !== undefined && (obj.reply = message.reply);
        return obj;
    },
    create(base) {
        return exports.SyncArchiveAnalyzeStatsResponse.fromPartial(base !== null && base !== void 0 ? base : {});
    },
    fromPartial(object) {
        var _a;
        const message = createBaseSyncArchiveAnalyzeStatsResponse();
        message.reply = (_a = object.reply) !== null && _a !== void 0 ? _a : "";
        return message;
    },
};
function createBaseSyncViewerQuitTrendRequest() {
    return { archiveViewerQuit: undefined };
}
exports.SyncViewerQuitTrendRequest = {
    encode(message, writer = minimal_1.default.Writer.create()) {
        if (message.archiveViewerQuit !== undefined) {
            exports.ArchiveViewerQuit.encode(message.archiveViewerQuit, writer.uint32(10).fork()).ldelim();
        }
        return writer;
    },
    decode(input, length) {
        const reader = input instanceof minimal_1.default.Reader ? input : minimal_1.default.Reader.create(input);
        let end = length === undefined ? reader.len : reader.pos + length;
        const message = createBaseSyncViewerQuitTrendRequest();
        while (reader.pos < end) {
            const tag = reader.uint32();
            switch (tag >>> 3) {
                case 1:
                    if (tag !== 10) {
                        break;
                    }
                    message.archiveViewerQuit = exports.ArchiveViewerQuit.decode(reader, reader.uint32());
                    continue;
            }
            if ((tag & 7) === 4 || tag === 0) {
                break;
            }
            reader.skipType(tag & 7);
        }
        return message;
    },
    fromJSON(object) {
        return {
            archiveViewerQuit: isSet(object.archiveViewerQuit)
                ? exports.ArchiveViewerQuit.fromJSON(object.archiveViewerQuit)
                : undefined,
        };
    },
    toJSON(message) {
        const obj = {};
        message.archiveViewerQuit !== undefined && (obj.archiveViewerQuit = message.archiveViewerQuit
            ? exports.ArchiveViewerQuit.toJSON(message.archiveViewerQuit)
            : undefined);
        return obj;
    },
    create(base) {
        return exports.SyncViewerQuitTrendRequest.fromPartial(base !== null && base !== void 0 ? base : {});
    },
    fromPartial(object) {
        const message = createBaseSyncViewerQuitTrendRequest();
        message.archiveViewerQuit = (object.archiveViewerQuit !== undefined && object.archiveViewerQuit !== null)
            ? exports.ArchiveViewerQuit.fromPartial(object.archiveViewerQuit)
            : undefined;
        return message;
    },
};
function createBaseSyncViewerQuitTrendResponse() {
    return { reply: "" };
}
exports.SyncViewerQuitTrendResponse = {
    encode(message, writer = minimal_1.default.Writer.create()) {
        if (message.reply !== "") {
            writer.uint32(10).string(message.reply);
        }
        return writer;
    },
    decode(input, length) {
        const reader = input instanceof minimal_1.default.Reader ? input : minimal_1.default.Reader.create(input);
        let end = length === undefined ? reader.len : reader.pos + length;
        const message = createBaseSyncViewerQuitTrendResponse();
        while (reader.pos < end) {
            const tag = reader.uint32();
            switch (tag >>> 3) {
                case 1:
                    if (tag !== 10) {
                        break;
                    }
                    message.reply = reader.string();
                    continue;
            }
            if ((tag & 7) === 4 || tag === 0) {
                break;
            }
            reader.skipType(tag & 7);
        }
        return message;
    },
    fromJSON(object) {
        return { reply: isSet(object.reply) ? String(object.reply) : "" };
    },
    toJSON(message) {
        const obj = {};
        message.reply !== undefined && (obj.reply = message.reply);
        return obj;
    },
    create(base) {
        return exports.SyncViewerQuitTrendResponse.fromPartial(base !== null && base !== void 0 ? base : {});
    },
    fromPartial(object) {
        var _a;
        const message = createBaseSyncViewerQuitTrendResponse();
        message.reply = (_a = object.reply) !== null && _a !== void 0 ? _a : "";
        return message;
    },
};
exports.BiliBotService = {
    /** Client streaming */
    syncQrCodeLogin: {
        path: "/BiliBot/syncQrCodeLogin",
        requestStream: true,
        responseStream: false,
        requestSerialize: (value) => Buffer.from(exports.SyncQrCodeLoginRequest.encode(value).finish()),
        requestDeserialize: (value) => exports.SyncQrCodeLoginRequest.decode(value),
        responseSerialize: (value) => Buffer.from(exports.SyncQrCodeLoginResponse.encode(value).finish()),
        responseDeserialize: (value) => exports.SyncQrCodeLoginResponse.decode(value),
    },
    /** Unary */
    syncHittedComment: {
        path: "/BiliBot/syncHittedComment",
        requestStream: false,
        responseStream: false,
        requestSerialize: (value) => Buffer.from(exports.SyncHittedCommentRequest.encode(value).finish()),
        requestDeserialize: (value) => exports.SyncHittedCommentRequest.decode(value),
        responseSerialize: (value) => Buffer.from(exports.SyncHittedCommentResponse.encode(value).finish()),
        responseDeserialize: (value) => exports.SyncHittedCommentResponse.decode(value),
    },
    /** Unary */
    syncHittedDanmaku: {
        path: "/BiliBot/syncHittedDanmaku",
        requestStream: false,
        responseStream: false,
        requestSerialize: (value) => Buffer.from(exports.SyncHittedDanmakuRequest.encode(value).finish()),
        requestDeserialize: (value) => exports.SyncHittedDanmakuRequest.decode(value),
        responseSerialize: (value) => Buffer.from(exports.SyncHittedDanmakuResponse.encode(value).finish()),
        responseDeserialize: (value) => exports.SyncHittedDanmakuResponse.decode(value),
    },
    /** Unary */
    syncBiliAccount: {
        path: "/BiliBot/syncBiliAccount",
        requestStream: false,
        responseStream: false,
        requestSerialize: (value) => Buffer.from(exports.SyncBiliAccountRequest.encode(value).finish()),
        requestDeserialize: (value) => exports.SyncBiliAccountRequest.decode(value),
        responseSerialize: (value) => Buffer.from(exports.SyncBiliAccountResponse.encode(value).finish()),
        responseDeserialize: (value) => exports.SyncBiliAccountResponse.decode(value),
    },
    /** Bi-directional streaming */
    syncBiliTask: {
        path: "/BiliBot/syncBiliTask",
        requestStream: true,
        responseStream: true,
        requestSerialize: (value) => Buffer.from(exports.BiliTaskRequest.encode(value).finish()),
        requestDeserialize: (value) => exports.BiliTaskRequest.decode(value),
        responseSerialize: (value) => Buffer.from(exports.BiliTaskResponse.encode(value).finish()),
        responseDeserialize: (value) => exports.BiliTaskResponse.decode(value),
    },
    /** Unary */
    syncPlayRateAnalysis: {
        path: "/BiliBot/syncPlayRateAnalysis",
        requestStream: false,
        responseStream: false,
        requestSerialize: (value) => Buffer.from(exports.SyncPlayRateAnalysisRequest.encode(value).finish()),
        requestDeserialize: (value) => exports.SyncPlayRateAnalysisRequest.decode(value),
        responseSerialize: (value) => Buffer.from(exports.SyncPlayRateAnalysisResponse.encode(value).finish()),
        responseDeserialize: (value) => exports.SyncPlayRateAnalysisResponse.decode(value),
    },
    /** Unary */
    syncArchiveAnalyzeStats: {
        path: "/BiliBot/syncArchiveAnalyzeStats",
        requestStream: false,
        responseStream: false,
        requestSerialize: (value) => Buffer.from(exports.SyncArchiveAnalyzeStatsRequest.encode(value).finish()),
        requestDeserialize: (value) => exports.SyncArchiveAnalyzeStatsRequest.decode(value),
        responseSerialize: (value) => Buffer.from(exports.SyncArchiveAnalyzeStatsResponse.encode(value).finish()),
        responseDeserialize: (value) => exports.SyncArchiveAnalyzeStatsResponse.decode(value),
    },
    /** Unary */
    syncViewerQuitTrend: {
        path: "/BiliBot/syncViewerQuitTrend",
        requestStream: false,
        responseStream: false,
        requestSerialize: (value) => Buffer.from(exports.SyncViewerQuitTrendRequest.encode(value).finish()),
        requestDeserialize: (value) => exports.SyncViewerQuitTrendRequest.decode(value),
        responseSerialize: (value) => Buffer.from(exports.SyncViewerQuitTrendResponse.encode(value).finish()),
        responseDeserialize: (value) => exports.SyncViewerQuitTrendResponse.decode(value),
    },
};
exports.BiliBotClient = (0, grpc_js_1.makeGenericClientConstructor)(exports.BiliBotService, "BiliBot");
function isSet(value) {
    return value !== null && value !== undefined;
}
//# sourceMappingURL=bili_bot_services.js.map