"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.addCron = void 0;
const node_schedule_1 = __importDefault(require("node-schedule"));
const data_1 = require("./data");
const runCron_1 = require("../shared/runCron");
const logger_1 = __importDefault(require("../loaders/logger"));
const simpleTask_1 = require("../data/models/simpleTask");
const simpleTaskExecuteRecord_1 = require("../data/models/simpleTaskExecuteRecord");
const simpleTaskExecuteItem_1 = require("../data/models/simpleTaskExecuteItem");
const commentTask_1 = require("../data/models/commentTask");
const commentTaskExecuteRecord_1 = require("../data/models/commentTaskExecuteRecord");
const commentTaskExecuteItem_1 = require("../data/models/commentTaskExecuteItem");
const danmakuTask_1 = require("../data/models/danmakuTask");
const danmakuTaskExecuteRecord_1 = require("../data/models/danmakuTaskExecuteRecord");
const danmakuTaskExecuteItem_1 = require("../data/models/danmakuTaskExecuteItem");
const const_1 = require("../config/const");
const addCron = (call, callback) => {
    var _a;
    for (const item of call.request.crons) {
        const { id, recordId, schedule, command, cronName } = item;
        if (data_1.scheduleStacks.has(id)) {
            (_a = data_1.scheduleStacks.get(id)) === null || _a === void 0 ? void 0 : _a.cancel();
        }
        const cmdStr = `ID=${id} RecordId=${recordId} ${command}`;
        logger_1.default.info('[schedule][创建定时任务], 任务ID: %s, cron: %s, 执行命令: %s', id, schedule, command);
        data_1.scheduleStacks.set(id, node_schedule_1.default.scheduleJob(id, schedule, async () => {
            switch (cronName) {
                case const_1.CRON_NAME_DIC['9']:
                    await scheduleCommentTask(recordId, cmdStr, cronName, schedule);
                    break;
                case const_1.CRON_NAME_DIC['11']:
                    await scheduleDanmakuTask(recordId, cmdStr, cronName, schedule);
                    break;
                default:
                    await scheduleSimpleTask(recordId, cmdStr, cronName, schedule);
                    break;
            }
        }));
    }
    callback(null, null);
};
exports.addCron = addCron;
async function scheduleDanmakuTask(recordId, cmdStr, cronName, schedule) {
    const danmakuTaskExecuteRecord = await danmakuTaskExecuteRecord_1.DanmakuTaskExecuteRecordModel.findOne({
        where: {
            id: recordId,
        },
    });
    if (danmakuTaskExecuteRecord) {
        const danmakuTask = await danmakuTask_1.DanmakuTaskModel.findOne({
            where: {
                id: danmakuTaskExecuteRecord.task_id,
            },
        });
        if (danmakuTask &&
            danmakuTask.enable_flag &&
            danmakuTask.control_flag &&
            !danmakuTask.del_flag) {
            if (danmakuTask.status == simpleTask_1.TaskStatusEnum.execute_complete) {
                // 将任务状态设置为排队中
                await danmakuTask_1.DanmakuTaskModel.update({
                    status: simpleTask_1.TaskStatusEnum.be_queuing,
                    update_by: 'system',
                    update_time: new Date(),
                }, {
                    where: {
                        id: danmakuTask.id,
                    },
                });
                await danmakuTaskExecuteItem_1.DanmakuTaskExecuteItemModel.update({
                    status: simpleTaskExecuteItem_1.ExecuteItemStatusEnum.WAIT,
                    update_time: new Date(),
                }, {
                    where: {
                        record_id: recordId,
                    },
                });
                logger_1.default.info(`[schedule][准备运行任务] 命令: ${cmdStr}`);
                (0, runCron_1.runCron)(cmdStr, { taskId: danmakuTask.id, name: cronName, schedule });
            }
            else {
                logger_1.default.info('Failed to run schedule cron task [%s], Its status is [%d], waiting for next schedule', cronName, danmakuTask.status);
            }
        }
    }
}
async function scheduleCommentTask(recordId, cmdStr, cronName, schedule) {
    const commentTaskExecuteRecord = await commentTaskExecuteRecord_1.CommentTaskExecuteRecordModel.findOne({
        where: {
            id: recordId,
        },
    });
    if (commentTaskExecuteRecord) {
        const commentTask = await commentTask_1.CommentTaskModel.findOne({
            where: {
                id: commentTaskExecuteRecord.task_id,
            },
        });
        if (commentTask &&
            commentTask.enable_flag &&
            commentTask.control_flag &&
            !commentTask.del_flag) {
            if (commentTask.status == simpleTask_1.TaskStatusEnum.execute_complete) {
                // 将任务状态设置为排队中
                await commentTask_1.CommentTaskModel.update({
                    status: simpleTask_1.TaskStatusEnum.be_queuing,
                    update_by: 'system',
                    update_time: new Date(),
                }, {
                    where: {
                        id: commentTask.id,
                    },
                });
                await commentTaskExecuteItem_1.CommentTaskExecuteItemModel.update({
                    status: simpleTaskExecuteItem_1.ExecuteItemStatusEnum.WAIT,
                    update_time: new Date(),
                }, {
                    where: {
                        record_id: recordId,
                    },
                });
                logger_1.default.info(`[schedule][准备运行任务] 命令: ${cmdStr}`);
                (0, runCron_1.runCron)(cmdStr, { taskId: commentTask.id, name: cronName, schedule });
            }
            else {
                logger_1.default.info('Failed to run schedule cron task [%s], Its status is [%d], waiting for next schedule', cronName, commentTask.status);
            }
        }
    }
}
async function scheduleSimpleTask(recordId, cmdStr, cronName, schedule) {
    const simpleTaskExecuteRecord = await simpleTaskExecuteRecord_1.SimpleTaskExecuteRecordModel.findOne({
        where: {
            id: recordId,
        },
    });
    if (simpleTaskExecuteRecord) {
        const simpleTask = await simpleTask_1.SimpleTaskModel.findOne({
            where: {
                id: simpleTaskExecuteRecord.task_id,
            },
        });
        if (simpleTask && simpleTask.enable_flag && !simpleTask.del_flag) {
            if (simpleTask.status == simpleTask_1.TaskStatusEnum.execute_complete) {
                // 将任务状态设置为排队中
                await simpleTask_1.SimpleTaskModel.update({
                    status: simpleTask_1.TaskStatusEnum.be_queuing,
                    update_by: 'system',
                    update_time: new Date(),
                }, {
                    where: {
                        id: simpleTask.id,
                    },
                });
                await simpleTaskExecuteItem_1.SimpleTaskExecuteItemModel.update({
                    status: simpleTaskExecuteItem_1.ExecuteItemStatusEnum.WAIT,
                    update_time: new Date(),
                }, {
                    where: {
                        record_id: recordId,
                    },
                });
                logger_1.default.info(`[schedule][准备运行任务] 命令: ${cmdStr}`);
                (0, runCron_1.runCron)(cmdStr, { taskId: simpleTask.id, name: cronName, schedule });
            }
            else {
                logger_1.default.info('Failed to run schedule cron task [%s], Its status is [%d], waiting for next schedule', cronName, simpleTask.status);
            }
        }
    }
}
//# sourceMappingURL=addCron.js.map