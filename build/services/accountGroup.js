"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const typedi_1 = require("typedi");
const winston_1 = __importDefault(require("winston"));
const sequelize_1 = require("sequelize");
const accountGroup_1 = require("../data/models/accountGroup");
const cityDistributeTemplateItem_1 = require("../data/models/cityDistributeTemplateItem");
const biliAccountGroupBindRecord_1 = require("../data/models/binding/biliAccountGroupBindRecord");
const biliAccount_1 = require("../data/models/biliAccount");
let AccountGroupService = class AccountGroupService {
    constructor(logger) {
        this.logger = logger;
    }
    async list() {
        try {
            const result = await accountGroup_1.AccountGroupModel.findAll({
                where: { publish_flag: true, enable_flag: true, del_flag: false },
            });
            return result;
        }
        catch (error) {
            throw error;
        }
    }
    /**
     * 获取账号组列表
     *
     * @param payloads
     */
    async getList(payloads) {
        const { page_index, page_size, search_text } = payloads;
        const where = {};
        if (search_text) {
            Object.assign(where, {
                template_name: {
                    [sequelize_1.Op.like]: `%${search_text}%`,
                },
            });
        }
        Object.assign(where, { del_flag: false });
        const result = await accountGroup_1.AccountGroupModel.findAndCountAll({
            order: [['create_time', 'DESC']],
            offset: (page_index - 1) * page_size,
            limit: page_size,
            where,
        });
        let list = [];
        for (let i = 0; i < result.rows.length; i++) {
            let _item = result.rows[i].toJSON();
            const itemList = await cityDistributeTemplateItem_1.CityDistributeTemplateItemModel.findAll({
                where: {
                    template_code: _item.template_code,
                },
            });
            const cities = [];
            for (let j = 0; j < itemList.length; j++) {
                cities.push({
                    code: itemList[j].city_code,
                    name: itemList[j].city_name,
                });
            }
            const accountCount = await biliAccountGroupBindRecord_1.BiliAccountGroupBindRecordModel.count({
                where: {
                    group_code: _item.template_code,
                },
            });
            list.push(Object.assign(Object.assign({}, _item), { cities, account_count: accountCount }));
        }
        return {
            code: 0,
            msg: 'Success',
            data: {
                total: result.count,
                list,
            },
        };
    }
    async getAccountList(payloads) {
        const { page_index, page_size, template_code } = payloads;
        const result = await biliAccountGroupBindRecord_1.BiliAccountGroupBindRecordModel.findAndCountAll({
            attributes: ['id', 'mid', 'create_time'],
            order: [['create_time', 'DESC']],
            offset: (page_index - 1) * page_size,
            limit: page_size,
            where: {
                del_flag: false,
                group_code: template_code,
            },
            include: [
                {
                    model: biliAccount_1.BiliAccountModel,
                    as: 'biliAccount', // 关联模型的别名
                    attributes: [
                        'name',
                        'avatar',
                        'proxy_ip',
                        'login_time',
                        'login_status',
                    ],
                },
            ],
        });
        return {
            code: 0,
            data: {
                total: result.count,
                list: result.rows,
            },
        };
    }
    /**
     * 搜索账号组
     *
     * @param payloads
     */
    async search(payloads) {
        const { search_text } = payloads;
        const where = {};
        if (search_text) {
            where[sequelize_1.Op.or] = [
                { template_code: { [sequelize_1.Op.like]: `%${search_text}%` } },
                { template_name: { [sequelize_1.Op.like]: `%${search_text}%` } },
            ];
        }
        const result = await accountGroup_1.AccountGroupModel.findAll({
            order: [['create_time', 'DESC']],
            where,
        });
        return {
            code: 0,
            msg: 'Success',
            data: result,
        };
    }
    /**
     * 修改账号组
     *
     * @param payloads 请求参数
     */
    async modify(payloads, userId) {
        if (!payloads.cities.length) {
            return { code: 400, msg: '城市明细为空' };
        }
        const now = new Date();
        const { template_code, template_name, distribute_type, cities, publish_remark, } = payloads;
        let accountGroup = await accountGroup_1.AccountGroupModel.findOne({
            where: {
                template_code: template_code,
            },
        });
        if (accountGroup) {
            await accountGroup_1.AccountGroupModel.update({
                template_name,
                distribute_type,
                publish_remark,
                update_by: userId,
                update_time: now,
            }, {
                where: {
                    template_code,
                },
            });
            await cityDistributeTemplateItem_1.CityDistributeTemplateItemModel.destroy({
                where: {
                    template_code,
                },
            });
        }
        else {
            await accountGroup_1.AccountGroupModel.create({
                template_code,
                template_name,
                distribute_type,
                publish_flag: false,
                publish_remark,
                enable_flag: true,
                del_flag: false,
                create_by: userId,
                create_time: now,
                update_by: userId,
                update_time: now,
            });
        }
        // 明细列表
        const items = [];
        cities.forEach((city) => {
            items.push({
                template_code,
                city_code: city.code,
                city_name: city.name,
                enable_flag: true,
                create_by: userId,
                create_time: now,
                update_by: userId,
                update_time: now,
            });
        });
        await cityDistributeTemplateItem_1.CityDistributeTemplateItemModel.bulkCreate(items);
        return { code: 0, msg: '保存成功' };
    }
    /**
     * 删除账号组
     *
     * @param payloads
     * @param accessToken
     */
    async del(payloads, accessToken) {
        const { template_code } = payloads;
        await accountGroup_1.AccountGroupModel.destroy({
            where: {
                template_code,
            },
        });
        await cityDistributeTemplateItem_1.CityDistributeTemplateItemModel.destroy({
            where: {
                template_code,
            },
        });
        return { code: 0, msg: '删除成功' };
    }
    /**
     * 删除账号组
     *
     * @param payloads
     * @param userId
     */
    async delete(payloads, userId) {
        const { template_code } = payloads;
        const now = new Date();
        // 逻辑删除
        let template = await accountGroup_1.AccountGroupModel.findOne({
            where: {
                template_code,
            },
        });
        if (!template) {
            return { code: 400, msg: '账号组不存在' };
        }
        if (template.del_flag) {
            return { code: 400, msg: '账号组已删除' };
        }
        await accountGroup_1.AccountGroupModel.update({
            del_flag: true,
            update_by: userId,
            update_time: now,
        }, {
            where: {
                template_code,
            },
        });
        return { code: 0, msg: '删除成功' };
    }
    /**
     * 变更是否启用
     *
     * @param payloads
     * @param userId
     * @returns
     */
    async changeEnable(payloads, userId) {
        const now = new Date();
        const { template_code, enable_flag } = payloads;
        let accountGroup = await accountGroup_1.AccountGroupModel.findOne({
            where: {
                template_code,
            },
        });
        if (!accountGroup) {
            return { code: 400, msg: '账号组不存在' };
        }
        if (accountGroup.enable_flag != enable_flag) {
            await accountGroup_1.AccountGroupModel.update({
                enable_flag,
                update_by: userId,
                update_time: now,
            }, {
                where: {
                    template_code,
                },
            });
        }
        return { code: 0, msg: '操作成功' };
    }
    /**
     * 发布
     *
     * @param payloads
     * @param userId
     * @returns
     */
    async publish(payloads, userId) {
        const now = new Date();
        const { template_code } = payloads;
        let accountGroup = await accountGroup_1.AccountGroupModel.findOne({
            where: {
                template_code,
            },
        });
        if (!accountGroup || !accountGroup.enable_flag) {
            return { code: 400, msg: '账号组不存在或已禁用' };
        }
        if (accountGroup.publish_flag) {
            return { code: 400, msg: '账号组已发布' };
        }
        await accountGroup_1.AccountGroupModel.update({
            publish_flag: true,
            publish_time: now,
            publish_by: userId,
            update_by: userId,
            update_time: now,
        }, {
            where: {
                template_code,
            },
        });
        return { code: 0, msg: '操作成功' };
    }
    /**
     * 获取城市列表
     */
    async getCities() {
        const cities = [
            {
                value: '320500',
                label: '苏州市',
            },
            {
                value: '370200',
                label: '青岛市',
            },
            {
                value: '430100',
                label: '长沙市',
            },
            {
                value: '440100',
                label: '广州市',
            },
            {
                value: '440300',
                label: '深圳市',
            },
        ];
        return { code: 0, msg: 'Success', data: cities };
    }
};
AccountGroupService = __decorate([
    (0, typedi_1.Service)(),
    __param(0, (0, typedi_1.Inject)('logger')),
    __metadata("design:paramtypes", [winston_1.default.Logger])
], AccountGroupService);
exports.default = AccountGroupService;
//# sourceMappingURL=accountGroup.js.map