"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const typedi_1 = require("typedi");
const winston_1 = __importDefault(require("winston"));
const uuid_1 = require("uuid");
const biliAccountGroupBindRecord_1 = require("../data/models/binding/biliAccountGroupBindRecord");
let BiliAccountBindGroupRecordService = class BiliAccountBindGroupRecordService {
    constructor(logger) {
        this.logger = logger;
    }
    async create(payload) {
        const accountGroup = new biliAccountGroupBindRecord_1.BiliAccountGroupBindRecord(payload);
        const doc = await this.insert(accountGroup);
        return doc;
    }
    async createBindAccountGroupRecordIfNotExist(mid, group_code, create_by) {
        const accountGroup = await biliAccountGroupBindRecord_1.BiliAccountGroupBindRecordModel.findOne({
            where: {
                mid,
                group_code,
            },
        });
        if (accountGroup) {
            return;
        }
        const now = new Date();
        const biliAccountBindGroup = new biliAccountGroupBindRecord_1.BiliAccountGroupBindRecord({
            id: (0, uuid_1.v4)(),
            mid: mid,
            group_code: group_code,
            del_flag: false,
            create_by: create_by,
            create_time: now,
            update_by: create_by,
            update_time: now,
        });
        await this.create(biliAccountBindGroup);
    }
    async insert(payload) {
        return await biliAccountGroupBindRecord_1.BiliAccountGroupBindRecordModel.create(payload, {
            returning: true,
        });
    }
    async delete(ids, userId) {
        await biliAccountGroupBindRecord_1.BiliAccountGroupBindRecordModel.update({ del_flag: true, update_by: userId, update_time: new Date() }, { where: { id: ids } });
        return { code: 0, msg: '移除账号成功' };
    }
};
BiliAccountBindGroupRecordService = __decorate([
    (0, typedi_1.Service)(),
    __param(0, (0, typedi_1.Inject)('logger')),
    __metadata("design:paramtypes", [winston_1.default.Logger])
], BiliAccountBindGroupRecordService);
exports.default = BiliAccountBindGroupRecordService;
//# sourceMappingURL=biliAccountBindGroupRecord.js.map