"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const typedi_1 = require("typedi");
const winston_1 = __importDefault(require("winston"));
const uuid_1 = require("uuid");
const sequelize_1 = require("sequelize");
const biliAccountBindProxyIpRecord_1 = require("../data/models/biliAccountBindProxyIpRecord");
let BiliAccountBindProxyIpRecordService = class BiliAccountBindProxyIpRecordService {
    constructor(logger) {
        this.logger = logger;
    }
    async create(payload) {
        const bTag = new biliAccountBindProxyIpRecord_1.BiliAccountBindProxyIpRecord(payload);
        const doc = await this.insert(bTag);
        return doc;
    }
    async createBindProxyIpRecordIfNotExist(mid, proxy_ip) {
        const proxyIpRecord = await biliAccountBindProxyIpRecord_1.BiliAccountBindProxyIpRecordModel.findOne({
            where: {
                mid,
                proxy_ip,
            },
        });
        if (proxyIpRecord) {
            return;
        }
        const biliAccountBindProxyIp = new biliAccountBindProxyIpRecord_1.BiliAccountBindProxyIpRecord({
            id: (0, uuid_1.v4)(),
            mid: mid,
            proxy_ip: proxy_ip,
            create_time: new Date()
        });
        await this.create(biliAccountBindProxyIp);
    }
    async insert(payload) {
        return await biliAccountBindProxyIpRecord_1.BiliAccountBindProxyIpRecordModel.create(payload, { returning: true });
    }
    async update(payload) {
        const doc = await this.getDb({ id: payload.id });
        const tab = new biliAccountBindProxyIpRecord_1.BiliAccountBindProxyIpRecord(Object.assign(Object.assign({}, doc), payload));
        const newDoc = await this.updateDb(tab);
        return newDoc;
    }
    async updateDb(payload) {
        await biliAccountBindProxyIpRecord_1.BiliAccountBindProxyIpRecordModel.update(payload, { where: { id: payload.id } });
        return await this.getDb({ id: payload.id });
    }
    async remove(ids) {
        await biliAccountBindProxyIpRecord_1.BiliAccountBindProxyIpRecordModel.destroy({ where: { id: ids } });
    }
    async list() {
        try {
            const result = await biliAccountBindProxyIpRecord_1.BiliAccountBindProxyIpRecordModel.findAll({
                where: {},
                order: [['create_time', 'DESC']],
            });
            return result;
        }
        catch (error) {
            throw error;
        }
    }
    async getDb(query) {
        const doc = await biliAccountBindProxyIpRecord_1.BiliAccountBindProxyIpRecordModel.findOne({ where: Object.assign({}, query) });
        return doc && doc.get({ plain: true });
    }
    async biliAccountTags(searchText = '', query = {}) {
        let condition = Object.assign({}, query);
        if (searchText) {
            const encodeText = encodeURI(searchText);
            const reg = {
                [sequelize_1.Op.or]: [
                    { [sequelize_1.Op.like]: `%${searchText}%` },
                    { [sequelize_1.Op.like]: `%${encodeText}%` },
                ],
            };
            condition = Object.assign(Object.assign({}, condition), { [sequelize_1.Op.or]: [
                    {
                        name: reg,
                    },
                ] });
        }
        try {
            const result = await this.find(condition, [
                ['create_time', 'ASC'],
            ]);
            return result;
        }
        catch (error) {
            console.error('BiliAccountBindProxyIpRecordService error', error);
            throw error;
        }
    }
    async find(query, sort = []) {
        const docs = await biliAccountBindProxyIpRecord_1.BiliAccountBindProxyIpRecordModel.findAll({
            where: Object.assign({}, query),
            order: [...sort],
        });
        return docs;
    }
};
BiliAccountBindProxyIpRecordService = __decorate([
    (0, typedi_1.Service)(),
    __param(0, (0, typedi_1.Inject)('logger')),
    __metadata("design:paramtypes", [winston_1.default.Logger])
], BiliAccountBindProxyIpRecordService);
exports.default = BiliAccountBindProxyIpRecordService;
//# sourceMappingURL=biliAccountBindProxyIpRecord.js.map