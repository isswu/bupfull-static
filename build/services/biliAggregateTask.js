"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const winston_1 = __importDefault(require("winston"));
const typedi_1 = require("typedi");
const danmakuTask_1 = require("../data/models/danmakuTask");
const commentTask_1 = require("../data/models/commentTask");
const biliAccount_1 = require("../data/models/biliAccount");
let BiliAggregateTaskService = class BiliAggregateTaskService {
    constructor(logger, memoryCache) {
        this.logger = logger;
        this.memoryCache = memoryCache;
    }
    async getAggregatedTaskList() {
        var _a;
        // 获取CommentTask模型的查询结果
        const commentTasks = await commentTask_1.CommentTaskModel.findAll({
            attributes: ['uid', 'bvid'],
            where: {
                del_flag: false,
            },
            raw: true,
        });
        // 获取DanmakuTaskModel模型的查询结果
        const danmakuTasks = await danmakuTask_1.DanmakuTaskModel.findAll({
            attributes: ['uid', 'bvid'],
            where: {
                del_flag: false,
            },
            raw: true,
        });
        // 合并查询结果
        const mergedTasks = commentTasks
            .map((item) => {
            return { uid: item.uid, bvid: item.bvid };
        })
            .concat(danmakuTasks.map((item) => {
            return { uid: item.uid, bvid: item.bvid };
        }));
        // 按照uid进行分组
        const groupedTasks = mergedTasks.reduce((acc, task) => {
            if (acc[task.uid]) {
                acc[task.uid].push(task);
            }
            else {
                acc[task.uid] = [task];
            }
            return acc;
        }, {});
        const groupedList = [];
        Object.keys(groupedTasks).forEach(async (key) => {
            const obj = {
                uid: '',
                taskList: [],
                taskCount: 0,
            };
            obj.uid = key;
            obj.taskList = groupedTasks[key];
            obj.taskCount = groupedTasks[key].length;
            groupedList.push(obj);
        });
        let linkedList = [];
        for (const item of groupedList) {
            // UP主信息
            const biliUpAccount = await biliAccount_1.BiliAccountModel.findOne({
                where: {
                    mid: item.uid,
                },
            });
            linkedList.push(Object.assign(Object.assign({}, item), { uname: (_a = biliUpAccount === null || biliUpAccount === void 0 ? void 0 : biliUpAccount.name) !== null && _a !== void 0 ? _a : '', loginTime: biliUpAccount === null || biliUpAccount === void 0 ? void 0 : biliUpAccount.login_time }));
        }
        return {
            code: 0,
            data: {
                total: linkedList.length,
                list: linkedList,
            },
        };
    }
};
BiliAggregateTaskService = __decorate([
    (0, typedi_1.Service)(),
    __param(0, (0, typedi_1.Inject)('logger')),
    __param(1, (0, typedi_1.Inject)('CACHE_MANAGER')),
    __metadata("design:paramtypes", [winston_1.default.Logger, Object])
], BiliAggregateTaskService);
exports.default = BiliAggregateTaskService;
//# sourceMappingURL=biliAggregateTask.js.map