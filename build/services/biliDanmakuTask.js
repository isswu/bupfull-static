"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const typedi_1 = require("typedi");
const winston_1 = __importDefault(require("winston"));
const models_1 = require("../data/models");
const sequelize_1 = require("sequelize");
const uuid_1 = require("uuid");
const util_1 = require("../config/util");
const const_1 = require("../config/const");
const simpleTask_1 = require("../data/models/simpleTask");
const simpleTaskExecuteItem_1 = require("../data/models/simpleTaskExecuteItem");
const simpleTaskExecuteRecord_1 = require("../data/models/simpleTaskExecuteRecord");
const danmakuTask_1 = require("../data/models/danmakuTask");
const accountGroup_1 = require("../data/models/accountGroup");
const cityDistributeTemplateItem_1 = require("../data/models/cityDistributeTemplateItem");
const danmakuTaskExecuteRecord_1 = require("../data/models/danmakuTaskExecuteRecord");
const danmakuTaskExecuteItem_1 = require("../data/models/danmakuTaskExecuteItem");
const simpleTaskExecuteStatistic_1 = require("../data/models/simpleTaskExecuteStatistic");
const danmakuTaskExecuteStatistic_1 = require("../data/models/danmakuTaskExecuteStatistic");
const danmaku_1 = require("../data/models/danmaku");
const tag_1 = require("../data/models/tag");
const biliAccount_1 = require("../data/models/biliAccount");
const bili_bot_services_1 = require("../protos/bili_bot_services");
const typedi_2 = require("typedi");
const cron_1 = __importDefault(require("../services/cron"));
const toad_scheduler_1 = require("toad-scheduler");
const userBiliDanmuTaskBindRecord_1 = require("../data/models/binding/userBiliDanmuTaskBindRecord");
let BiliDanmakuTaskService = class BiliDanmakuTaskService {
    constructor(logger, memoryCache) {
        this.logger = logger;
        this.memoryCache = memoryCache;
        this.DANMAKU_RUN_TASKS = 'PostDanmaku'; // 发弹幕任务
        this.CONTROL_DANMAKU_RUN_TASKS = 'ControlDanmuku'; // 删弹幕任务
        this.intervalSchedule = new toad_scheduler_1.ToadScheduler();
    }
    /**
     * 获取弹幕任务响应
     *
     * @param payloads
     */
    async getBiliDanmakuTaskResp(payloads) {
        const { record_id, run_tasks } = payloads;
        const executeRecord = await danmakuTaskExecuteRecord_1.DanmakuTaskExecuteRecordModel.findOne({
            where: {
                id: record_id,
            },
        });
        if (!executeRecord) {
            return { code: 1, msg: 'ID参数无效, 未找到记录' };
        }
        const danmakuTask = await danmakuTask_1.DanmakuTaskModel.findOne({
            where: {
                id: executeRecord.task_id,
            },
        });
        if (!danmakuTask || danmakuTask.del_flag || !danmakuTask.enable_flag) {
            return { code: 1, msg: '任务不存在或已禁用' };
        }
        if (danmakuTask.status != simpleTask_1.TaskStatusEnum.be_queuing) {
            return { code: 1, msg: '任务状态非排队中' };
        }
        if (danmakuTask.task_type == danmakuTask_1.DanmakuTaskTypeEnum.CONTROL &&
            !danmakuTask.control_flag) {
            return { code: 1, msg: '控制弹幕开关已关闭' };
        }
        const blCookies = [];
        const payLoad = {
            type: '2',
            extra: '',
            blCookies,
        };
        const biliTask = {
            uid: danmakuTask.uid,
            bvid: danmakuTask.bvid,
            recordId: record_id,
            runTasks: run_tasks,
            payLoad,
        };
        const itemList = await danmakuTaskExecuteItem_1.DanmakuTaskExecuteItemModel.findAll({
            where: {
                record_id: record_id,
                status: simpleTaskExecuteItem_1.ExecuteItemStatusEnum.WAIT,
            },
            order: [['execute_time', 'ASC']],
        });
        // 存在则将弹幕塞入列表，不存在则新增BlCookie实例
        itemList.forEach((item) => {
            let danmakuWrap;
            const executeTime = new Date(item.execute_time || new Date());
            if (run_tasks == this.DANMAKU_RUN_TASKS) {
                const biliTaskDanmaku = {
                    id: '',
                    content: item.content,
                };
                danmakuWrap = {
                    danmaku: biliTaskDanmaku,
                    danmakuController: undefined,
                };
            }
            else if (run_tasks == this.CONTROL_DANMAKU_RUN_TASKS) {
                const controlTime = new Date(danmakuTask.control_time || new Date());
                const timestamp = controlTime.getTime() / 1000;
                const midWhiteList = danmakuTask.mid_white_list
                    .split('\n')
                    .filter((item) => item !== '')
                    .map((item) => {
                    return {
                        mid: item,
                    };
                });
                const danmakuController = {
                    controlTime: timestamp.toString(),
                    whiteList: midWhiteList,
                };
                danmakuWrap = {
                    danmaku: undefined,
                    danmakuController,
                };
            }
            blCookies.push({
                snapId: item.id,
                mid: item.mid,
                proxyIp: item.proxy_ip,
                cookie: item.cookie,
                ua: item.user_agent,
                carrier: {
                    carryType: bili_bot_services_1.BiliTask_CarryType.CDanmaku,
                    commentWrap: undefined,
                    danmakuWrap: danmakuWrap,
                },
                scheduleTime: executeTime.getTime().toString(),
            });
        });
        // 当前时间
        const now = new Date();
        await danmakuTask_1.DanmakuTaskModel.update({
            status: simpleTask_1.TaskStatusEnum.on_execute,
            update_by: 'system',
            update_time: now,
        }, {
            where: {
                id: danmakuTask.id,
            },
        });
        return { code: 200, data: biliTask };
    }
    /**
     * 统计弹幕任务结果
     *
     * @param payloads
     */
    async countDanmakuTaskResult(payloads) {
        const { snapId, mid, recordId, status } = payloads;
        // 任务执行记录
        const executeRecord = await danmakuTaskExecuteRecord_1.DanmakuTaskExecuteRecordModel.findOne({
            where: {
                id: recordId,
            },
        });
        if (!executeRecord) {
            this.logger.error('DanmakuTaskExecuteRecord NotFound, id: {}', recordId);
            return;
        }
        // 任务执行明细
        const executeItem = await danmakuTaskExecuteItem_1.DanmakuTaskExecuteItemModel.findOne({
            where: {
                id: snapId,
            },
        });
        if (!executeItem) {
            this.logger.error('DanmakuTaskExecuteItem NotFound, id: {}', snapId);
            return;
        }
        let itemStatus = simpleTaskExecuteItem_1.ExecuteItemStatusEnum.UNRECOGNIZED;
        switch (status) {
            case bili_bot_services_1.SubtaskResult_Status.Running:
                itemStatus = simpleTaskExecuteItem_1.ExecuteItemStatusEnum.RUNNING;
                break;
            case bili_bot_services_1.SubtaskResult_Status.Success:
                itemStatus = simpleTaskExecuteItem_1.ExecuteItemStatusEnum.SUCCESS;
                break;
            case bili_bot_services_1.SubtaskResult_Status.Failed:
                itemStatus = simpleTaskExecuteItem_1.ExecuteItemStatusEnum.FAIL;
                break;
        }
        // 当前时间
        const now = new Date();
        // 更新明细
        await danmakuTaskExecuteItem_1.DanmakuTaskExecuteItemModel.update({
            status: itemStatus,
            update_by: 'system',
            update_time: now,
        }, {
            where: {
                id: snapId,
            },
        });
        if (itemStatus == simpleTaskExecuteItem_1.ExecuteItemStatusEnum.SUCCESS ||
            itemStatus == simpleTaskExecuteItem_1.ExecuteItemStatusEnum.FAIL) {
            // 是否成功
            const success = itemStatus == simpleTaskExecuteItem_1.ExecuteItemStatusEnum.SUCCESS;
            // 更新记录
            await danmakuTaskExecuteRecord_1.DanmakuTaskExecuteRecordModel.update({
                total_count: executeRecord.total_count + 1,
                success_count: executeRecord.success_count + (success ? 1 : 0),
                update_by: 'system',
                update_time: now,
            }, {
                where: {
                    id: recordId,
                },
            });
            // 更新运行统计数据
            const fmtDate = (0, util_1.dateFormat)(executeRecord.create_time, 2);
            const executeStatistic = await danmakuTaskExecuteStatistic_1.DanmakuTaskExecuteStatisticModel.findOne({
                where: {
                    mid,
                    type: simpleTaskExecuteStatistic_1.StatisticTypeEnum.DAY,
                    date: fmtDate,
                },
            });
            if (executeStatistic == null) {
                await danmakuTaskExecuteStatistic_1.DanmakuTaskExecuteStatisticModel.create({
                    id: (0, uuid_1.v4)(),
                    mid,
                    type: simpleTaskExecuteStatistic_1.StatisticTypeEnum.DAY,
                    date: fmtDate,
                    total_count: 1,
                    success_count: success ? 1 : 0,
                    create_by: 'system',
                    create_time: now,
                    update_by: 'system',
                    update_time: now,
                });
            }
            else {
                await danmakuTaskExecuteStatistic_1.DanmakuTaskExecuteStatisticModel.update({
                    total_count: executeStatistic.total_count + 1,
                    success_count: executeStatistic.success_count + (success ? 1 : 0),
                    update_by: 'system',
                    update_time: now,
                }, {
                    where: {
                        id: executeStatistic.id,
                    },
                });
            }
        }
    }
    /**
     * 结束执弹幕任务
     *
     * @param payloads
     */
    async endDanmakuTask(payloads) {
        const { recordId } = payloads;
        // 任务执行记录
        const executeRecord = await danmakuTaskExecuteRecord_1.DanmakuTaskExecuteRecordModel.findOne({
            where: {
                id: recordId,
            },
        });
        if (!executeRecord) {
            this.logger.error('DanmaTaskExecuteRecord NotFound, id: {}', recordId);
            return;
        }
        // 任务
        const danmakuTask = await danmakuTask_1.DanmakuTaskModel.findOne({
            where: {
                id: executeRecord.task_id,
            },
        });
        if (!danmakuTask) {
            this.logger.error('DanmaTask NotFound, id: {}', recordId);
            return;
        }
        // 当前时间
        const now = new Date();
        await danmakuTask_1.DanmakuTaskModel.update({
            status: simpleTask_1.TaskStatusEnum.execute_complete,
            update_by: 'system',
            update_time: now,
        }, {
            where: {
                id: danmakuTask.id,
            },
        });
    }
    async createIntervalTask(userId, accessToken) {
        const mins = 9;
        this.logger.info('[panel][创建interval任务], 任务名: %s', 'danmaku-task');
        const job = new toad_scheduler_1.SimpleIntervalJob({ minutes: mins }, new toad_scheduler_1.Task('danmaku-task', () => {
            this.logger.info('[panel][开始运行interval任务], 任务名: %s will run every %d minutes', 'danmaku-task', mins);
            this.autoRunDanmakuTask(userId, accessToken);
        }));
        // 添加任务到调度器
        this.intervalSchedule.addSimpleIntervalJob(job);
    }
    async autoRunDanmakuTask(userId, accessToken) {
        const tasks = await danmakuTask_1.DanmakuTaskModel.findAll({
            where: {
                del_flag: false,
                enable_flag: true,
                control_flag: true,
                publish_flag: true,
                task_type: danmakuTask_1.DanmakuTaskTypeEnum.CONTROL,
                status: {
                    [sequelize_1.Op.or]: [
                        simpleTask_1.TaskStatusEnum.be_queuing,
                        simpleTask_1.TaskStatusEnum.on_execute,
                        simpleTask_1.TaskStatusEnum.execute_complete,
                    ],
                },
                update_time: {
                    [sequelize_1.Op.lt]: new Date(new Date().getTime() - 6 * 60000),
                },
            },
            order: [['update_time', 'ASC']],
            offset: 0,
            limit: 6,
        });
        this.logger.info('[panel][运行自启动任务], 任务名: %s, 任务数: %d', 'danmaku-task', tasks.length);
        for (let i = 0; i < tasks.length; i++) {
            const element = tasks[i];
            await (0, util_1.delay)((0, util_1.getRandomNumber)(1, 5) * 1000);
            await this.stopDanmakuControlTask({ id: element.id }, userId, false);
            await this.changeDanmakuControl({ id: element.id, control_flag: true }, userId);
            await this.executeDanmakuTask({ id: element.id }, userId, accessToken, true);
        }
    }
    async resetDanmakuTaskStatus(userId) {
        await danmakuTask_1.DanmakuTaskModel.update({
            status: simpleTask_1.TaskStatusEnum.execute_complete,
            update_by: userId,
            update_time: new Date(),
        }, {
            where: {
                del_flag: false,
                enable_flag: true,
                control_flag: true,
                publish_flag: true,
                task_type: danmakuTask_1.DanmakuTaskTypeEnum.CONTROL,
            },
        });
    }
    async autoRunDanmakuTasksWithoutLimit(userId, accessToken) {
        const taskFlag = await this.memoryCache.get('danmaku-task');
        if (taskFlag) {
            return {
                code: 400,
                msg: '弹幕任务正在运行中，请勿重复启动！',
            };
        }
        await this.memoryCache.set('danmaku-task', true);
        // 重置弹幕任务状态
        await this.resetDanmakuTaskStatus(userId);
        const tasks = await danmakuTask_1.DanmakuTaskModel.findAll({
            where: {
                del_flag: false,
                enable_flag: true,
                control_flag: true,
                publish_flag: true,
                task_type: danmakuTask_1.DanmakuTaskTypeEnum.CONTROL,
                status: {
                    [sequelize_1.Op.or]: [simpleTask_1.TaskStatusEnum.execute_complete],
                },
            },
        });
        this.createIntervalTask(userId, accessToken);
        this.executeDanmakuTasksWithDelay(userId, accessToken, tasks);
        return {
            code: 200,
            msg: '弹幕任务启动成功',
        };
    }
    async executeDanmakuTasksWithDelay(userId, accessToken, tasks) {
        for (let i = 0; i < tasks.length; i++) {
            const element = tasks[i];
            await (0, util_1.delay)((0, util_1.getRandomNumber)(2, 8) * 1000);
            await this.executeDanmakuTask({ id: element.id }, userId, accessToken);
        }
    }
    /**
     * 获取弹幕任务列表
     *
     * @param payloads
     * @param isAdmin
     * @param dataIn
     */
    async getDanmakuList(payloads, isAdmin, dataIn = []) {
        const uid = payloads.uid;
        const taskType = payloads.taskType;
        const searchText = payloads.searchValue;
        const page_index = Number(payloads.page_index || '0');
        const page_size = Number(payloads.page_size || '0');
        const sorterQuery = JSON.parse(payloads.sorter || '{}');
        let query = {};
        let data_in = {};
        let order = [];
        let other = {};
        if (sorterQuery) {
            const { field, type } = sorterQuery;
            if (field && type) {
                order.unshift([field, type]);
            }
        }
        if (searchText) {
            const reg = {
                [sequelize_1.Op.or]: [
                    { [sequelize_1.Op.like]: `%${searchText}%` },
                    { [sequelize_1.Op.like]: `%${encodeURI(searchText)}%` },
                ],
            };
            query = {
                [sequelize_1.Op.or]: [
                    {
                        bvid: reg,
                    },
                    {
                        uid: reg,
                    },
                ],
            };
        }
        if (!isAdmin) {
            data_in = { id: { [sequelize_1.Op.in]: dataIn } };
        }
        if (taskType) {
            if (uid) {
                other = {
                    uid,
                    task_type: Number(taskType),
                    del_flag: false,
                };
            }
            else {
                other = {
                    task_type: Number(taskType),
                    del_flag: false,
                };
            }
        }
        else {
            other = {
                del_flag: false,
            };
        }
        const result = await danmakuTask_1.DanmakuTaskModel.findAndCountAll({
            order: order,
            offset: (page_index - 1) * page_size,
            limit: page_size,
            where: Object.assign(Object.assign(Object.assign({}, query), data_in), other),
        });
        let list = [];
        for (let i = 0; i < result.rows.length; i++) {
            let _item = result.rows[i].toJSON();
            // 账号组信息
            const accountGroup = await accountGroup_1.AccountGroupModel.findOne({
                where: {
                    template_code: _item.template_code,
                },
            });
            // UP主信息
            const biliUpAccount = await biliAccount_1.BiliAccountModel.findOne({
                where: {
                    mid: _item.uid,
                },
            });
            // 标签信息
            const danmakuTag = await tag_1.TagModel.findOne({
                where: {
                    id: _item.danmaku_tag_id,
                },
            });
            list.push(Object.assign(Object.assign({}, _item), { template_name: (accountGroup === null || accountGroup === void 0 ? void 0 : accountGroup.template_name) || '', bili_up_account: {
                    mid: _item.uid,
                    name: biliUpAccount === null || biliUpAccount === void 0 ? void 0 : biliUpAccount.name,
                    avatar: biliUpAccount === null || biliUpAccount === void 0 ? void 0 : biliUpAccount.avatar,
                }, danmaku_tag: danmakuTag }));
        }
        return {
            code: 0,
            msg: 'Success',
            data: {
                total: result.count,
                list,
            },
        };
    }
    /**
     * 创建或修改弹幕任务
     *
     * @param payloads 请求参数
     * @param userId
     */
    async createOrUpdateDanmakuTask(payloads, userId) {
        const { id, task_type, uid, bvid, template_code, danmaku_tag_id, control_time, mid_white_list, publish_remark, } = payloads;
        // 校验参数
        if (danmakuTask_1.DanmakuTaskTypeEnum.PUBLISH == task_type) {
            if (!template_code) {
                return { code: -1, msg: '账号组为空' };
            }
        }
        else {
            if (!control_time) {
                return { code: -1, msg: '请设置控制弹幕时间' };
            }
            if (!mid_white_list) {
                return { code: -1, msg: '弹幕白名单为空' };
            }
        }
        const now = new Date();
        if (id) {
            let danmakuTask = await danmakuTask_1.DanmakuTaskModel.findOne({
                where: {
                    id,
                },
            });
            if (!danmakuTask || !danmakuTask.enable_flag || danmakuTask.del_flag) {
                return { code: 400, msg: '任务已停用或已删除' };
            }
            switch (danmakuTask.task_type) {
                case danmakuTask_1.DanmakuTaskTypeEnum.PUBLISH:
                    await danmakuTask_1.DanmakuTaskModel.update({
                        danmaku_tag_id,
                        template_code,
                        publish_remark,
                        update_by: userId,
                        update_time: now,
                    }, {
                        where: {
                            id: danmakuTask.id,
                        },
                    });
                    break;
                case danmakuTask_1.DanmakuTaskTypeEnum.CONTROL:
                    await danmakuTask_1.DanmakuTaskModel.update({
                        control_time,
                        mid_white_list,
                        publish_remark,
                        update_by: userId,
                        update_time: now,
                    }, {
                        where: {
                            id: danmakuTask.id,
                        },
                    });
                    break;
            }
        }
        else {
            switch (task_type) {
                case danmakuTask_1.DanmakuTaskTypeEnum.PUBLISH:
                    const danamakuPublishTask = await danmakuTask_1.DanmakuTaskModel.findOne({
                        where: {
                            task_type: danmakuTask_1.DanmakuTaskTypeEnum.PUBLISH,
                            bvid,
                            del_flag: false,
                        },
                    });
                    if (danamakuPublishTask) {
                        return {
                            code: -1,
                            msg: `发布弹幕任务已存在, 不可重复创建, 视频ID: ${bvid}`,
                        };
                    }
                    const dTaskId = (0, uuid_1.v4)();
                    await danmakuTask_1.DanmakuTaskModel.create({
                        id: dTaskId,
                        task_type,
                        uid,
                        bvid,
                        template_code,
                        danmaku_tag_id,
                        execute_duration: 1,
                        duration_unit: simpleTask_1.DurationUnitEnum.H,
                        danmaku_type: danmakuTask_1.DanmakuTypeEnum.BY_SORT,
                        publish_flag: false,
                        publish_remark,
                        status: simpleTask_1.TaskStatusEnum.draft,
                        enable_flag: true,
                        control_flag: false,
                        mid_white_list: '',
                        create_by: userId,
                        create_time: now,
                        update_by: userId,
                        update_time: now,
                        del_flag: false,
                    });
                    await userBiliDanmuTaskBindRecord_1.UserBiliDanmuTaskBindRecordModel.create({
                        id: (0, uuid_1.v4)(),
                        user_id: userId,
                        danmu_task_id: dTaskId,
                        readable: true,
                        writable: true,
                        create_by: userId,
                        create_time: now,
                        update_by: userId,
                        update_time: now,
                    });
                    break;
                case danmakuTask_1.DanmakuTaskTypeEnum.CONTROL:
                    const danmakuControlTask = await danmakuTask_1.DanmakuTaskModel.findOne({
                        where: {
                            task_type: danmakuTask_1.DanmakuTaskTypeEnum.CONTROL,
                            bvid,
                            del_flag: false,
                        },
                    });
                    if (danmakuControlTask) {
                        return {
                            code: -1,
                            msg: `控制弹幕任务已存在, 不可重复创建, 视频ID: ${bvid}`,
                        };
                    }
                    const dcTaskId = (0, uuid_1.v4)();
                    await danmakuTask_1.DanmakuTaskModel.create({
                        id: dcTaskId,
                        task_type,
                        uid,
                        bvid,
                        template_code: '',
                        danmaku_tag_id: '',
                        execute_duration: 1,
                        duration_unit: simpleTask_1.DurationUnitEnum.H,
                        danmaku_type: danmakuTask_1.DanmakuTypeEnum.BY_SORT,
                        publish_flag: false,
                        publish_remark,
                        status: simpleTask_1.TaskStatusEnum.draft,
                        enable_flag: true,
                        control_flag: false,
                        control_time,
                        mid_white_list,
                        create_by: userId,
                        create_time: now,
                        update_by: userId,
                        update_time: now,
                        del_flag: false,
                    });
                    await userBiliDanmuTaskBindRecord_1.UserBiliDanmuTaskBindRecordModel.create({
                        id: (0, uuid_1.v4)(),
                        user_id: userId,
                        danmu_task_id: dcTaskId,
                        readable: true,
                        writable: true,
                        create_by: userId,
                        create_time: now,
                        update_by: userId,
                        update_time: now,
                    });
                    break;
            }
        }
        return { code: 0, msg: '操作成功' };
    }
    /**
     * 发布弹幕任务
     *
     * @param payloads 请求参数
     * @param userId
     */
    async publishDanmaku(payloads, userId) {
        const { id } = payloads;
        let danmakuTask = await danmakuTask_1.DanmakuTaskModel.findOne({
            where: {
                id: id,
            },
        });
        if (!danmakuTask) {
            return { code: 400, msg: 'ID无效, 任务没找到' };
        }
        if (danmakuTask.publish_flag) {
            return { code: 400, msg: '无效操作, 请刷新后重试' };
        }
        const now = new Date();
        await danmakuTask_1.DanmakuTaskModel.update({
            status: simpleTask_1.TaskStatusEnum.wait_execute,
            publish_flag: true,
            publish_by: userId,
            publish_time: now,
            publish_remark: '',
            update_by: userId,
            update_time: now,
        }, {
            where: {
                id: id,
            },
            returning: true,
        });
        return { code: 0, msg: '发布成功' };
    }
    /**
     * 修改弹幕任务的状态
     *
     * @param payloads
     * @param userId
     */
    async changeDanmakuEnable(payloads, userId) {
        const { id, enable_flag } = payloads;
        const danmakuTask = await danmakuTask_1.DanmakuTaskModel.findOne({
            where: {
                id,
            },
        });
        if (!danmakuTask || danmakuTask.del_flag) {
            return { code: 1, msg: 'ID参数无效或已删除' };
        }
        if (danmakuTask.enable_flag == enable_flag) {
            return { code: 0, msg: '操作成功' };
        }
        const now = new Date();
        await danmakuTask_1.DanmakuTaskModel.update({
            enable_flag,
            update_by: userId,
            update_time: now,
        }, {
            where: {
                id: danmakuTask.id,
            },
            returning: true,
        });
        return { code: 0, msg: '操作成功' };
    }
    /**
     * 修改弹幕任务的控制开关状态
     *
     * @param payloads
     * @param userId
     */
    async changeDanmakuControl(payloads, userId) {
        const { id, control_flag } = payloads;
        const danmakuTask = await danmakuTask_1.DanmakuTaskModel.findOne({
            where: {
                id,
            },
        });
        if (!danmakuTask || danmakuTask.del_flag) {
            return { code: 1, msg: 'ID参数无效或已删除' };
        }
        if (danmakuTask.control_flag == control_flag) {
            return { code: 0, msg: '操作成功' };
        }
        // 控制开关打开, 判断当前up账号是否已登录
        if (control_flag) {
            const biliUpAccount = await biliAccount_1.BiliAccountModel.findOne({
                where: {
                    mid: danmakuTask.uid,
                },
            });
            if (!biliUpAccount || !biliUpAccount.cookie) {
                return { code: -1, msg: '请先扫码登录UP主账号' };
            }
        }
        else {
            //开关关闭, 关闭定时任务
            const cronService = typedi_2.Container.get(cron_1.default);
            cronService.delCronByTaskId(danmakuTask.id);
        }
        const now = new Date();
        await danmakuTask_1.DanmakuTaskModel.update({
            control_flag,
            update_by: userId,
            update_time: now,
        }, {
            where: {
                id: danmakuTask.id,
            },
            returning: true,
        });
        return { code: 0, msg: '操作成功' };
    }
    /**
     * 删除弹幕任务
     *
     * @param payloads
     * @param userId
     */
    async delDanmakuTask(payloads, userId) {
        const { id } = payloads;
        const danmakuTask = await danmakuTask_1.DanmakuTaskModel.findOne({
            where: {
                id,
            },
        });
        if (!danmakuTask) {
            return { code: 1, msg: 'ID参数无效' };
        }
        if (danmakuTask.del_flag) {
            return { code: 1, msg: '该任务已删除' };
        }
        const now = new Date();
        await danmakuTask_1.DanmakuTaskModel.update({
            del_flag: true,
            update_by: userId,
            update_time: now,
        }, {
            where: {
                id: danmakuTask.id,
            },
            returning: true,
        });
        // 停止定时任务
        const cronService = typedi_2.Container.get(cron_1.default);
        cronService.delCronByTaskId(danmakuTask.id);
        // cronService.stopTask(CRON_NAME_DIC['11'], danmakuTask.id, recordId);
        return { code: 0, msg: '操作成功' };
    }
    async stopDanmakuControlTask(payloads, userId, delCron = true) {
        const { id } = payloads;
        // 获取任务实例
        const danmakuTask = await danmakuTask_1.DanmakuTaskModel.findOne({
            where: {
                id: id,
                publish_flag: true,
                //status: TaskStatusEnum.on_execute,
                status: {
                    [sequelize_1.Op.or]: [simpleTask_1.TaskStatusEnum.be_queuing, simpleTask_1.TaskStatusEnum.on_execute],
                },
                task_type: danmakuTask_1.DanmakuTaskTypeEnum.CONTROL,
            },
        });
        if (!danmakuTask) {
            return {
                code: 400,
                msg: `该任务未找到, 不存在或未发布, 任务ID: ${id}`,
            };
        }
        // 将任务状态设置为已完成，并关闭控制开关
        await danmakuTask_1.DanmakuTaskModel.update({
            control_flag: false,
            status: simpleTask_1.TaskStatusEnum.execute_complete,
            update_by: userId,
            update_time: new Date(),
        }, {
            where: {
                id: danmakuTask.id,
            },
        });
        if (delCron) {
            // 停止定时任务
            const cronService = typedi_2.Container.get(cron_1.default);
            cronService.delCronByTaskId(danmakuTask.id);
            // cronService.stopTask(CRON_NAME_DIC['11'], danmakuTask.id, recordId);
        }
        return { code: 0, msg: '执行成功' };
    }
    /**
     * 执行弹幕任务
     *
     * @param payloads 请求参数
     * @param userId
     * @param accessToken
     * @param toSchedule
     */
    async executeDanmakuTask(payloads, userId, accessToken, toSchedule = true) {
        const { id } = payloads;
        // 获取任务实例
        const danmakuTask = await danmakuTask_1.DanmakuTaskModel.findOne({
            where: {
                id: id,
                publish_flag: true,
            },
        });
        if (!danmakuTask) {
            return {
                code: 400,
                msg: `该任务未找到, 不存在或未发布, 任务ID: ${id}`,
            };
        }
        // 判断一下任务类型
        if (danmakuTask.task_type == danmakuTask_1.DanmakuTaskTypeEnum.CONTROL) {
            // 开启了控制开关
            if (danmakuTask.control_flag) {
                await this.createAndRunControlDanmakuTask(userId, accessToken, danmakuTask, toSchedule);
                return { code: 0, msg: '执行成功' };
            }
            else {
                return { code: -1, msg: '请先开启控制弹幕开关' };
            }
        }
        // 获取弹幕池记录列表
        const danmakuList = await danmaku_1.DanmakuModel.findAll({
            where: {
                tag_id: danmakuTask.danmaku_tag_id,
            },
            raw: true,
        });
        if (!danmakuList.length) {
            return {
                code: 400,
                msg: `该标签对应的弹幕记录为空, 请检查后重新执行`,
            };
        }
        // 获取账号组
        const { template_code } = danmakuTask;
        const accountGroup = await accountGroup_1.AccountGroupModel.findOne({
            where: {
                template_code: template_code,
                publish_flag: true,
                enable_flag: true,
            },
        });
        if (!accountGroup) {
            return {
                code: 400,
                msg: `账号组未找到, 不存在或未发布或已停用, 模版编码: ${template_code}`,
            };
        }
        // 获取账号组明细
        const cityDistributeTemplateItemList = await cityDistributeTemplateItem_1.CityDistributeTemplateItemModel.findAll({
            where: {
                template_code: template_code,
                enable_flag: true,
            },
            raw: true,
        });
        if (!cityDistributeTemplateItemList.length) {
            return {
                code: 400,
                msg: `有效的账号组明细为空, 模版编码: ${template_code}`,
            };
        }
        // 分配类型
        const { distribute_type } = accountGroup;
        // 判断分配类型
        switch (distribute_type) {
            case accountGroup_1.DistributeTypeEnum.ROUND_ROBIN:
                // 以轮询的方式执行弹幕任务
                await this.roundRobinExecuteDanmakuTask(userId, accessToken, danmakuTask, danmakuList, cityDistributeTemplateItemList);
                return { code: 0, msg: '执行成功' };
            default:
                return {
                    code: 400,
                    msg: `无效的分配方式, 模版编码: ${template_code}`,
                };
        }
    }
    async createAndRunControlDanmakuTask(userId, accessToken, danmakuTask, toSchedule) {
        const biliUpAccount = await biliAccount_1.BiliAccountModel.findOne({
            where: {
                mid: danmakuTask.uid,
            },
        });
        // 创建任务执行记录
        const now = new Date();
        let recordId = (0, uuid_1.v4)();
        await danmakuTaskExecuteRecord_1.DanmakuTaskExecuteRecordModel.create({
            id: recordId,
            schedule_type: simpleTaskExecuteRecord_1.ScheduleTypeEnum.RIGHT_NOW,
            schedule_id: recordId,
            task_id: danmakuTask.id,
            start_time: now,
            total_count: 1,
            success_count: 0,
            create_by: userId,
            create_time: now,
            update_by: userId,
            update_time: now,
        });
        // 弹幕控制任务明细列表
        let controlTaskExecuteItem = {
            id: (0, uuid_1.v4)(),
            record_id: recordId,
            mid: (biliUpAccount === null || biliUpAccount === void 0 ? void 0 : biliUpAccount.mid) || '',
            content: '',
            proxy_ip: (biliUpAccount === null || biliUpAccount === void 0 ? void 0 : biliUpAccount.proxy_ip) || '',
            cookie: (biliUpAccount === null || biliUpAccount === void 0 ? void 0 : biliUpAccount.cookie) || '',
            user_agent: (biliUpAccount === null || biliUpAccount === void 0 ? void 0 : biliUpAccount.user_agent) || '',
            status: simpleTaskExecuteItem_1.ExecuteItemStatusEnum.WAIT,
            execute_time: new Date(now.getTime()),
            execute_result: '',
            create_by: userId,
            create_time: now,
            update_by: userId,
            update_time: now,
        };
        // 写入数据库
        await danmakuTaskExecuteItem_1.DanmakuTaskExecuteItemModel.create(controlTaskExecuteItem);
        // 将任务状态设置为排队中
        await danmakuTask_1.DanmakuTaskModel.update({
            status: simpleTask_1.TaskStatusEnum.be_queuing,
            update_by: userId,
            update_time: now,
        }, {
            where: {
                id: danmakuTask.id,
            },
        });
        // 执行定时任务
        const cronService = typedi_2.Container.get(cron_1.default);
        // 启动BiliBot
        cronService.runTask(accessToken, const_1.CRON_NAME_DIC['11'], danmakuTask.id, recordId, toSchedule);
    }
    /**
     * 以轮询的方式执行弹幕任务
     *
     * @param userId
     * @param accessToken
     * @param danmakuTask
     * @param danmakuList
     * @param cityDistributeTemplateItemList
     */
    async roundRobinExecuteDanmakuTask(userId, accessToken, danmakuTask, danmakuList, cityDistributeTemplateItemList) {
        const now = new Date();
        const durationSecs = this.getDurationSecs(danmakuTask.execute_duration, danmakuTask.duration_unit);
        let danmakuCount = danmakuList.length;
        // 每次毫秒数
        const perTimesMilliSecs = (durationSecs * 1000) / danmakuCount;
        // 创建任务执行记录
        let recordId = (0, uuid_1.v4)();
        await danmakuTaskExecuteRecord_1.DanmakuTaskExecuteRecordModel.create({
            id: recordId,
            schedule_type: simpleTaskExecuteRecord_1.ScheduleTypeEnum.RIGHT_NOW,
            schedule_id: recordId,
            task_id: danmakuTask.id,
            start_time: now,
            total_count: danmakuCount,
            success_count: 0,
            create_by: userId,
            create_time: now,
            update_by: userId,
            update_time: now,
        });
        // 平均每个城市分配多少条
        let avgPerCityCount = (danmakuCount / cityDistributeTemplateItemList.length) | 0;
        let index = 0;
        let danmakuIndex = 0;
        while (index < cityDistributeTemplateItemList.length) {
            // 平均每个城市要发送的弹幕数
            let limit = avgPerCityCount;
            if (index == cityDistributeTemplateItemList.length - 1) {
                limit = danmakuCount - avgPerCityCount * index;
            }
            // 城市分配明细
            let cityDistributeTemplateItem = cityDistributeTemplateItemList[index];
            // 根据城市编码获取执行任务最少的小号
            let cityCode = cityDistributeTemplateItem.city_code;
            // 获取待执行发送弹幕的小号列表
            const results = await models_1.sequelize.query('select t.* ' +
                'from (' +
                'select t1.mid, t1.login_time, t1.proxy_ip, t1.cookie, t1.user_agent, ' +
                'ifnull(t3.total_count, 0) as total_count ' +
                'from bili_account t1 ' +
                'join proxy_ip t2 on t1.proxy_ip = t2.ip and t2.enable_flag = 1 and t2.city_code = :cityCode ' +
                'left join danmaku_task_execute_statistic t3 on t1.mid = t3.mid and t3.type = 3 and t3.date = :date ' +
                'where t1.login_status = 1 ) t ' +
                'order by t.total_count asc, t.login_time asc ' +
                'limit :limit', {
                type: sequelize_1.QueryTypes.SELECT,
                replacements: {
                    cityCode,
                    date: (0, util_1.dateFormat)(now, 2),
                    limit,
                },
            });
            // 任务明细列表
            let taskExecuteItemList = [];
            // 遍历要发送的弹幕数, 小号不够时, 需要再从头取小号
            for (let i = 0; i < limit; i++) {
                // 取小号
                const biliAccount = results[i % results.length];
                // 取弹幕
                let danmu = danmakuList[danmakuIndex++].content;
                // 记录数据
                taskExecuteItemList.push({
                    id: (0, uuid_1.v4)(),
                    record_id: recordId,
                    mid: biliAccount.mid,
                    content: danmu,
                    proxy_ip: biliAccount.proxy_ip || '',
                    cookie: biliAccount.cookie,
                    user_agent: biliAccount.user_agent || '',
                    status: simpleTaskExecuteItem_1.ExecuteItemStatusEnum.WAIT,
                    execute_time: new Date(now.getTime() + index * perTimesMilliSecs),
                    execute_result: '',
                    create_by: userId,
                    create_time: now,
                    update_by: userId,
                    update_time: now,
                });
            }
            // 写入数据库
            if (taskExecuteItemList.length > 0) {
                await danmakuTaskExecuteItem_1.DanmakuTaskExecuteItemModel.bulkCreate(taskExecuteItemList);
            }
            index++;
        }
        // 将任务状态设置为排队中
        await danmakuTask_1.DanmakuTaskModel.update({
            status: simpleTask_1.TaskStatusEnum.be_queuing,
            update_by: userId,
            update_time: now,
        }, {
            where: {
                id: danmakuTask.id,
            },
        });
        // 执行定时任务
        const cronService = typedi_2.Container.get(cron_1.default);
        // 启动BiliBot
        cronService.runTask(accessToken, const_1.CRON_NAME_DIC['10'], danmakuTask.id, recordId);
    }
    /**
     * 获取时长，单位为秒
     *
     * @param executeDuration
     * @param durationUnit
     * @returns
     */
    getDurationSecs(executeDuration, durationUnit) {
        switch (String(durationUnit)) {
            case simpleTask_1.DurationUnitEnum.H:
                return executeDuration * 60 * 60;
            case simpleTask_1.DurationUnitEnum.M:
                return executeDuration * 60;
            case simpleTask_1.DurationUnitEnum.S:
                return executeDuration;
            default:
                throw new Error('时长单位不支持');
        }
    }
};
BiliDanmakuTaskService = __decorate([
    (0, typedi_1.Service)(),
    __param(0, (0, typedi_1.Inject)('logger')),
    __param(1, (0, typedi_1.Inject)('CACHE_MANAGER')),
    __metadata("design:paramtypes", [winston_1.default.Logger, Object])
], BiliDanmakuTaskService);
exports.default = BiliDanmakuTaskService;
//# sourceMappingURL=biliDanmakuTask.js.map