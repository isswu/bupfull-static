"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const typedi_1 = require("typedi");
const winston_1 = __importDefault(require("winston"));
const models_1 = require("../data/models");
const sequelize_1 = require("sequelize");
const uuid_1 = require("uuid");
const util_1 = require("../config/util");
const const_1 = require("../config/const");
const commentTask_1 = require("../data/models/commentTask");
const simpleTask_1 = require("../data/models/simpleTask");
const accountGroup_1 = require("../data/models/accountGroup");
const cityDistributeTemplateItem_1 = require("../data/models/cityDistributeTemplateItem");
const simpleTaskExecuteRecord_1 = require("../data/models/simpleTaskExecuteRecord");
const simpleTaskExecuteItem_1 = require("../data/models/simpleTaskExecuteItem");
const commentTaskExecuteRecord_1 = require("../data/models/commentTaskExecuteRecord");
const commentTaskExecuteItem_1 = require("../data/models/commentTaskExecuteItem");
const biliAccount_1 = require("../data/models/biliAccount");
const bili_bot_services_1 = require("../protos/bili_bot_services");
const simpleTaskExecuteStatistic_1 = require("../data/models/simpleTaskExecuteStatistic");
const commentTaskExecuteStatistic_1 = require("../data/models/commentTaskExecuteStatistic");
const typedi_2 = require("typedi");
const cron_1 = __importDefault(require("../services/cron"));
const tag_1 = require("../data/models/tag");
const commentPool_1 = require("../data/models/commentPool");
const toad_scheduler_1 = require("toad-scheduler");
const userBiliSimpleTaskBindRecord_1 = require("../data/models/binding/userBiliSimpleTaskBindRecord");
const userBiliCommentTaskBindRecord_1 = require("../data/models/binding/userBiliCommentTaskBindRecord");
const difyAxios_1 = __importDefault(require("../plugins/axios/difyAxios"));
const commentHitItemRecord_1 = require("../data/models/commentHitItemRecord");
const biliAccountGroupBindRecord_1 = require("../data/models/binding/biliAccountGroupBindRecord");
const proxyIp_1 = require("../data/models/proxyIp");
let BiliTaskService = class BiliTaskService {
    constructor(logger, memoryCache) {
        this.logger = logger;
        this.memoryCache = memoryCache;
        this.COMMENT_RUN_TASKS = 'PostComments'; // 评论任务
        this.CONTROL_COMMENT_RUN_TASKS = 'PullAndDelComments'; // 评论删除任务
        this.intervalSchedule = new toad_scheduler_1.ToadScheduler();
    }
    /**
     * 获取简单任务列表
     *
     * @param payloads
     * @param isAdmin
     * @param dataIn
     */
    async getSimpleList(payloads, isAdmin, dataIn = []) {
        const { searchText, page_index, page_size, sorter } = payloads;
        const sorterQuery = JSON.parse(sorter || '{}');
        let query = {};
        let data_in = {};
        let order = [];
        if (searchText) {
            const reg = {
                [sequelize_1.Op.or]: [
                    { [sequelize_1.Op.like]: `%${searchText}%` },
                    { [sequelize_1.Op.like]: `%${encodeURI(searchText)}%` },
                ],
            };
            query = {
                [sequelize_1.Op.or]: [
                    {
                        bvid: reg,
                    },
                    {
                        uid: reg,
                    },
                ],
            };
        }
        if (!isAdmin) {
            data_in = { id: { [sequelize_1.Op.in]: dataIn } };
        }
        if (sorterQuery) {
            const { field, type } = sorterQuery;
            if (field && type) {
                order.unshift([field, type]);
            }
        }
        const result = await simpleTask_1.SimpleTaskModel.findAndCountAll({
            order: order,
            offset: (page_index - 1) * page_size,
            limit: page_size,
            where: Object.assign(Object.assign(Object.assign({}, query), data_in), { del_flag: false }),
        });
        let list = [];
        for (let i = 0; i < result.rows.length; i++) {
            let _item = result.rows[i].toJSON();
            // 账号组信息
            const accountGroup = await accountGroup_1.AccountGroupModel.findOne({
                where: {
                    template_code: _item.template_code,
                },
            });
            // UP主信息
            const biliUpAccount = await biliAccount_1.BiliAccountModel.findOne({
                where: {
                    mid: _item.uid,
                },
            });
            list.push(Object.assign(Object.assign({}, _item), { template_name: (accountGroup === null || accountGroup === void 0 ? void 0 : accountGroup.template_name) || '', bili_up_account: {
                    mid: _item.uid,
                    name: biliUpAccount === null || biliUpAccount === void 0 ? void 0 : biliUpAccount.name,
                    avatar: biliUpAccount === null || biliUpAccount === void 0 ? void 0 : biliUpAccount.avatar,
                } }));
        }
        return {
            code: 0,
            msg: 'Success',
            data: {
                total: result.count,
                list,
            },
        };
    }
    /**
     * 编辑简单任务
     *
     * @param payloads 请求参数
     * @param userId
     */
    async modifySimple(payloads, userId) {
        const { id, type, bvid, uid, template_code, execute_duration, duration_unit, times, } = payloads;
        const now = new Date();
        if (id) {
            const simpleTask = await simpleTask_1.SimpleTaskModel.findOne({
                where: {
                    id,
                },
            });
            // 判断ID是否有效
            if (!simpleTask || simpleTask.del_flag) {
                return { code: 1, msg: 'ID参数无效或已删除' };
            }
            // 已发布不可修改
            if (simpleTask.publish_flag) {
                return { code: 1, msg: '已发布的任务不可修改' };
            }
            await simpleTask_1.SimpleTaskModel.update({
                type,
                bvid,
                uid,
                template_code,
                execute_duration,
                duration_unit,
                times,
                update_by: userId,
                update_time: now,
            }, {
                where: {
                    id: simpleTask.id,
                },
                returning: true,
            });
        }
        else {
            const taskId = (0, uuid_1.v4)();
            await simpleTask_1.SimpleTaskModel.create({
                id: taskId,
                type,
                bvid,
                uid,
                template_code,
                execute_duration,
                duration_unit,
                times,
                publish_flag: false,
                status: simpleTask_1.TaskStatusEnum.draft,
                enable_flag: true,
                del_flag: false,
                create_by: userId,
                create_time: now,
                update_by: userId,
                update_time: now,
            });
            await userBiliSimpleTaskBindRecord_1.UserBiliSimpleTaskBindRecordModel.create({
                id: (0, uuid_1.v4)(),
                user_id: userId,
                simple_task_id: taskId,
                readable: true,
                writable: true,
                create_by: userId,
                create_time: now,
                update_by: userId,
                update_time: now,
            });
        }
        return { code: 0, msg: '操作成功' };
    }
    /**
     * 发布简单任务
     *
     * @param payloads 请求参数
     * @param userId
     */
    async publishSimple(payloads, userId) {
        const { id } = payloads;
        let simpleTask = await simpleTask_1.SimpleTaskModel.findOne({
            where: {
                id: id,
            },
        });
        if (!simpleTask || simpleTask.del_flag) {
            return { code: 400, msg: 'ID无效, 任务没找到' };
        }
        if (simpleTask.publish_flag == true) {
            return { code: 400, msg: '无效操作, 请刷新后重试' };
        }
        const now = new Date();
        await simpleTask_1.SimpleTaskModel.update({
            status: simpleTask_1.TaskStatusEnum.wait_execute,
            publish_flag: true,
            publish_by: userId,
            publish_time: now,
            publish_remark: '',
            update_by: userId,
            update_time: now,
        }, {
            where: {
                id: id,
            },
            returning: true,
        });
        return { code: 0, msg: '操作成功' };
    }
    /**
     * 修改简单任务的状态
     *
     * @param payloads
     * @param userId
     */
    async changeSimpleEnable(payloads, userId) {
        const { id, enable_flag } = payloads;
        const simpleTask = await simpleTask_1.SimpleTaskModel.findOne({
            where: {
                id,
            },
        });
        if (!simpleTask || simpleTask.del_flag) {
            return { code: 1, msg: 'ID参数无效或已删除' };
        }
        if (simpleTask.enable_flag == enable_flag) {
            return { code: 0, msg: '操作成功' };
        }
        const now = new Date();
        await simpleTask_1.SimpleTaskModel.update({
            enable_flag,
            update_by: userId,
            update_time: now,
        }, {
            where: {
                id: simpleTask.id,
            },
            returning: true,
        });
        return { code: 0, msg: '操作成功' };
    }
    /**
     * 删除简单任务
     *
     * @param payloads
     * @param userId
     */
    async delSimple(payloads, userId) {
        const { id } = payloads;
        const simpleTask = await simpleTask_1.SimpleTaskModel.findOne({
            where: {
                id,
            },
        });
        if (!simpleTask) {
            return { code: 1, msg: 'ID参数无效' };
        }
        if (simpleTask.del_flag) {
            return { code: 1, msg: '该任务已删除' };
        }
        const now = new Date();
        await simpleTask_1.SimpleTaskModel.update({
            del_flag: true,
            update_by: userId,
            update_time: now,
        }, {
            where: {
                id: simpleTask.id,
            },
            returning: true,
        });
        return { code: 0, msg: '操作成功' };
    }
    /**
     * 执行简单任务
     *
     * @param payloads 请求参数
     * @param userId
     * @param accessToken
     */
    async executeSimple(payloads, userId, accessToken) {
        const { id } = payloads;
        // 获取任务实例
        const simpleTask = await simpleTask_1.SimpleTaskModel.findOne({
            where: {
                id: id,
            },
        });
        if (!simpleTask || simpleTask.del_flag || !simpleTask.publish_flag) {
            return {
                code: 1,
                msg: `该任务未找到或未发布, 任务ID: ${id}`,
            };
        }
        // 获取账号组
        const { template_code } = simpleTask;
        const accountGroup = await accountGroup_1.AccountGroupModel.findOne({
            where: {
                template_code: template_code,
                publish_flag: true,
                enable_flag: true,
            },
        });
        if (!accountGroup) {
            return {
                code: 1,
                msg: `账号组未找到或未发布或已停用, 模版编码: ${template_code}`,
            };
        }
        // 获取账号组明细
        const cityDistributeTemplateItemList = await cityDistributeTemplateItem_1.CityDistributeTemplateItemModel.findAll({
            where: {
                template_code: template_code,
                enable_flag: true,
            },
            raw: true,
        });
        if (!cityDistributeTemplateItemList.length) {
            return {
                code: 1,
                msg: `有效的账号组明细为空, 模版编码: ${template_code}`,
            };
        }
        // 分配类型
        const { distribute_type } = accountGroup;
        // 判断分配类型
        switch (distribute_type) {
            case accountGroup_1.DistributeTypeEnum.ROUND_ROBIN:
                // 以轮询的方式执行简单任务
                await this.roundRobinExecuteSimpleTask(userId, accessToken, simpleTask, cityDistributeTemplateItemList);
                return { code: 0, msg: '执行成功' };
        }
    }
    /**
     * 获取简单任务响应
     *
     * @param payloads
     */
    async getBiliSimpleTaskResp(payloads) {
        const { record_id } = payloads;
        const simpleTaskExecuteRecord = await simpleTaskExecuteRecord_1.SimpleTaskExecuteRecordModel.findOne({
            where: {
                id: record_id,
            },
        });
        if (!simpleTaskExecuteRecord) {
            return { code: 1, msg: 'ID参数无效, 未找到记录' };
        }
        const simpleTask = await simpleTask_1.SimpleTaskModel.findOne({
            where: {
                id: simpleTaskExecuteRecord.task_id,
            },
        });
        if (!simpleTask || simpleTask.del_flag || !simpleTask.enable_flag) {
            return { code: 1, msg: '任务不存在或已禁用' };
        }
        if (simpleTask.status != simpleTask_1.TaskStatusEnum.be_queuing) {
            return { code: 1, msg: '任务状态非排队中' };
        }
        const blCookies = [];
        const payLoad = {
            type: '1',
            extra: '',
            blCookies,
        };
        const biliTask = {
            uid: simpleTask.uid,
            bvid: simpleTask.bvid,
            recordId: record_id,
            runTasks: simpleTask.type,
            payLoad,
        };
        const itemList = await simpleTaskExecuteItem_1.SimpleTaskExecuteItemModel.findAll({
            where: {
                record_id: record_id,
                status: simpleTaskExecuteItem_1.ExecuteItemStatusEnum.WAIT,
            },
            order: [['execute_time', 'ASC']],
        });
        itemList.forEach((item) => {
            const executeTime = new Date(item.execute_time || new Date());
            blCookies.push({
                snapId: item.id,
                mid: item.mid,
                proxyIp: item.proxy_ip,
                cookie: item.cookie,
                ua: item.user_agent,
                carrier: undefined,
                scheduleTime: executeTime.getTime().toString(),
            });
        });
        // 当前时间
        const now = new Date();
        await simpleTask_1.SimpleTaskModel.update({
            status: simpleTask_1.TaskStatusEnum.on_execute,
            update_by: 'system',
            update_time: now,
        }, {
            where: {
                id: simpleTask.id,
            },
        });
        return { code: 200, data: biliTask };
    }
    /**
     * 获取评论任务响应
     *
     * @param payloads
     */
    async getBiliCommentTaskResp(payloads) {
        const { record_id, run_tasks } = payloads;
        const executeRecord = await commentTaskExecuteRecord_1.CommentTaskExecuteRecordModel.findOne({
            where: {
                id: record_id,
            },
        });
        if (!executeRecord) {
            return { code: 1, msg: 'ID参数无效, 未找到记录' };
        }
        const commentTask = await commentTask_1.CommentTaskModel.findOne({
            where: {
                id: executeRecord.task_id,
            },
        });
        if (!commentTask || commentTask.del_flag || !commentTask.enable_flag) {
            return { code: 1, msg: '任务不存在或已禁用' };
        }
        if (commentTask.status != simpleTask_1.TaskStatusEnum.be_queuing) {
            return { code: 1, msg: '任务状态非排队中' };
        }
        if ((commentTask.task_type == commentTask_1.CommentTaskTypeEnum.CONTROL ||
            commentTask.task_type == commentTask_1.CommentTaskTypeEnum.AI_REVIEW) &&
            !commentTask.control_flag) {
            return { code: 1, msg: '控评开关已关闭' };
        }
        const blCookies = [];
        const payLoad = {
            type: '1',
            extra: '',
            blCookies,
        };
        const biliTask = {
            uid: commentTask.uid,
            bvid: commentTask.bvid,
            recordId: record_id,
            runTasks: run_tasks,
            payLoad,
        };
        const itemList = await commentTaskExecuteItem_1.CommentTaskExecuteItemModel.findAll({
            where: {
                record_id: record_id,
                status: simpleTaskExecuteItem_1.ExecuteItemStatusEnum.WAIT,
            },
            order: [['execute_time', 'ASC']],
        });
        // 存在则将评论塞入列表，不存在则新增BlCookie实例
        itemList.forEach((item) => {
            let commentWrap;
            const executeTime = new Date(item.execute_time || new Date());
            if (run_tasks == this.COMMENT_RUN_TASKS) {
                const biliTaskComment = {
                    id: '',
                    content: item.comment,
                };
                commentWrap = {
                    comment: biliTaskComment,
                    commentController: undefined,
                };
            }
            else if (run_tasks == this.CONTROL_COMMENT_RUN_TASKS) {
                const controlTime = new Date(commentTask.control_time || new Date());
                const timestamp = controlTime.getTime() / 1000;
                const midWhiteList = commentTask.mid_white_list
                    .split('\n')
                    .filter((item) => item !== '')
                    .map((item) => {
                    return {
                        mid: item,
                    };
                });
                const commentController = {
                    action: commentTask.task_type == commentTask_1.CommentTaskTypeEnum.AI_REVIEW ? '1' : '0',
                    controlTime: timestamp.toString(),
                    whiteList: midWhiteList,
                };
                commentWrap = {
                    comment: undefined,
                    commentController,
                };
            }
            blCookies.push({
                snapId: item.id,
                mid: item.mid,
                proxyIp: item.proxy_ip,
                cookie: item.cookie,
                ua: item.user_agent,
                carrier: {
                    carryType: bili_bot_services_1.BiliTask_CarryType.CComment,
                    commentWrap,
                    danmakuWrap: undefined,
                },
                scheduleTime: executeTime.getTime().toString(),
            });
        });
        // 当前时间
        const now = new Date();
        await commentTask_1.CommentTaskModel.update({
            status: simpleTask_1.TaskStatusEnum.on_execute,
            update_by: 'system',
            update_time: now,
        }, {
            where: {
                id: commentTask.id,
            },
        });
        return { code: 200, data: biliTask };
    }
    /**
     * 统计简单任务结果
     *
     * @param payloads
     */
    async countSimpleTaskResult(payloads) {
        const { snapId, mid, recordId, status } = payloads;
        // 任务执行记录
        const simpleTaskExecuteRecord = await simpleTaskExecuteRecord_1.SimpleTaskExecuteRecordModel.findOne({
            where: {
                id: recordId,
            },
        });
        if (!simpleTaskExecuteRecord) {
            this.logger.error('SimpleTaskExecuteRecord NotFound, id: {}', recordId);
            return;
        }
        // 任务执行明细
        const simpleTaskExecuteItem = await simpleTaskExecuteItem_1.SimpleTaskExecuteItemModel.findOne({
            where: {
                id: snapId,
            },
        });
        if (!simpleTaskExecuteItem) {
            this.logger.error('SimpleTaskExecuteItem NotFound, id: {}', snapId);
            return;
        }
        let itemStatus = simpleTaskExecuteItem_1.ExecuteItemStatusEnum.UNRECOGNIZED;
        switch (status) {
            case bili_bot_services_1.SubtaskResult_Status.Running:
                itemStatus = simpleTaskExecuteItem_1.ExecuteItemStatusEnum.RUNNING;
                break;
            case bili_bot_services_1.SubtaskResult_Status.Success:
                itemStatus = simpleTaskExecuteItem_1.ExecuteItemStatusEnum.SUCCESS;
                break;
            case bili_bot_services_1.SubtaskResult_Status.Failed:
                itemStatus = simpleTaskExecuteItem_1.ExecuteItemStatusEnum.FAIL;
                break;
        }
        // 当前时间
        const now = new Date();
        // 更新明细
        await simpleTaskExecuteItem_1.SimpleTaskExecuteItemModel.update({
            status: itemStatus,
            update_by: 'system',
            update_time: now,
        }, {
            where: {
                id: snapId,
            },
        });
        if (itemStatus == simpleTaskExecuteItem_1.ExecuteItemStatusEnum.SUCCESS ||
            itemStatus == simpleTaskExecuteItem_1.ExecuteItemStatusEnum.FAIL) {
            // 是否成功
            const success = itemStatus == simpleTaskExecuteItem_1.ExecuteItemStatusEnum.SUCCESS;
            // 更新记录
            await simpleTaskExecuteRecord_1.SimpleTaskExecuteRecordModel.update({
                total_count: simpleTaskExecuteRecord.total_count + 1,
                success_count: simpleTaskExecuteRecord.success_count + (success ? 1 : 0),
                update_by: 'system',
                update_time: now,
            }, {
                where: {
                    id: recordId,
                },
            });
            // 更新运行统计数据
            const fmtDate = (0, util_1.dateFormat)(simpleTaskExecuteRecord.create_time, 2);
            const simpleTaskExecuteStatistic = await simpleTaskExecuteStatistic_1.SimpleTaskExecuteStatisticModel.findOne({
                where: {
                    mid,
                    type: simpleTaskExecuteStatistic_1.StatisticTypeEnum.DAY,
                    date: fmtDate,
                },
            });
            if (simpleTaskExecuteStatistic == null) {
                await simpleTaskExecuteStatistic_1.SimpleTaskExecuteStatisticModel.create({
                    id: (0, uuid_1.v4)(),
                    mid,
                    type: simpleTaskExecuteStatistic_1.StatisticTypeEnum.DAY,
                    date: fmtDate,
                    total_count: 1,
                    success_count: success ? 1 : 0,
                    create_by: 'system',
                    create_time: now,
                    update_by: 'system',
                    update_time: now,
                });
            }
            else {
                await simpleTaskExecuteStatistic_1.SimpleTaskExecuteStatisticModel.update({
                    total_count: simpleTaskExecuteStatistic.total_count + 1,
                    success_count: simpleTaskExecuteStatistic.success_count + (success ? 1 : 0),
                    update_by: 'system',
                    update_time: now,
                }, {
                    where: {
                        id: simpleTaskExecuteStatistic.id,
                    },
                });
            }
        }
    }
    /**
     * 统计评论任务结果
     *
     * @param payloads
     */
    async countCommentTaskResult(payloads) {
        const { snapId, mid, recordId, status } = payloads;
        // 任务执行记录
        const executeRecord = await commentTaskExecuteRecord_1.CommentTaskExecuteRecordModel.findOne({
            where: {
                id: recordId,
            },
        });
        if (!executeRecord) {
            this.logger.error('CommentTaskExecuteRecord NotFound, id: {}', recordId);
            return;
        }
        // 任务执行明细
        const executeItem = await commentTaskExecuteItem_1.CommentTaskExecuteItemModel.findOne({
            where: {
                id: snapId,
            },
        });
        if (!executeItem) {
            this.logger.error('CommentTaskExecuteItem NotFound, id: {}', snapId);
            return;
        }
        let itemStatus = simpleTaskExecuteItem_1.ExecuteItemStatusEnum.UNRECOGNIZED;
        switch (status) {
            case bili_bot_services_1.SubtaskResult_Status.Running:
                itemStatus = simpleTaskExecuteItem_1.ExecuteItemStatusEnum.RUNNING;
                break;
            case bili_bot_services_1.SubtaskResult_Status.Success:
                itemStatus = simpleTaskExecuteItem_1.ExecuteItemStatusEnum.SUCCESS;
                break;
            case bili_bot_services_1.SubtaskResult_Status.Failed:
                itemStatus = simpleTaskExecuteItem_1.ExecuteItemStatusEnum.FAIL;
                break;
        }
        // 当前时间
        const now = new Date();
        // 更新明细
        await commentTaskExecuteItem_1.CommentTaskExecuteItemModel.update({
            status: itemStatus,
            update_by: 'system',
            update_time: now,
        }, {
            where: {
                id: snapId,
            },
        });
        if (itemStatus == simpleTaskExecuteItem_1.ExecuteItemStatusEnum.SUCCESS ||
            itemStatus == simpleTaskExecuteItem_1.ExecuteItemStatusEnum.FAIL) {
            // 是否成功
            const success = itemStatus == simpleTaskExecuteItem_1.ExecuteItemStatusEnum.SUCCESS;
            // 更新记录
            await commentTaskExecuteRecord_1.CommentTaskExecuteRecordModel.update({
                total_count: executeRecord.total_count + 1,
                success_count: executeRecord.success_count + (success ? 1 : 0),
                update_by: 'system',
                update_time: now,
            }, {
                where: {
                    id: recordId,
                },
            });
            // 更新运行统计数据
            const fmtDate = (0, util_1.dateFormat)(executeRecord.create_time, 2);
            const executeStatistic = await commentTaskExecuteStatistic_1.CommentTaskExecuteStatisticModel.findOne({
                where: {
                    mid,
                    type: simpleTaskExecuteStatistic_1.StatisticTypeEnum.DAY,
                    date: fmtDate,
                },
            });
            if (executeStatistic == null) {
                await commentTaskExecuteStatistic_1.CommentTaskExecuteStatisticModel.create({
                    id: (0, uuid_1.v4)(),
                    mid,
                    type: simpleTaskExecuteStatistic_1.StatisticTypeEnum.DAY,
                    date: fmtDate,
                    total_count: 1,
                    success_count: success ? 1 : 0,
                    create_by: 'system',
                    create_time: now,
                    update_by: 'system',
                    update_time: now,
                });
            }
            else {
                await commentTaskExecuteStatistic_1.CommentTaskExecuteStatisticModel.update({
                    total_count: executeStatistic.total_count + 1,
                    success_count: executeStatistic.success_count + (success ? 1 : 0),
                    update_by: 'system',
                    update_time: now,
                }, {
                    where: {
                        id: executeStatistic.id,
                    },
                });
            }
        }
    }
    /**
     * 结束执行简单任务
     *
     * @param payloads
     */
    async endSimpleTask(payloads) {
        const { recordId } = payloads;
        // 任务执行记录
        const simpleTaskExecuteRecord = await simpleTaskExecuteRecord_1.SimpleTaskExecuteRecordModel.findOne({
            where: {
                id: recordId,
            },
        });
        if (!simpleTaskExecuteRecord) {
            this.logger.error('SimpleTaskExecuteRecord NotFound, id: {}', recordId);
            return;
        }
        // 任务
        const simpleTask = await simpleTask_1.SimpleTaskModel.findOne({
            where: {
                id: simpleTaskExecuteRecord.task_id,
            },
        });
        if (!simpleTask) {
            this.logger.error('SimpleTask NotFound, id: {}', recordId);
            return;
        }
        // 当前时间
        const now = new Date();
        await simpleTask_1.SimpleTaskModel.update({
            status: simpleTask_1.TaskStatusEnum.execute_complete,
            update_by: 'system',
            update_time: now,
        }, {
            where: {
                id: simpleTask.id,
            },
        });
    }
    /**
     * 结束执行评论任务
     *
     * @param payloads
     */
    async endCommentTask(payloads) {
        const { recordId } = payloads;
        // 任务执行记录
        const executeRecord = await commentTaskExecuteRecord_1.CommentTaskExecuteRecordModel.findOne({
            where: {
                id: recordId,
            },
        });
        if (!executeRecord) {
            this.logger.error('CommentTaskExecuteRecord NotFound, id: {}', recordId);
            return;
        }
        // 任务
        const commentTask = await commentTask_1.CommentTaskModel.findOne({
            where: {
                id: executeRecord.task_id,
            },
        });
        if (!commentTask) {
            this.logger.error('CommentTask NotFound, id: {}', recordId);
            return;
        }
        // 当前时间
        const now = new Date();
        await commentTask_1.CommentTaskModel.update({
            status: simpleTask_1.TaskStatusEnum.execute_complete,
            update_by: 'system',
            update_time: now,
        }, {
            where: {
                id: commentTask.id,
            },
        });
    }
    /**
     * 以轮询的方式执行简单任务
     *
     * @param userId
     * @param accessToken
     * @param simpleTask
     * @param cityDistributeTemplateItemList
     */
    async roundRobinExecuteSimpleTask(userId, accessToken, simpleTask, cityDistributeTemplateItemList) {
        const now = new Date();
        // 创建任务执行记录
        let recordId = (0, uuid_1.v4)();
        await simpleTaskExecuteRecord_1.SimpleTaskExecuteRecordModel.create({
            id: recordId,
            schedule_type: simpleTaskExecuteRecord_1.ScheduleTypeEnum.RIGHT_NOW,
            schedule_id: recordId,
            task_id: simpleTask.id,
            start_time: now,
            total_count: simpleTask.times,
            success_count: 0,
            create_by: userId,
            create_time: now,
            update_by: userId,
            update_time: now,
        });
        // 启动BiliBot
        switch (simpleTask.type) {
            case simpleTask_1.TaskTypeEnum.daily:
                // 每日任务类型特殊处理
                this.createDailyTaskItems(userId, now, recordId, '每日练号', simpleTask, cityDistributeTemplateItemList);
                break;
            case simpleTask_1.TaskTypeEnum.earn_coins:
                this.createDailyTaskItems(userId, now, recordId, '每日登录赚硬币', simpleTask, cityDistributeTemplateItemList);
                break;
            case simpleTask_1.TaskTypeEnum.moxin_stats:
                this.createDailyTaskItems(userId, now, recordId, 'Moxin统计', simpleTask, cityDistributeTemplateItemList);
                break;
            case simpleTask_1.TaskTypeEnum.like:
            case simpleTask_1.TaskTypeEnum.add_coins:
            case simpleTask_1.TaskTypeEnum.favorite:
            case simpleTask_1.TaskTypeEnum.three_in_one:
            case simpleTask_1.TaskTypeEnum.share:
            case simpleTask_1.TaskTypeEnum.watch_video:
            default:
                // 非每日任务类型按分配逻辑处理
                this.createOtherTaskItems(userId, now, recordId, simpleTask, cityDistributeTemplateItemList);
                break;
        }
        // 将任务状态设置为排队中
        await simpleTask_1.SimpleTaskModel.update({
            status: simpleTask_1.TaskStatusEnum.be_queuing,
            update_by: userId,
            update_time: now,
        }, {
            where: {
                id: simpleTask.id,
            },
        });
        // 执行定时任务
        const cronService = typedi_2.Container.get(cron_1.default);
        // 启动BiliBot
        switch (simpleTask.type) {
            case simpleTask_1.TaskTypeEnum.like:
                cronService.runTask(accessToken, const_1.CRON_NAME_DIC['1'], simpleTask.id, recordId);
                break;
            case simpleTask_1.TaskTypeEnum.add_coins:
                cronService.runTask(accessToken, const_1.CRON_NAME_DIC['2'], simpleTask.id, recordId);
                break;
            case simpleTask_1.TaskTypeEnum.favorite:
                cronService.runTask(accessToken, const_1.CRON_NAME_DIC['3'], simpleTask.id, recordId);
                break;
            case simpleTask_1.TaskTypeEnum.three_in_one:
                cronService.runTask(accessToken, const_1.CRON_NAME_DIC['4'], simpleTask.id, recordId);
                break;
            case simpleTask_1.TaskTypeEnum.share:
                cronService.runTask(accessToken, const_1.CRON_NAME_DIC['5'], simpleTask.id, recordId);
                break;
            case simpleTask_1.TaskTypeEnum.daily:
                cronService.runTask(accessToken, const_1.CRON_NAME_DIC['6'], simpleTask.id, recordId);
                break;
            case simpleTask_1.TaskTypeEnum.watch_video:
                cronService.runTask(accessToken, const_1.CRON_NAME_DIC['7'], simpleTask.id, recordId);
                break;
            case simpleTask_1.TaskTypeEnum.earn_coins:
                cronService.runTask(accessToken, const_1.CRON_NAME_DIC['12'], simpleTask.id, recordId);
                break;
            case simpleTask_1.TaskTypeEnum.moxin_stats:
                cronService.runTask(accessToken, const_1.CRON_NAME_DIC['13'], simpleTask.id, recordId);
                break;
        }
    }
    /**
     * 创建非每日任务类型任务明细
     *
     * @param userId
     * @param now
     * @param recordId
     * @param simpleTask
     * @param cityDistributeTemplateItemList
     */
    async createDailyTaskItems(userId, now, recordId, tagName, simpleTask, cityDistributeTemplateItemList) {
        const durationSecs = this.getDurationSecs(simpleTask.execute_duration, simpleTask.duration_unit);
        // 每次毫秒数
        const perTimesMilliSecs = (durationSecs * 1000) / simpleTask.times;
        // 根据标签获取需要升级的小号列表
        const itemLen = cityDistributeTemplateItemList.length;
        for (let i = 0; i < itemLen; i++) {
            const cityDistributeTemplateItem = cityDistributeTemplateItemList[i];
            let cityCode = cityDistributeTemplateItem.city_code;
            // 获取查询结果
            const queryResult = await models_1.sequelize.query('select t.* ' +
                'from (' +
                'select t1.mid, t1.login_time, t1.proxy_ip, t1.cookie, t1.user_agent ' +
                'from bili_account t1 ' +
                'join proxy_ip t2 on t1.proxy_ip = t2.ip and t2.enable_flag = 1 and t2.city_code = ? ' +
                'join bili_account_tag t3 on t1.mid = t3.mid ' +
                'join tag t4 on t3.tag_id = t4.id and t4.type = ? and t4.name = ? ' +
                'where t1.login_status = 1 and t1.current_level < 8) t ', {
                type: sequelize_1.QueryTypes.SELECT,
                replacements: [cityCode, tag_1.TagTypeEnum.ACCOUNT_OPERATION, tagName],
                logging: false,
            });
            if (queryResult.length > 0) {
                // 任务明细列表
                let simpleTaskExecuteItemList = [];
                // 解析查询结果
                for (let i = 0; i < queryResult.length; i++) {
                    const biliAccount = queryResult[i];
                    simpleTaskExecuteItemList.push({
                        id: (0, uuid_1.v4)(),
                        record_id: recordId,
                        mid: biliAccount.mid,
                        proxy_ip: biliAccount.proxy_ip || '',
                        cookie: biliAccount.cookie,
                        user_agent: biliAccount.user_agent || '',
                        status: simpleTaskExecuteItem_1.ExecuteItemStatusEnum.WAIT,
                        execute_time: new Date(now.getTime() + i * perTimesMilliSecs),
                        execute_result: '',
                        create_by: userId,
                        create_time: now,
                        update_by: userId,
                        update_time: now,
                    });
                }
                // 写入数据库
                if (simpleTaskExecuteItemList.length > 0) {
                    await simpleTaskExecuteItem_1.SimpleTaskExecuteItemModel.bulkCreate(simpleTaskExecuteItemList);
                }
            }
        }
    }
    /**
     * 创建非每日任务类型任务明细
     *
     * @param userId
     * @param now
     * @param recordId
     * @param simpleTask
     * @param cityDistributeTemplateItemList
     */
    async createOtherTaskItems(userId, now, recordId, simpleTask, cityDistributeTemplateItemList) {
        const durationSecs = this.getDurationSecs(simpleTask.execute_duration, simpleTask.duration_unit);
        // 每次毫秒数
        const perTimesMilliSecs = (durationSecs * 1000) / simpleTask.times;
        const itemLen = cityDistributeTemplateItemList.length;
        // 平均每个城市分配的次数
        let avgPerCityTimes = (simpleTask.times / itemLen) | 0;
        let index = 0;
        while (index < itemLen) {
            let limit = avgPerCityTimes;
            if (index == itemLen - 1) {
                limit = simpleTask.times - avgPerCityTimes * index;
            }
            let cityDistributeTemplateItem = cityDistributeTemplateItemList[index];
            // 根据城市编码获取执行任务最少的小号
            let cityCode = cityDistributeTemplateItem.city_code;
            // 获取查询结果
            const queryResult = await models_1.sequelize.query('select t.* ' +
                'from (' +
                'select t1.mid, t1.login_time, t1.proxy_ip, t1.cookie, t1.user_agent, ' +
                'ifnull(t3.total_count, 0) as total_count ' +
                'from bili_account t1 ' +
                'join proxy_ip t2 on t1.proxy_ip = t2.ip and t2.enable_flag = 1 and t2.city_code = ? ' +
                'left join simple_task_execute_statistic t3 on t1.mid = t3.mid and t3.type = 3 and t3.date = ? ' +
                'where t1.login_status = 1 ) t ' +
                'order by t.total_count asc, t.login_time asc ' +
                'limit ?', {
                type: sequelize_1.QueryTypes.SELECT,
                replacements: [cityCode, (0, util_1.dateFormat)(now, 2), limit],
                logging: false,
            });
            if (queryResult.length > 0) {
                // 任务明细列表
                let simpleTaskExecuteItemList = [];
                // 解析查询结果
                for (let i = 0; i < queryResult.length; i++) {
                    const biliAccount = queryResult[i];
                    simpleTaskExecuteItemList.push({
                        id: (0, uuid_1.v4)(),
                        record_id: recordId,
                        mid: biliAccount.mid,
                        proxy_ip: biliAccount.proxy_ip || '',
                        cookie: biliAccount.cookie,
                        user_agent: biliAccount.user_agent || '',
                        status: simpleTaskExecuteItem_1.ExecuteItemStatusEnum.WAIT,
                        execute_time: new Date(now.getTime() + i * perTimesMilliSecs),
                        execute_result: '',
                        create_by: userId,
                        create_time: now,
                        update_by: userId,
                        update_time: now,
                    });
                }
                // 写入数据库
                if (simpleTaskExecuteItemList.length > 0) {
                    await simpleTaskExecuteItem_1.SimpleTaskExecuteItemModel.bulkCreate(simpleTaskExecuteItemList);
                }
            }
            index++;
        }
    }
    /**
     * 获取时长，单位为秒
     *
     * @param executeDuration
     * @param durationUnit
     * @returns
     */
    getDurationSecs(executeDuration, durationUnit) {
        switch (String(durationUnit)) {
            case simpleTask_1.DurationUnitEnum.H:
                return executeDuration * 60 * 60;
            case simpleTask_1.DurationUnitEnum.M:
                return executeDuration * 60;
            case simpleTask_1.DurationUnitEnum.S:
                return executeDuration;
            default:
                throw new Error('时长单位不支持');
        }
    }
    createIntervalTask(userId, accessToken) {
        const mins = 6;
        this.logger.info('[panel][创建interval任务], 任务名: %s', 'comment-task');
        const job = new toad_scheduler_1.SimpleIntervalJob({ minutes: mins }, new toad_scheduler_1.Task('comment-task', () => {
            this.logger.info('[panel][开始运行interval任务], 任务名: %s will run every %d minutes', 'comment-task', mins);
            this.autoRunCommentTasksWithLimit(userId, accessToken);
        }));
        // 添加任务到调度器
        this.intervalSchedule.addSimpleIntervalJob(job);
    }
    async autoRunCommentTasksWithLimit(userId, accessToken) {
        const tasks = await commentTask_1.CommentTaskModel.findAll({
            where: {
                del_flag: false,
                enable_flag: true,
                control_flag: true,
                publish_flag: true,
                task_type: commentTask_1.CommentTaskTypeEnum.CONTROL,
                status: {
                    [sequelize_1.Op.or]: [
                        simpleTask_1.TaskStatusEnum.be_queuing,
                        simpleTask_1.TaskStatusEnum.on_execute,
                        simpleTask_1.TaskStatusEnum.execute_complete,
                    ],
                },
                update_time: {
                    [sequelize_1.Op.lt]: new Date(new Date().getTime() - 5 * 60000),
                },
            },
            order: [['update_time', 'ASC']],
            offset: 0,
            limit: 8,
        });
        this.logger.info('[panel][运行自启动任务], 任务名: %s, 任务数: %d', 'comment-task', tasks.length);
        for (let i = 0; i < tasks.length; i++) {
            const element = tasks[i];
            await (0, util_1.delay)((0, util_1.getRandomNumber)(1, 5) * 1000);
            await this.stopCommentTask({ id: element.id }, userId, false);
            await this.changeCommentControl({ id: element.id, control_flag: true }, userId);
            await this.executeCommentTask({ id: element.id }, userId, accessToken, true);
        }
    }
    async resetCommentTaskStatus(userId) {
        await commentTask_1.CommentTaskModel.update({
            status: simpleTask_1.TaskStatusEnum.execute_complete,
            update_by: userId,
            update_time: new Date(),
        }, {
            where: {
                del_flag: false,
                enable_flag: true,
                control_flag: true,
                publish_flag: true,
                task_type: commentTask_1.CommentTaskTypeEnum.CONTROL,
            },
        });
    }
    /**
     * Automatically runs comment tasks without any limit.
     *
     * This method first checks if a comment task is already running, and if so, returns an error response. It then resets the comment task status and creates a new interval task to execute the comment tasks.
     *
     * The method retrieves all comment tasks that are not deleted, enabled, controlled, published, and have a status of "execute_complete". It then calls the `executeCommentTasksWithDelay` method to execute these tasks with a random delay between 2 and 8 seconds.
     *
     * @param userId - The ID of the user who is running the comment tasks.
     * @param accessToken - The access token of the user who is running the comment tasks.
     * @returns - A promise that resolves to an object with a code and message property, indicating the success or failure of the operation.
     */
    async autoRunCommentTasksWithoutLimit(userId, accessToken) {
        const taskFlag = await this.memoryCache.get('comment-task');
        if (taskFlag) {
            return {
                code: 400,
                msg: '评论任务正在运行中，请勿重复启动！',
            };
        }
        await this.memoryCache.set('comment-task', true);
        // 重置评控任务状态
        await this.resetCommentTaskStatus(userId);
        const tasks = await commentTask_1.CommentTaskModel.findAll({
            where: {
                del_flag: false,
                enable_flag: true,
                control_flag: true,
                publish_flag: true,
                task_type: commentTask_1.CommentTaskTypeEnum.CONTROL,
                status: {
                    [sequelize_1.Op.or]: [simpleTask_1.TaskStatusEnum.execute_complete],
                },
            },
        });
        this.createIntervalTask(userId, accessToken);
        this.executeCommentTasksWithDelay(userId, accessToken, tasks);
        return {
            code: 200,
            msg: '评论任务启动成功',
        };
    }
    async executeCommentTasksWithDelay(userId, accessToken, tasks) {
        for (let i = 0; i < tasks.length; i++) {
            const element = tasks[i];
            await (0, util_1.delay)((0, util_1.getRandomNumber)(2, 8) * 1000);
            await this.executeCommentTask({ id: element.id }, userId, accessToken);
        }
    }
    /**
     * 获取评论任务列表
     *
     * @param payloads
     * @param isAdmin
     * @param dataIn
     */
    async getCommentList(payloads, isAdmin, dataIn = []) {
        const uid = payloads.uid;
        const taskType = payloads.taskType;
        const searchText = payloads.searchValue;
        const page_index = Number(payloads.page_index || '0');
        const page_size = Number(payloads.page_size || '0');
        const sorterQuery = JSON.parse(payloads.sorter || '{}');
        let query = {};
        let data_in = {};
        let order = [];
        let other = {};
        if (sorterQuery) {
            const { field, type } = sorterQuery;
            if (field && type) {
                order.unshift([field, type]);
            }
        }
        if (searchText) {
            const reg = {
                [sequelize_1.Op.or]: [
                    { [sequelize_1.Op.like]: `%${searchText}%` },
                    { [sequelize_1.Op.like]: `%${encodeURI(searchText)}%` },
                ],
            };
            query = {
                [sequelize_1.Op.or]: [
                    {
                        bvid: reg,
                    },
                    {
                        uid: reg,
                    },
                ],
            };
        }
        if (!isAdmin) {
            data_in = { id: { [sequelize_1.Op.in]: dataIn } };
        }
        if (taskType) {
            if (uid) {
                other = {
                    uid,
                    task_type: Number(taskType),
                    del_flag: false,
                };
            }
            else {
                other = {
                    task_type: Number(taskType),
                    del_flag: false,
                };
            }
        }
        else {
            other = {
                del_flag: false,
            };
        }
        const result = await commentTask_1.CommentTaskModel.findAndCountAll({
            order: order,
            offset: (page_index - 1) * page_size,
            limit: page_size,
            where: Object.assign(Object.assign(Object.assign({}, query), data_in), other),
            include: [
                {
                    model: biliAccount_1.BiliAccountModel,
                    as: 'biliAccount', // 关联模型的别名
                    attributes: ['mid', 'name', 'avatar'],
                },
                {
                    model: accountGroup_1.AccountGroupModel,
                    as: 'accountGroup',
                    attributes: ['template_code', 'template_name'],
                },
                {
                    model: tag_1.TagModel,
                    as: 'tag',
                    attributes: ['id', 'type', 'name'],
                },
            ],
        });
        return {
            code: 0,
            msg: 'Success',
            data: {
                total: result.count,
                list: result.rows,
            },
        };
    }
    /**
     * 创建评论任务
     *
     * @param payloads 请求参数
     * @param userId
     */
    async modifyComment(payloads, userId) {
        const { id, task_type, uid, bvid, template_code, comment_tag_id, control_time, mid_white_list, publish_remark, execute_duration, duration_unit, } = payloads;
        // 校验参数
        if (commentTask_1.CommentTaskTypeEnum.PUBLISH == task_type) {
            if (!template_code) {
                return { code: -1, msg: '账号组为空' };
            }
        }
        else {
            if (!control_time) {
                return { code: -1, msg: '请设置控评时间' };
            }
            if (!mid_white_list) {
                return { code: -1, msg: '评论白名单为空' };
            }
        }
        const now = new Date();
        if (id) {
            let commentTask = await commentTask_1.CommentTaskModel.findOne({
                where: {
                    id,
                },
            });
            if (!commentTask || !commentTask.enable_flag || commentTask.del_flag) {
                return { code: 400, msg: '任务已停用或已删除' };
            }
            switch (commentTask.task_type) {
                case commentTask_1.CommentTaskTypeEnum.PUBLISH:
                    await commentTask_1.CommentTaskModel.update({
                        comment_tag_id,
                        template_code,
                        publish_remark,
                        execute_duration,
                        duration_unit,
                        update_by: userId,
                        update_time: now,
                    }, {
                        where: {
                            id: commentTask.id,
                        },
                    });
                    break;
                case commentTask_1.CommentTaskTypeEnum.CONTROL:
                    await commentTask_1.CommentTaskModel.update({
                        control_time,
                        mid_white_list,
                        publish_remark,
                        update_by: userId,
                        update_time: now,
                    }, {
                        where: {
                            id: commentTask.id,
                        },
                    });
                    break;
                case commentTask_1.CommentTaskTypeEnum.AI_REVIEW:
                    await commentTask_1.CommentTaskModel.update({
                        control_time,
                        mid_white_list,
                        publish_remark,
                        update_by: userId,
                        update_time: now,
                    }, {
                        where: {
                            id: commentTask.id,
                        },
                    });
                    break;
            }
        }
        else {
            switch (task_type) {
                case commentTask_1.CommentTaskTypeEnum.PUBLISH:
                    const commentPublishTask = await commentTask_1.CommentTaskModel.findOne({
                        where: {
                            task_type: commentTask_1.CommentTaskTypeEnum.PUBLISH,
                            bvid,
                            comment_tag_id,
                            del_flag: false,
                        },
                    });
                    if (commentPublishTask) {
                        return {
                            code: -1,
                            msg: `发布评论任务已存在, 不可重复创建, 视频ID: ${bvid}`,
                        };
                    }
                    const cTaskId = (0, uuid_1.v4)();
                    await commentTask_1.CommentTaskModel.create({
                        id: cTaskId,
                        task_type,
                        uid,
                        bvid,
                        template_code,
                        comment_tag_id,
                        execute_duration,
                        duration_unit,
                        comment_type: commentTask_1.CommentTypeEnum.BY_SORT,
                        publish_flag: false,
                        publish_remark,
                        status: simpleTask_1.TaskStatusEnum.draft,
                        enable_flag: true,
                        control_flag: false,
                        mid_white_list: '',
                        create_by: userId,
                        create_time: now,
                        update_by: userId,
                        update_time: now,
                        del_flag: false,
                    });
                    await userBiliCommentTaskBindRecord_1.UserBiliCommentTaskBindRecordModel.create({
                        id: (0, uuid_1.v4)(),
                        user_id: userId,
                        comment_task_id: cTaskId,
                        readable: true,
                        writable: true,
                        create_by: userId,
                        create_time: now,
                        update_by: userId,
                        update_time: now,
                    });
                    break;
                case commentTask_1.CommentTaskTypeEnum.CONTROL:
                    const commentControlTask = await commentTask_1.CommentTaskModel.findOne({
                        where: {
                            task_type: commentTask_1.CommentTaskTypeEnum.CONTROL,
                            bvid,
                            del_flag: false,
                        },
                    });
                    if (commentControlTask) {
                        return {
                            code: -1,
                            msg: `控制评论任务已存在, 不可重复创建, 视频ID: ${bvid}`,
                        };
                    }
                    const ccTaskId = (0, uuid_1.v4)();
                    await commentTask_1.CommentTaskModel.create({
                        id: ccTaskId,
                        task_type,
                        uid,
                        bvid,
                        template_code: '',
                        comment_tag_id: '',
                        execute_duration: 1,
                        duration_unit: simpleTask_1.DurationUnitEnum.H,
                        comment_type: commentTask_1.CommentTypeEnum.BY_SORT,
                        publish_flag: false,
                        publish_remark,
                        status: simpleTask_1.TaskStatusEnum.draft,
                        enable_flag: true,
                        control_flag: false,
                        control_time,
                        mid_white_list,
                        create_by: userId,
                        create_time: now,
                        update_by: userId,
                        update_time: now,
                        del_flag: false,
                    });
                    await userBiliCommentTaskBindRecord_1.UserBiliCommentTaskBindRecordModel.create({
                        id: (0, uuid_1.v4)(),
                        user_id: userId,
                        comment_task_id: ccTaskId,
                        readable: true,
                        writable: true,
                        create_by: userId,
                        create_time: now,
                        update_by: userId,
                        update_time: now,
                    });
                    break;
                case commentTask_1.CommentTaskTypeEnum.AI_REVIEW:
                    const commentReviewTask = await commentTask_1.CommentTaskModel.findOne({
                        where: {
                            task_type: commentTask_1.CommentTaskTypeEnum.AI_REVIEW,
                            bvid,
                            del_flag: false,
                        },
                    });
                    if (commentReviewTask) {
                        return {
                            code: -1,
                            msg: `审核评论任务已存在, 不可重复创建, 视频ID: ${bvid}`,
                        };
                    }
                    const crTaskId = (0, uuid_1.v4)();
                    await commentTask_1.CommentTaskModel.create({
                        id: crTaskId,
                        task_type,
                        uid,
                        bvid,
                        template_code: '',
                        comment_tag_id: '',
                        execute_duration: 1,
                        duration_unit: simpleTask_1.DurationUnitEnum.H,
                        comment_type: commentTask_1.CommentTypeEnum.BY_SORT,
                        publish_flag: false,
                        publish_remark,
                        status: simpleTask_1.TaskStatusEnum.draft,
                        enable_flag: true,
                        control_flag: false,
                        control_time,
                        mid_white_list,
                        create_by: userId,
                        create_time: now,
                        update_by: userId,
                        update_time: now,
                        del_flag: false,
                    });
                    await userBiliCommentTaskBindRecord_1.UserBiliCommentTaskBindRecordModel.create({
                        id: (0, uuid_1.v4)(),
                        user_id: userId,
                        comment_task_id: crTaskId,
                        readable: true,
                        writable: true,
                        create_by: userId,
                        create_time: now,
                        update_by: userId,
                        update_time: now,
                    });
                    break;
            }
        }
        return { code: 0, msg: '操作成功' };
    }
    /**
     * 发布评论任务
     *
     * @param payloads 请求参数
     * @param userId
     */
    async publishComment(payloads, userId) {
        const { id } = payloads;
        let commentTask = await commentTask_1.CommentTaskModel.findOne({
            where: {
                id: id,
            },
        });
        if (!commentTask) {
            return { code: 400, msg: 'ID无效, 任务没找到' };
        }
        if (commentTask.publish_flag) {
            return { code: 400, msg: '无效操作, 请刷新后重试' };
        }
        const now = new Date();
        await commentTask_1.CommentTaskModel.update({
            status: simpleTask_1.TaskStatusEnum.wait_execute,
            publish_flag: true,
            publish_by: userId,
            publish_time: now,
            publish_remark: '',
            update_by: userId,
            update_time: now,
        }, {
            where: {
                id: id,
            },
            returning: true,
        });
        return { code: 0, msg: '发布成功' };
    }
    /**
     * 修改评论任务的状态
     *
     * @param payloads
     * @param userId
     */
    async changeCommentEnable(payloads, userId) {
        const { id, enable_flag } = payloads;
        const commentTask = await commentTask_1.CommentTaskModel.findOne({
            where: {
                id,
            },
        });
        if (!commentTask || commentTask.del_flag) {
            return { code: 1, msg: 'ID参数无效或已删除' };
        }
        if (commentTask.enable_flag == enable_flag) {
            return { code: 0, msg: '操作成功' };
        }
        const now = new Date();
        await commentTask_1.CommentTaskModel.update({
            enable_flag,
            update_by: userId,
            update_time: now,
        }, {
            where: {
                id: commentTask.id,
            },
            returning: true,
        });
        return { code: 0, msg: '操作成功' };
    }
    /**
     * 修改评论任务的评论控制开关状态
     *
     * @param payloads
     * @param userId
     */
    async changeCommentControl(payloads, userId) {
        const { id, control_flag } = payloads;
        const commentTask = await commentTask_1.CommentTaskModel.findOne({
            where: {
                id,
            },
        });
        if (!commentTask || commentTask.del_flag) {
            return { code: 1, msg: 'ID参数无效或已删除' };
        }
        if (commentTask.control_flag == control_flag) {
            return { code: 0, msg: '操作成功' };
        }
        // 控制开关打开, 判断当前up账号是否已登录
        if (control_flag) {
            const biliUpAccount = await biliAccount_1.BiliAccountModel.findOne({
                where: {
                    mid: commentTask.uid,
                },
            });
            if (!biliUpAccount || !biliUpAccount.cookie) {
                return { code: -1, msg: '请先扫码登录UP主账号' };
            }
        }
        else {
            //开关关闭, 关闭定时任务
            const cronService = typedi_2.Container.get(cron_1.default);
            cronService.delCronByTaskId(commentTask.id);
        }
        const now = new Date();
        await commentTask_1.CommentTaskModel.update({
            control_flag,
            update_by: userId,
            update_time: now,
        }, {
            where: {
                id: commentTask.id,
            },
            returning: true,
        });
        return { code: 0, msg: '操作成功' };
    }
    /**
     * 删除评论任务
     *
     * @param payloads
     * @param userId
     */
    async delComment(payloads, userId) {
        const { id } = payloads;
        const commentTask = await commentTask_1.CommentTaskModel.findOne({
            where: {
                id,
            },
        });
        if (!commentTask) {
            return { code: 1, msg: 'ID参数无效' };
        }
        if (commentTask.del_flag) {
            return { code: 1, msg: '该任务已删除' };
        }
        const now = new Date();
        await commentTask_1.CommentTaskModel.update({
            del_flag: true,
            update_by: userId,
            update_time: now,
        }, {
            where: {
                id: commentTask.id,
            },
            returning: true,
        });
        // 停止定时任务
        const cronService = typedi_2.Container.get(cron_1.default);
        cronService.delCronByTaskId(commentTask.id);
        // cronService.stopTask(CRON_NAME_DIC['9'], commentTask.id, recordId);
        return { code: 0, msg: '操作成功' };
    }
    async stopCommentTask(payloads, userId, delCron = true) {
        const { id } = payloads;
        // 获取任务实例
        const commentTask = await commentTask_1.CommentTaskModel.findOne({
            where: {
                id: id,
                publish_flag: true,
                status: {
                    [sequelize_1.Op.or]: [simpleTask_1.TaskStatusEnum.be_queuing, simpleTask_1.TaskStatusEnum.on_execute],
                },
                task_type: [
                    commentTask_1.CommentTaskTypeEnum.PUBLISH,
                    commentTask_1.CommentTaskTypeEnum.CONTROL,
                    commentTask_1.CommentTaskTypeEnum.AI_REVIEW,
                ],
            },
        });
        if (!commentTask) {
            return {
                code: 400,
                msg: `该任务未找到, 不存在或未发布, 任务ID: ${id}`,
            };
        }
        // 将任务状态设置为已完成，并关闭控评开关
        await commentTask_1.CommentTaskModel.update({
            control_flag: false,
            status: simpleTask_1.TaskStatusEnum.execute_complete,
            update_by: userId,
            update_time: new Date(),
        }, {
            where: {
                id: commentTask.id,
            },
        });
        if (delCron) {
            // 停止定时任务
            const cronService = typedi_2.Container.get(cron_1.default);
            cronService.delCronByTaskId(commentTask.id);
            // cronService.stopTask(CRON_NAME_DIC['9'], commentTask.id, recordId);
        }
        return { code: 0, msg: '执行成功' };
    }
    /**
     * 执行评论任务
     *
     * @param payloads 请求参数
     * @param userId
     * @param accessToken
     * @param [toSchedule=true]
     */
    async executeCommentTask(payloads, userId, accessToken, toSchedule = true) {
        const { id } = payloads;
        // 获取任务实例
        const commentTask = await commentTask_1.CommentTaskModel.findOne({
            where: {
                id: id,
                publish_flag: true,
            },
        });
        if (!commentTask) {
            return {
                code: 400,
                msg: `该任务未找到, 不存在或未发布, 任务ID: ${id}`,
            };
        }
        // 判断一下任务类型
        if (commentTask.task_type == commentTask_1.CommentTaskTypeEnum.CONTROL ||
            commentTask.task_type == commentTask_1.CommentTaskTypeEnum.AI_REVIEW) {
            // 开启了控制开关
            if (commentTask.control_flag) {
                await this.createAndRunControlCommentTask(userId, accessToken, commentTask, toSchedule);
                return { code: 0, msg: '执行成功' };
            }
            else {
                return { code: -1, msg: '请先开启控评开关' };
            }
        }
        // 获取评论池记录列表
        const commentPoolList = await commentPool_1.CommentPoolModel.findAll({
            where: {
                bvid: commentTask.bvid,
                tag_id: commentTask.comment_tag_id,
            },
            raw: true,
        });
        if (!commentPoolList.length) {
            return {
                code: 400,
                msg: `该标签对应的评论记录为空, 请检查后重新执行`,
            };
        }
        // 获取账号组
        const { template_code } = commentTask;
        const accountGroup = await accountGroup_1.AccountGroupModel.findOne({
            where: {
                template_code: template_code,
                publish_flag: true,
                enable_flag: true,
                del_flag: false,
            },
        });
        if (!accountGroup) {
            return {
                code: 400,
                msg: `账号组未找到, 不存在或未发布或已停用, 模版编码: ${template_code}`,
            };
        }
        // 分配类型
        const { distribute_type } = accountGroup;
        // 判断分配类型
        switch (distribute_type) {
            case accountGroup_1.DistributeTypeEnum.ROUND_ROBIN:
                // 以轮询的方式执行评论任务
                const result = await this.roundRobinExecuteCommentTask(userId, accessToken, commentTask, commentPoolList, template_code);
                return result;
            default:
                return {
                    code: 400,
                    msg: `无效的分配方式, 模版编码: ${template_code}`,
                };
        }
    }
    async createAndRunControlCommentTask(userId, accessToken, commentTask, toSchedule) {
        const biliUpAccount = await biliAccount_1.BiliAccountModel.findOne({
            where: {
                mid: commentTask.uid,
            },
        });
        // 创建任务执行记录
        const now = new Date();
        let recordId = (0, uuid_1.v4)();
        await commentTaskExecuteRecord_1.CommentTaskExecuteRecordModel.create({
            id: recordId,
            schedule_type: simpleTaskExecuteRecord_1.ScheduleTypeEnum.RIGHT_NOW,
            schedule_id: recordId,
            task_id: commentTask.id,
            start_time: now,
            total_count: 1,
            success_count: 0,
            create_by: userId,
            create_time: now,
            update_by: userId,
            update_time: now,
        });
        // 评控任务明细列表
        let controlTaskExecuteItem = {
            id: (0, uuid_1.v4)(),
            record_id: recordId,
            mid: (biliUpAccount === null || biliUpAccount === void 0 ? void 0 : biliUpAccount.mid) || '',
            comment: '',
            proxy_ip: (biliUpAccount === null || biliUpAccount === void 0 ? void 0 : biliUpAccount.proxy_ip) || '',
            cookie: (biliUpAccount === null || biliUpAccount === void 0 ? void 0 : biliUpAccount.cookie) || '',
            user_agent: (biliUpAccount === null || biliUpAccount === void 0 ? void 0 : biliUpAccount.user_agent) || '',
            status: simpleTaskExecuteItem_1.ExecuteItemStatusEnum.WAIT,
            execute_time: new Date(now.getTime()),
            execute_result: '',
            create_by: userId,
            create_time: now,
            update_by: userId,
            update_time: now,
        };
        // 写入数据库
        await commentTaskExecuteItem_1.CommentTaskExecuteItemModel.create(controlTaskExecuteItem);
        // 将任务状态设置为排队中
        await commentTask_1.CommentTaskModel.update({
            status: simpleTask_1.TaskStatusEnum.be_queuing,
            update_by: userId,
            update_time: now,
        }, {
            where: {
                id: commentTask.id,
            },
        });
        // 执行定时任务
        const cronService = typedi_2.Container.get(cron_1.default);
        // 启动BiliBot
        cronService.runTask(accessToken, const_1.CRON_NAME_DIC[commentTask.task_type == commentTask_1.CommentTaskTypeEnum.AI_REVIEW ? '14' : '9'], commentTask.id, recordId, toSchedule);
    }
    /**
     * 以轮询的方式执行评论任务
     *
     * @param userId
     * @param accessToken
     * @param commentTask
     * @param commentPoolList
     * @param cityDistributeTemplateItemList
     */
    async roundRobinExecuteCommentTask(userId, accessToken, commentTask, commentPoolList, template_code) {
        const t = await models_1.sequelize.transaction();
        try {
            // 获取账号组城市分配明细
            const cityList = await cityDistributeTemplateItem_1.CityDistributeTemplateItemModel.findAll({
                attributes: ['city_code', 'city_name'],
                where: {
                    template_code,
                    enable_flag: true,
                },
                transaction: t,
            });
            if (!cityList.length) {
                return {
                    code: 400,
                    msg: `有效的城市分配明细为空, 模版编码: ${template_code}`,
                };
            }
            const proxyIps = await proxyIp_1.ProxyIpModel.findAll({
                attributes: ['ip', 'city_code', 'city_name'],
                where: {
                    city_code: cityList.map((item) => {
                        return item.city_code;
                    }),
                    del_flag: false,
                },
            });
            if (!proxyIps.length) {
                return {
                    code: 400,
                    msg: `代理IP暂未配置或无可用的IP, 模版编码: ${template_code}`,
                };
            }
            // 先从账号组中提取所有可用账号
            const allAvailableAccounts = await biliAccountGroupBindRecord_1.BiliAccountGroupBindRecordModel.findAll({
                attributes: ['id', 'mid', 'group_code', 'create_time'],
                order: [['create_time', 'DESC']],
                where: {
                    del_flag: false,
                    group_code: template_code,
                },
                include: [
                    {
                        model: biliAccount_1.BiliAccountModel,
                        as: 'biliAccount', // 关联模型的别名
                        attributes: [
                            'mid',
                            'name',
                            'cookie',
                            'user_agent',
                            'proxy_ip',
                            'login_time',
                        ],
                        where: { login_status: true, del_flag: false },
                    },
                ],
                transaction: t,
            });
            this.logger.info('[panel][提取账号], 账号组[%s]的可用账号数量: %d', template_code, allAvailableAccounts.length);
            // 如果没有找到任何账号，则抛出错误
            if (allAvailableAccounts.length === 0) {
                // throw new Error('没有可用的账号发送评论');
                return { code: 400, msg: '没有可用的账号发送评论' };
            }
            // 创建一个对象来保存每个city的BiliAccount分组
            const cityAccountsMap = {};
            // 初始化每个城市的分组数组
            proxyIps.forEach((proxyIp) => {
                const { city_code } = proxyIp;
                cityAccountsMap[city_code] = [];
            });
            // 初始化一个“未分配”分组，用于存放没有匹配到的账号
            const unassignedAccounts = [];
            // 遍历所有可用账号，分组到相应的city中
            allAvailableAccounts.forEach((accountRecord) => {
                const { biliAccount } = accountRecord;
                if (!biliAccount)
                    return;
                // 查找当前账号的proxy_ip是否在proxyIps列表中
                const proxyMatch = proxyIps.find((proxyIp) => proxyIp.ip === biliAccount.proxy_ip);
                if (proxyMatch) {
                    // 找到匹配的city_code，将账号分配到对应的city
                    cityAccountsMap[proxyMatch.city_code].push(biliAccount);
                }
                else {
                    // 如果没有匹配到任何proxy_ip，则将账号放入未分配分组
                    unassignedAccounts.push(biliAccount);
                }
            });
            // 打印分组结果
            this.logger.info('[panel][账号分组], 分组结果: %O', {
                cityAccountsMap,
                unassignedAccounts,
            });
            const now = new Date();
            const durationSecs = this.getDurationSecs(commentTask.execute_duration, commentTask.duration_unit);
            // 待发送评论总数
            let commentCount = commentPoolList.length;
            // 每次毫秒数
            const perTimesMilliSecs = (durationSecs * 1000) / commentCount;
            // 创建任务执行记录
            let recordId = (0, uuid_1.v4)();
            await commentTaskExecuteRecord_1.CommentTaskExecuteRecordModel.create({
                id: recordId,
                schedule_type: simpleTaskExecuteRecord_1.ScheduleTypeEnum.RIGHT_NOW,
                schedule_id: recordId,
                task_id: commentTask.id,
                start_time: now,
                total_count: commentCount,
                success_count: 0,
                create_by: userId,
                create_time: now,
                update_by: userId,
                update_time: now,
            }, { transaction: t });
            // 获取城市分组数量
            const cityAccountsMapLength = Object.keys(cityAccountsMap).length;
            // 平均每个城市分配评论数
            let avgPerCityCount = Math.floor(commentCount / cityAccountsMapLength);
            let remainingComments = commentCount; // 剩余评论数
            // 遍历所有城市模版分配评论
            let taskExecuteItemList = [];
            Object.entries(cityAccountsMap).forEach(([cityCode, accounts], index) => {
                this.logger.info(`City Code: ${cityCode},  Accounts Count: ${accounts.length}`);
                let limit = avgPerCityCount; // 默认分配的评论
                //  如果是最后一个城市或者剩余评论不足以平均分配，则分配剩余评论
                if (index === cityAccountsMapLength - 1 ||
                    remainingComments < avgPerCityCount) {
                    limit = Math.max(remainingComments, 0); // 直接分配剩余评论
                }
                else {
                    remainingComments -= avgPerCityCount; // 减去预期分配数量
                }
                this.logger.info('[panel][分配评论], 评论总数[%d], 剩余评论数[%d], {%s}发送评论数量: %d', commentCount, remainingComments, cityCode, limit);
                let currentTotal = taskExecuteItemList.length;
                for (let i = 0; i < limit; i++) {
                    const biliAccount = accounts[i % accounts.length];
                    const comment = commentPoolList.shift(); // 从评论池中取出评论
                    if (biliAccount && comment) {
                        taskExecuteItemList.push({
                            id: (0, uuid_1.v4)(),
                            record_id: recordId,
                            mid: biliAccount.mid,
                            comment: comment.comment,
                            proxy_ip: biliAccount.proxy_ip || '',
                            cookie: biliAccount.cookie,
                            user_agent: biliAccount.user_agent || '',
                            status: simpleTaskExecuteItem_1.ExecuteItemStatusEnum.WAIT,
                            execute_time: new Date(now.getTime() + (i + currentTotal) * perTimesMilliSecs),
                            execute_result: '',
                            create_by: userId,
                            create_time: now,
                            update_by: userId,
                            update_time: now,
                        });
                        this.logger.info('[panel][发送评论], 账号[%s]发送了评论: [%s]', biliAccount.name, comment.comment);
                    }
                }
            });
            // 写入数据库
            if (taskExecuteItemList.length > 0) {
                await commentTaskExecuteItem_1.CommentTaskExecuteItemModel.bulkCreate(taskExecuteItemList, {
                    transaction: t,
                });
            }
            // 更新任务状态
            await commentTask_1.CommentTaskModel.update({
                status: simpleTask_1.TaskStatusEnum.be_queuing,
                update_by: userId,
                update_time: now,
            }, {
                where: {
                    id: commentTask.id,
                },
                transaction: t,
            });
            // 启动任务
            const cronService = typedi_2.Container.get(cron_1.default);
            cronService.runTask(accessToken, const_1.CRON_NAME_DIC['8'], commentTask.id, recordId);
            // 提交事务
            await t.commit();
            return { code: 0, msg: '执行成功' };
        }
        catch (error) {
            // 回滚事务
            await t.rollback();
            this.logger.error('RoundRobin execute comment task 🔥 error: %o', error);
            return { code: 500, msg: error.message };
        }
    }
    async reviewComment(comment, userId) {
        try {
            let aiRes = await difyAxios_1.default.post('/completion-messages', {
                inputs: { query: comment },
                response_mode: 'blocking',
                user: userId,
            });
            if (aiRes.status === 200) {
                if ((0, util_1.isJSON)(aiRes.data.answer)) {
                    return { code: 0, data: JSON.parse(aiRes.data.answer) };
                }
                else {
                    return { code: 400, msg: 'AI生成数据格式错误' };
                }
            }
            else {
                return { code: aiRes.status, msg: aiRes.data.message };
            }
        }
        catch (err) {
            this.logger.error('Review comment task 🔥 error: %o', err);
            return { code: 500, msg: err.message };
        }
    }
    async getStandyComments(payloads) {
        const uid = payloads.uid;
        const bvid = payloads.bvid;
        const page_index = Number(payloads.page_index || '0');
        const page_size = Number(payloads.page_size || '0');
        const sorterQuery = JSON.parse(payloads.sorter || '{}');
        let query = { uid, bvid, del_flag: false, status: commentHitItemRecord_1.ReviewStatus.Pending };
        let order = [];
        if (sorterQuery) {
            const { field, type } = sorterQuery;
            if (field && type) {
                order.unshift([field, type]);
            }
        }
        const result = await commentHitItemRecord_1.CommentHitItemRecordModel.findAndCountAll({
            order: order,
            offset: (page_index - 1) * page_size,
            limit: page_size,
            where: Object.assign({}, query),
        });
        return {
            code: 0,
            data: {
                total: result.count,
                list: result.rows,
            },
        };
    }
    async delHitComments(payloads, userId, accessToken) {
        const { uid, aid, bvid, ids, rpids } = payloads;
        const biliUpAccount = await biliAccount_1.BiliAccountModel.findOne({
            where: {
                mid: uid,
            },
        });
        if (!biliUpAccount || !biliUpAccount.cookie) {
            return { code: 400, msg: '请先扫码登录UP主账号' };
        }
        const now = new Date();
        await commentHitItemRecord_1.CommentHitItemRecordModel.update({
            del_flag: true,
            update_by: userId,
            update_time: now,
        }, {
            where: { id: ids },
            returning: true,
        });
        // 删除命中的评论
        const cookie = (0, util_1.base64Encode)(biliUpAccount.cookie);
        const cronService = typedi_2.Container.get(cron_1.default);
        cronService.runHitTask(accessToken, cookie, const_1.CRON_NAME_DIC['15'], aid, bvid, rpids.join(','));
        return { code: 0, msg: '删除成功' };
    }
};
BiliTaskService = __decorate([
    (0, typedi_1.Service)(),
    __param(0, (0, typedi_1.Inject)('logger')),
    __param(1, (0, typedi_1.Inject)('CACHE_MANAGER')),
    __metadata("design:paramtypes", [winston_1.default.Logger, Object])
], BiliTaskService);
exports.default = BiliTaskService;
//# sourceMappingURL=biliTask.js.map