"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const typedi_1 = require("typedi");
const typedi_2 = require("typedi");
const winston_1 = __importDefault(require("winston"));
const sequelize_1 = require("sequelize");
const uuid_1 = require("uuid");
const commentPool_1 = require("../data/models/commentPool");
const tag_1 = require("../data/models/tag");
const tag_2 = __importDefault(require("../services/tag"));
const models_1 = require("../data/models");
let CommentPoolService = class CommentPoolService {
    constructor(logger) {
        this.logger = logger;
    }
    async findByTagIdGrouped(bvid) {
        const result = await commentPool_1.CommentPoolModel.findAndCountAll({
            attributes: [
                'tag_id',
                [
                    models_1.sequelize.fn('MAX', models_1.sequelize.col('comment_pool.create_time')),
                    'create_time',
                ],
                [models_1.sequelize.fn('COUNT', models_1.sequelize.col('tag_id')), 'count'], // 统计每个 tag_id 的数量
                [models_1.sequelize.fn('GROUP_CONCAT', models_1.sequelize.col('comment')), 'comments'], // 获取每个 tag_id 的所有评论
            ],
            include: [
                {
                    model: tag_1.TagModel,
                    as: 'tag',
                    required: true,
                    //attributes: ['name'],
                },
            ],
            group: ['tag_id', 'tag.id'], // 按 tag_id 和 tag.id 分组
            where: {
                bvid,
            },
        });
        this.logger.debug('Grouped tag count: %o', result.count.length);
        return {
            code: 0,
            data: {
                total: result.count.length,
                list: result.rows,
            },
        };
    }
    /**
     * 获取分页列表
     *
     * @param payloads 请求参数
     */
    async getList(payloads) {
        const { page_index, page_size, bvid, tag_id, search_text } = payloads;
        const where = {};
        if (bvid) {
            Object.assign(where, { bvid });
        }
        if (tag_id) {
            Object.assign(where, { tag_id });
        }
        if (search_text) {
            Object.assign(where, {
                comment: {
                    [sequelize_1.Op.like]: `%${search_text}%`,
                },
            });
        }
        const result = await commentPool_1.CommentPoolModel.findAndCountAll({
            where,
            order: [['create_time', 'DESC']],
            offset: (page_index - 1) * page_size,
            limit: page_size,
        });
        let list = [];
        for (let i = 0; i < result.rows.length; i++) {
            let _item = result.rows[i].toJSON();
            const tag = await tag_1.TagModel.findOne({
                where: {
                    id: _item.tag_id,
                },
            });
            list.push(Object.assign(Object.assign({}, _item), { tag }));
        }
        return {
            code: 0,
            msg: 'Success',
            data: {
                total: result.count,
                list,
            },
        };
    }
    /**
     * 新增/修改记录
     *
     * @param payloads 请求参数
     * @param userId
     */
    async modify(payloads, userId) {
        const now = new Date();
        const { id, bvid, comment, tag_id, tag_name } = payloads;
        let tag;
        if (tag_id) {
            tag = await tag_1.TagModel.findOne({
                where: {
                    id: tag_id,
                    type: tag_1.TagTypeEnum.COMMENT_MATERIAL,
                },
            });
            if (!tag) {
                return { code: 400, msg: '选择的标签不存在' };
            }
        }
        else if (tag_name) {
            tag = await tag_1.TagModel.findOne({
                where: {
                    name: tag_name,
                    type: tag_1.TagTypeEnum.COMMENT_MATERIAL,
                },
            });
            if (!tag) {
                tag = new tag_1.Tag({
                    id: (0, uuid_1.v4)(),
                    name: tag_name,
                    type: tag_1.TagTypeEnum.COMMENT_MATERIAL,
                    enable_flag: true,
                    del_flag: false,
                    create_by: userId,
                    create_time: now,
                    update_by: userId,
                    update_time: now,
                });
                await tag_1.TagModel.create(tag);
            }
        }
        else {
            return { code: 400, msg: '标签为空' };
        }
        let commentPool;
        if (id) {
            commentPool = await commentPool_1.CommentPoolModel.findOne({
                where: {
                    id,
                },
            });
        }
        if (commentPool) {
            await commentPool_1.CommentPoolModel.update({
                comment: comment,
                tag_id: tag.id,
                update_by: userId,
                update_time: now,
            }, {
                where: {
                    id,
                },
            });
        }
        else {
            await commentPool_1.CommentPoolModel.create({
                id,
                bvid,
                comment,
                tag_id: tag.id,
                create_by: userId,
                create_time: now,
                update_by: userId,
                update_time: now,
            });
        }
        return { code: 0, msg: '保存成功' };
    }
    /**
     * 删除记录
     *
     * @param payloads
     * @param accessToken
     */
    async del(payloads, accessToken) {
        const { id } = payloads;
        await commentPool_1.CommentPoolModel.destroy({
            where: {
                id,
            },
        });
        return { code: 0, msg: '删除成功' };
    }
    /**
     * 导入记录
     *
     * @param payloads
     * @param bvid
     * @param userId
     */
    async upload(payloads, bvid, userId) {
        const now = new Date();
        const tagService = typedi_2.Container.get(tag_2.default);
        let total = 0;
        for (let i = 0; i < payloads.length; i++) {
            const tag = await tagService.getDb({
                type: tag_1.TagTypeEnum.COMMENT_MATERIAL,
                name: payloads[i].tag_name,
            });
            if (!tag) {
                let tagId = (0, uuid_1.v4)();
                await tagService.create({
                    id: tagId,
                    type: tag_1.TagTypeEnum.COMMENT_MATERIAL,
                    name: payloads[i].tag_name,
                    enable_flag: true,
                    del_flag: false,
                    create_by: userId,
                    create_time: now,
                    update_by: userId,
                    update_time: now,
                });
                await commentPool_1.CommentPoolModel.create({
                    id: (0, uuid_1.v4)(),
                    bvid,
                    comment: payloads[i].comment,
                    tag_id: tagId,
                    create_by: userId,
                    create_time: now,
                    update_by: userId,
                    update_time: now,
                });
            }
            else {
                const commentPool = await commentPool_1.CommentPoolModel.findOne({
                    where: {
                        comment: payloads[i].comment,
                        tag_id: tag.id,
                        bvid,
                    },
                });
                if (!commentPool) {
                    await commentPool_1.CommentPoolModel.create({
                        id: (0, uuid_1.v4)(),
                        bvid,
                        comment: payloads[i].comment,
                        tag_id: tag.id,
                        create_by: userId,
                        create_time: now,
                        update_by: userId,
                        update_time: now,
                    });
                }
            }
            total++;
        }
        return { total };
    }
};
CommentPoolService = __decorate([
    (0, typedi_1.Service)(),
    __param(0, (0, typedi_1.Inject)('logger')),
    __metadata("design:paramtypes", [winston_1.default.Logger])
], CommentPoolService);
exports.default = CommentPoolService;
//# sourceMappingURL=commentPool.js.map