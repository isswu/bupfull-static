"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const typedi_1 = require("typedi");
const winston_1 = __importDefault(require("winston"));
const uuid_1 = require("uuid");
const userBiliAccountBindRecord_1 = require("../data/models/binding/userBiliAccountBindRecord");
const userBiliSimpleTaskBindRecord_1 = require("../data/models/binding/userBiliSimpleTaskBindRecord");
const userBiliCommentTaskBindRecord_1 = require("../data/models/binding/userBiliCommentTaskBindRecord");
const userBiliDanmuTaskBindRecord_1 = require("../data/models/binding/userBiliDanmuTaskBindRecord");
let DataAuthorizeService = class DataAuthorizeService {
    constructor(logger) {
        this.logger = logger;
    }
    /**
     * 授权用户访问B站账号
     *
     * @param payloads - 请求参数
     * @param userId - 操作用户ID
     * @returns Promise<any>
     */
    async authorizeBiliAccounts(payloads, userId) {
        try {
            const now = new Date();
            const { dataIds, userIds } = payloads;
            for (const dataId of dataIds) {
                for (const authId of userIds) {
                    const record = await userBiliAccountBindRecord_1.UserBiliAccountBindRecordModel.findOne({
                        where: { user_id: authId, mid: dataId },
                    });
                    if (!record) {
                        await userBiliAccountBindRecord_1.UserBiliAccountBindRecordModel.create({
                            id: (0, uuid_1.v4)(),
                            user_id: authId,
                            mid: dataId,
                            readable: true,
                            writable: true,
                            create_by: userId,
                            create_time: now,
                            update_by: userId,
                            update_time: now,
                        });
                        this.logger.info('B站账号访问授权成功, [userId: %s, mid: %s]', userId, dataId);
                    }
                    else {
                        this.logger.warn('B站账号访问记录已存在, [userId: %s, mid: %s]', userId, dataId);
                    }
                }
            }
            return {
                code: 0,
                msg: '数据访问授权成功',
            };
        }
        catch (error) {
            this.logger.error(`数据访问授权失败，${error}`);
            return {
                code: 400,
                msg: `数据访问授权失败，${error}`,
            };
        }
    }
    /**
     * 授权用户访问简单任务
     *
     * @param payloads - 请求参数
     * @param userId - 操作用户ID
     * @returns Promise<any>
     */
    async authorizeSimpleTasks(payloads, userId) {
        try {
            const now = new Date();
            const { dataIds, userIds } = payloads;
            for (const dataId of dataIds) {
                for (const authId of userIds) {
                    const record = await userBiliSimpleTaskBindRecord_1.UserBiliSimpleTaskBindRecordModel.findOne({
                        where: { user_id: authId, simple_task_id: dataId },
                    });
                    if (!record) {
                        await userBiliSimpleTaskBindRecord_1.UserBiliSimpleTaskBindRecordModel.create({
                            id: (0, uuid_1.v4)(),
                            user_id: authId,
                            simple_task_id: dataId,
                            readable: true,
                            writable: true,
                            create_by: userId,
                            create_time: now,
                            update_by: userId,
                            update_time: now,
                        });
                        this.logger.info('简单任务访问授权成功, [userId: %s, mid: %s]', userId, dataId);
                    }
                    else {
                        this.logger.info('简单任务访问记录已存在, [userId: %s, mid: %s]', userId, dataId);
                    }
                }
            }
            return {
                code: 0,
                msg: '数据访问授权成功',
            };
        }
        catch (error) {
            this.logger.error(`数据访问授权失败，${error}`);
            return {
                code: 400,
                msg: `数据访问授权失败，${error}`,
            };
        }
    }
    /**
     * 授权用户访问评论任务
     *
     * @param payloads - 请求参数
     * @param userId - 操作用户ID
     * @returns Promise<any>
     */
    async authorizeCommentTasks(payloads, userId) {
        try {
            const now = new Date();
            const { dataIds, userIds } = payloads;
            for (const dataId of dataIds) {
                for (const authId of userIds) {
                    const record = await userBiliCommentTaskBindRecord_1.UserBiliCommentTaskBindRecordModel.findOne({
                        where: { user_id: authId, comment_task_id: dataId },
                    });
                    if (!record) {
                        await userBiliCommentTaskBindRecord_1.UserBiliCommentTaskBindRecordModel.create({
                            id: (0, uuid_1.v4)(),
                            user_id: authId,
                            comment_task_id: dataId,
                            readable: true,
                            writable: true,
                            create_by: userId,
                            create_time: now,
                            update_by: userId,
                            update_time: now,
                        });
                        this.logger.info('评论任务访问授权成功, [userId: %s, mid: %s]', userId, dataId);
                    }
                    else {
                        this.logger.info('评论任务访问记录已存在, [userId: %s, mid: %s]', userId, dataId);
                    }
                }
            }
            return {
                code: 0,
                msg: '数据访问授权成功',
            };
        }
        catch (error) {
            this.logger.error(`数据访问授权失败，${error}`);
            return {
                code: 400,
                msg: `数据访问授权失败，${error}`,
            };
        }
    }
    /**
     * 授权用户访问弹幕任务
     *
     * @param payloads - 请求参数
     * @param userId - 操作用户ID
     * @returns Promise<any>
     */
    async authorizeDanmakuTasks(payloads, userId) {
        try {
            const now = new Date();
            const { dataIds, userIds } = payloads;
            for (const dataId of dataIds) {
                for (const authId of userIds) {
                    const record = await userBiliDanmuTaskBindRecord_1.UserBiliDanmuTaskBindRecordModel.findOne({
                        where: { user_id: authId, danmu_task_id: dataId },
                    });
                    if (!record) {
                        await userBiliDanmuTaskBindRecord_1.UserBiliDanmuTaskBindRecordModel.create({
                            id: (0, uuid_1.v4)(),
                            user_id: authId,
                            danmu_task_id: dataId,
                            readable: true,
                            writable: true,
                            create_by: userId,
                            create_time: now,
                            update_by: userId,
                            update_time: now,
                        });
                        this.logger.info('弹幕任务访问授权成功, [userId: %s, mid: %s]', userId, dataId);
                    }
                    else {
                        this.logger.info('弹幕任务访问记录已存在, [userId: %s, mid: %s]', userId, dataId);
                    }
                }
            }
            return {
                code: 0,
                msg: '数据访问授权成功',
            };
        }
        catch (error) {
            this.logger.error(`数据访问授权失败，${error}`);
            return {
                code: 400,
                msg: `数据访问授权失败，${error}`,
            };
        }
    }
};
DataAuthorizeService = __decorate([
    (0, typedi_1.Service)(),
    __param(0, (0, typedi_1.Inject)('logger')),
    __metadata("design:paramtypes", [winston_1.default.Logger])
], DataAuthorizeService);
exports.default = DataAuthorizeService;
//# sourceMappingURL=dataAuthorize.js.map