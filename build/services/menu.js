"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const typedi_1 = require("typedi");
const winston_1 = __importDefault(require("winston"));
const bupAxios_1 = __importDefault(require("../plugins/axios/bupAxios"));
let MenuService = class MenuService {
    constructor(logger) {
        this.logger = logger;
    }
    /**
     * 获取分页列表
     *
     * @param payloads 请求参数
     */
    async getList(accessToken) {
        const res = await bupAxios_1.default.get('/auth/menu', {
            params: {},
            headers: {
                token: accessToken,
            },
        });
        if (res.data.err != 0) {
            return {
                code: 400,
                msg: res.data.errMsg,
            };
        }
        return {
            code: 0,
            msg: 'Success',
            data: res.data.data,
        };
    }
    /**
     * 新增/修改记录
     *
     * @param payloads 请求参数
     */
    async modify(payloads, accessToken) {
        const { id, title, parentMenuId, index, order } = payloads;
        let res;
        if (id) {
            res = await bupAxios_1.default.patch(`/auth/menu/${id}`, {
                id,
                title,
                index,
                parentMenuId,
                order,
            }, {
                headers: {
                    token: accessToken,
                },
            });
        }
        else {
            res = await bupAxios_1.default.post('/auth/menu', {
                id,
                title,
                index,
                parentMenuId,
                order,
            }, {
                headers: {
                    token: accessToken,
                },
            });
        }
        if (res.data.err != 0) {
            return {
                code: 400,
                msg: res.data.errMsg,
            };
        }
        return {
            code: 0,
            msg: 'Success',
        };
    }
    /**
     * 删除记录
     *
     * @param payloads 请求参数
     */
    async delete(payloads, accessToken) {
        const { id } = payloads;
        const res = await bupAxios_1.default.patch('/auth/menu/' + id, {
            isActive: false,
        }, {
            headers: {
                token: accessToken,
            },
        });
        if (res.data.err != 0) {
            return {
                code: 400,
                msg: res.data.errMsg,
            };
        }
        return {
            code: 0,
            msg: 'Success',
        };
    }
};
MenuService = __decorate([
    (0, typedi_1.Service)(),
    __param(0, (0, typedi_1.Inject)('logger')),
    __metadata("design:paramtypes", [winston_1.default.Logger])
], MenuService);
exports.default = MenuService;
//# sourceMappingURL=menu.js.map