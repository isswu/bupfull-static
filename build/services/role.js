"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const typedi_1 = require("typedi");
const winston_1 = __importDefault(require("winston"));
const bupAxios_1 = __importDefault(require("../plugins/axios/bupAxios"));
let RoleService = class RoleService {
    constructor(logger) {
        this.logger = logger;
    }
    /**
     * 获取分页列表
     *
     * @param payloads 请求参数
     */
    async getList(payloads, accessToken) {
        const { page_index, page_size } = payloads;
        const res = await bupAxios_1.default.get('/auth/roles', {
            params: {
                type: 2,
                page: page_index,
                pageSize: page_size,
            },
            headers: {
                token: accessToken,
            },
        });
        if (res.data.err != 0) {
            return {
                code: 400,
                msg: res.data.errMsg,
            };
        }
        return {
            code: 0,
            msg: 'Success',
            data: res.data.data,
        };
    }
    /**
     * 新增/修改记录
     *
     * @param payloads 请求参数
     */
    async modify(payloads, accessToken) {
        const { id, code, name, enName, tenantId, desc } = payloads;
        let res;
        if (id) {
            res = await bupAxios_1.default.patch(`/auth/roles/${id}`, {
                name,
                enName,
                desc,
            }, {
                headers: {
                    token: accessToken,
                },
            });
        }
        else {
            res = await bupAxios_1.default.post('/auth/roles', {
                type: 2,
                code,
                name,
                enName,
                tenantId,
                desc,
            }, {
                headers: {
                    token: accessToken,
                },
            });
        }
        if (res.data.err != 0) {
            return {
                code: 400,
                msg: res.data.errMsg,
            };
        }
        return {
            code: 0,
            msg: 'Success',
        };
    }
    /**
     * 根据角色ID获取角色信息
     *
     * @param roleId
     * @param accessToken
     * @returns
     */
    async getRoleInfoById(roleId, accessToken) {
        const res = await bupAxios_1.default.get(`/auth/roles/${roleId}`, {
            headers: {
                token: accessToken,
            },
        });
        if (res.data.err != 0) {
            return { code: 400, message: res.data.errMsg };
        }
        return {
            code: 0,
            message: 'Success',
            data: res.data.data,
        };
    }
    /**
     * 删除记录
     *
     * @param payloads 请求参数
     */
    async delete(payloads, accessToken) {
        const { id } = payloads;
        const res = await bupAxios_1.default.patch('/auth/roles/' + id, {
            isActive: false,
        }, {
            headers: {
                token: accessToken,
            },
        });
        if (res.data.err != 0) {
            return {
                code: 400,
                msg: res.data.errMsg,
            };
        }
        return {
            code: 0,
            msg: 'Success',
        };
    }
    /**
     * 根据角色ID获取对应的用户主体
     *
     * @param roleId
     * @param accessToken
     * @returns
     */
    async getSubjects(roleId, accessToken) {
        let res = await bupAxios_1.default.get(`/auth/roles/${roleId}/users`, {
            headers: {
                token: accessToken,
            },
        });
        if (res.data.err != 0) {
            return { code: 400, message: res.data.errMsg };
        }
        const roleUsers = res.data.data.rows; // 角色用户列表
        res = await bupAxios_1.default.get(`/auth/roles/${roleId}/roles/assigned`, {
            params: {
                type: 1,
            },
            headers: {
                token: accessToken,
            },
        });
        if (res.data.err != 0) {
            return { code: 400, message: res.data.errMsg };
        }
        const roleUserGroups = res.data.data.rows; // 角色用户组列表
        let subjects = []; // 角色主体列表
        // 拼装数据
        if (roleUsers) {
            for (let i = 0; i < roleUsers.length; i++) {
                subjects.push({
                    id: roleUsers[i].id,
                    name: roleUsers[i].userName,
                    type: '1',
                    createAt: roleUsers[i].createAt,
                    updateAt: roleUsers[i].updateAt,
                });
            }
        }
        if (roleUserGroups) {
            for (let i = 0; i < roleUserGroups.length; i++) {
                subjects.push({
                    id: roleUserGroups[i].id,
                    name: roleUserGroups[i].name,
                    type: '2',
                    createAt: roleUserGroups[i].createAt,
                    updateAt: roleUserGroups[i].updateAt,
                });
            }
        }
        return {
            code: 0,
            message: 'Success',
            data: {
                count: subjects.length,
                rows: subjects,
            },
        };
    }
    /**
     * 添加角色主体
     *
     * @param roleId
     * @param type
     * @param subjectIds
     * @param accessToken
     */
    async addSubjects(roleId, type, subjectIds, accessToken) {
        switch (type) {
            case '1': // 用户
                const res = await bupAxios_1.default.post(`/auth/roles/${roleId}/users`, {
                    userIds: subjectIds,
                }, {
                    headers: {
                        token: accessToken,
                    },
                });
                if (res.data.err != 0) {
                    return { code: 400, message: res.data.errMsg };
                }
                break;
            case '2': // 用户组
                for (let i = 0; i < subjectIds.length; i++) {
                    const res = await bupAxios_1.default.post(`/auth/roles/${subjectIds[i]}/roles`, {
                        roleIds: [roleId],
                    }, {
                        headers: {
                            token: accessToken,
                        },
                    });
                    if (res.data.err != 0) {
                        return { code: 400, message: res.data.errMsg };
                    }
                }
                break;
        }
        return {
            code: 0,
            message: '操作成功',
        };
    }
    /**
     * 删除角色主体
     *
     * @param roleId
     * @param type
     * @param subjectIds
     * @param accessToken
     */
    async delSubject(roleId, type, subjectId, accessToken) {
        let res;
        switch (type) {
            case '1': // 用户
                res = await bupAxios_1.default.delete(`/auth/roles/${roleId}/users/${subjectId}`, {
                    headers: {
                        token: accessToken,
                    },
                });
                if (res.data.err != 0) {
                    return { code: 400, message: res.data.errMsg };
                }
                break;
            case '2': // 用户组
                res = await bupAxios_1.default.delete(`/auth/roles/${roleId}/roles/${subjectId}`, {
                    headers: {
                        token: accessToken,
                    },
                });
                if (res.data.err != 0) {
                    return { code: 400, message: res.data.errMsg };
                }
                break;
        }
        return {
            code: 0,
            message: '操作成功',
        };
    }
    /**
     * 根据角色ID获取对应的菜单
     *
     * @param roleId
     * @param accessToken
     * @returns
     */
    async getMenus(roleId, accessToken) {
        let res = await bupAxios_1.default.get(`/auth/uiRoleMenu`, {
            params: {
                roleId: roleId,
            },
            headers: {
                token: accessToken,
            },
        });
        if (res.data.err != 0) {
            return { code: 400, message: res.data.errMsg };
        }
        return {
            code: 0,
            message: 'Success',
            data: res.data.data,
        };
    }
    /**
     * 添加角色菜单
     *
     * @param roleId
     * @param menuIds
     * @param accessToken
     */
    async addMenus(roleId, menuIds, accessToken) {
        for (let i = 0; i < menuIds.length; i++) {
            const res = await bupAxios_1.default.post(`/auth/uiRoleMenu`, {
                roleId: roleId,
                menuId: menuIds[i],
            }, {
                headers: {
                    token: accessToken,
                },
            });
            if (res.data.err != 0) {
                return { code: 400, message: res.data.errMsg };
            }
        }
        return {
            code: 0,
            message: '操作成功',
        };
    }
    /**
     * 删除角色菜单
     *
     * @param roleId
     * @param recordId
     * @param accessToken
     */
    async delMenu(roleId, recordId, accessToken) {
        let res = await bupAxios_1.default.delete(`/auth/uiRoleMenu/${recordId}`, {
            headers: {
                token: accessToken,
            },
        });
        if (res.data.err != 0) {
            return { code: 400, message: res.data.errMsg };
        }
        return {
            code: 0,
            message: '删除成功',
        };
    }
    /**
     * 根据角色ID获取资源列表
     *
     * @param roleId
     * @param accessToken
     * @returns
     */
    async getResources(roleId, accessToken) {
        let res = await bupAxios_1.default.get(`/auth/roles/${roleId}/permissions`, {
            params: {
                roleId: roleId,
            },
            headers: {
                token: accessToken,
            },
        });
        if (res.data.err != 0) {
            return { code: 400, message: res.data.errMsg };
        }
        return {
            code: 0,
            message: 'Success',
            data: res.data.data,
        };
    }
    /**
     * 添加角色资源
     *
     * @param roleId
     * @param type
     * @param resourceIds
     * @param accessToken
     */
    async addResources(roleId, type, resourceIds, accessToken) {
        for (let i = 0; i < resourceIds.length; i++) {
            const res = await bupAxios_1.default.post(`/auth/roles/${roleId}/permissions`, {
                resourceId: resourceIds[i],
                action: '*',
                effect: 'allow',
            }, {
                headers: {
                    token: accessToken,
                },
            });
            if (res.data.err != 0) {
                return { code: 400, message: res.data.errMsg };
            }
        }
        return {
            code: 0,
            message: '操作成功',
        };
    }
    /**
     * 删除角色菜单
     *
     * @param roleId
     * @param recordId
     * @param accessToken
     */
    async delResource(roleId, recordId, accessToken) {
        let res = await bupAxios_1.default.delete(`/auth/roles/${roleId}/permissions`, 
        // {
        //   resourceId: recordId,
        //   action: '*',
        //   effect: 'allow',
        // },
        {
            headers: {
                token: accessToken,
            },
        });
        if (res.data.err != 0) {
            return { code: 400, message: res.data.errMsg };
        }
        return {
            code: 0,
            message: '删除成功',
        };
    }
};
RoleService = __decorate([
    (0, typedi_1.Service)(),
    __param(0, (0, typedi_1.Inject)('logger')),
    __metadata("design:paramtypes", [winston_1.default.Logger])
], RoleService);
exports.default = RoleService;
//# sourceMappingURL=role.js.map