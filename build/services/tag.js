"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const typedi_1 = require("typedi");
const winston_1 = __importDefault(require("winston"));
const uuid_1 = require("uuid");
const sequelize_1 = require("sequelize");
const tag_1 = require("../data/models/tag");
let TagService = class TagService {
    constructor(logger) {
        this.logger = logger;
    }
    async create(payload) {
        const bTag = new tag_1.Tag(payload);
        const doc = await this.insert(bTag);
        return doc;
    }
    async save(payloads, userId) {
        const now = new Date();
        const { name, type, remark } = payloads;
        const doc = await this.insert({
            id: (0, uuid_1.v4)(),
            type,
            name,
            enable_flag: true,
            del_flag: false,
            create_by: userId,
            create_time: now,
            update_by: userId,
            update_time: now,
            remark,
        });
        return doc;
    }
    async insert(payload) {
        return await tag_1.TagModel.create(payload, { returning: true });
    }
    async update(payloads, userId) {
        const now = new Date();
        const doc = await this.getDb({ id: payloads.id });
        const newTag = new tag_1.Tag(Object.assign(Object.assign(Object.assign({}, doc), payloads), { update_by: userId, update_time: now }));
        const newDoc = await this.updateDb(newTag);
        return newDoc;
    }
    async updateDb(payload) {
        await tag_1.TagModel.update(payload, { where: { id: payload.id } });
        return await this.getDb({ id: payload.id });
    }
    async remove(ids) {
        await tag_1.TagModel.destroy({ where: { id: ids } });
    }
    /**
     * 删除标签
     *
     * @param payloads
     * @param userId
     */
    async delete(ids, userId) {
        const now = new Date();
        let tag = await tag_1.TagModel.findOne({
            where: { id: ids },
        });
        if (!tag) {
            return { code: 400, msg: '标签不存在' };
        }
        if (tag.del_flag) {
            return { code: 400, msg: '标签已删除' };
        }
        await tag_1.TagModel.update({
            del_flag: true,
            update_by: userId,
            update_time: now,
        }, {
            where: { id: ids },
        });
        return { code: 0, msg: '删除成功' };
    }
    /**
     * 获取列表
     *
     * @param payloads
     */
    async getList(payloads) {
        const { page_index, page_size, search_text } = payloads;
        const where = {};
        Object.assign(where, { del_flag: false });
        if (search_text) {
            Object.assign(where, {
                name: {
                    [sequelize_1.Op.like]: `%${search_text}%`,
                },
            });
        }
        const result = await tag_1.TagModel.findAndCountAll({
            order: [['create_time', 'DESC']],
            offset: (page_index - 1) * page_size,
            limit: page_size,
            where,
        });
        return {
            code: 0,
            msg: 'Success',
            data: {
                total: result.count,
                list: result.rows,
            },
        };
    }
    async list() {
        try {
            const result = await tag_1.TagModel.findAll({
                where: {},
                order: [['id', 'DESC']],
            });
            return result;
        }
        catch (error) {
            throw error;
        }
    }
    async getDb(query) {
        const doc = await tag_1.TagModel.findOne({ where: Object.assign({}, query) });
        return doc && doc.get({ plain: true });
    }
    async tags(searchText = '', query = {}) {
        let condition = Object.assign({}, query);
        if (searchText) {
            const encodeText = encodeURI(searchText);
            const reg = {
                [sequelize_1.Op.or]: [
                    { [sequelize_1.Op.like]: `%${searchText}%` },
                    { [sequelize_1.Op.like]: `%${encodeText}%` },
                ],
            };
            condition = Object.assign(Object.assign({}, condition), { [sequelize_1.Op.or]: [
                    {
                        name: reg,
                    },
                ] });
        }
        try {
            const result = await this.find(condition, [['create_time', 'ASC']]);
            return result;
        }
        catch (error) {
            console.error('TagService error', error);
            throw error;
        }
    }
    /**
     * 搜索标签
     *
     * @param payloads
     */
    async search(payloads) {
        const { type, search_text } = payloads;
        let where = {
            type,
        };
        if (search_text) {
            where['name'] = { [sequelize_1.Op.like]: `%${search_text}%` };
        }
        where['enable_flag'] = true;
        where['del_flag'] = false;
        const result = await tag_1.TagModel.findAll({
            order: [['create_time', 'DESC']],
            where,
        });
        return {
            code: 0,
            msg: 'Success',
            data: result,
        };
    }
    async find(query, sort = []) {
        const docs = await tag_1.TagModel.findAll({
            where: Object.assign(Object.assign({}, query), { del_flag: false }),
            order: [...sort],
        });
        return docs;
    }
};
TagService = __decorate([
    (0, typedi_1.Service)(),
    __param(0, (0, typedi_1.Inject)('logger')),
    __metadata("design:paramtypes", [winston_1.default.Logger])
], TagService);
exports.default = TagService;
//# sourceMappingURL=tag.js.map