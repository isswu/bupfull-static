"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const typedi_1 = require("typedi");
const winston_1 = __importDefault(require("winston"));
const bupAxios_1 = __importDefault(require("../plugins/axios/bupAxios"));
let TenantService = class TenantService {
    constructor(logger) {
        this.logger = logger;
    }
    /**
     * 获取租户列表
     *
     * @param payloads
     * @param accessToken
     */
    async getList(payloads, accessToken) {
        const { page_index, page_size } = payloads;
        const res = await bupAxios_1.default.get('/tenant', {
            params: {
                page: page_index,
                pageSize: page_size,
            },
            headers: {
                token: accessToken,
            },
        });
        if (res.data.err != 0) {
            return {
                code: 400,
                msg: res.data.errMsg,
            };
        }
        return {
            code: 0,
            msg: 'Success',
            data: res.data.data,
        };
    }
    /**
     * 创建租户
     * @param payloads
     * @param accessToken
     * @returns
     */
    async createTenant(payloads, accessToken) {
        const { name, code } = payloads;
        const res = await bupAxios_1.default.post('/tenant', {
            name,
            code,
        }, {
            headers: {
                token: accessToken,
            },
        });
        if (res.data.err != 0) {
            return {
                code: 400,
                msg: res.data.errMsg,
            };
        }
        return {
            code: 0,
            msg: 'Success',
        };
    }
    async getTenantDetail(id, accessToken) {
        let res = await bupAxios_1.default.get(`/tenant/${id}`, {
            headers: {
                token: accessToken,
            },
        });
        if (res.data.err === 0) {
            return {
                code: 0,
                msg: 'Success',
                data: res.data.data,
            };
        }
        else {
            return { code: res.data.err, msg: res.data.errMsg };
        }
    }
    async delete(id, accessToken) {
        let res = await bupAxios_1.default.delete(`/tenant/${id}`, {
            headers: {
                token: accessToken,
            },
        });
        if (res.data.err === 0) {
            return { code: 0, msg: '删除成功' };
        }
        else {
            return { code: res.data.err, msg: res.data.errMsg };
        }
    }
};
TenantService = __decorate([
    (0, typedi_1.Service)(),
    __param(0, (0, typedi_1.Inject)('logger')),
    __metadata("design:paramtypes", [winston_1.default.Logger])
], TenantService);
exports.default = TenantService;
//# sourceMappingURL=tenant.js.map