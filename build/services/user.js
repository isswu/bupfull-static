"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
var UserService_1;
Object.defineProperty(exports, "__esModule", { value: true });
const typedi_1 = require("typedi");
const winston_1 = __importDefault(require("winston"));
const util_1 = require("../config/util");
const auth_1 = require("../data/models/auth");
const notify_1 = __importDefault(require("./notify"));
const dayjs_1 = __importDefault(require("dayjs"));
const crypto_1 = __importDefault(require("crypto"));
const bupAxios_1 = __importDefault(require("../plugins/axios/bupAxios"));
let UserService = UserService_1 = class UserService {
    constructor(logger) {
        this.logger = logger;
    }
    /**
     * 获取系统是否已经初始化
     */
    async getSystemInit() {
        // let sysUser = await SysUserModel.findOne({
        //   where: {
        //     super_admin: true
        //   }
        // });
        // return sysUser != null;
        // 接入authing，默认已初始化完成
        return true;
    }
    async getList(payloads, accessToken) {
        const { page_index, page_size } = payloads;
        const res = await bupAxios_1.default.get('/auth/users', {
            params: {
                page: page_index,
                pageSize: page_size,
                isEnable: 1,
            },
            headers: {
                token: accessToken,
            },
        });
        if (res.data.err != 0) {
            return {
                code: 400,
                msg: res.data.errMsg,
            };
        }
        return {
            code: 0,
            msg: 'Success',
            data: res.data.data,
        };
    }
    async login(payloads, req, needTwoFactor = true) {
        // 系统是否初始化
        let systemInit = await this.getSystemInit();
        if (!systemInit) {
            return { code: 100, msg: '系统未初始化' };
        }
        // 取出payloads中的username，password变量
        let { username, password } = payloads;
        // 当前时间
        const timestamp = Date.now();
        const lastlogon = UserService_1.logonTimeMap.get(username) || Date.now();
        // 判断失败次数
        let retries = UserService_1.retryTimesMap.get(username) || 0;
        const retriesTime = Math.pow(3, retries) * 1000;
        if (lastlogon && retries > 2 && timestamp - lastlogon < retriesTime) {
            const waitTime = Math.ceil((retriesTime - (timestamp - lastlogon)) / 1000);
            return {
                code: 410,
                msg: `失败次数过多，请${waitTime}秒后重试`,
                data: waitTime,
            };
        }
        // 调用authing进行登录
        let loginRes = await bupAxios_1.default.post('/auth/users/login', {
            userName: username,
            passwd: password,
            //passwd: crypto.createHash('md5').update(password).digest('hex'),
        });
        // 获取ip和地址信息
        const { ip, address } = await (0, util_1.getNetIp)(req);
        // 账号和密码相等
        if (loginRes.data.err === 0) {
            // 获取token
            let token = loginRes.data.data.token;
            // 发送登录通知
            await this.notificationService.notify('登录通知', `你于${(0, dayjs_1.default)(timestamp).format('YYYY-MM-DD HH:mm:ss')}在 ${address} ${req.platform}端 登录成功，ip地址 ${ip}`);
            // 删除重试次数记录
            UserService_1.retryTimesMap.delete(username);
            UserService_1.logonTimeMap.delete(username);
            return {
                code: 200,
                data: {
                    token,
                    lastip: ip,
                    lastaddr: address,
                    lastlogon,
                    retries,
                    platform: req.platform,
                },
            };
        }
        else {
            await this.notificationService.notify('登录通知', `你于${(0, dayjs_1.default)(timestamp).format('YYYY-MM-DD HH:mm:ss')}在 ${address} ${req.platform}端 登录失败，ip地址 ${ip}`);
            // 记录失败次数记录
            UserService_1.retryTimesMap.set(username, retries + 1);
            UserService_1.logonTimeMap.set(username, timestamp);
            if (retries > 2) {
                const waitTime = Math.round(Math.pow(3, retries + 1));
                return {
                    code: 410,
                    msg: `失败次数过多，请${waitTime}秒后重试`,
                    data: waitTime,
                };
            }
            else {
                return { code: loginRes.data.err, msg: loginRes.data.errMsg };
            }
        }
    }
    async logout(platform, accessToken) {
        let res = await bupAxios_1.default.post('/auth/users/logout', {
            token: accessToken,
        });
        if (res.data.err === 0) {
            return { code: 0, msg: '退出登录成功' };
        }
        else {
            return { code: res.data.err, msg: res.data.errMsg };
        }
    }
    async getLoginLog() {
        const docs = await auth_1.AuthModel.findAll({
            where: { type: auth_1.AuthDataType.loginLog },
        });
        if (docs && docs.length > 0) {
            const result = docs.sort((a, b) => b.info.timestamp - a.info.timestamp);
            if (result.length > 100) {
                await auth_1.AuthModel.destroy({
                    where: { id: result[result.length - 1].id },
                });
            }
            return result.map((x) => x.info);
        }
        return [];
    }
    async createUser(payloads, accessToken) {
        const { userName, password, mobile, email, tenantId } = payloads;
        let res = await bupAxios_1.default.post('/auth/users', {
            userName,
            passwd: crypto_1.default.createHash('md5').update(password).digest('hex'),
            mobile,
            email,
            tenantId,
        }, {
            headers: {
                token: accessToken,
            },
        });
        if (res.data.err === 0) {
            return { code: 0, msg: '创建成功' };
        }
        else {
            return { code: res.data.err, msg: res.data.errMsg };
        }
    }
    async remove(id, accessToken) {
        let res = await bupAxios_1.default.post(`/auth/users/${id}/status`, {
            enable: false,
        }, {
            headers: {
                token: accessToken,
            },
        });
        if (res.data.err === 0) {
            return { code: 0, msg: '删除成功' };
        }
        else {
            return { code: res.data.err, msg: res.data.errMsg };
        }
    }
    async addRoleToUser(userId, payloads, accessToken) {
        const { roleIds, overwrite } = payloads;
        let res = await bupAxios_1.default.post(`/auth/users/${userId}/roles`, {
            roleIds,
            overwrite,
        }, {
            headers: {
                token: accessToken,
            },
        });
        if (res.data.err === 0) {
            return { code: 0, msg: '添加成功' };
        }
        else {
            return { code: res.data.err, msg: res.data.errMsg };
        }
    }
    /**
     *
     * @param userId
     * @param roleId
     * @param accessToken
     * @returns
     */
    async undoRoleFromUser(userId, roleId, accessToken) {
        let res = await bupAxios_1.default.delete(`/auth/users/${userId}/role/${roleId}`, {
            headers: {
                token: accessToken,
            },
        });
        if (res.data.err === 0) {
            return { code: 0, msg: '撤销成功' };
        }
        else {
            return { code: res.data.err, msg: res.data.errMsg };
        }
    }
    async updatePassword({ oldPassword, newPassword, }, accessToken) {
        const res = await bupAxios_1.default.patch(`/auth/users/passwd`, {
            oldPasswd: crypto_1.default.createHash('md5').update(oldPassword).digest('hex'),
            newPasswd: crypto_1.default.createHash('md5').update(newPassword).digest('hex'),
        }, {
            headers: {
                token: accessToken,
            },
        });
        if (res.data.err != 0) {
            return {
                code: res.data.err,
                msg: res.data.errMsg,
            };
        }
        return {
            code: 0,
            msg: '密码修改成功',
        };
    }
    async updateAvatar(accessToken, avatar) {
        let sysUser = await this.getUserInfo(accessToken);
        if (!sysUser) {
            return { code: 400, msg: '鉴权参数无效' };
        }
        const res = await bupAxios_1.default.patch(`/auth/users/${sysUser.data.id}`, {
            avatar,
        }, {
            headers: {
                token: accessToken,
            },
        });
        if (res.data.err != 0) {
            return {
                code: res.data.err,
                msg: res.data.errMsg,
            };
        }
        return {
            code: 0,
            data: avatar,
            msg: '更新成功',
        };
    }
    async getUserInfo(accessToken) {
        // 获取用户详情
        const res = await bupAxios_1.default.get(`/auth/users/detail/${accessToken}`);
        if (res.data.err != 0) {
            return { code: res.data.err, msg: res.data.errMsg };
        }
        return {
            code: res.data.err,
            data: res.data.data,
        };
    }
    async getUserInfoById(userId, accessToken) {
        // 获取用户详情
        const res = await bupAxios_1.default.get(`/auth/users/${userId}`, {
            headers: {
                token: accessToken,
            },
        });
        if (res.data.err != 0) {
            return { code: 400, msg: res.data.errMsg };
        }
        return {
            code: 0,
            msg: 'Success',
            data: res.data.data,
        };
    }
    async getRolesByUserId(userId, accessToken) {
        const res = await bupAxios_1.default.get(`/auth/users/${userId}/roles`, {
            params: {
                type: 2,
            },
            headers: {
                token: accessToken,
            },
        });
        if (res.data.err != 0) {
            return {
                code: 400,
                msg: res.data.errMsg,
            };
        }
        return {
            code: 0,
            msg: 'Success',
            data: { rows: res.data.data.rows.filter((item) => item.type === 2) },
        };
    }
    async getUserGroupsByUserId(userId, accessToken) {
        const res = await bupAxios_1.default.get(`/auth/users/${userId}/roles`, {
            params: {
                type: 1,
            },
            headers: {
                token: accessToken,
            },
        });
        if (res.data.err != 0) {
            return {
                code: 400,
                msg: res.data.errMsg,
            };
        }
        return {
            code: 0,
            msg: 'Success',
            data: { rows: res.data.data.rows.filter((item) => item.type === 1) },
        };
    }
    async getMenuDataByLogin(accessToken) {
        const res = await bupAxios_1.default.get(`/auth/menu/privileged`, {
            headers: {
                token: accessToken,
            },
        });
        if (res.data.err != 0) {
            return {
                code: 400,
                msg: res.data.errMsg,
            };
        }
        return {
            code: res.data.err,
            msg: 'Success',
            data: res.data.data,
        };
    }
    async getNotificationMode() {
        const doc = await this.getDb({ type: auth_1.AuthDataType.notification });
        return (doc && doc.info) || {};
    }
    async updateAuthDb(payload) {
        let doc = await auth_1.AuthModel.findOne({ type: payload.type });
        if (doc) {
            const updateResult = await auth_1.AuthModel.update(payload, {
                where: { id: doc.id },
                returning: true,
            });
            doc = updateResult[1][0];
        }
        else {
            doc = await auth_1.AuthModel.create(payload, { returning: true });
        }
        return doc;
    }
    async getDb(query) {
        const doc = await auth_1.AuthModel.findOne({ where: Object.assign({}, query) });
        return doc && doc.get({ plain: true });
    }
    async updateNotificationMode(accessToken, notificationInfo) {
        const code = Math.random().toString().slice(-6);
        const isSuccess = await this.notificationService.testNotify(notificationInfo, 'BUP', `【蛟龙】测试通知 https://t.me/jiao_long`);
        if (isSuccess) {
            const sysUser = await this.getUserInfo(accessToken);
            const userId = (sysUser === null || sysUser === void 0 ? void 0 : sysUser.id) || 0;
            const result = await this.updateAuthDb({
                user_id: userId,
                type: auth_1.AuthDataType.notification,
                info: Object.assign({}, notificationInfo),
            });
            return { code: 200, data: Object.assign(Object.assign({}, result), { code }) };
        }
        else {
            return { code: 400, msg: '通知发送失败，请检查参数' };
        }
    }
};
// 失败次数map
UserService.retryTimesMap = new Map();
// 登录时间map
UserService.logonTimeMap = new Map();
// 默认userId
UserService.defaultUserId = -1;
__decorate([
    (0, typedi_1.Inject)((type) => notify_1.default),
    __metadata("design:type", notify_1.default)
], UserService.prototype, "notificationService", void 0);
UserService = UserService_1 = __decorate([
    (0, typedi_1.Service)(),
    __param(0, (0, typedi_1.Inject)('logger')),
    __metadata("design:paramtypes", [winston_1.default.Logger])
], UserService);
exports.default = UserService;
//# sourceMappingURL=user.js.map