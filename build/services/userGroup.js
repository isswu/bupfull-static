"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const typedi_1 = require("typedi");
const winston_1 = __importDefault(require("winston"));
const bupAxios_1 = __importDefault(require("../plugins/axios/bupAxios"));
let UserGroupService = class UserGroupService {
    constructor(logger) {
        this.logger = logger;
    }
    /**
     * 获取用户组列表
     *
     * @param payloads
     * @param accessToken
     */
    async getList(payloads, accessToken) {
        const { page_index, page_size } = payloads;
        const res = await bupAxios_1.default.get('/auth/roles', {
            params: {
                type: 1,
                page: page_index,
                pageSize: page_size,
            },
            headers: {
                token: accessToken,
            },
        });
        if (res.data.err != 0) {
            return {
                code: 400,
                msg: res.data.errMsg,
            };
        }
        return {
            code: 0,
            msg: 'Success',
            data: res.data.data,
        };
    }
    async createUserGroup(payloads, accessToken) {
        const { type, name, code, desc, adminUserId } = payloads;
        const res = await bupAxios_1.default.post('/auth/roles', {
            type,
            name,
            enName: '',
            code,
            desc,
            adminUserId,
        }, {
            headers: {
                token: accessToken,
            },
        });
        if (res.data.err != 0) {
            return {
                code: 400,
                msg: res.data.errMsg,
            };
        }
        return {
            code: 0,
            msg: `已成功创建：${name}`,
        };
    }
    async update(id, payloads, accessToken) {
        const { name, desc, adminUserId } = payloads;
        const res = await bupAxios_1.default.patch(`/auth/roles/${id}`, {
            name,
            desc,
            adminUserId,
        }, {
            headers: {
                token: accessToken,
            },
        });
        if (res.data.err != 0) {
            return {
                code: res.data.err,
                msg: res.data.errMsg,
            };
        }
        return {
            code: 0,
            msg: '更新成功',
        };
    }
    async delete(id, accessToken) {
        const res = await bupAxios_1.default.patch(`/auth/roles/${id}`, {
            isActive: false,
        }, {
            headers: {
                token: accessToken,
            },
        });
        if (res.data.err != 0) {
            return {
                code: res.data.err,
                msg: res.data.errMsg,
            };
        }
        return {
            code: 0,
            msg: '删除成功',
        };
    }
    async deleteUserFromGroup(id, userId, accessToken) {
        const res = await bupAxios_1.default.delete(`/auth/roles/${id}/users/${userId}`, {
            headers: {
                token: accessToken,
            },
        });
        if (res.data.err != 0) {
            return {
                code: res.data.err,
                msg: res.data.errMsg,
            };
        }
        return {
            code: 0,
            msg: '移除成功',
        };
    }
    /**
     * 获取该用户组下的用户列表
     *
     * @param payloads
     * @param accessToken
     */
    async getUserListByGroupId(payloads, accessToken) {
        const { group_id, page_index, page_size } = payloads;
        const res = await bupAxios_1.default.get(`/auth/roles/${group_id}/users`, {
            params: {
                roleId: group_id,
                page: page_index,
                pageSize: page_size,
            },
            headers: {
                token: accessToken,
            },
        });
        if (res.data.err != 0) {
            return {
                code: 400,
                msg: res.data.errMsg,
            };
        }
        return {
            code: 0,
            msg: 'Success',
            data: res.data.data,
        };
    }
    async userGrouping(payloads, accessToken) {
        const { groupId, userIds } = payloads;
        const res = await bupAxios_1.default.post(`/auth/roles/${groupId}/users`, {
            userIds,
        }, {
            headers: {
                token: accessToken,
            },
        });
        if (res.data.err != 0) {
            return {
                code: 400,
                msg: res.data.errMsg,
            };
        }
        return {
            code: 0,
            msg: 'Success',
        };
    }
};
UserGroupService = __decorate([
    (0, typedi_1.Service)(),
    __param(0, (0, typedi_1.Inject)('logger')),
    __metadata("design:paramtypes", [winston_1.default.Logger])
], UserGroupService);
exports.default = UserGroupService;
//# sourceMappingURL=userGroup.js.map