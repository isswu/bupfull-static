(self.webpackChunk=self.webpackChunk||[]).push([[7648],{155:function(T,c,e){"use strict";e.r(c);var k=e(25359),b=e.n(k),E=e(57213),P=e.n(E),y=e(49811),f=e.n(y),C=e(54306),o=e.n(C),_=e(63313),d=e.n(_),p=e(67393),D=e(9835),g=e(84163),h=e(22159),B=e(45663),u=e(24919),a=e(11527),i=function(s){var r=s.tenant,M=s.handleCancel,t=s.visible,R=p.Z.useForm(),L=o()(R,1),O=L[0],q=(0,_.useState)(!1),j=o()(q,2),H=j[0],S=j[1],Z=function(){var U=f()(b()().mark(function m(v){var F,K,A,I,w,x,W,G;return b()().wrap(function(l){for(;;)switch(l.prev=l.next){case 0:return S(!0),F=v.name,K=v.code,A=v.remark,I=r?"put":"post",r?w=P()(P()({},v),{},{id:r.id}):w={name:F,code:K,remark:A},l.prev=4,l.next=7,B.W[I]("".concat(u.Z.apiPrefix,"tenant"),{data:w});case 7:x=l.sent,W=x.code,G=x.data,W===0&&(D.ZP.success("\u64CD\u4F5C\u6210\u529F"),M()),l.next=16;break;case 13:l.prev=13,l.t0=l.catch(4),console.error(l.t0);case 16:return l.prev=16,S(!1),l.finish(16);case 19:case"end":return l.stop()}},m,null,[[4,13,16,19]])}));return function(v){return U.apply(this,arguments)}}();(0,_.useEffect)(function(){O.resetFields()},[t]);var V=r?"\u7F16\u8F91\u79DF\u6237":"\u521B\u5EFA\u79DF\u6237";return(0,a.jsx)(g.Z,{title:V,open:t,forceRender:!0,centered:!0,maskClosable:!1,onOk:function(){O.validateFields().then(function(m){Z(m)}).catch(function(m){console.log("Validate Failed:",m)})},onCancel:function(){return M()},confirmLoading:H,children:(0,a.jsxs)(p.Z,{form:O,labelCol:{span:6},wrapperCol:{span:16},name:"user_group_modal",initialValues:r,children:[(0,a.jsx)(p.Z.Item,{label:"\u79DF\u6237\u540D\u79F0",name:"name",rules:[{required:!0,message:"\u8BF7\u8F93\u5165\u79DF\u6237\u540D\u79F0"}],children:(0,a.jsx)(h.Z,{placeholder:"\u8BF7\u8F93\u5165\u79DF\u6237\u540D\u79F0"})}),(0,a.jsx)(p.Z.Item,{label:"\u79DF\u6237\u6807\u8BC6\u7B26",name:"code",rules:[{required:!0,message:"\u8BF7\u8F93\u5165\u79DF\u6237\u6807\u8BC6\u7B26"}],children:(0,a.jsx)(h.Z,{placeholder:"\u8BF7\u8F93\u5165\u79DF\u6237\u6807\u8BC6\u7B26",disabled:r==null?void 0:r.id})}),(0,a.jsx)(p.Z.Item,{label:"\u79DF\u6237\u63CF\u8FF0",name:"remark",children:(0,a.jsx)(h.Z,{placeholder:"\u8BF7\u8F93\u5165\u5173\u4E8E\u79DF\u6237\u7684\u5907\u6CE8\u4FE1\u606F"})})]})})};c.default=i},24919:function(T,c){"use strict";var e=window.__ENV__QlBaseUrl||"/";c.Z={siteName:"BUP",apiPrefix:"".concat(e,"api/"),authKey:"token",everyDayExp:65,layouts:[{name:"primary",include:[/.*/],exclude:[/(\/(en|zh))*\/login/]}],i18n:{languages:[{key:"pt-br",title:"Portugu\xEAs",flag:"/portugal.svg"},{key:"en",title:"English",flag:"/america.svg"},{key:"zh",title:"\u4E2D\u6587",flag:"/china.svg"}],defaultLanguage:"en"},scopes:[{name:"\u5B9A\u65F6\u4EFB\u52A1",value:"crons"},{name:"\u73AF\u5883\u53D8\u91CF",value:"envs"},{name:"\u8BA2\u9605\u7BA1\u7406",value:"subscriptions"},{name:"\u914D\u7F6E\u6587\u4EF6",value:"configs"},{name:"\u811A\u672C\u7BA1\u7406",value:"scripts"},{name:"\u65E5\u5FD7\u7BA1\u7406",value:"logs"},{name:"\u4F9D\u8D56\u7BA1\u7406",value:"dependencies"},{name:"\u7CFB\u7EDF\u4FE1\u606F",value:"system"}],scopesMap:{crons:"\u5B9A\u65F6\u4EFB\u52A1",envs:"\u73AF\u5883\u53D8\u91CF",subscriptions:"\u8BA2\u9605\u7BA1\u7406",configs:"\u914D\u7F6E\u6587\u4EF6",scripts:"\u811A\u672C\u7BA1\u7406",logs:"\u65E5\u5FD7\u7BA1\u7406",dependencies:"\u4F9D\u8D56\u7BA1\u7406",system:"\u7CFB\u7EDF\u4FE1\u606F"},notificationModes:[{value:"gotify",label:"Gotify"},{value:"goCqHttpBot",label:"GoCqHttpBot"},{value:"serverChan",label:"Server\u9171"},{value:"pushDeer",label:"PushDeer"},{value:"bark",label:"Bark"},{value:"telegramBot",label:"Telegram\u673A\u5668\u4EBA"},{value:"dingtalkBot",label:"\u9489\u9489\u673A\u5668\u4EBA"},{value:"weWorkBot",label:"\u4F01\u4E1A\u5FAE\u4FE1\u673A\u5668\u4EBA"},{value:"weWorkApp",label:"\u4F01\u4E1A\u5FAE\u4FE1\u5E94\u7528"},{value:"aibotk",label:"\u667A\u80FD\u5FAE\u79D8\u4E66"},{value:"iGot",label:"IGot"},{value:"pushPlus",label:"PushPlus"},{value:"chat",label:"\u7FA4\u6656chat"},{value:"email",label:"\u90AE\u7BB1"},{value:"lark",label:"\u98DE\u4E66\u673A\u5668\u4EBA"},{value:"webhook",label:"\u81EA\u5B9A\u4E49\u901A\u77E5"},{value:"closed",label:"\u5DF2\u5173\u95ED"}],notificationModeMap:{gotify:[{label:"gotifyUrl",tip:"gotify\u7684url\u5730\u5740,\u4F8B\u5982 https://push.example.de:8080",required:!0},{label:"gotifyToken",tip:"gotify\u7684\u6D88\u606F\u5E94\u7528token\u7801",required:!0},{label:"gotifyPriority",tip:"\u63A8\u9001\u6D88\u606F\u7684\u4F18\u5148\u7EA7"}],chat:[{label:"chatUrl",tip:"chat\u7684url\u5730\u5740",required:!0},{label:"chatToken",tip:"chat\u7684token\u7801",required:!0}],goCqHttpBot:[{label:"goCqHttpBotUrl",tip:"\u63A8\u9001\u5230\u4E2A\u4EBAQQ: http://127.0.0.1/send_private_msg\uFF0C\u7FA4\uFF1Ahttp://127.0.0.1/send_group_msg",required:!0},{label:"goCqHttpBotToken",tip:"\u8BBF\u95EE\u5BC6\u94A5",required:!0},{label:"goCqHttpBotQq",tip:"\u5982\u679CGOBOT_URL\u8BBE\u7F6E /send_private_msg \u5219\u9700\u8981\u586B\u5165 user_id=\u4E2A\u4EBAQQ \u76F8\u53CD\u5982\u679C\u662F /send_group_msg \u5219\u9700\u8981\u586B\u5165 group_id=QQ\u7FA4",required:!0}],serverChan:[{label:"serverChanKey",tip:"Server\u9171SENDKEY",required:!0}],pushDeer:[{label:"pushDeerKey",tip:"PushDeer\u7684Key\uFF0Chttps://github.com/easychen/pushdeer",required:!0},{label:"pushDeerUrl",tip:"PushDeer\u7684\u81EA\u67B6API endpoint\uFF0C\u9ED8\u8BA4\u662F https://api2.pushdeer.com/message/push"}],bark:[{label:"barkPush",tip:"Bark\u7684\u4FE1\u606FIP/\u8BBE\u5907\u7801\uFF0C\u4F8B\u5982\uFF1Ahttps://api.day.app/XXXXXXXX",required:!0},{label:"barkIcon",tip:"BARK\u63A8\u9001\u56FE\u6807,\u81EA\u5B9A\u4E49\u63A8\u9001\u56FE\u6807 (\u9700iOS15\u6216\u4EE5\u4E0A\u624D\u80FD\u663E\u793A)"},{label:"barkSound",tip:"BARK\u63A8\u9001\u94C3\u58F0,\u94C3\u58F0\u5217\u8868\u53BBAPP\u67E5\u770B\u590D\u5236\u586B\u5199"},{label:"barkGroup",tip:"BARK\u63A8\u9001\u6D88\u606F\u7684\u5206\u7EC4, \u9ED8\u8BA4\u4E3Aqinglong"}],telegramBot:[{label:"telegramBotToken",tip:"telegram\u673A\u5668\u4EBA\u7684token\uFF0C\u4F8B\u5982\uFF1A1077xxx4424:AAFjv0FcqxxxxxxgEMGfi22B4yh15R5uw",required:!0},{label:"telegramBotUserId",tip:"telegram\u7528\u6237\u7684id\uFF0C\u4F8B\u5982\uFF1A129xxx206",required:!0},{label:"telegramBotProxyHost",tip:"\u4EE3\u7406IP"},{label:"telegramBotProxyPort",tip:"\u4EE3\u7406\u7AEF\u53E3"},{label:"telegramBotProxyAuth",tip:"telegram\u4EE3\u7406\u914D\u7F6E\u8BA4\u8BC1\u53C2\u6570, \u7528\u6237\u540D\u4E0E\u5BC6\u7801\u7528\u82F1\u6587\u5192\u53F7\u8FDE\u63A5 user:password"},{label:"telegramBotApiHost",tip:"telegram api\u81EA\u5EFA\u7684\u53CD\u5411\u4EE3\u7406\u5730\u5740\uFF0C\u9ED8\u8BA4tg\u5B98\u65B9api"}],dingtalkBot:[{label:"dingtalkBotToken",tip:"\u9489\u9489\u673A\u5668\u4EBAwebhook token\uFF0C\u4F8B\u5982\uFF1A5a544165465465645d0f31dca676e7bd07415asdasd",required:!0},{label:"dingtalkBotSecret",tip:"\u5BC6\u94A5\uFF0C\u673A\u5668\u4EBA\u5B89\u5168\u8BBE\u7F6E\u9875\u9762\uFF0C\u52A0\u7B7E\u4E00\u680F\u4E0B\u9762\u663E\u793A\u7684SEC\u5F00\u5934\u7684\u5B57\u7B26\u4E32"}],weWorkBot:[{label:"weWorkBotKey",tip:"\u4F01\u4E1A\u5FAE\u4FE1\u673A\u5668\u4EBA\u7684 webhook(\u8BE6\u89C1\u6587\u6863 https://work.weixin.qq.com/api/doc/90000/90136/91770)\uFF0C\u4F8B\u5982\uFF1A693a91f6-7xxx-4bc4-97a0-0ec2sifa5aaa",required:!0},{label:"weWorkOrigin",tip:"\u4F01\u4E1A\u5FAE\u4FE1\u4EE3\u7406\u5730\u5740"}],weWorkApp:[{label:"weWorkAppKey",tip:"corpid,corpsecret,touser(\u6CE8:\u591A\u4E2A\u6210\u5458ID\u4F7F\u7528|\u9694\u5F00),agentid,\u6D88\u606F\u7C7B\u578B(\u9009\u586B,\u4E0D\u586B\u9ED8\u8BA4\u6587\u672C\u6D88\u606F\u7C7B\u578B) \u6CE8\u610F\u7528,\u53F7\u9694\u5F00(\u82F1\u6587\u8F93\u5165\u6CD5\u7684\u9017\u53F7)\uFF0C\u4F8B\u5982\uFF1Awwcfrs,B-76WERQ,qinglong,1000001,2COat",required:!0},{label:"weWorkOrigin",tip:"\u4F01\u4E1A\u5FAE\u4FE1\u4EE3\u7406\u5730\u5740"}],aibotk:[{label:"aibotkKey",tip:"\u5BC6\u94A5key,\u667A\u80FD\u5FAE\u79D8\u4E66\u4E2A\u4EBA\u4E2D\u5FC3\u83B7\u53D6apikey\uFF0C\u7533\u8BF7\u5730\u5740\uFF1Ahttps://wechat.aibotk.com/signup?from=ql",required:!0},{label:"aibotkType",tip:"\u53D1\u9001\u7684\u76EE\u6807\uFF0C\u7FA4\u7EC4\u6216\u8005\u597D\u53CB",required:!0,placeholder:"\u8BF7\u8F93\u5165\u8981\u53D1\u9001\u7684\u76EE\u6807",items:[{value:"room",label:"\u7FA4\u804A"},{value:"contact",label:"\u597D\u53CB"}]},{label:"aibotkName",tip:"\u8981\u53D1\u9001\u7684\u7528\u6237\u6635\u79F0\u6216\u7FA4\u540D\uFF0C\u5982\u679C\u76EE\u6807\u662F\u7FA4\uFF0C\u9700\u8981\u586B\u7FA4\u540D\uFF0C\u5982\u679C\u76EE\u6807\u662F\u597D\u53CB\uFF0C\u9700\u8981\u586B\u597D\u53CB\u6635\u79F0",required:!0}],iGot:[{label:"iGotPushKey",tip:"iGot\u7684\u4FE1\u606F\u63A8\u9001key\uFF0C\u4F8B\u5982\uFF1Ahttps://push.hellyw.com/XXXXXXXX",required:!0}],pushPlus:[{label:"pushPlusToken",tip:"\u5FAE\u4FE1\u626B\u7801\u767B\u5F55\u540E\u4E00\u5BF9\u4E00\u63A8\u9001\u6216\u4E00\u5BF9\u591A\u63A8\u9001\u4E0B\u9762\u7684token(\u60A8\u7684Token)\uFF0C\u4E0D\u63D0\u4F9BPUSH_PLUS_USER\u5219\u9ED8\u8BA4\u4E3A\u4E00\u5BF9\u4E00\u63A8\u9001\uFF0C\u53C2\u8003 https://www.pushplus.plus/",required:!0},{label:"pushPlusUser",tip:"\u4E00\u5BF9\u591A\u63A8\u9001\u7684\u201C\u7FA4\u7EC4\u7F16\u7801\u201D\uFF08\u4E00\u5BF9\u591A\u63A8\u9001\u4E0B\u9762->\u60A8\u7684\u7FA4\u7EC4(\u5982\u65E0\u5219\u65B0\u5EFA)->\u7FA4\u7EC4\u7F16\u7801\uFF0C\u5982\u679C\u60A8\u662F\u521B\u5EFA\u7FA4\u7EC4\u4EBA\u3002\u4E5F\u9700\u70B9\u51FB\u201C\u67E5\u770B\u4E8C\u7EF4\u7801\u201D\u626B\u63CF\u7ED1\u5B9A\uFF0C\u5426\u5219\u4E0D\u80FD\u63A5\u53D7\u7FA4\u7EC4\u6D88\u606F\u63A8\u9001\uFF09"}],lark:[{label:"larkKey",tip:"\u98DE\u4E66\u7FA4\u7EC4\u673A\u5668\u4EBA\uFF1Ahttps://www.feishu.cn/hc/zh-CN/articles/360024984973",required:!0}],email:[{label:"emailService",tip:"\u90AE\u7BB1\u670D\u52A1\u540D\u79F0\uFF0C\u6BD4\u5982126\u3001163\u3001Gmail\u3001QQ\u7B49\uFF0C\u652F\u6301\u5217\u8868https://nodemailer.com/smtp/well-known/",required:!0},{label:"emailUser",tip:"\u90AE\u7BB1\u5730\u5740",required:!0},{label:"emailPass",tip:"\u90AE\u7BB1SMTP\u6388\u6743\u7801",required:!0}],webhook:[{label:"webhookMethod",tip:"\u8BF7\u6C42\u65B9\u6CD5",required:!0,items:[{value:"GET"},{value:"POST"},{value:"PUT"}]},{label:"webhookContentType",tip:"\u8BF7\u6C42\u5934Content-Type",required:!0,items:[{value:"application/json"},{value:"multipart/form-data"},{value:"application/x-www-form-urlencoded"}]},{label:"webhookUrl",tip:"\u8BF7\u6C42\u94FE\u63A5\u4EE5http\u6216\u8005https\u5F00\u5934\u3002url\u6216\u8005body\u4E2D\u5FC5\u987B\u5305\u542B$title\uFF0C$content\u53EF\u9009\uFF0C\u5BF9\u5E94api\u5185\u5BB9\u7684\u4F4D\u7F6E",required:!0,placeholder:`https://xxx.cn/api?content=$title
`},{label:"webhookHeaders",tip:"\u8BF7\u6C42\u5934\u683C\u5F0FCustom-Header1: Header1\uFF0C\u591A\u4E2A\u6362\u884C\u5206\u5272",placeholder:`Custom-Header1: Header1
Custom-Header2: Header2`},{label:"webhookBody",tip:"\u8BF7\u6C42\u4F53\u683C\u5F0Fkey1: value1\uFF0C\u591A\u4E2A\u6362\u884C\u5206\u5272\u3002url\u6216\u8005body\u4E2D\u5FC5\u987B\u5305\u542B$title\uFF0C$content\u53EF\u9009\uFF0C\u5BF9\u5E94api\u5185\u5BB9\u7684\u4F4D\u7F6E",placeholder:`key1: $title
key2: $content`}]},documentTitleMap:{"/login":"\u767B\u5F55","/initialization":"\u521D\u59CB\u5316","/crontab":"\u5B9A\u65F6\u4EFB\u52A1","/env":"\u73AF\u5883\u53D8\u91CF","/subscription":"\u8BA2\u9605\u7BA1\u7406","/config":"\u914D\u7F6E\u6587\u4EF6","/script":"\u811A\u672C\u7BA1\u7406","/diff":"\u5BF9\u6BD4\u5DE5\u5177","/log":"\u65E5\u5FD7\u7BA1\u7406","/setting":"\u7CFB\u7EDF\u8BBE\u7F6E","/error":"\u9519\u8BEF\u65E5\u5FD7","/dependence":"\u4F9D\u8D56\u7BA1\u7406","/account_bank":"\u8D26\u53F7\u5E93","/bili_account":"\u8D26\u53F7\u5217\u8868","/bili_account_training":"\u8D26\u53F7\u5347\u7EA7","/ip_proxy":"IP\u4EE3\u7406\u7BA1\u7406","/tag":"\u6807\u7B7E\u7BA1\u7406","/account_group":"\u8D26\u53F7\u7EC4","/account_group/detail":"\u8D26\u53F7\u7EC4\u660E\u7EC6","/bili_simple_task":"\u7B80\u5355\u4EFB\u52A1\u7BA1\u7406","/interactive_task":"\u4E92\u52A8\u4EFB\u52A1\u7BA1\u7406","/comment_material_list":"\u8BC4\u8BBA\u7D20\u6750\u660E\u7EC6","/user_manage":"\u7528\u6237\u7BA1\u7406","/tenant":"\u591A\u79DF\u6237","/user":"\u7528\u6237\u5217\u8868","/user_group":"\u7528\u6237\u7EC4","/privilege":"\u6743\u9650\u7BA1\u7406","/role":"\u89D2\u8272\u7BA1\u7406","/role/new":"\u521B\u5EFA\u89D2\u8272","/resource":"\u8D44\u6E90\u6743\u9650","/menu":"\u83DC\u5355\u6743\u9650","/user/detail":"\u7528\u6237\u8BE6\u60C5","/user_group/detail":"\u7528\u6237\u7EC4\u8BE6\u60C5","/role/detail":"\u89D2\u8272\u8BE6\u60C5","/interactive_task/up-list":"UP\u4E3B\u4EFB\u52A1\u660E\u7EC6","/interactive_task/video":"\u89C6\u9891\u8BE6\u60C5"},routeComponentMap:{"/login":"@/pages/login/index","/initialization":"@/pages/initialization/index","/crontab":"@/pages/crontab/index","/subscription":"@/pages/subscription/index","/log":"@/pages/log/index","/setting":"@/pages/setting/index","/bili_account":"@/pages/bili_account/index","/bili_account_training":"@/pages/bili_account_training/index","/ip_proxy":"@/pages/ip_proxy/index","/tag":"@/pages/tag/index","/account_group":"@/pages/account_group/index","/bili_simple_task":"@/pages/bili_simple_task/index","/interactive_task":"@/pages/interactive_task/index","/user_manage":"@/pages/user_manage/index","/tenant":"@/pages/tenant/index","/user":"@/pages/user/index","/user_group":"@/pages/user_group/index","/privilege":"@/pages/privilege/index","/role":"@/pages/role/index","/role/new":"@/pages/role/new/index","/resource":"@/pages/resource/index","/menu":"@/pages/menu/index","/user/detail":"@/pages/user/detail/$id","/user_group/detail":"@/pages/user_group/detail/$id","/role/detail":"@/pages/role/detail/$id"},iconComponentMap:{"/crontab":"icon-shujujianguan","/subscription":"icon-zhinengheyue","/ip_proxy":"icon-wangluo","/account_group":"icon-yingyongchangjing","/bili_simple_task":"icon-jishufuwu","/interactive_task":"icon-suyuan","/bili_account":"icon-user","/account_bank":"icon-user","/bili_account_training":"icon-shangyun","/log":"icon-shujuku","/setting":"icon-guanli"},dependenceTypes:["nodejs","python3","linux"]}},45663:function(T,c,e){"use strict";e.d(c,{W:function(){return B}});var k=e(25359),b=e.n(k),E=e(49811),P=e.n(E),y=e(57213),f=e.n(y),C=e(50659),o=e(9835),_=e(24919),d=e(26748);o.ZP.config({duration:2});var p=Date.now(),D=function(a){if(a.response){var i=a.data?a.data.message||a.message||a.data:a.response.statusText,n=a.response.status;[502,504].includes(n)?d.history.push("/error"):n===401?d.history.location.pathname!=="/login"&&(o.ZP.error("\u767B\u5F55\u5DF2\u8FC7\u671F\uFF0C\u8BF7\u91CD\u65B0\u767B\u5F55"),localStorage.removeItem(_.Z.authKey),d.history.push("/login")):o.ZP.error({content:i,style:{maxWidth:500,margin:"0 auto"}})}else console.log(a.message);throw a},g=(0,C.l7)({timeout:6e4,params:{t:p},errorHandler:D}),h=["/api/user/login","/open/auth/token","/api/system","/api/user/init","/api/user/notification/init"];g.interceptors.request.use(function(u,a){var i=localStorage.getItem(_.Z.authKey);if(i&&!h.includes(u)){var n={Authorization:"Bearer ".concat(i)};return{url:u,options:f()(f()({},a),{},{headers:n})}}return{url:u,options:a}}),g.interceptors.response.use(function(){var u=P()(b()().mark(function a(i){var n,s,r;return b()().wrap(function(t){for(;;)switch(t.prev=t.next){case 0:if(n=i.status,![502,504].includes(n)){t.next=5;break}d.history.push("/error"),t.next=14;break;case 5:if(n!==401){t.next=9;break}d.history.location.pathname!=="/login"&&(localStorage.removeItem(_.Z.authKey),d.history.push("/login")),t.next=14;break;case 9:return t.next=11,i.clone().json();case 11:return s=t.sent,s.code!==200&&s.code!==0&&(r=s.msg,r&&o.ZP.error({content:r,style:{maxWidth:500,margin:"0 auto"}})),t.abrupt("return",s);case 14:return t.abrupt("return",i);case 15:case"end":return t.stop()}},a)}));return function(a){return u.apply(this,arguments)}}());var B=g},57002:function(){}}]);
